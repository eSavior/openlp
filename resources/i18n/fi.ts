<?xml version="1.0" ?><!DOCTYPE TS><TS language="fi" version="2.0">
<context>
    <name>AlertsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>&amp;Alert</source>
        <translation>&amp;Uusi huomioviesti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>Show an alert message.</source>
        <translation>Näytä huomioviesti.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="212"/>
        <source>&lt;strong&gt;Alerts Plugin&lt;/strong&gt;&lt;br /&gt;The alert plugin controls the displaying of alerts on the display screen.</source>
        <translation>&lt;strong&gt;Huomioviestit&lt;/strong&gt;&lt;br /&gt;&lt;br/&gt;
Tämä moduuli mahdollistaa huomioviestien&lt;br/&gt;
näyttämisen esityksen aikana.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="222"/>
        <source>Alert</source>
        <comment>name singular</comment>
        <translation>Huomioviestit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="223"/>
        <source>Alerts</source>
        <comment>name plural</comment>
        <translation>Huomioviestit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="227"/>
        <source>Alerts</source>
        <comment>container title</comment>
        <translation>Huomioviestit</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertForm</name>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="93"/>
        <source>Alert Message</source>
        <translation>Uusi huomioviesti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="94"/>
        <source>Alert &amp;text:</source>
        <translation>&amp;Viesti:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="95"/>
        <source>&amp;Parameter:</source>
        <translation>&amp;Muuttuja:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="96"/>
        <source>&amp;New</source>
        <translation>&amp;Tallenna pohjaksi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="97"/>
        <source>&amp;Save</source>
        <translation>&amp;Tallenna</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="98"/>
        <source>Displ&amp;ay</source>
        <translation>&amp;Näytä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="99"/>
        <source>Display &amp;&amp; Cl&amp;ose</source>
        <translation>Näytä &amp;&amp; &amp;Sulje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>New Alert</source>
        <translation>Uusi huomioviesti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>You haven't specified any text for your alert. 
Please type in some text before clicking New.</source>
        <translation>Tyhjää pohjaa ei voida luoda,
viesti ei voi olla tyhjä.

Luodaksesi uuden pohjan, sinun 
on kirjoitettava &quot;Viesti&quot; kenttään tekstiä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>No Parameter Found</source>
        <translation>Muuttujaa ei löydy</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>You have not entered a parameter to be replaced.
Do you want to continue anyway?</source>
        <translation>Viestissä oleva &lt;&gt; muuttuja on tyhjä!
”Muuttuja” kenttä on tyhjä, haluatko jatkaa siitä huolimatta?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>No Placeholder Found</source>
        <translation>Viesti ei sisällä muuttujaa &lt;&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>The alert text does not contain '&lt;&gt;'.
Do you want to continue anyway?</source>
        <translation>Viestissä ei ole &lt;&gt; muuttujaa.
Haluatko jatkaa siitä huolimatta?

Voit lisätä muuttujan kirjoittamalla &lt;&gt; osaksi viestiä.

Esimerkki:
Viesti: Auto &lt;&gt; tukkii pelastustien.
Muuttuja: ABC-123
Näytetään: Auto ABC-123 tukkii pelastustien.</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsManager</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertsmanager.py" line="73"/>
        <source>Alert message created and displayed.</source>
        <translation>Huomioviesti on luotu ja näytetty.</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsTab</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="119"/>
        <source>Font Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="120"/>
        <source>Font name:</source>
        <translation>Fontin nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="121"/>
        <source>Font color:</source>
        <translation>Fontin väri:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="123"/>
        <source>Font size:</source>
        <translation>Fontin koko:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="125"/>
        <source>Background Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="126"/>
        <source>Other Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="127"/>
        <source>Alert timeout:</source>
        <translation>Huomioviestin kesto:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="129"/>
        <source>Repeat (no. of times):</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="130"/>
        <source>Enable Scrolling</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin</name>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="125"/>
        <source>&amp;Bible</source>
        <translation>&amp;Raamattu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="141"/>
        <source>&lt;strong&gt;Bible Plugin&lt;/strong&gt;&lt;br /&gt;The Bible plugin provides the ability to display Bible verses from different sources during the service.</source>
        <translation>&lt;strong&gt;Raamatut&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;

Tämän moduulin avulla voidaan näyttää
Raamatuntekstejä eri lähteistä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="174"/>
        <source>Bible</source>
        <comment>name singular</comment>
        <translation>Raamatut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="175"/>
        <source>Bibles</source>
        <comment>name plural</comment>
        <translation>Raamatut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="179"/>
        <source>Bibles</source>
        <comment>container title</comment>
        <translation>Raamatut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="184"/>
        <source>Import a Bible.</source>
        <translation>Tuo Raamattu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="185"/>
        <source>Add a new Bible.</source>
        <translation>Lisää uusi Raamattu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="186"/>
        <source>Edit the selected Bible.</source>
        <translation>Muokkaa valittua Raamattua.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="187"/>
        <source>Delete the selected Bible.</source>
        <translation>Poista valittu Raamattu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="188"/>
        <source>Preview the selected Bible.</source>
        <translation>Esikatsele valittua Raamatunpaikkaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="189"/>
        <source>Send the selected Bible live.</source>
        <translation>Lähetä valittu paikka Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="190"/>
        <source>Add the selected Bible to the service.</source>
        <translation>Lisää valittu Raamatunpaikka Listaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="76"/>
        <source>Genesis</source>
        <translation>1. Mooseksen kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="77"/>
        <source>Exodus</source>
        <translation>2. Mooseksen kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="78"/>
        <source>Leviticus</source>
        <translation>3. Mooseksen kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="79"/>
        <source>Numbers</source>
        <translation>4. Mooseksen kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="80"/>
        <source>Deuteronomy</source>
        <translation>5. Mooseksen kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="81"/>
        <source>Joshua</source>
        <translation>Joosuan kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="82"/>
        <source>Judges</source>
        <translation>Tuomarien kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="83"/>
        <source>Ruth</source>
        <translation>Ruutin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="84"/>
        <source>1 Samuel</source>
        <translation>1. Samuelin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="85"/>
        <source>2 Samuel</source>
        <translation>2. Samuelin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="86"/>
        <source>1 Kings</source>
        <translation>1. Kuninkaiden kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="87"/>
        <source>2 Kings</source>
        <translation>2. Kuninkaiden kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="88"/>
        <source>1 Chronicles</source>
        <translation>1. Aikakirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="89"/>
        <source>2 Chronicles</source>
        <translation>2. Aikakirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="90"/>
        <source>Ezra</source>
        <translation>Esran kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="91"/>
        <source>Nehemiah</source>
        <translation>Nehemian kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="92"/>
        <source>Esther</source>
        <translation>Esterin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="93"/>
        <source>Job</source>
        <translation>Jobin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="94"/>
        <source>Psalms</source>
        <translation>Psalmit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="95"/>
        <source>Proverbs</source>
        <translation>Sananlaskujen kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="96"/>
        <source>Ecclesiastes</source>
        <translation>Saarnaajan kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="97"/>
        <source>Song of Solomon</source>
        <translation>Laulujen laulu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="98"/>
        <source>Isaiah</source>
        <translation>Jesajan kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="99"/>
        <source>Jeremiah</source>
        <translation>Jeremian kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="100"/>
        <source>Lamentations</source>
        <translation>Valitusvirret</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="101"/>
        <source>Ezekiel</source>
        <translation>Hesekielin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="102"/>
        <source>Daniel</source>
        <translation>Danielin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="103"/>
        <source>Hosea</source>
        <translation>Hoosean kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="104"/>
        <source>Joel</source>
        <translation>Joelin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="105"/>
        <source>Amos</source>
        <translation>Aamoksen kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="106"/>
        <source>Obadiah</source>
        <translation>Obadjan kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="107"/>
        <source>Jonah</source>
        <translation>Joonan kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="108"/>
        <source>Micah</source>
        <translation>Miikan kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="109"/>
        <source>Nahum</source>
        <translation>Nahumin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="110"/>
        <source>Habakkuk</source>
        <translation>Habakukin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="111"/>
        <source>Zephaniah</source>
        <translation>Sefanjan kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="112"/>
        <source>Haggai</source>
        <translation>Haggain kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="113"/>
        <source>Zechariah</source>
        <translation>Sakarjan kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="114"/>
        <source>Malachi</source>
        <translation>Malakian kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="115"/>
        <source>Matthew</source>
        <translation>Matteuksen evankeliumi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="116"/>
        <source>Mark</source>
        <translation>Markuksen evankeliumi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="117"/>
        <source>Luke</source>
        <translation>Luukkaan evankeliumi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="118"/>
        <source>John</source>
        <translation>Johanneksen evankeliumi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="119"/>
        <source>Acts</source>
        <translation>Apostolien teot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="120"/>
        <source>Romans</source>
        <translation>Roomalaiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="121"/>
        <source>1 Corinthians</source>
        <translation>1. Korinttolaiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="122"/>
        <source>2 Corinthians</source>
        <translation>2. Korinttolaiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="123"/>
        <source>Galatians</source>
        <translation>Galatalaiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="124"/>
        <source>Ephesians</source>
        <translation>Efesolaiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="125"/>
        <source>Philippians</source>
        <translation>Filippiläiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="126"/>
        <source>Colossians</source>
        <translation>Kolossalaiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="127"/>
        <source>1 Thessalonians</source>
        <translation>1. Tessalonikalaiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="128"/>
        <source>2 Thessalonians</source>
        <translation>2. Tessalonikalaiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="129"/>
        <source>1 Timothy</source>
        <translation>1. Timoteuskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="130"/>
        <source>2 Timothy</source>
        <translation>2. Timoteuskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="131"/>
        <source>Titus</source>
        <translation>Tituksen kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="132"/>
        <source>Philemon</source>
        <translation>Filemonin kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="133"/>
        <source>Hebrews</source>
        <translation>Heprealaiskirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="134"/>
        <source>James</source>
        <translation>Jaakobin kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="135"/>
        <source>1 Peter</source>
        <translation>1. Pietarin kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="136"/>
        <source>2 Peter</source>
        <translation>2. Pietarin kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="137"/>
        <source>1 John</source>
        <translation>1. Johanneksen kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="138"/>
        <source>2 John</source>
        <translation>2. Johanneksen kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="139"/>
        <source>3 John</source>
        <translation>3. Johanneksen kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="140"/>
        <source>Jude</source>
        <translation>Juudaksen kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="141"/>
        <source>Revelation</source>
        <translation>Ilmestyskirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="142"/>
        <source>Judith</source>
        <translation>Juudit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="143"/>
        <source>Wisdom</source>
        <translation>Viisauden kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="144"/>
        <source>Tobit</source>
        <translation>Tobit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="145"/>
        <source>Sirach</source>
        <translation>Siirakin kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="146"/>
        <source>Baruch</source>
        <translation>Baruk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="147"/>
        <source>1 Maccabees</source>
        <translation>1. Makkabealaiskirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="148"/>
        <source>2 Maccabees</source>
        <translation>2. Makkabealaiskirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="149"/>
        <source>3 Maccabees</source>
        <translation>3. Makkabealaiskirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="150"/>
        <source>4 Maccabees</source>
        <translation>4. Makkabealaiskirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="151"/>
        <source>Rest of Daniel</source>
        <translation>Danielin kirjan lisäykset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="152"/>
        <source>Rest of Esther</source>
        <translation>Esterin kirjan lisäykset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="153"/>
        <source>Prayer of Manasses</source>
        <translation>Manassen rukous</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="154"/>
        <source>Letter of Jeremiah</source>
        <translation>Jeremian kirje</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="155"/>
        <source>Prayer of Azariah</source>
        <translation>Asariaan rukous</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="156"/>
        <source>Susanna</source>
        <translation>Susanna</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="157"/>
        <source>Bel</source>
        <translation>Bel ja loikäärme</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="158"/>
        <source>1 Esdras</source>
        <translation>1. Esran kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="159"/>
        <source>2 Esdras</source>
        <translation>2. Esran kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>:</source>
        <comment>Verse identifier e.g. Genesis 1 : 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>v</source>
        <comment>Verse identifier e.g. Genesis 1 v 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>j</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>V</source>
        <comment>Verse identifier e.g. Genesis 1 V 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verse</source>
        <comment>Verse identifier e.g. Genesis 1 verse 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>jae</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verses</source>
        <comment>Verse identifier e.g. Genesis 1 verses 1 - 2 = Genesis Chapter 1 Verses 1 to 2</comment>
        <translation>jakeet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>-</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>to</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>,</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2, 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>and</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2 and 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>ja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="185"/>
        <source>end</source>
        <comment>ending identifier e.g. Genesis 1 verse 1 - end = Genesis Chapter 1 Verses 1 To The Last Verse</comment>
        <translation>loppu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No Book Found</source>
        <translation>Kirjaa ei löydy</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No matching book could be found in this Bible. Check that you have spelled the name of the book correctly.</source>
        <translation>Vastaavaa kirjaa ei löytynyt käännöksestä.
Ole hyvä ja tarkista oikeinkirjoitus.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="64"/>
        <source>The proxy server {proxy} was found in the bible {name}.&lt;br&gt;Would you like to set it as the proxy for OpenLP?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="69"/>
        <source>both</source>
        <translation>sekä</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleEditForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="162"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>Ole hyvä ja nimeä Raamattu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="168"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>Raamatun tekijänoikeus kenttä ei voi olla tyhjä!
Ole hyvä ja kirjoita tekijänoikeuskenttään jotakin.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>Bible Exists</source>
        <translation>Raamattu on jo olemassa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>Raamattu on jo olemassa. Ole hyvä ja tuo jokin toinen Raamattu tai poista ensin nykyinen versio.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="190"/>
        <source>You need to specify a book name for &quot;{text}&quot;.</source>
        <translation>Ole hyvä ja nimeä tämä kirja:  &quot;{text}&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="197"/>
        <source>The book name &quot;{name}&quot; is not correct.
Numbers can only be used at the beginning and must
be followed by one or more non-numeric characters.</source>
        <translation>Kirjan nimi &quot;{name}&quot; ei ole kelvollinen.
Numeroita voidaan käyttää ainoastaan alussa
ja niiden jälkeen täytyy tulla kirjaimia.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>Duplicate Book Name</source>
        <translation>Päällekkäinen kirjan nimi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>The Book Name &quot;{name}&quot; has been entered more than once.</source>
        <translation>Kirjan nimi &quot;{name}&quot; on syötetty useamman kerran.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="53"/>
        <source>The file &quot;{file}&quot; you supplied is compressed. You must decompress it before import.</source>
        <translation>Annettu tiedosto &quot;{file}&quot; on pakattu. Tiedosto tulee purkaa ennen kuin sen voi tuoda järjestelmään.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="224"/>
        <source>unknown type of</source>
        <comment>This looks like an unknown type of XML bible.</comment>
        <translation>tuntematon tyyppi</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleManager</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Web Bible cannot be used in Text Search</source>
        <translation>Nettiraamatuissa voidaan käyttää vain jaeviitehakua</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Text Search is not available with Web Bibles.
Please use the Scripture Reference Search instead.

This means that the currently selected Bible is a Web Bible.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>Scripture Reference Error</source>
        <translation>Virhe jaeviitteessä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>&lt;strong&gt;The reference you typed is invalid!&lt;br&gt;&lt;br&gt;Please make sure that your reference follows one of these patterns:&lt;/strong&gt;&lt;br&gt;&lt;br&gt;%s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BiblesTab</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="178"/>
        <source>Verse Display</source>
        <translation>Jakeiden näyttäminen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="179"/>
        <source>Show verse numbers</source>
        <translation>Näytä luku ja jae numerot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="180"/>
        <source>Only show new chapter numbers</source>
        <translation>Näytä luvun numero vain ensimmäisessä jakeessa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="183"/>
        <source>Bible theme:</source>
        <translation>Käytettävä teema:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="187"/>
        <source>No Brackets</source>
        <translation>Ei sulkuja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="189"/>
        <source>( And )</source>
        <translation>( ja )</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="191"/>
        <source>{ And }</source>
        <translation>{ ja }</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="193"/>
        <source>[ And ]</source>
        <translation>[ ja ]</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="195"/>
        <source>Note: Changes do not affect verses in the Service</source>
        <translation>Huom: Muutokset eivät vaikuta Listassa oleviin jakeisiin.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="197"/>
        <source>Display second Bible verses</source>
        <translation>Näytä vertailutekstin valinnan kenttä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="198"/>
        <source>Custom Scripture References</source>
        <translation>Mukautetut jaeviitteet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="199"/>
        <source>Verse separator:</source>
        <translation>Jakeen erotinmerkki:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="200"/>
        <source>Range separator:</source>
        <translation>Alueen erotinmerkki:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="201"/>
        <source>List separator:</source>
        <translation>Luettelon erotinmerkki:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="202"/>
        <source>End mark:</source>
        <translation>Loppumerkki:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="203"/>
        <source>Multiple alternative verse separators may be defined.
They have to be separated by a vertical bar &quot;|&quot;.
Please clear this edit line to use the default value.</source>
        <translation>Useampia vaihtoehtoisia jakeen erotinmerkkejä voidaan määritellä.
Ne pitää erottaa pystyviivalla &quot;|&quot;.
Käyttääksesi oletusarvoja tyhjennä tämä kenttä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="210"/>
        <source>Default Bible Language</source>
        <translation>Kirjojen nimien kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="211"/>
        <source>Book name language in search field,
search results and on display:</source>
        <translation>Kirjan nimen kieli hakukentässä,
hakutuloksissa ja näytöllä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="213"/>
        <source>Bible Language</source>
        <translation>Raamatun kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="215"/>
        <source>Application Language</source>
        <translation>Sovelluksen kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="217"/>
        <source>English</source>
        <translation>Suomi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="219"/>
        <source>Quick Search Settings</source>
        <translation>Haun asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="220"/>
        <source>Reset search type to &quot;Text or Scripture Reference&quot; on startup</source>
        <translation>Palauta käynnistäessä haun tyypiksi &quot;Teksti tai Jaeviite&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="223"/>
        <source>Don&apos;t show error if nothing is found in &quot;Text or Scripture Reference&quot;</source>
        <translation>Älä näytä ilmoitusta,  jos &quot;Teksti tai Jaeviite&quot; haussa ei löydetä mitään.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="226"/>
        <source>Search automatically while typing (Text search must contain a
minimum of {count} characters and a space for performance reasons)</source>
        <translation>Hae jo kirjoittaessa (Hakusanoilla etsittäessä minimipituus on {count} merkkiä ja haussa pitää käyttää myös välilyöntiä.)</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="84"/>
        <source>Select Book Name</source>
        <translation>Valitse kirjan nimi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="85"/>
        <source>The following book name cannot be matched up internally. Please select the corresponding name from the list.</source>
        <translation>Kirjanimeä ei tunnistettu, ole hyvä ja valitse &lt;br&gt;
listasta vastaava suomenkielinen käännös</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="88"/>
        <source>Current name:</source>
        <translation>Kirjan nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="89"/>
        <source>Corresponding name:</source>
        <translation>Vastaava nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="90"/>
        <source>Show Books From</source>
        <translation>Näytä nimilistassa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="91"/>
        <source>Old Testament</source>
        <translation>Vanha testamentti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="92"/>
        <source>New Testament</source>
        <translation>Uusi testamentti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="93"/>
        <source>Apocrypha</source>
        <translation>Deuterokanoniset kirjat</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknameform.py" line="109"/>
        <source>You need to select a book.</source>
        <translation>SInun pitää valita kirja.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.CSVBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="123"/>
        <source>Importing books... {book}</source>
        <translation>Tuodaan kirjaa: {book}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="145"/>
        <source>Importing verses from {book}...</source>
        <comment>Importing verses from &lt;book name&gt;...</comment>
        <translation>Tuodaan jakeita {book}...</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="132"/>
        <source>Bible Editor</source>
        <translation>Raamatun muokkaaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="136"/>
        <source>License Details</source>
        <translation>Nimi ja tekijänoikeudet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="137"/>
        <source>Version name:</source>
        <translation>Käännöksen nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="138"/>
        <source>Copyright:</source>
        <translation>Tekijäinoikeudet:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="139"/>
        <source>Permissions:</source>
        <translation>Luvat:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="140"/>
        <source>Full license:</source>
        <translation>Lisenssi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="141"/>
        <source>Default Bible Language</source>
        <translation>Kirjojen nimien kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="142"/>
        <source>Book name language in search field, search results and on display:</source>
        <translation>Kieli, jota käytetään Raamatun kirjoissa niin haussa kuin Esityksessäkin.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="145"/>
        <source>Global Settings</source>
        <translation>Yleiset asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="146"/>
        <source>Bible Language</source>
        <translation>Raamatun kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="149"/>
        <source>Application Language</source>
        <translation>Sovelluksen kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="151"/>
        <source>English</source>
        <translation>Suomi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="93"/>
        <source>This is a Web Download Bible.
It is not possible to customize the Book Names.</source>
        <translation>Tämä käännös haetaan netin kautta.
Netistä haettavien käännösten kirjoja
ei voida uudelleennimetä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="98"/>
        <source>To use the customized book names, &quot;Bible language&quot; must be selected on the Meta Data tab or, if &quot;Global settings&quot; is selected, on the Bible page in Configure OpenLP.</source>
        <translation>Voit halutessasi uudelleenimetä käännöksen raamatunkirjat. Jos käännökset eivät toimi, 
tarkista ”Asetukset &gt; Raamatut” sivulta, ettei Raamattujen kieleksi ole valittu ”Englantia”.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.HTTPBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="607"/>
        <source>Registering Bible and loading books...</source>
        <translation>Rekisteröidään Raamattua ja ladataan kirjoja...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="623"/>
        <source>Registering Language...</source>
        <translation>Rekisteröidään kieli...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="630"/>
        <source>Importing {book}...</source>
        <comment>Importing &lt;book name&gt;...</comment>
        <translation>Tuodaan {book}...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>Download Error</source>
        <translation>Latauksen aikana tapahtui virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>There was a problem downloading your verse selection. Please check your Internet connection, and if this error continues to occur please consider reporting a bug.</source>
        <translation>Ohjelma havaitsi ongelmia valittujen jakeiden lataamisessa. Ole hyvä ja tarkasta internet-yhteyden toimivuus. Jos ongelma ei poistu, harkitse raportointia virheestä kehittäjille.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>Parse Error</source>
        <translation>Jäsennysvirhe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>There was a problem extracting your verse selection. If this error continues to occur please consider reporting a bug.</source>
        <translation>Ohjelma havaitsi ongelmia valittujen jakeiden purkamisessa. Jos ongelma ei poistu, harkitse raportointia virheestä kehittäjille.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="192"/>
        <source>CSV File</source>
        <translation>CSV-tiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="371"/>
        <source>Bible Import Wizard</source>
        <translation>Raamatun ohjattu  tuonti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="374"/>
        <source>This wizard will help you to import Bibles from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>&lt;font size=&quot;4&quot;&gt;Tämä toiminto auttaa sinua lisäämään ohjelmaan &lt;br&gt; raamatunkäännöksiä eri tiedostomuodoista. &lt;br&gt;&lt;br&gt; Paina ”Seuraava” aloittaaksesi. &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="386"/>
        <source>Web Download</source>
        <translation>Lataaminen netistä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="396"/>
        <source>Bible file:</source>
        <translation>Raamattutiedosto:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="391"/>
        <source>Books file:</source>
        <translation>Kirjatiedosto:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="392"/>
        <source>Verses file:</source>
        <translation>Jaetiedosto:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="394"/>
        <source>Location:</source>
        <translation>Sijainti:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="397"/>
        <source>Click to download bible list</source>
        <translation>Klikkaa ladataksesi luettelo Raamatuista</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="398"/>
        <source>Download bible list</source>
        <translation>Lataa raamattuluettelo</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="399"/>
        <source>Crosswalk</source>
        <translation>Crosswalk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="401"/>
        <source>BibleGateway</source>
        <translation>BibleGateway</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="403"/>
        <source>Bibleserver</source>
        <translation>Raamattupalvelin</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="405"/>
        <source>Bible:</source>
        <translation>Raamattu:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="409"/>
        <source>Bibles:</source>
        <translation>Raamatut:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="407"/>
        <source>SWORD data folder:</source>
        <translation>SWORD hakemisto:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="408"/>
        <source>SWORD zip-file:</source>
        <translation>SWORD zip-tiedosto:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="410"/>
        <source>Import from folder</source>
        <translation>Tuo hakemistosta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="412"/>
        <source>Import from Zip-file</source>
        <translation>Tuo Zip-tiedostosta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="417"/>
        <source>To import SWORD bibles the pysword python module must be installed. Please read the manual for instructions.</source>
        <translation>Jotta voit tuoda SWORD muotoisia Raamattuja, on sinun asennettava pysword python moduuli.
Tähän löydät ohjeista apua englanniksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="420"/>
        <source>License Details</source>
        <translation>Nimi ja tekijänoikeudet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="422"/>
        <source>Set up the Bible&apos;s license details.</source>
        <translation>Aseta Raamatun tekstin käyttöoikeuden tiedot.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="424"/>
        <source>Version name:</source>
        <translation>Käännöksen nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="425"/>
        <source>Copyright:</source>
        <translation>Tekijäinoikeudet:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="426"/>
        <source>Permissions:</source>
        <translation>Luvat:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="427"/>
        <source>Full license:</source>
        <translation>Lisenssi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="429"/>
        <source>Please wait while your Bible is imported.</source>
        <translation>Ole hyvä ja odota kunnes Raamattu on tuotu järjestelmään.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="461"/>
        <source>You need to specify a file with books of the Bible to use in the import.</source>
        <translation>Valitse tiedosto tuotavaksi, jossa on Raamatun tekstissä käytetyt kirjojen nimet. </translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="468"/>
        <source>You need to specify a file of Bible verses to import.</source>
        <translation>Valitse tiedosto tuotavaksi, jossa on Raamatun teksti jakeittain.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="526"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>Ole hyvä ja nimeä Raamattu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="532"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>Raamatun tekijänoikeus kenttä ei voi olla tyhjä!
Ole hyvä ja kirjoita tekijänoikeuskenttään jotakin.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>Bible Exists</source>
        <translation>Raamattu on jo olemassa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>Raamattu on jo olemassa. Ole hyvä ja tuo jokin toinen Raamattu tai poista ensin nykyinen versio.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>Error during download</source>
        <translation>Virhe ladattaessa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>An error occurred while downloading the list of bibles from %s.</source>
        <translation>Virhe ladattaessa raamattuluetteloa sivustolta %s.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="691"/>
        <source>Registering Bible...</source>
        <translation>Rekisteröidään Raamattua...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="757"/>
        <source>Registered Bible. Please note, that verses will be downloaded on demand and thus an internet connection is required.</source>
        <translation>Raamattu rekisteröity. Jakeet ladataan käytettäessä
verkon välityksellä, siksi tähän tarvitaan nettiyhteys.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="767"/>
        <source>Your Bible import failed.</source>
        <translation>Raamatun tuonti epäonnistui.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="66"/>
        <source>Select Language</source>
        <translation>Valitse kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="68"/>
        <source>OpenLP is unable to determine the language of this translation of the Bible. Please select the language from the list below.</source>
        <translation>OpenLP ei pysty määrittelemään tässä Raamatun käännöksessä käytettyä kieltä. Ole hyvä ja valitse kieli alla olevasta luettelosta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="72"/>
        <source>Language:</source>
        <translation>Kieli:</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languageform.py" line="62"/>
        <source>You need to choose a language.</source>
        <translation>Sinun tulee valita kieli.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="161"/>
        <source>Find</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="164"/>
        <source>Find:</source>
        <translation>Etsi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="170"/>
        <source>Select</source>
        <translation>Valitse</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="178"/>
        <source>Sort bible books alphabetically.</source>
        <translation>Järjestä raamatunkirjat aakkosjärjestykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="180"/>
        <source>Book:</source>
        <translation>Kirja:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="192"/>
        <source>From:</source>
        <translation>Alkaen:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="198"/>
        <source>To:</source>
        <translation>Asti:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="204"/>
        <source>Options</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="209"/>
        <source>Second:</source>
        <translation>Vertailuteksti:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="273"/>
        <source>Chapter:</source>
        <translation>Luku:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="274"/>
        <source>Verse:</source>
        <translation>Jae:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="278"/>
        <source>Clear the results on the current tab.</source>
        <translation>Tyhjennä viimeksi tulokset välilehdeltä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="279"/>
        <source>Add the search results to the saved list.</source>
        <translation>Lisää hakutulokset tallennettuun luetteloon.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference</source>
        <translation>Teksti tai Jaeviite</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference...</source>
        <translation>Hakusanoin tai Jaeviittein...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Scripture Reference</source>
        <translation>Jaeviite</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Scripture Reference...</source>
        <translation>Hae jaeviittauksin...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text Search</source>
        <translation>Hakusanoilla</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Text...</source>
        <translation>Hae hakusanoilla...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="458"/>
        <source>Are you sure you want to completely delete &quot;{bible}&quot; Bible from OpenLP?

You will need to re-import this Bible to use it again.</source>
        <translation>Haluatko varmasti poistaa Raamatun &quot;{bible} OpenLP:stä?

Raamattu poistetaan pysyvästi ja sinun on tuotava se
uudestaan, jotta voit käyttää sitä jälleen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="506"/>
        <source>Saved ({result_count})</source>
        <translation>Tallennettu ({result_count})</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="509"/>
        <source>Results ({result_count})</source>
        <translation>Tulokset ({result_count})</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="588"/>
        <source>OpenLP cannot combine single and dual Bible verse search results. Do you want to clear your saved results?</source>
        <translation>OpenLP ei voi yhdistää yhden- ja kahden käännöksen jaehakujen tuloksia. Haluatko tyhjentää tallennetut tulokset?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="618"/>
        <source>Bible not fully loaded.</source>
        <translation>Raamattu ei latautunut kokonaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>Verses not found</source>
        <translation>Jakeita ei löytynyt</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>The second Bible &quot;{second_name}&quot; does not contain all the verses that are in the main Bible &quot;{name}&quot;.
Only verses found in both Bibles will be shown.

{count:d} verses have not been included in the results.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.OsisImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="171"/>
        <source>Removing unused tags (this may take a few minutes)...</source>
        <translation>Poistetaan käyttämättömiä tageja (tämä voi kestää muutamia minuutteja)...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="200"/>
        <source>Importing {book} {chapter}...</source>
        <translation>Tuodaan: {book} {chapter}...</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.Sword</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="88"/>
        <source>Importing {name}...</source>
        <translation>Tuodaan: {name}...</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.SwordImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="93"/>
        <source>An unexpected error happened while importing the SWORD bible, please report this to the OpenLP developers.
{error}</source>
        <translation>Tuntematon virhe keskeytti SWORD muotoisen Raamatun tuonnin,
ole hyvä ja ilmoita tästä OpenLP:n kehittäjille.
{error}</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.ZefaniaImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="90"/>
        <source>Incorrect Bible file type supplied. Zefania Bibles may be compressed. You must decompress them before import.</source>
        <translation>Virheellinen raamattu-tiedostotyyppi. Tämä vaikuttaa pakatulta Zefania XML-raamatulta, ole hyvä ja pura tiedosto ennen tuontia.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.Zefnia</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="83"/>
        <source>Importing {book} {chapter}...</source>
        <translation>Tuodaan: {book} {chapter}...</translation>
    </message>
</context>
<context>
    <name>CustomPlugin</name>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="77"/>
        <source>&lt;strong&gt;Custom Slide Plugin &lt;/strong&gt;&lt;br /&gt;The custom slide plugin provides the ability to set up custom text slides that can be displayed on the screen the same way songs are. This plugin provides greater freedom over the songs plugin.</source>
        <translation>&lt;strong&gt;Tekstidiat&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;

Tämän moduulin avulla voidaan näyttää tekstidioja.
Tekstidioja on mahdollista muokata lauluja vapaammin
tiettyjä tarkoituksia varten.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="109"/>
        <source>Custom Slide</source>
        <comment>name singular</comment>
        <translation>Tekstidiat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="110"/>
        <source>Custom Slides</source>
        <comment>name plural</comment>
        <translation>Tekstidiat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="114"/>
        <source>Custom Slides</source>
        <comment>container title</comment>
        <translation>Tekstidiat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="118"/>
        <source>Load a new custom slide.</source>
        <translation>Lataa uusi tekstidia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="119"/>
        <source>Import a custom slide.</source>
        <translation>Tuo tekstidia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="120"/>
        <source>Add a new custom slide.</source>
        <translation>Lisää uusi tekstidia.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="121"/>
        <source>Edit the selected custom slide.</source>
        <translation>Muokkaa valittua tekstidiaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="122"/>
        <source>Delete the selected custom slide.</source>
        <translation>Poista valittu tekstidia.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="123"/>
        <source>Preview the selected custom slide.</source>
        <translation>Esikatsele valittua tekstidiaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="124"/>
        <source>Send the selected custom slide live.</source>
        <translation>Lähetä valittu tekstidia Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="125"/>
        <source>Add the selected custom slide to the service.</source>
        <translation>Lisää valittu tekstidia Listaan.</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.CustomTab</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="56"/>
        <source>Custom Display</source>
        <translation>Tekstidiat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="57"/>
        <source>Display footer</source>
        <translation>Näytä alatunniste</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="58"/>
        <source>Import missing custom slides from service files</source>
        <translation>Tuo puuttuvat tekstidiat Listasta</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditCustomForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="104"/>
        <source>Edit Custom Slides</source>
        <translation>Muokkaa tekstidioja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="105"/>
        <source>&amp;Title:</source>
        <translation>&amp;Nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="107"/>
        <source>Add a new slide at bottom.</source>
        <translation>Lisää uusi dia loppuun.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="109"/>
        <source>Edit the selected slide.</source>
        <translation>Muokkaa valittua diaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="110"/>
        <source>Ed&amp;it All</source>
        <translation>Muokkaa &amp;kaikkia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="111"/>
        <source>Edit all the slides at once.</source>
        <translation>Muokkaa kaikki dioja kerralla.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="112"/>
        <source>The&amp;me:</source>
        <translation>Tee&amp;ma:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="113"/>
        <source>&amp;Credits:</source>
        <translation>&amp;Lopputeksti:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="239"/>
        <source>You need to type in a title.</source>
        <translation>Dian ”Otsikko” ei voi olla tyhjä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="243"/>
        <source>You need to add at least one slide.</source>
        <translation>Sinun pitää lisätä ainakin yksi dia.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="50"/>
        <source>Insert Slide</source>
        <translation>Lisää dia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="51"/>
        <source>Split a slide into two by inserting a slide splitter.</source>
        <translation>Jaa dia kahteen osaan lisäämällä splitterin.</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="47"/>
        <source>Edit Slide</source>
        <translation>Tekstidian muokkaus</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="200"/>
        <source>Are you sure you want to delete the &quot;{items:d}&quot; selected custom slide(s)?</source>
        <translation>Haluatko varmasti poistaa valitut Tekstidiat?
&quot;{items:d}&quot; </translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="261"/>
        <source>copy</source>
        <comment>For item cloning</comment>
        <translation>kopioi</translation>
    </message>
</context>
<context>
    <name>ImagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="70"/>
        <source>&lt;strong&gt;Image Plugin&lt;/strong&gt;&lt;br /&gt;The image plugin provides displaying of images.&lt;br /&gt;One of the distinguishing features of this plugin is the ability to group a number of images together in the service manager, making the displaying of multiple images easier. This plugin can also make use of OpenLP&apos;s &quot;timed looping&quot; feature to create a slide show that runs automatically. In addition to this, images from the plugin can be used to override the current theme&apos;s background, which renders text-based items like songs with the selected image as a background instead of the background provided by the theme.</source>
        <translation>&lt;strong&gt;Kuvat&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;

Tämä moduuli mahdollistaa &lt;br/&gt;
kuvien näyttämisen. &lt;br/&gt;&lt;br/&gt;

Moduulin avulla voidaan esimerkiksi &lt;br/&gt;
luoda kuvista ryhmiä, toistaa niitä&lt;br/&gt;
automaattisesti sekä asettaa taustakuvia.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="89"/>
        <source>Image</source>
        <comment>name singular</comment>
        <translation>Kuvat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="90"/>
        <source>Images</source>
        <comment>name plural</comment>
        <translation>Kuvat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="93"/>
        <source>Images</source>
        <comment>container title</comment>
        <translation>Kuvat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="96"/>
        <source>Add new image(s).</source>
        <translation>Lisää uusia kuvia.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="98"/>
        <source>Add a new image.</source>
        <translation>Lisää uusi kuva.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="99"/>
        <source>Edit the selected image.</source>
        <translation>Muokkaa valittua kuvaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="100"/>
        <source>Delete the selected image.</source>
        <translation>Poista valittu kuva tai ryhmä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="101"/>
        <source>Preview the selected image.</source>
        <translation>Esikatsele valittua kuvaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="102"/>
        <source>Send the selected image live.</source>
        <translation>Lähetä valittu kuva Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="103"/>
        <source>Add the selected image to the service.</source>
        <translation>Lisää valittu kuva Listaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="191"/>
        <source>Add new image(s)</source>
        <translation>Lisää uusia kuvia</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.AddGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="54"/>
        <source>Add group</source>
        <translation>Luo ryhmä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="55"/>
        <source>Parent group:</source>
        <translation>Ylempi ryhmä:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="56"/>
        <source>Group name:</source>
        <translation>Ryhmän nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="67"/>
        <source>You need to type in a group name.</source>
        <translation>Sinun pitää syöttää ryhmälle nimi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="655"/>
        <source>Could not add the new group.</source>
        <translation>Ryhmää ei voida lisätä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="658"/>
        <source>This group already exists.</source>
        <translation>Ryhmä on jo olemassa.</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ChooseGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="82"/>
        <source>Select Image Group</source>
        <translation>Valitse ryhmä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="83"/>
        <source>Add images to group:</source>
        <translation>Lisää kuvia ryhmään:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="84"/>
        <source>No group</source>
        <translation>Ei ryhmää</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="85"/>
        <source>Existing group</source>
        <translation>Olemassaoleva ryhmä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="86"/>
        <source>New group</source>
        <translation>Uusi ryhmä</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="170"/>
        <source>Select Attachment</source>
        <translation>Valitse liite</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="54"/>
        <source>-- Top-level group --</source>
        <translation>-- Päätason ryhmä --</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="81"/>
        <source>Select Image(s)</source>
        <translation>Valitse kuva(t)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="243"/>
        <source>You must select an image or group to delete.</source>
        <translation>Valitse kuva tai ryhmä poistoa varten.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Remove group</source>
        <translation>Ryhmän poistaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Are you sure you want to remove &quot;{name}&quot; and everything in it?</source>
        <translation>Haluatko varmasti poistaa &quot;{name}&quot; ja sen kaiken sisällön?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>Missing Image(s)</source>
        <translation>Puuttuvat kuva(t)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="596"/>
        <source>The following image(s) no longer exist: {names}</source>
        <translation>Seuraavia kuvia ei enää ole olemassa: {names}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>The following image(s) no longer exist: {names}
Do you want to add the other images anyway?</source>
        <translation>Seuraavia kuvia ei enää ole olemassa: {names}
Haluatko kuitenkin lisätä muut kuvat?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="679"/>
        <source>You must select an image to replace the background with.</source>
        <translation>Sinun pitää valita kuva, jolla korvaa taustan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="693"/>
        <source>There was no display item to amend.</source>
        <translation>Muutettavaa näytön kohdetta ei ole.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="697"/>
        <source>There was a problem replacing your background, the image file &quot;{name}&quot; no longer exists.</source>
        <translation>Taustakuvan korvaaminen ei onnistunut. 
OpenLP ei löytänyt tätä tiedostoa: &quot;{name}&quot;</translation>
    </message>
</context>
<context>
    <name>ImagesPlugin.ImageTab</name>
    <message>
        <location filename="../../openlp/plugins/images/lib/imagetab.py" line="63"/>
        <source>Visible background for images with aspect ratio different to screen.</source>
        <translation>Taustaväri kuville jotka eivät täytä koko näyttöä. </translation>
    </message>
</context>
<context>
    <name>MediaPlugin</name>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="81"/>
        <source>&lt;strong&gt;Media Plugin&lt;/strong&gt;&lt;br /&gt;The media plugin provides playback of audio and video.</source>
        <translation>&lt;strong&gt;Media&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;

Tämän moduulin avulla on mahdollista&lt;br/&gt;
toistaa video ja äänitiedostoja.&lt;br/&gt;&lt;br/&gt;
Nämä vaativat toimiakseen mediasoittimen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="91"/>
        <source>Media</source>
        <comment>name singular</comment>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="92"/>
        <source>Media</source>
        <comment>name plural</comment>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="96"/>
        <source>Media</source>
        <comment>container title</comment>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="100"/>
        <source>Load new media.</source>
        <translation>Tuo videoita tai äänitiedostoja.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="102"/>
        <source>Add new media.</source>
        <translation>Lisää uusi media.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="103"/>
        <source>Edit the selected media.</source>
        <translation>Muokkaa valittua mediaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="104"/>
        <source>Delete the selected media.</source>
        <translation>Poista valittu media.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="105"/>
        <source>Preview the selected media.</source>
        <translation>Esikatsele valittua mediaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="106"/>
        <source>Send the selected media live.</source>
        <translation>Lähetä valittu media Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="107"/>
        <source>Add the selected media to the service.</source>
        <translation>Lisää valittu media Listaan.</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelector</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="184"/>
        <source>Select Media Clip</source>
        <translation>Levyn toisto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="185"/>
        <source>Source</source>
        <translation>Levyaseman sijainti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="186"/>
        <source>Media path:</source>
        <translation>Lähde:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="187"/>
        <source>Select drive from list</source>
        <translation>Valitse asema luettelosta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="189"/>
        <source>Load disc</source>
        <translation>Avaa levy</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="190"/>
        <source>Track Details</source>
        <translation>Valitse toiston asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="191"/>
        <source>Title:</source>
        <translation>Nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="192"/>
        <source>Audio track:</source>
        <translation>Ääniraita:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="193"/>
        <source>Subtitle track:</source>
        <translation>Tekstitys:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="201"/>
        <source>HH:mm:ss.z</source>
        <translation>TT:mm:ss.z</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="195"/>
        <source>Clip Range</source>
        <translation>Leikkausalue</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="196"/>
        <source>Start point:</source>
        <translation>Alkupiste:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="198"/>
        <source>Set start point</source>
        <translation>Aseta alkupiste</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="199"/>
        <source>Jump to start point</source>
        <translation>Siirry alkuun</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="200"/>
        <source>End point:</source>
        <translation>Loppupiste:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="202"/>
        <source>Set end point</source>
        <translation>Aseta loppupiste</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="203"/>
        <source>Jump to end point</source>
        <translation>Siirry loppuun</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelectorForm</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="195"/>
        <source>No path was given</source>
        <translation>Polkua ei annettu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="201"/>
        <source>Given path does not exists</source>
        <translation>Annettua polkua ei ole olemassa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="218"/>
        <source>An error happened during initialization of VLC player</source>
        <translation>Virhe alustustettaessa VLC-soitinta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="239"/>
        <source>VLC player failed playing the media</source>
        <translation>VLC-soitin ei voi soittaa mediaa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>CD not loaded correctly</source>
        <translation>CD:tä ei ladattu oikein</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>The CD was not loaded correctly, please re-load and try again.</source>
        <translation>CD:tä ei ladattu oikein, yritä avata se uudestaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>DVD not loaded correctly</source>
        <translation>DVD:tä ei ladattu oikein</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>The DVD was not loaded correctly, please re-load and try again.</source>
        <translation>DVD:tä ei ladattu oikein, yritä avata se uudestaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Set name of mediaclip</source>
        <translation>Nimeäminen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Name of mediaclip:</source>
        <translation>Nimi toistoa varten:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="602"/>
        <source>Enter a valid name or cancel</source>
        <translation>Anna kelvollinen nimi tai peruuta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>Invalid character</source>
        <translation>Virheellinen merkki</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>The name of the mediaclip must not contain the character &quot;:&quot;</source>
        <translation>Mediatiedoston nimessä ei voi käyttää kaksoispistettä &quot;:&quot;</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="290"/>
        <source>Unsupported File</source>
        <translation>Tiedostomuotoa ei tueta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="93"/>
        <source>Select Media</source>
        <translation>Valitse media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="123"/>
        <source>Load CD/DVD</source>
        <translation>Avaa CD/DVD.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>Missing Media File</source>
        <translation>Puuttuva mediatiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="155"/>
        <source>The optical disc {name} is no longer available.</source>
        <translation>Levy {name} ei ole enää käytettävissä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>The file {name} no longer exists.</source>
        <translation>Tiedostoa {name} ei enää ole olemassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="206"/>
        <source>Videos ({video});;Audio ({audio});;{files} (*)</source>
        <translation>Videoita ({video});;Äänitiedostoja ({audio});;{files} (*)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="216"/>
        <source>You must select a media file to delete.</source>
        <translation>Sinun täytyy valita mediatiedosto poistettavaksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="233"/>
        <source>Live Stream</source>
        <translation>Suoratoisto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="237"/>
        <source>Show Live Stream</source>
        <translation>Näytä suoratoisto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>Mediaclip already saved</source>
        <translation>Medialeike on jo tallennettu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>This mediaclip has already been saved</source>
        <translation>Medialeike on tallennettu aiemmin</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="74"/>
        <source>Video:</source>
        <translation>Kuva:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="76"/>
        <source>Audio:</source>
        <translation>Ääni:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="95"/>
        <source>Live Media</source>
        <translation>Suoratoisto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="96"/>
        <source>Stream Media Command</source>
        <translation>Suoratoisto käsky</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="97"/>
        <source>VLC arguments</source>
        <translation>VLC parametrit</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="98"/>
        <source>Start Live items automatically</source>
        <translation>Aloita Esityksen kohteet automaattisesti</translation>
    </message>
</context>
<context>
    <name>OpenLP</name>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>Data Directory Error</source>
        <translation>Tiedostokansion virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>OpenLP data folder was not found in:

{path}

The location of the data folder was previously changed from the OpenLP's default location. If the data was stored on removable device, that device needs to be made available.

You may reset the data location back to the default location, or you can try to make the current location available.

Do you want to reset to the default data location? If not, OpenLP will be closed so you can try to fix the the problem.</source>
        <translation>OpenLP datahakemistoa ei löytynyt

%s

Tämä datahakemisto on siirtynyt OpenLP:n oletussijainnista. Jos se sijaitsi siirrettävällä medialla, tällöin median pitää olla kytkettynä koneeseen..

Voit nollata sijainnnin takaisin oletusarvoo tai voit yrittää kytkeä siirrettävän median takaisin koneeseen.

Tahdotko nollata sijainnin? Jos et, OpenLP suljetaan ja voit yrittää korjata ongelman myöhemmin uudelleen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="238"/>
        <source>Backup</source>
        <translation>Varmuuskopiointi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="221"/>
        <source>OpenLP has been upgraded, do you want to create
a backup of the old data folder?</source>
        <translation>OpenLP on päivitetty, haluatko varmuuskopioida 
OpenLP:n vanhan tiedostokansion?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="232"/>
        <source>Backup of the data folder failed!</source>
        <translation>Tiedostokansion varmuuskopionti epäonnistui!</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="235"/>
        <source>A backup of the data folder has been created at:

{text}</source>
        <translation>Tiedostokansio on varmuuskopioitu kansioon: 

{text}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings Upgrade</source>
        <translation>Asetusten päivittäminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="426"/>
        <source>Your settings are about to be upgraded. A backup will be created at {back_up_path}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings back up failed.

Continuining to upgrade.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/__init__.py" line="415"/>
        <source>Image Files</source>
        <translation>Kuvatiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Open</source>
        <translation>Avaa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="79"/>
        <source>Video Files</source>
        <translation>Video tiedostot</translation>
    </message>
</context>
<context>
    <name>OpenLP.AboutForm</name>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="103"/>
        <source>&lt;p&gt;OpenLP {{version}}{{revision}} - Open Source Lyrics Projection&lt;br&gt;Copyright {crs} 2004-{yr} OpenLP Developers&lt;/p&gt;&lt;p&gt;Find out more about OpenLP: &lt;a href=&quot;https://openlp.org/&quot;&gt;https://openlp.org/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with this program.  If not, see &lt;a href=&quot;https://www.gnu.org/licenses/&quot;&gt;https://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="118"/>
        <source>OpenLP is written and maintained by volunteers all over the world in their spare time. If you would like to see this project succeed, please consider contributing to it by clicking the &quot;contribute&quot; button below.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="122"/>
        <source>OpenLP would not be possible without the following software libraries:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="155"/>
        <source>&lt;h3&gt;Final credit:&lt;/h3&gt;&lt;blockquote&gt;&lt;p&gt;For God so loved the world that He gave His one and only Son, so that whoever believes in Him will not perish but inherit eternal life.&lt;/p&gt;&lt;p&gt;John 3:16&lt;/p&gt;&lt;/blockquote&gt;&lt;p&gt;And last but not least, final credit goes to God our Father, for sending His Son to die on the cross, setting us free from sin. We bring this software to you for free because He has set us free.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="164"/>
        <source>Credits</source>
        <translation>Kiitokset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="744"/>
        <source>License</source>
        <translation>Lisenssi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="746"/>
        <source>Contribute</source>
        <translation>Avustus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutform.py" line="57"/>
        <source> build {version}</source>
        <translation>versio {version}</translation>
    </message>
</context>
<context>
    <name>OpenLP.AdvancedTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="59"/>
        <source>Advanced</source>
        <translation>Lisäasetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="252"/>
        <source>UI Settings</source>
        <translation>Käyttöliittymän asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="253"/>
        <source>Data Location</source>
        <translation>Tietojen sijainti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="254"/>
        <source>Number of recent service files to display:</source>
        <translation>Viimeisten näytettävien listatiedostojen määrä:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="255"/>
        <source>Open the last used Library tab on startup</source>
        <translation>Avaa käynnistäessä sulkiessa avoimena ollut kirjasto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="257"/>
        <source>Double-click to send items straight to Live</source>
        <translation>Lähetä diat suoraan Esitykseen tupla-klikkaamalla.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="259"/>
        <source>Preview items when clicked in Library</source>
        <translation>Esikatsele dioja,  kun kohdetta klikataan Kirjastossa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="261"/>
        <source>Preview items when clicked in Service</source>
        <translation>Esikatsele kohdetta, kun klikataan Listassa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="263"/>
        <source>Expand new service items on creation</source>
        <translation>Näytä uudet Listan kohteet laajennettuina</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="265"/>
        <source>Max height for non-text slides
in slide controller:</source>
        <translation>Maksimikorkeus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="267"/>
        <source>Disabled</source>
        <translation>Ei käytössä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="268"/>
        <source>Automatic</source>
        <translation>Automaattinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="269"/>
        <source>When changing slides:</source>
        <translation>Diaa vaihtaessa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="271"/>
        <source>Do not auto-scroll</source>
        <translation>Pidä näkyvissä nykyinen dia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="272"/>
        <source>Auto-scroll the previous slide into view</source>
        <translation>Siirrä edellinen dia näkyviin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="274"/>
        <source>Auto-scroll the previous slide to top</source>
        <translation>Siirrä edellinen dia ensimmäiseksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="276"/>
        <source>Auto-scroll the previous slide to middle</source>
        <translation>Keskitä edellinen dia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="278"/>
        <source>Auto-scroll the current slide into view</source>
        <translation>Siirrä nykyinen dia näkyviin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="280"/>
        <source>Auto-scroll the current slide to top</source>
        <translation>Siirrä nykyinen dia ensimmäiseksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="282"/>
        <source>Auto-scroll the current slide to middle</source>
        <translation>Keskitä nykyinen dia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="284"/>
        <source>Auto-scroll the current slide to bottom</source>
        <translation>Siirrä nykyinen dia viimeiseksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="286"/>
        <source>Auto-scroll the next slide into view</source>
        <translation>Siirrä seuraava dia näkyviin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="288"/>
        <source>Auto-scroll the next slide to top</source>
        <translation>Siirrä seuraava dia ensimmäiseksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="290"/>
        <source>Auto-scroll the next slide to middle</source>
        <translation>Keskitä seuraava dia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="292"/>
        <source>Auto-scroll the next slide to bottom</source>
        <translation>Siirrä seuraava dia viimeiseksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="294"/>
        <source>Enable application exit confirmation</source>
        <translation>Vahvista sovelluksen sulkeminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="299"/>
        <source>Use dark style (needs restart)</source>
        <translation>Käytä tummaa tyyliä (vaatii uudelleenkäynnistyksen)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="300"/>
        <source>Default Service Name</source>
        <translation>Listan oletusnimi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="301"/>
        <source>Enable default service name</source>
        <translation>Käytä Listoissa oletusnimeä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="302"/>
        <source>Date and Time:</source>
        <translation>Päivä ja aika:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="303"/>
        <source>Monday</source>
        <translation>Maanantai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="304"/>
        <source>Tuesday</source>
        <translation>Tiistai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="305"/>
        <source>Wednesday</source>
        <translation>Keskiviikko</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="306"/>
        <source>Thursday</source>
        <translation>Torstai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="307"/>
        <source>Friday</source>
        <translation>Perjantai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="308"/>
        <source>Saturday</source>
        <translation>Lauantai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="309"/>
        <source>Sunday</source>
        <translation>Sunnuntai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="310"/>
        <source>Now</source>
        <translation>Nyt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="311"/>
        <source>Time when usual service starts.</source>
        <translation>Ajankohta, jolloin jumalanpalvelus yleensä alkaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="312"/>
        <source>Name:</source>
        <translation>Nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="313"/>
        <source>Consult the OpenLP manual for usage.</source>
        <translation>Tarkista OpenLP:n ohjekirjasta käyttö.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="314"/>
        <source>Revert to the default service name &quot;{name}&quot;.</source>
        <translation>Palauta Listan oletusnimeksi &quot;{name}&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="317"/>
        <source>Example:</source>
        <translation>Esimerkki:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="318"/>
        <source>Hide mouse cursor when over display window</source>
        <translation>Piilota hiiren osoitin, kun se on näyttöikkunan päällä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="319"/>
        <source>Path:</source>
        <translation>Polku:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="320"/>
        <source>Cancel</source>
        <translation>Peruuta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="321"/>
        <source>Cancel OpenLP data directory location change.</source>
        <translation>Peruuta OpenLP:n tiedostokansion sijainnin muuttaminen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="323"/>
        <source>Copy data to new location.</source>
        <translation>Kopioi tiedot uuteen sijaintiin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="324"/>
        <source>Copy the OpenLP data files to the new location.</source>
        <translation>Kopioi OpenLP:n tiedostokansio uuteen sijaintiin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="326"/>
        <source>&lt;strong&gt;WARNING:&lt;/strong&gt; New data directory location contains OpenLP data files.  These files WILL be replaced during a copy.</source>
        <translation>&lt;strong&gt;HUOMIO:&lt;/strong&gt; Uudessa tiedostokansion 
sijainnissa on jo ennestään OpenLP:n tiedostoja. 
Nämä tiedostot tuhotaan kopioinnin yhteydessä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="329"/>
        <source>Display Workarounds</source>
        <translation>Erikoisasetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="330"/>
        <source>Ignore Aspect Ratio</source>
        <translation>Älä huomioi kuvasuhdetta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="331"/>
        <source>Bypass X11 Window Manager</source>
        <translation>Ohita X11:n ikkunamanageri (ei koske Windows tai Mac käyttäjiä, 
jos käytössäsi on Linux, tämä voi ratkaista näyttöongelmia)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="332"/>
        <source>Use alternating row colours in lists</source>
        <translation>Värjää joka toinen luetteloiden kohteista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="490"/>
        <source>Syntax error.</source>
        <translation>Kielioppivirhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Confirm Data Directory Change</source>
        <translation>Vahvista tiedostokansion sijainnin muuttaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Are you sure you want to change the location of the OpenLP data directory to:

{path}

The data directory will be changed when OpenLP is closed.</source>
        <translation>Haluatko varmasti muuttaa OpenLP tiedostokansion sijainnin?

Sijainti vaihdetaan ohjelman sulkeutuessa seuraavaan kansioon:

{path}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="559"/>
        <source>Overwrite Existing Data</source>
        <translation>Ylikirjoita olemassaolevat tiedot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>Restart Required</source>
        <translation>Uudelleenkäynnistys vaaditaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>This change will only take effect once OpenLP has been restarted.</source>
        <translation>Muutokset tuleva voimaan vasta, kun OpenLP on käynnistetty uudelleen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="186"/>
        <source>Select Logo File</source>
        <translation>Valitse Logo tiedosto</translation>
    </message>
</context>
<context>
    <name>OpenLP.ColorButton</name>
    <message>
        <location filename="../../openlp/core/widgets/buttons.py" line="44"/>
        <source>Click to select a color.</source>
        <translation>Valitse väri klikkaamalla.</translation>
    </message>
</context>
<context>
    <name>OpenLP.DB</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="542"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="543"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="544"/>
        <source>Digital</source>
        <translation>Digitaalinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="545"/>
        <source>Storage</source>
        <translation>Tietovarasto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="546"/>
        <source>Network</source>
        <translation>Verkko</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="547"/>
        <source>Internal</source>
        <translation>Sisäinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="551"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="552"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="553"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="554"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="555"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="556"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="557"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="558"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="559"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="560"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="561"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="562"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="563"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="564"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="565"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="566"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="567"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="568"/>
        <source>I</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="569"/>
        <source>J</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="570"/>
        <source>K</source>
        <translation>K</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="571"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="572"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="573"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="574"/>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="575"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="576"/>
        <source>Q</source>
        <translation>Q</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="577"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="578"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="579"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="580"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="581"/>
        <source>V</source>
        <translation>J</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="582"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="583"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="584"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="585"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
</context>
<context>
    <name>OpenLP.DisplayWindow</name>
    <message>
        <location filename="../../openlp/core/display/window.py" line="121"/>
        <source>Display Window</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="94"/>
        <source>Error Occurred</source>
        <translation>Tapahtui virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="96"/>
        <source>&lt;strong&gt;Please describe what you were trying to do.&lt;/strong&gt; &amp;nbsp;If possible, write in English.</source>
        <translation>&lt;strong&gt;Ole hyvä ja kirjoita alapuolella olevaan tekstikenttään mitä olit
tekemässä, kun virhe tapahtui. &lt;/strong&gt;  (Mielellään englanniksi)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="99"/>
        <source>&lt;strong&gt;Oops, OpenLP hit a problem and couldn&apos;t recover!&lt;br&gt;&lt;br&gt;You can help &lt;/strong&gt; the OpenLP developers to &lt;strong&gt;fix this&lt;/strong&gt; by&lt;br&gt; sending them a &lt;strong&gt;bug report to {email}&lt;/strong&gt;{newlines}</source>
        <translation>&lt;strong&gt;Oho, OpenLP kohtasi ongelman, josta ei voi toipua! &lt;br&gt;&lt;br&gt;Voit auttaa&lt;/strong&gt;OpenLP kehittäjiä &lt;strong&gt;korjaamaan sen&lt;/strong&gt; by&lt;br&gt; lähettämällä heille &lt;strong&gt;vikaraportin&lt;/strong&gt; osoitteeseen {email}{newlines}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="105"/>
        <source>{first_part}&lt;strong&gt;No email app? &lt;/strong&gt; You can &lt;strong&gt;save&lt;/strong&gt; this information to a &lt;strong&gt;file&lt;/strong&gt; and&lt;br&gt;send it from your &lt;strong&gt;mail on browser&lt;/strong&gt; via an &lt;strong&gt;attachment.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;&lt;strong&gt;Thank you&lt;/strong&gt; for being part of making OpenLP better!&lt;br&gt;</source>
        <translation>{first_part}&lt;strong&gt;Ei sähköpostia? &lt;/strong&gt; Voit &lt;strong&gt;tallentaa&lt;/strong&gt; tiedot &lt;strong&gt;tiedostoon&lt;/strong&gt; ja&lt;br&gt;lähettää ne &lt;strong&gt;selaimesi sähköpostilla&lt;/strong&gt; liittämällä mukaan &lt;strong&gt;liitteen.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;&lt;strong&gt;Kiitos,&lt;strong&gt; että osallistuit tekemään OpenLP:stä paremman!&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="112"/>
        <source>Send E-Mail</source>
        <translation>Lähetä sähköposti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="113"/>
        <source>Save to File</source>
        <translation>Tallenna tiedostoksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="114"/>
        <source>Attach File</source>
        <translation>Liitä tiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>Failed to Save Report</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>The following error occured when saving the report.

{exception}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="153"/>
        <source>&lt;strong&gt;Thank you for your description!&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Kiitos kuvauksestasi!&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="157"/>
        <source>&lt;strong&gt;Tell us what you were doing when this happened.&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Ole hyvä ja kuvaile mitä tapahtui&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="161"/>
        <source>&lt;strong&gt;Please enter a more detailed description of the situation&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Ole hyvä ja anna yksityiskohtaisempi kuvaus tapauksesta. &lt;/strong&gt;</translation>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionForm</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="77"/>
        <source>Platform: {platform}
</source>
        <translation>Järjestelmä: {platform}
</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Save Crash Report</source>
        <translation>Tallenna virheraportti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Text files (*.txt *.log *.text)</source>
        <translation>Teksti tiedostot (*.txt *.log *.text)</translation>
    </message>
</context>
<context>
    <name>OpenLP.FileRenameForm</name>
    <message>
        <location filename="../../openlp/core/ui/filerenamedialog.py" line="60"/>
        <source>New File Name:</source>
        <translation>Uusi tiedostonimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="55"/>
        <source>File Copy</source>
        <translation>Tiedoston kopiointi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="57"/>
        <source>File Rename</source>
        <translation>Uudelleennimeäminen</translation>
    </message>
</context>
<context>
    <name>OpenLP.FirstTimeLanguageForm</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="68"/>
        <source>Select Translation</source>
        <translation>Valitse käännös</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="69"/>
        <source>Choose the translation you&apos;d like to use in OpenLP.</source>
        <translation>Valitse käännös, jota haluat käyttää OpenLP:ssä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="71"/>
        <source>Translation:</source>
        <translation>Käännös:</translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>OpenLP.FirstTimeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="520"/>
        <source>Network Error</source>
        <translation>Verkkovirhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="177"/>
        <source>There was a network error attempting to connect to retrieve initial configuration information</source>
        <translation>Verkkovirhe ladatessa oletusasetuksia  palvelimelta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="184"/>
        <source>Downloading {name}...</source>
        <translation>Ladataan {name}...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>Invalid index file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>OpenLP was unable to read the resource index file. Please try again later.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>Download Error</source>
        <translation>Latauksen aikana tapahtui virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="417"/>
        <source>There was a connection problem during download, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation>Oli yhteysongelmia lataamisen aikana, minkä tähden seuraavat lataukset jätetään välistä. Yritä ajaa uudelleen Ensimmäisen käynnistyksen avustaja myöhemmin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="429"/>
        <source>Setting Up And Downloading</source>
        <translation>Määritetään asetuksia ja ladataan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="430"/>
        <source>Please wait while OpenLP is set up and your data is downloaded.</source>
        <translation>Ole hyvä ja odota kunnes OpenLP on määrittänyt asetukset ja kaikki tiedostot on ladattu.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="434"/>
        <source>Setting Up</source>
        <translation>Kaikki on valmista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="448"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="451"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="455"/>
        <source>Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="457"/>
        <source>Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>There was a connection problem while downloading, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation>Oli yhteysongelmia lataamisen aikana, minkä tähden seuraavat lataukset jätetään välistä. Yritä ajaa Ensimmäisen käynnistyksen avustaja uudelleen myöhemmin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="521"/>
        <source>Unable to download some files</source>
        <translation>Joitain tiedostoja ei voitu ladata</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="262"/>
        <source>First Time Wizard</source>
        <translation>Ensimmäisen käyttökerran avustaja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="263"/>
        <source>Welcome to the First Time Wizard</source>
        <translation>Ensimmäisen käyttökerran avustaja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="265"/>
        <source>This wizard will help you to configure OpenLP for initial use. Click the &apos;{next_button}&apos; button below to start.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="269"/>
        <source>Internet Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="271"/>
        <source>Downloading Resource Index</source>
        <translation>Ladataan resursseja...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="272"/>
        <source>Please wait while the resource index is downloaded.</source>
        <translation>Resursseja ladataan, tämän pitäisi viedä vain hetki.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="274"/>
        <source>Please wait while OpenLP downloads the resource index file...</source>
        <translation>Ladataan tarvittavia resursseja...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="276"/>
        <source>Select parts of the program you wish to use</source>
        <translation>Valitse ohjelman osat joita haluat käyttää</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="277"/>
        <source>You can also change these settings after the Wizard.</source>
        <translation>Voit muuttaa näitä asetuksia myös myöhemmin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="279"/>
        <source>Displays</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="280"/>
        <source>Choose the main display screen for OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="282"/>
        <source>Songs</source>
        <translation>Laulut</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="283"/>
        <source>Custom Slides – Easier to manage than songs and they have their own list of slides</source>
        <translation>Tekstidiat – Helpompi hallinoida kuin laulut, näillä on oma listansa</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="286"/>
        <source>Bibles – Import and show Bibles</source>
        <translation>Raamatut – Näytä ja tuo Raamattuja</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="287"/>
        <source>Images – Show images or replace background with them</source>
        <translation>Kuvat – Näytä kuvia OpenLP:n avulla</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="289"/>
        <source>Presentations – Show .ppt, .odp and .pdf files</source>
        <translation>Presentaatiot – Näytä .ppt, .odp ja pdf tiedostoja</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="291"/>
        <source>Media – Playback of Audio and Video files</source>
        <translation>Media – Toista ääni ja kuvatiedostoja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="292"/>
        <source>Song Usage Monitor</source>
        <translation>Laulujen käyttötilastointi</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="293"/>
        <source>Alerts – Display informative messages while showing other slides</source>
        <translation>Huomioviestit – Näytä informatiivisia viestejä muun materiaalin päällä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="295"/>
        <source>Resource Data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="296"/>
        <source>Can OpenLP download some resource data?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="297"/>
        <source>OpenLP has collected some resources that we have permission to distribute.

If you would like to download some of these resources click the &apos;{next_button}&apos; button, otherwise click the &apos;{finish_button}&apos; button.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="303"/>
        <source>No Internet Connection</source>
        <translation>Ei internetyhteyttä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="304"/>
        <source>Cannot connect to the internet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="305"/>
        <source>OpenLP could not connect to the internet to get information about the sample data available.

Please check your internet connection. If your church uses a proxy server click the 'Internet Settings' button below and enter the server details there.

Click the '{back_button}' button to try again.

If you click the &apos;{finish_button}&apos; button you can download the data at a later time by selecting &apos;Re-run First Time Wizard&apos; from the &apos;Tools&apos; menu in OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="314"/>
        <source>Sample Songs</source>
        <translation>Tekijänoikeusvapaita lauluja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="315"/>
        <source>Select and download public domain songs.</source>
        <translation>Valitse listasta esimerkkilauluja halutuilla kielillä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="316"/>
        <source>Sample Bibles</source>
        <translation>Tekijänoikeusvapaita Raamatunkäännöksiä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="317"/>
        <source>Select and download free Bibles.</source>
        <translation>Valitse listasta ne Raamatunkäännökset jotka haluat ladata.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="319"/>
        <source>Sample Themes</source>
        <translation>Esimerkkiteemat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="320"/>
        <source>Select and download sample themes.</source>
        <translation>Voit ladata sovellukseen esimerkkiteemoja valitsemalla listasta haluamasi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="321"/>
        <source>Default theme:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="322"/>
        <source>Select all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="323"/>
        <source>Deselect all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="324"/>
        <source>Downloading and Configuring</source>
        <translation>Lataaminen ja konfigurointi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="325"/>
        <source>Please wait while resources are downloaded and OpenLP is configured.</source>
        <translation>Ole hyvä ja odota kunnes resurssit ovat latautuneet ja OpenLP on konfiguroitu.</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagDialog</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="111"/>
        <source>Configure Formatting Tags</source>
        <translation>Tekstin muotoilutunnuksien hallinta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="114"/>
        <source>Default Formatting</source>
        <translation>&lt;font size=&quot;4&quot;&gt;Tunnukset mahdollistavat tekstin muotoilun teemasta poikkeavaksi.&lt;br&gt;&lt;br&gt;

Käyttö: Maalaa tekstiä laulua tai tekstidiaa muokatessa ja klikkaa hiiren oikeaa painiketta, valitse muotoilu. &lt;br&gt;
Voit myös kirjoitaa tunnuksen itse: {tunnus} teksti johon muotoilu tulee {/tunnus}&lt;/font&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="125"/>
        <source>Description</source>
        <translation>Efekti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="126"/>
        <source>Tag</source>
        <translation>Tunnus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="127"/>
        <source>Start HTML</source>
        <translation>HTML koodin alku</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="128"/>
        <source>End HTML</source>
        <translation>HTML koodin loppu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="124"/>
        <source>Custom Formatting</source>
        <translation>Omat muotoilutunnukset</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagForm</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="79"/>
        <source>Tag {tag} already defined.</source>
        <translation>Tagi {tag} on jo määritelty.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="81"/>
        <source>Description {tag} already defined.</source>
        <translation>Kuvaus {tag} on jo määritelty.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="148"/>
        <source>Start tag {tag} is not valid HTML</source>
        <translation>Aloittava tagi {tag} ei ole kelvollista HTML:ää</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="168"/>
        <source>End tag {end} does not match end tag for start tag {start}</source>
        <translation>Lopetus tunnus {end} ei vastaa {start} aloitus tunnusta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="92"/>
        <source>New Tag {row:d}</source>
        <translation>Uusi tagi {row:d}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="96"/>
        <source>&lt;HTML here&gt;</source>
        <translation>&lt;Kirjoita HTML koodi tähän&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="199"/>
        <source>Validation Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="179"/>
        <source>Description is missing</source>
        <translation>Kuvaus ei voi olla tyhjä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="182"/>
        <source>Tag is missing</source>
        <translation>Tunnus puuttuu</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTags</name>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="63"/>
        <source>Red</source>
        <translation>Punainen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="69"/>
        <source>Black</source>
        <translation>Musta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="75"/>
        <source>Blue</source>
        <translation>Sininen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="81"/>
        <source>Yellow</source>
        <translation>Keltainen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="87"/>
        <source>Green</source>
        <translation>Vihreä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="93"/>
        <source>Pink</source>
        <translation>Vaaleanpunainen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="99"/>
        <source>Orange</source>
        <translation>Oranssi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="105"/>
        <source>Purple</source>
        <translation>Purppura</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="111"/>
        <source>White</source>
        <translation>Valkoinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="117"/>
        <source>Superscript</source>
        <translation>Yläindeksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="122"/>
        <source>Subscript</source>
        <translation>Alaindeksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="127"/>
        <source>Paragraph</source>
        <translation>Kappale</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="132"/>
        <source>Bold</source>
        <translation>Lihavointi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="137"/>
        <source>Italics</source>
        <translation>Kursiivi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="141"/>
        <source>Underline</source>
        <translation>Alleviivaus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="147"/>
        <source>Break</source>
        <translation>Rivinvaihto</translation>
    </message>
</context>
<context>
    <name>OpenLP.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="296"/>
        <source>Experimental features (use at your own risk)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="334"/>
        <source>Service Item Slide Limits</source>
        <translation>Esityksen kierto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="335"/>
        <source>Behavior of next/previous on the last/first slide:</source>
        <translation>Kun viimeistä diaa toistetaan ja siirrytään seuraavaan tai,
kun ensimmäistä diaa toistetaan ja siirrytään edelliseen:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="336"/>
        <source>&amp;Remain on Slide</source>
        <translation>&amp;Mitään ei tapahdu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="337"/>
        <source>&amp;Wrap around</source>
        <translation>&amp;Siirry ensimmäiseen tai viimeiseen diaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="338"/>
        <source>&amp;Move to next/previous service item</source>
        <translation>&amp;Lähetä seuraava tai edellinen Listan kohde Esitykseen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="160"/>
        <source>General</source>
        <translation>Yleiset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="161"/>
        <source>Application Startup</source>
        <translation>Ohjelman käynnistys</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="162"/>
        <source>Show blank screen warning</source>
        <translation>Varoita pimennetystä näytöstä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="163"/>
        <source>Automatically open the previous service file</source>
        <translation>Avaa edellinen Lista automaattisesti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="164"/>
        <source>Show the splash screen</source>
        <translation>Näytä OpenLP:n logo käynnistyksen aikana</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="165"/>
        <source>Logo</source>
        <translation>Logo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="167"/>
        <source>Logo file:</source>
        <translation>Logon valinta:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="168"/>
        <source>Don&apos;t show logo on startup</source>
        <translation>Älä näytä logoa käynnistyksen yhteydessä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="169"/>
        <source>Check for updates to OpenLP</source>
        <translation>Tarkista OpenLP:n päivitykset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="170"/>
        <source>Application Settings</source>
        <translation>Sovelluksen asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="171"/>
        <source>Prompt to save before starting a new service</source>
        <translation>Pyydä tallentamaan ennen uuden Listan luomista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="173"/>
        <source>Unblank display when changing slide in Live</source>
        <translation>Lopeta näytön pimennys vaihtamalla diaa Esityksessä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="175"/>
        <source>Unblank display when sending items to Live</source>
        <translation>Lopeta näytön pimennys, kun kohde lähetetään Esitykseen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="177"/>
        <source>Automatically preview the next item in service</source>
        <translation>Esikatsele seuraavaa Listan kohdetta automaattisesti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="179"/>
        <source>Timed slide interval:</source>
        <translation>Automaattisen toiston nopeus:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="180"/>
        <source> sec</source>
        <translation> s</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="181"/>
        <source>CCLI Details</source>
        <translation>CCLI tiedot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="183"/>
        <source>SongSelect username:</source>
        <translation>SongSelect käyttäjätunnus:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="184"/>
        <source>SongSelect password:</source>
        <translation>SongSelect salasana:</translation>
    </message>
</context>
<context>
    <name>OpenLP.LanguageManager</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Language</source>
        <translation>Kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Please restart OpenLP to use your new language setting.</source>
        <translation>Ole hyvä ja käynnistä OpenLP uudelleen käyttääksesi uusia kieliasetuksia.</translation>
    </message>
</context>
<context>
    <name>OpenLP.MainWindow</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="261"/>
        <source>English</source>
        <comment>Please add the name of your language here</comment>
        <translation>Suomi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="304"/>
        <source>General</source>
        <translation>Yleiset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="361"/>
        <source>&amp;File</source>
        <translation>&amp;Tiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="362"/>
        <source>&amp;Import</source>
        <translation>&amp;Tuo tiedostosta...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="363"/>
        <source>&amp;Export</source>
        <translation>Vie tiedostoksi…</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="364"/>
        <source>&amp;Recent Services</source>
        <translation>&amp;Viimeisimmät Listatiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="365"/>
        <source>&amp;View</source>
        <translation>&amp;Ulkoasu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="366"/>
        <source>&amp;Layout Presets</source>
        <translation>&amp;Valmisasettelut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="367"/>
        <source>&amp;Tools</source>
        <translation>&amp;Työkalut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="368"/>
        <source>&amp;Settings</source>
        <translation>&amp;Asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="395"/>
        <source>&amp;Language</source>
        <translation>&amp;Kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="370"/>
        <source>&amp;Help</source>
        <translation>&amp;Ohjeet</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="371"/>
        <source>Library</source>
        <translation>Kirjastot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="372"/>
        <source>Service</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="373"/>
        <source>Themes</source>
        <translation>Teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="374"/>
        <source>Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="375"/>
        <source>&amp;New Service</source>
        <translation>&amp;Uusi Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="378"/>
        <source>&amp;Open Service</source>
        <translation>&amp;Avaa Listatiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="380"/>
        <source>Open an existing service.</source>
        <translation>Avaa Listatiedosto.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="381"/>
        <source>&amp;Save Service</source>
        <translation>&amp;Tallenna Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="383"/>
        <source>Save the current service to disk.</source>
        <translation>Tallenna nykyinen Lista.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="384"/>
        <source>Save Service &amp;As...</source>
        <translation>Tallenna Lista &amp;nimellä...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="385"/>
        <source>Save Service As</source>
        <translation>Tallenna Lista nimellä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="386"/>
        <source>Save the current service under a new name.</source>
        <translation>Tallenna nykyinen Lista uudella nimellä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="389"/>
        <source>Print the current service.</source>
        <translation>Tulosta nykyinen Lista.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="390"/>
        <source>E&amp;xit</source>
        <translation>&amp;Sulje sovellus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="391"/>
        <source>Close OpenLP - Shut down the program.</source>
        <translation>Sulje OpenLP - Sammuta ohjelma.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="394"/>
        <source>&amp;Theme</source>
        <translation>T&amp;eema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="396"/>
        <source>Configure &amp;Shortcuts...</source>
        <translation>&amp;Pikanäppäimet</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="397"/>
        <source>Configure &amp;Formatting Tags...</source>
        <translation>Tekstin &amp;muotoilutunnukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="398"/>
        <source>&amp;Configure OpenLP...</source>
        <translation>&amp;Määritä asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="399"/>
        <source>Export settings to a *.config file.</source>
        <translation>Vie asetukset *.config tiedostoon.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="405"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="402"/>
        <source>Import settings from a *.config file previously exported from this or another machine.</source>
        <translation>Tuo OpenLP:n asetukset .config muotoisesta asetustiedostosta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="406"/>
        <source>&amp;Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="407"/>
        <source>Hide or show Projectors.</source>
        <translation>Näytä tai piilota Projektorien hallinta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="408"/>
        <source>Toggle visibility of the Projectors.</source>
        <translation>Näytä tai piilota Projektorien hallinta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="410"/>
        <source>L&amp;ibrary</source>
        <translation>&amp;Kirjasto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="411"/>
        <source>Hide or show the Library.</source>
        <translation>Näytä tai piilota Kirjasto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="412"/>
        <source>Toggle the visibility of the Library.</source>
        <translation>Näytä tai piilota Kirjasto.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="414"/>
        <source>&amp;Themes</source>
        <translation>&amp;Teemat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="415"/>
        <source>Hide or show themes</source>
        <translation>Näytä tai piilota teemat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="416"/>
        <source>Toggle visibility of the Themes.</source>
        <translation>Näytä tai piilota teemojen hallinta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="418"/>
        <source>&amp;Service</source>
        <translation>&amp;Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="419"/>
        <source>Hide or show Service.</source>
        <translation>Näytä tai piilota Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="420"/>
        <source>Toggle visibility of the Service.</source>
        <translation>Näytä tai piilota Lista.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="422"/>
        <source>&amp;Preview</source>
        <translation>&amp;Esikatselu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="423"/>
        <source>Hide or show Preview.</source>
        <translation>Näytä tai piilota Esikatselu.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="424"/>
        <source>Toggle visibility of the Preview.</source>
        <translation>Näytä tai piilota Esikatselu.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="426"/>
        <source>Li&amp;ve</source>
        <translation>Esi&amp;tys</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="427"/>
        <source>Hide or show Live</source>
        <translation>Näytä tai piilota Esitys</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="428"/>
        <source>L&amp;ock visibility of the panels</source>
        <translation>&amp;Lukitse paneelien näkyvyys</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="429"/>
        <source>Lock visibility of the panels.</source>
        <translation>Lukitsee paneelien näkyvyysasetukset.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="430"/>
        <source>Toggle visibility of the Live.</source>
        <translation>Näytä tai piilota Esitys</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="431"/>
        <source>&amp;Manage Plugins</source>
        <translation>&amp;Moduulien hallinta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="432"/>
        <source>You can enable and disable plugins from here.</source>
        <translation>Täältä voit ottaa tai poistaa käytöstä ohjelman osia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="434"/>
        <source>&amp;About</source>
        <translation>&amp;Tietoa OpenLP:stä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="435"/>
        <source>More information about OpenLP.</source>
        <translation>Lisää tietoa OpenLP:stä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="436"/>
        <source>&amp;User Manual</source>
        <translation>&amp;Käyttöohjeet</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="438"/>
        <source>Jump to the search box of the current active plugin.</source>
        <translation>Siirry aktiivisen liitännäisen hakukenttään.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="440"/>
        <source>&amp;Web Site</source>
        <translation>&amp;Kotisivut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="443"/>
        <source>Set the interface language to {name}</source>
        <translation>Aseta käyttöliittymän kieleksi {name}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="445"/>
        <source>&amp;Autodetect</source>
        <translation>&amp;Tunnista automaattisesti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="446"/>
        <source>Use the system language, if available.</source>
        <translation>Käytä järjestelmän kieltä, jos se on saatavilla.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="447"/>
        <source>Add &amp;Tool...</source>
        <translation>Lisää &amp;Työkalu...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="448"/>
        <source>Add an application to the list of tools.</source>
        <translation>Lisää sovellus työkalujen luetteloon.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="450"/>
        <source>Open &amp;Data Folder...</source>
        <translation>Avaa &amp;Tiedostokansion sijainti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="451"/>
        <source>Open the folder where songs, bibles and other data resides.</source>
        <translation>Avaa kansio, jossa laulut, Raamatut ja muut tiedot sijaitsevat.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="453"/>
        <source>Re-run First Time Wizard</source>
        <translation>Suorita ensimmäisen käyttökerran avustaja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="454"/>
        <source>Re-run the First Time Wizard, importing songs, Bibles and themes.</source>
        <translation>Suorita ensimmäisen käyttökerran avustaja uudelleen.
Voit tuoda sovellukseen lauluja, Raamattuja ja teemoja.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="456"/>
        <source>Update Theme Images</source>
        <translation>Päivitä teemojen kuvat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="457"/>
        <source>Update the preview images for all themes.</source>
        <translation>Päivitä kaikkien teemojen esikatselukuvat.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="459"/>
        <source>&amp;Show all</source>
        <translation>&amp;Näytä kaikki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="460"/>
        <source>Reset the interface back to the default layout and show all the panels.</source>
        <translation>Palauta ulkoasun oletusasetukset ja näytä kaikki paneelit.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="462"/>
        <source>&amp;Setup</source>
        <translation>&amp;Esikatselu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="463"/>
        <source>Use layout that focuses on setting up the Service.</source>
        <translation>Käytä ulkoasua, joka keskittyy Listan tekemiseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="465"/>
        <source>&amp;Live</source>
        <translation>&amp;Esitys</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="466"/>
        <source>Use layout that focuses on Live.</source>
        <translation>Käytä ulkoasua, joka keskittyy Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="548"/>
        <source>Waiting for some things to finish...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="550"/>
        <source>Please Wait</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="631"/>
        <source>Version {new} of OpenLP is now available for download (you are currently running version {current}). 

You can download the latest version from https://openlp.org/.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="634"/>
        <source>OpenLP Version Updated</source>
        <translation>OpenLP:n versio on nyt päivitetty.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Re-run First Time Wizard?</source>
        <translation>Ensimmäisen käyttökerran avustaja - Vahvistus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Are you sure you want to re-run the First Time Wizard?

Re-running this wizard may make changes to your current OpenLP configuration and possibly add songs to your existing songs list and change your default theme.</source>
        <translation>Haluatko varmasti suorittaa ensimmäisen käyttökerran avustajan?

Tämän avulla voidaan muuttaa joitakin asetuksia
sekä ladata esimerkiksi lauluja ja Raamattuja.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>OpenLP Main Display Blanked</source>
        <translation>OpenLP:n Esitys on pimennetty</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>The Main Display has been blanked out</source>
        <translation>Esityksen näyttö on pimennetty</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Import settings?</source>
        <translation>Tuodaanko asetukset?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Are you sure you want to import settings?

 Importing settings will make permanent changes to your current OpenLP configuration.

 Importing incorrect settings may cause erratic behaviour or OpenLP to terminate abnormally.</source>
        <translation>Haluatko varmasti tuoda asetukset?&lt;br&gt;&lt;br&gt;

Asetusten tuominen muuttaa sovelluksen käyttämiä asetuksia.&lt;br&gt;&lt;br&gt;

Viallinen asetustiedosto voi aiheuttaa virheellistä&lt;br&gt;
toimintaa ja ohjelma saattaa sulkeutua yllättäen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>Import settings</source>
        <translation>Tuo asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>OpenLP Settings (*.conf)</source>
        <translation>OpenLP:n asetukset (*.conf)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="889"/>
        <source>The file you have selected does not appear to be a valid OpenLP settings file.

Processing has terminated and no changes have been made.</source>
        <translation>Tiedosto, jonka olet valinnut ei ole kelvollinen OpenLP:n asetustiedosto.

Käsittely on keskeytetty eikä muutoksia ole tehty.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>OpenLP will now close.  Imported settings will be applied the next time you start OpenLP.</source>
        <translation>OpenLP sulkeutuu nyt. Tuodut asetukset otetaan käyttöön seuraavan käynnistyksen yhteydessä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>Export Settings File</source>
        <translation>Vie asetustiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>Export setting error</source>
        <translation>Asetusten vienti epäonnistui</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>An error occurred while exporting the settings: {err}</source>
        <translation>Asetuksia viedessä tapahtui virhe:
{err}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Exit OpenLP</source>
        <translation>Sulje OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Are you sure you want to exit OpenLP?</source>
        <translation>Haluatko varmasti sulkea OpenLP:n?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1042"/>
        <source>&amp;Exit OpenLP</source>
        <translation>&amp;Sulje OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1108"/>
        <source>Default Theme: {theme}</source>
        <translation>Yleinen teema: {theme}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear List</source>
        <comment>Clear List of recent files</comment>
        <translation>Tyhjennä luettelo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear the list of recent files.</source>
        <translation>Tyhjentää viimeksi käytettyjen listatiedostojen luettelon.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1345"/>
        <source>Copying OpenLP data to new data directory location - {path} - Please wait for copy to finish</source>
        <translation>Kopioidaan OpenLP:n tiedostoja uuteen tiedostokansion sijaintiin:
{path} 
Ole hyvä ja odota kopioinnin loppumista.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1353"/>
        <source>OpenLP Data directory copy failed

{err}</source>
        <translation>Tiedostokansion kopioiminen epäonnistui

{err}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1355"/>
        <source>New Data Directory Error</source>
        <translation>Virhe uudessa tiedostokansiossa</translation>
    </message>
</context>
<context>
    <name>OpenLP.Manager</name>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>Database Error</source>
        <translation>Tietokantavirhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="157"/>
        <source>OpenLP cannot load your database.

Database: {db}</source>
        <translation>OpenLP ei pystynyt avaamaan tietokantaasi:

{db}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>The database being loaded was created in a more recent version of OpenLP. The database is version {db_ver}, while OpenLP expects version {db_up}. The database will not be loaded.

Database: {db_name}</source>
        <translation>Tuotava tietokanta on luotu uudemmassa OpenLP:n versiossa.

Tietokannan versio on: {db_ver} 
 Tämä OpenLP:n versio kuitenkin on:
 {db_up}.

Tietokantaa ei voida tuoda.</translation>
    </message>
</context>
<context>
    <name>OpenLP.MediaController</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="100"/>
        <source>The media integration library is missing (python - vlc is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="102"/>
        <source>The media integration library is missing (python - pymediainfo is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="122"/>
        <source>No Displays have been configured, so Live Media has been disabled</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.MediaManagerItem</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="382"/>
        <source>No Items Selected</source>
        <translation>Valinta on tyhjä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="161"/>
        <source>&amp;Add to selected Service Item</source>
        <translation>&amp;Lisää osaksi Listan kuvaryhmää</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File Type</source>
        <translation>Virheellinen tiedostomuoto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File {file_path}.
File extension not supported</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="391"/>
        <source>Duplicate files were found on import and were ignored.</source>
        <translation>Tuotaesa löytyi päällekkäisiä tiedostoja ja ne ohitettiin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="496"/>
        <source>You must select one or more items to preview.</source>
        <translation>Sinun pitää valita yksi tai useampi kohta esikatseluun.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="514"/>
        <source>You must select one or more items to send live.</source>
        <translation>Valitse yksi tai useampi kohde lähetettäväksi Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="563"/>
        <source>You must select one or more items to add.</source>
        <translation>Sinun täytyy valita yksi tai useampi kohta lisättäväksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="607"/>
        <source>You must select one or more items.</source>
        <translation>Sinun pitää valita yksi tai useampi kohta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="614"/>
        <source>You must select an existing service item to add to.</source>
        <translation>Sinun täytyy valita Listan kohta johon haluat lisätä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>Invalid Service Item</source>
        <translation>Vihreellinen Listan kohta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>You must select a {title} service item.</source>
        <translation>Ole hyvä ja valitse {title} Listasta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="119"/>
        <source>&amp;Clone</source>
        <translation>&amp;Luo kopio</translation>
    </message>
</context>
<context>
    <name>OpenLP.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="51"/>
        <source>Media</source>
        <translation>Media</translation>
    </message>
</context>
<context>
    <name>OpenLP.OpenLyricsImportError</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="707"/>
        <source>&lt;lyrics&gt; tag is missing.</source>
        <translation>&lt;lyrics&gt; Tunniste puuttuu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="712"/>
        <source>&lt;verse&gt; tag is missing.</source>
        <translation>&lt;verse&gt; Tunniste puuttuu.</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="494"/>
        <source>Fan</source>
        <translation>Tuuletin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="495"/>
        <source>Lamp</source>
        <translation>Lamppu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="496"/>
        <source>Temperature</source>
        <translation>Lämpötila</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="497"/>
        <source>Cover</source>
        <translation>Peite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="498"/>
        <source>Filter</source>
        <translation>Suodin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="430"/>
        <source>No message</source>
        <translation>Ei viestiä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="759"/>
        <source>Error while sending data to projector</source>
        <translation>Virhe lähetettäessä tiedot projektorille</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLinkConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="166"/>
        <source>Acknowledge a PJLink SRCH command - returns MAC address.</source>
        <translation>Kuittaa PJLink SRCH komento - palauttaa MAC osoitteen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="171"/>
        <source>Blank/unblank video and/or mute audio.</source>
        <translation>Pimennä kuva ja/tai mykistä ääni.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="176"/>
        <source>Query projector PJLink class support.</source>
        <translation>Selvitä projektorin PJLink luokkatuki.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="181"/>
        <source>Query error status from projector. Returns fan/lamp/temp/cover/filter/other error status.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="187"/>
        <source>Query number of hours on filter.</source>
        <translation>Kysy laitteelta suotimen tuntimäärä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="192"/>
        <source>Freeze or unfreeze current image being projected.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="197"/>
        <source>Query projector manufacturer name.</source>
        <translation>Kysy projektorilta valmistajan nimi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="202"/>
        <source>Query projector product name.</source>
        <translation>Tiedustele projektorin tuotenimi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="207"/>
        <source>Query projector for other information set by manufacturer.</source>
        <translation>Tiedustele projektorilta muut tiedot, joita valmistaja toimittaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="212"/>
        <source>Query specified input source name</source>
        <translation>Tiedustele määritellyn tulon nimi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="217"/>
        <source>Switch to specified video source.</source>
        <translation>Vaihda määriteltyyn videotuloon.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="222"/>
        <source>Query available input sources.</source>
        <translation>Tiedustele saatavilla olevat tulot.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="227"/>
        <source>Query current input resolution.</source>
        <translation>Kysy laitteelta käytössä oleva resoluutio.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="232"/>
        <source>Query lamp time and on/off status. Multiple lamps supported.</source>
        <translation>Kysy laitteelta lampun aika ja tila. Useammat lamput ovat tuettuna.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="237"/>
        <source>UDP Status - Projector is now available on network. Includes MAC address.</source>
        <translation>UDP tila - Yhteys projektoriin verkon kautta. Sisältää MAC osoitteen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="242"/>
        <source>Adjust microphone volume by 1 step.</source>
        <translation>Säädä mikrofonin äänentasoa yhdellä pykälällä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="247"/>
        <source>Query customer-set projector name.</source>
        <translation>Tiedustele asiakkaan antama projektorin nimi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="252"/>
        <source>Initial connection with authentication/no authentication request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="257"/>
        <source>Turn lamp on or off/standby.</source>
        <translation>Lamppu päälle / pois tai valmiustila.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="262"/>
        <source>Query replacement air filter model number.</source>
        <translation>Tiedustele varaosa suodattimen mallinumero.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="267"/>
        <source>Query replacement lamp model number.</source>
        <translation>Tiedustele varaosa lampun mallinumero.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="272"/>
        <source>Query recommended resolution.</source>
        <translation>Tiedustele suositeltu erottelukyky.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="277"/>
        <source>Query projector serial number.</source>
        <translation>Tiedustele projektorin sarjanumero.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="282"/>
        <source>UDP broadcast search request for available projectors. Reply is ACKN.</source>
        <translation>UDP kysely saatavilla olevien projektorien paikallistamiseksi. Vaste on ACKN.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="287"/>
        <source>Query projector software version number.</source>
        <translation>Tiedustele projektorin ohjelmiston versio numeroa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="292"/>
        <source>Adjust speaker volume by 1 step.</source>
        <translation>Säädä kaiuttimen äänenvoimakkuutta 1 askeleella.</translation>
    </message>
</context>
<context>
    <name>OpenLP.PathEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="299"/>
        <source>Browse for directory.</source>
        <translation>Selaa hakemistoa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="300"/>
        <source>Revert to default directory.</source>
        <translation>Palauta oletushakemistoksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="302"/>
        <source>Browse for file.</source>
        <translation>Selaa tiedostoja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="303"/>
        <source>Revert to default file.</source>
        <translation>Palauta oletustiedostoksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="317"/>
        <source>Select Directory</source>
        <translation>Valitse tiedostosijainti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="321"/>
        <source>Select File</source>
        <translation>Valitse tiedosto</translation>
    </message>
</context>
<context>
    <name>OpenLP.PluginForm</name>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="74"/>
        <source>Manage Plugins</source>
        <translation>Moduulien hallinta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="75"/>
        <source>Plugin Details</source>
        <translation>Moduulin tiedot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="77"/>
        <source>Status:</source>
        <translation>Tila:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="78"/>
        <source>Active</source>
        <translation>Käytössä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="149"/>
        <source>{name} (Disabled)</source>
        <translation>{name} (Ei käytössä)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="145"/>
        <source>{name} (Active)</source>
        <translation>{name} (Käytössä)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="147"/>
        <source>{name} (Inactive)</source>
        <translation>{name} (Ei käytössä)</translation>
    </message>
</context>
<context>
    <name>OpenLP.PluginManager</name>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="173"/>
        <source>Unable to initialise the following plugins:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="179"/>
        <source>See the log file for more details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceDialog</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Page</source>
        <translation>Sovita sivulle</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Width</source>
        <translation>Sovita leveyteen</translation>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceForm</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="61"/>
        <source>Print</source>
        <translation>Tulosta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="69"/>
        <source>Copy as Text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="71"/>
        <source>Copy as HTML</source>
        <translation>Kopioi HTML:nä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="137"/>
        <source>Zoom Out</source>
        <translation>Loitonna</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="138"/>
        <source>Zoom Original</source>
        <translation>Alkuperäiseen kokoon</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="139"/>
        <source>Zoom In</source>
        <translation>Lähennä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="140"/>
        <source>Options</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="141"/>
        <source>Title:</source>
        <translation>Nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="142"/>
        <source>Service Note Text:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="143"/>
        <source>Other Options</source>
        <translation>Muut asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="144"/>
        <source>Include slide text if available</source>
        <translation>Ota mukaan dian teksti, jos saatavilla</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="145"/>
        <source>Add page break before each text item</source>
        <translation>Lisää sivunvaihto ennen jokaista tekstiä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="146"/>
        <source>Include service item notes</source>
        <translation>Ota mukaan muistiinpanot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="147"/>
        <source>Include play length of media items</source>
        <translation>Ota mukaan toistettavan median pituus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="148"/>
        <source>Show chords</source>
        <translation>Näytä soinnut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="149"/>
        <source>Service Sheet</source>
        <translation>OpenLP:n lista</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="401"/>
        <source>The address specified with socket.bind() is already in use and was set to be exclusive</source>
        <translation>Annettu socket.bind() on jo käytössä eikä sitä ole sallittu jaettava</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="404"/>
        <source>PJLink returned &quot;ERRA: Authentication Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="405"/>
        <source>The connection was refused by the peer (or timed out)</source>
        <translation>Toinen osapuoli kieltäytyi yhteydestä (tai aikakatkaisu)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="407"/>
        <source>Projector cover open detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="408"/>
        <source>PJLink class not supported</source>
        <translation>PJLink luokka ei ole tuettu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="409"/>
        <source>The datagram was larger than the operating system&apos;s limit</source>
        <translation>Datagrammi oli suurempi kuin käyttöjärjestelmän rajoitus sallii</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="411"/>
        <source>Error condition detected</source>
        <translation>Virhetila havaittu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="412"/>
        <source>Projector fan error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="413"/>
        <source>Projector check filter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="414"/>
        <source>General projector error</source>
        <translation>Yleinen projektorivirhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="415"/>
        <source>The host address was not found</source>
        <translation>Palvelimen osoitetta ei löytynyt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="416"/>
        <source>PJLink invalid packet received</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="417"/>
        <source>Projector lamp error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="418"/>
        <source>An error occurred with the network (Possibly someone pulled the plug?)</source>
        <translation>Verkkovirhe (Mahdollisesti joku irroitti verkkojohdon?)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="420"/>
        <source>PJlink authentication Mismatch Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="421"/>
        <source>Projector not connected error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="422"/>
        <source>PJLink returned &quot;ERR2: Invalid Parameter&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="423"/>
        <source>PJLink Invalid prefix character</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="424"/>
        <source>PJLink returned &quot;ERR4: Projector/Display Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="425"/>
        <source>The socket is using a proxy, and the proxy requires authentication</source>
        <translation>Socket käyttää välityspalvelinta ja se vaatii tunnistautumisen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="428"/>
        <source>The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established)</source>
        <translation>Yhteys välityspalvelimeen on katkaistu odottamattomasti (ennen kuin yhteys on muodostunut toiseen koneeseen)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="431"/>
        <source>Could not contact the proxy server because the connection to that server was denied</source>
        <translation>Ei saada yhteyttä välityspalvelimeen, koska yhteys palvelimeen on kielletty</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="434"/>
        <source>The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase.</source>
        <translation>Yhteys välityspalvelimeen on aikakatkaistu tai välityspalvelin on lakannut vastaamasta tunnistautumisvaiheessa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="437"/>
        <source>The proxy address set with setProxy() was not found</source>
        <translation>Välityspalvelimen setProxy() osoitetta ei löytynyt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="439"/>
        <source>The connection negotiation with the proxy server failed because the response from the proxy server could not be understood</source>
        <translation>Yhteyden muodostaminen välityspalvelimeen epäonnistui, koska palvelin ei ymmärtänyt pyyntöä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="442"/>
        <source>The remote host closed the connection</source>
        <translation>Etäpalvelin sulki yhteyden</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="444"/>
        <source>The SSL/TLS handshake failed</source>
        <translation>SSL/TLS kättely epäonnistui</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="446"/>
        <source>The address specified to socket.bind() does not belong to the host</source>
        <translation>Socket.bind() osoite on määritelty kuuluvaksi toiselle palvelulle</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="449"/>
        <source>The socket operation failed because the application lacked the required privileges</source>
        <translation>Ohjelmalla ei ole riittäviä oikeuksia socket-rajanpinnan käyttämiseksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="452"/>
        <source>The local system ran out of resources (e.g., too many sockets)</source>
        <translation>Paikallisen järjestelmän resurssit loppuivat (esim. liikaa socketteja)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="454"/>
        <source>The socket operation timed out</source>
        <translation>Aikakatkaisu socket käytössä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="456"/>
        <source>Projector high temperature detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="457"/>
        <source>PJLink returned &quot;ERR3: Busy&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="458"/>
        <source>PJLink returned &quot;ERR1: Undefined Command&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="459"/>
        <source>The last operation attempted has not finished yet (still in progress in the background)</source>
        <translation>Viimeisintä toimintoa ei ole vielä suoritettu loppuun.
(Toimintoa suoritetaan yhä taustalla)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="462"/>
        <source>Unknown condiction detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="463"/>
        <source>An unidentified socket error occurred</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="464"/>
        <source>The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support)</source>
        <translation>Socket -toiminto ei ole tuettu paikallisessa käyttöjärjestelmässä (esim. IPV6-tuki puuttuu)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="467"/>
        <source>Warning condition detected</source>
        <translation>Varoitustila havaittu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="468"/>
        <source>Connection initializing with pin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="469"/>
        <source>Socket is bount to an address or port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="470"/>
        <source>Connection initializing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="471"/>
        <source>Socket is about to close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="472"/>
        <source>Connected</source>
        <translation>Yhdistetty</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="473"/>
        <source>Connecting</source>
        <translation>Yhdistetään</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="474"/>
        <source>Cooldown in progress</source>
        <translation>Jäähdytellään</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="475"/>
        <source>Command returned with OK</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="476"/>
        <source>Performing a host name lookup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="477"/>
        <source>Projector Information available</source>
        <translation>Projektorin tiedot on saatavilla</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="478"/>
        <source>Initialize in progress</source>
        <translation>Alustus käynnissä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="479"/>
        <source>Socket it listening (internal use only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="480"/>
        <source>No network activity at this time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="481"/>
        <source>Received data</source>
        <translation>Vastaanotettaan tietoa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="482"/>
        <source>Sending data</source>
        <translation>Lähetetään tietoja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="483"/>
        <source>Not Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="484"/>
        <source>Off</source>
        <translation>Pois</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="485"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="486"/>
        <source>Power is on</source>
        <translation>Virta on päällä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="487"/>
        <source>Power in standby</source>
        <translation>Standby -tila</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="488"/>
        <source>Getting status</source>
        <translation>Vastaanotettaan tila</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="489"/>
        <source>Warmup in progress</source>
        <translation>Lämpenee</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEdit</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>Name Not Set</source>
        <translation>Nimeä ei ole asetettu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>You must enter a name for this entry.&lt;br /&gt;Please enter a new name for this entry.</source>
        <translation>Sinun pitää antaa nimi syötteellesi.&lt;br /&gt;Ole hyvä ja anna uusi nimi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="196"/>
        <source>Duplicate Name</source>
        <translation>Nimi on jo käytössä</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEditForm</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="115"/>
        <source>Add New Projector</source>
        <translation>Lisää uusi projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="118"/>
        <source>Edit Projector</source>
        <translation>Muokkaa projektoria.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="120"/>
        <source>IP Address</source>
        <translation>IP-osoite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="123"/>
        <source>Port Number</source>
        <translation>Portti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="125"/>
        <source>PIN</source>
        <translation>PIN</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="127"/>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="129"/>
        <source>Location</source>
        <translation>Sijainti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="131"/>
        <source>Notes</source>
        <translation>Muistiinpanot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>Database Error</source>
        <translation>Tietokantavirhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>There was an error saving projector information. See the log for the error</source>
        <translation>Tapahtui virhe projektorin tietojen tallennuksessa. Katso logista virhettä</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorManager</name>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add Projector</source>
        <translation>Lisää projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add a new projector.</source>
        <translation>Lisää uusi projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit Projector</source>
        <translation>Muokkaa projektoria.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit selected projector.</source>
        <translation>Muokkaa valittua projektoria.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete Projector</source>
        <translation>Poista projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete selected projector.</source>
        <translation>Poista valittu projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Select Input Source</source>
        <translation>Valitse sisääntulon lähde</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Choose input source on selected projector.</source>
        <translation>Valitse sisääntulo valitulle projektorille.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View Projector</source>
        <translation>Näytä projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View selected projector information.</source>
        <translation>Näytä valitun projektorin tiedot.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="100"/>
        <source>Connect to selected projector.</source>
        <translation>Yhdistä valittuun projektoriin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors</source>
        <translation>Yhdistä valitut projektorit.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors.</source>
        <translation>Yhdistä valitut projektorit.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projectors</source>
        <translation>Lopeta yhteys valittuihin projektoreihin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projector.</source>
        <translation>Katkaise yhteys valittuun projektoriin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projector</source>
        <translation>Lopeta yhteys valittuun projektoriin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projectors.</source>
        <translation>Katkaise yhteys valittuihin projektoreihin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projector</source>
        <translation>Kytke valittu projektori päälle.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="129"/>
        <source>Power on selected projector.</source>
        <translation>Käynnistä valittu projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projectors.</source>
        <translation>Käynnistä valitut projektorit.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Standby selected projector</source>
        <translation>Projektori standby -tilaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="143"/>
        <source>Put selected projector in standby.</source>
        <translation>Aseta valittu projektori valmiustilaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Put selected projectors in standby.</source>
        <translation>Aseta valitut projektorit valmiustilaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="156"/>
        <source>Blank selected projector screen</source>
        <translation>Pimennä valittu projektorinäyttö.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen</source>
        <translation>Pimennä valittu projektorinäyttö</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen.</source>
        <translation>Pimennä valitut projektorinäyttöt.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projector screen</source>
        <translation>Näytä valittu projektorinäyttö.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="170"/>
        <source>Show selected projector screen.</source>
        <translation>Näytä valittu projektorinäyttö.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projectors screen.</source>
        <translation>Näytä valitut projektorinäytöt.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="199"/>
        <source>&amp;View Projector Information</source>
        <translation>&amp;Näytä projektorin tiedot.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="204"/>
        <source>&amp;Edit Projector</source>
        <translation>&amp;Muokkaa projektoria.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="210"/>
        <source>&amp;Connect Projector</source>
        <translation>&amp;Yhdistä projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="215"/>
        <source>D&amp;isconnect Projector</source>
        <translation>K&amp;ytke projektori pois.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="221"/>
        <source>Power &amp;On Projector</source>
        <translation>Käynnistä projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="226"/>
        <source>Power O&amp;ff Projector</source>
        <translation>Sammuta projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="232"/>
        <source>Select &amp;Input</source>
        <translation>&amp;Valitse tulo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="237"/>
        <source>Edit Input Source</source>
        <translation>Muokkaa tuloa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="242"/>
        <source>&amp;Blank Projector Screen</source>
        <translation>&amp;Pimennä projektorin näyttö.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="247"/>
        <source>&amp;Show Projector Screen</source>
        <translation>&amp;Näytä projektorin näyttö.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="253"/>
        <source>&amp;Delete Projector</source>
        <translation>P&amp;oista projektori.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="503"/>
        <source>Are you sure you want to delete this projector?</source>
        <translation>Haluatko varmasti poistaa tämän projektorin?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="648"/>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="650"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="652"/>
        <source>Port</source>
        <translation>Portti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="654"/>
        <source>Notes</source>
        <translation>Muistiinpanot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="658"/>
        <source>Projector information not available at this time.</source>
        <translation>Projektorin tiedot eivät ole saatavilla juuri nyt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="660"/>
        <source>Projector Name</source>
        <translation>Projektorin nimi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="663"/>
        <source>Manufacturer</source>
        <translation>Valmistaja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="665"/>
        <source>Model</source>
        <translation>Malli</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="667"/>
        <source>PJLink Class</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="670"/>
        <source>Software Version</source>
        <translation>Ohjelmiston versio</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="673"/>
        <source>Serial Number</source>
        <translation>Sarjanumero</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="676"/>
        <source>Lamp Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="679"/>
        <source>Filter Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="682"/>
        <source>Other info</source>
        <translation>Muut tiedot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="685"/>
        <source>Power status</source>
        <translation>Virran tila</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Shutter is</source>
        <translation>Sulkija on</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Closed</source>
        <translation>suljettu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="691"/>
        <source>Current source input is</source>
        <translation>Nykyinen tulo on</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="698"/>
        <source>Unavailable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="700"/>
        <source>ON</source>
        <translation>PÄÄLLÄ</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="702"/>
        <source>OFF</source>
        <translation>POIS</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="703"/>
        <source>Lamp</source>
        <translation>Lamppu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="708"/>
        <source>Hours</source>
        <translation>Tuntia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="713"/>
        <source>No current errors or warnings</source>
        <translation>Ei virheitä tai varoituksia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="715"/>
        <source>Current errors/warnings</source>
        <translation>Nykyiset virheet/varoiutukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="718"/>
        <source>Projector Information</source>
        <translation>Projektorin tiedot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="941"/>
        <source>Authentication Error</source>
        <translation>Kirjautusmisvirhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="956"/>
        <source>No Authentication Error</source>
        <translation>Tunnistautumisessa tapahtui virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="998"/>
        <source>Not Implemented Yet</source>
        <translation>Ei ole vielä toteutettu</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="52"/>
        <source>Projector</source>
        <translation>Projektori</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="111"/>
        <source>Communication Options</source>
        <translation>Tiedonsiirron asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="113"/>
        <source>Connect to projectors on startup</source>
        <translation>Yhdistä projektorit käynnistäessä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="115"/>
        <source>Socket timeout (seconds)</source>
        <translation>Socketin aikakatkaisu (sekuntia)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="117"/>
        <source>Poll time (seconds)</source>
        <translation>Virkistysaika (sekuntia)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="121"/>
        <source>Tabbed dialog box</source>
        <translation>Välilehdin varustettu viesti-ikkuna</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="123"/>
        <source>Single dialog box</source>
        <translation>Yksinkertainen viesti-ikkuna</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="125"/>
        <source>Connect to projector when LINKUP received (v2 only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="127"/>
        <source>Enable listening for PJLink2 broadcast messages</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorWizard</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="216"/>
        <source>Duplicate IP Address</source>
        <translation>Päällekkäinen IP-osoite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="224"/>
        <source>Invalid IP Address</source>
        <translation>Virheellinen IP-osoite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="233"/>
        <source>Invalid Port Number</source>
        <translation>Virheellinen porttinumero</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProxyDialog</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="177"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProxyWidget</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="116"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="117"/>
        <source>No prox&amp;y</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="118"/>
        <source>&amp;Use system proxy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="119"/>
        <source>&amp;Manual proxy configuration</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="120"/>
        <source>e.g. proxy_server_address:port_no</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="121"/>
        <source>HTTP:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="123"/>
        <source>HTTPS:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="125"/>
        <source>Username:</source>
        <translation>Käyttäjätunnus:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="126"/>
        <source>Password:</source>
        <translation>Salasana:</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreenList</name>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="254"/>
        <source>Screen</source>
        <translation>Näyttö</translation>
    </message>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="257"/>
        <source>primary</source>
        <translation>ensisijainen</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreensTab</name>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="44"/>
        <source>Screens</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="69"/>
        <source>Generic screen settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="70"/>
        <source>Display if a single screen</source>
        <translation>Näytä Esitys ensisijaisessa näytössä,
jos toista näyttöä ei ole kytketty</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="297"/>
        <source>F&amp;ull screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="298"/>
        <source>Width:</source>
        <translation>Leveys:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="299"/>
        <source>Use this screen as a display</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="302"/>
        <source>Left:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="303"/>
        <source>Custom &amp;geometry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="304"/>
        <source>Top:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="305"/>
        <source>Height:</source>
        <translation>Korkeus:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="306"/>
        <source>Identify Screens</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItem</name>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="289"/>
        <source>[slide {frame:d}]</source>
        <translation>[dia {frame:d}]</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="590"/>
        <source>&lt;strong&gt;Start&lt;/strong&gt;: {start}</source>
        <translation>&lt;strong&gt;Alku&lt;/strong&gt;: {start}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="594"/>
        <source>&lt;strong&gt;Length&lt;/strong&gt;: {length}</source>
        <translation>&lt;strong&gt;Pituus&lt;/strong&gt;: {length}</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItemEditForm</name>
    <message>
        <location filename="../../openlp/core/ui/serviceitemeditdialog.py" line="70"/>
        <source>Reorder Service Item</source>
        <translation>Uudelleenjärjestä Lista</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceManager</name>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="199"/>
        <source>Service Notes: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="246"/>
        <source>Notes: </source>
        <translation>Muistiinpanot:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="254"/>
        <source>Playing time: </source>
        <translation>Toistoaika:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="148"/>
        <source>Load an existing service.</source>
        <translation>Avaa Listatiedosto.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="152"/>
        <source>Save this service.</source>
        <translation>Tallenna tämä Lista.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="162"/>
        <source>Select a theme for the service.</source>
        <translation>Valitse teema Listalle.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move to &amp;top</source>
        <translation>Siirrä &amp;ylös</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move item to the top of the service.</source>
        <translation>Siirrä kohde Listan ensimmäiseksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move &amp;up</source>
        <translation>Siirrä &amp;ylös</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move item up one position in the service.</source>
        <translation>Siirrä kohde yhden ylemmäksi Listassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move &amp;down</source>
        <translation>Siirrä &amp;alas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move item down one position in the service.</source>
        <translation>Siirrä kohde yhden alemmaksi Listassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move to &amp;bottom</source>
        <translation>Siirrä &amp;loppuun</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move item to the end of the service.</source>
        <translation>Siirrä kohde Listan viimeiseksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>&amp;Delete From Service</source>
        <translation>&amp;Poista Listasta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>Delete the selected item from the service.</source>
        <translation>Poista valittu kohde Listasta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>&amp;Expand all</source>
        <translation>&amp;Laajenna kaikki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>Expand all the service items.</source>
        <translation>Laajenna kaikki Listan kohteet.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>&amp;Collapse all</source>
        <translation>&amp;Supista kaikki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>Collapse all the service items.</source>
        <translation>Supista kaikki Listan kohteet.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Go Live</source>
        <translation>Lähetä Esitykseen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Send the selected item to Live.</source>
        <translation>Lähetä kohde Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="241"/>
        <source>&amp;Add New Item</source>
        <translation>&amp;Lisää uusi rivi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="243"/>
        <source>&amp;Add to Selected Item</source>
        <translation>Lisää valittuun kohtaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="247"/>
        <source>&amp;Edit Item</source>
        <translation>&amp;Muokkaa valittua kohdetta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="249"/>
        <source>&amp;Rename...</source>
        <translation>&amp;Uudelleennimeä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="252"/>
        <source>&amp;Reorder Item</source>
        <translation>Järjestä &amp;uudelleen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="255"/>
        <source>&amp;Notes</source>
        <translation>Muistiin&amp;panot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="258"/>
        <source>&amp;Start Time</source>
        <translation>&amp;Alku aika</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="265"/>
        <source>Create New &amp;Custom Slide</source>
        <translation>Luo kohteesta &amp;Tekstidia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="272"/>
        <source>&amp;Auto play slides</source>
        <translation>Toista &amp;automaatisesti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="276"/>
        <source>Auto play slides &amp;Loop</source>
        <translation>Toista &amp;loputtomasti.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="281"/>
        <source>Auto play slides &amp;Once</source>
        <translation>Toista &amp;loppuun.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="812"/>
        <source>&amp;Delay between slides</source>
        <translation>&amp;Viive diojen välissä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="292"/>
        <source>Show &amp;Preview</source>
        <translation>&amp;Esikatsele</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="297"/>
        <source>&amp;Change Item Theme</source>
        <translation>&amp;Muuta kohteen teemaa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="371"/>
        <source>Untitled Service</source>
        <translation>Tallentamaton Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>Open File</source>
        <translation>Tiedoston valinta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>OpenLP Service Files (*.osz *.oszl)</source>
        <translation>OpenLP:n Lista (*.osz *.oszl)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>Modified Service</source>
        <translation>Sulkeminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>The current service has been modified. Would you like to save this service?</source>
        <translation>Nykyistä Listaa on muokattu, haluatko tallentaa sen?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="572"/>
        <source>Service File(s) Missing</source>
        <translation>Jumalanpalveluksen tiedosto(t) puuttuvat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="573"/>
        <source>The following file(s) in the service are missing: {name}

These files will be removed if you continue to save.</source>
        <translation>Seuraavat Listan tiedosto(t) puuttuvat: {name}

Nämä tiedostot poistetaan jos jatkat tallenntamista.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>Error Saving File</source>
        <translation>Virhe tallennettaessa tiedostoa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>There was an error saving your file.

{error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="653"/>
        <source>OpenLP Service Files - lite (*.oszl)</source>
        <translation>OpenLP palvelutiedostot - lite (*.oszl)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="654"/>
        <source>OpenLP Service Files (*.osz)</source>
        <translation>OpenLP ajolistat (*.osz)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="726"/>
        <source>The service file {file_path} could not be loaded because it is either corrupt, inaccessible, or not a valid OpenLP 2 or OpenLP 3 service file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="823"/>
        <source>&amp;Auto Start - active</source>
        <translation>&amp;Automaattinen toisto - päällä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="827"/>
        <source>&amp;Auto Start - inactive</source>
        <translation>&amp;Automaattinen toisto - Ei päällä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Input delay</source>
        <translation>Vaihtoaika</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Delay between slides in seconds.</source>
        <translation>Automaattisen toiston vaihtoaika sekunteina.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1220"/>
        <source>Edit</source>
        <translation>Muokkaa valittua</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1221"/>
        <source>Service copy only</source>
        <translation>Vain ajoilstan kopio</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1224"/>
        <source>Slide theme</source>
        <translation>Dian teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1228"/>
        <source>Notes</source>
        <translation>Muistiinpanot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Missing Display Handler</source>
        <translation>Puuttuva näytön käsittelijä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1399"/>
        <source>Your item cannot be displayed as there is no handler to display it</source>
        <translation>Näyttöelementtiä ei voi näyttää, koska sen näyttämiseen ei ole määritelty näyttöä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Your item cannot be displayed as the plugin required to display it is missing or inactive</source>
        <translation>Kohdetta ei voida näyttää!

Kohteen tarvitsemaa moduulia ei ole asennettu tai se on otettu pois käytöstä.

Voit hallita moduuleja ”Asetukset” valikon kautta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Rename item title</source>
        <translation>&amp;Uudelleennimeä otsikko</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Title:</source>
        <translation>Nimi:</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceNoteForm</name>
    <message>
        <location filename="../../openlp/core/ui/servicenoteform.py" line="72"/>
        <source>Service Item Notes</source>
        <translation>Listan kohteen muistiinpanot</translation>
    </message>
</context>
<context>
    <name>OpenLP.SettingsForm</name>
    <message>
        <location filename="../../openlp/core/ui/settingsdialog.py" line="62"/>
        <source>Configure OpenLP</source>
        <translation>Asetukset</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListDialog</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="138"/>
        <source>Configure Shortcuts</source>
        <translation>Muokkaa Pikanäppäimiä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="139"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>Valitse toiminto ja paina jotain alla olevista painikkeista nauhoittaaksesi uuden pääasiallisen tai vaihtoehtoisen pikanäppäimen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Action</source>
        <translation>Toiminta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Shortcut</source>
        <translation>Pikanäppäin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Alternate</source>
        <translation>Vaihtoehtoinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="145"/>
        <source>Default</source>
        <translation>Oletusarvo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="146"/>
        <source>Custom</source>
        <translation>Mukautettu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="148"/>
        <source>Capture shortcut.</source>
        <translation>Nauhoita pikanäppäin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="151"/>
        <source>Restore the default shortcut of this action.</source>
        <translation>Palauta toiminnon oletusarvoinen pikanäppäin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Restore Default Shortcuts</source>
        <translation>Palauta oletusarvoiset pikanäppäimet</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Do you want to restore all shortcuts to their defaults?</source>
        <translation>Oletko varma, että haluat palauttaa kaikki pikanäppäimet oletusarvoiksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="437"/>
        <source>The shortcut &quot;{key}&quot; is already assigned to another action,
please use a different shortcut.</source>
        <translation>Pikanäppäin &quot;{key}&quot; on jo käytössä,
ole hyvä ja valitse toinen pikanäppäin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="441"/>
        <source>Duplicate Shortcut</source>
        <translation>Painike on jo käytössä</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListForm</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an Action</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>Valitse toiminto ja paina jotain alla olevista painikkeista nauhoittaaksesi uuden pääasiallisen tai vaihtoehtoisen pikanäppäimen.</translation>
    </message>
</context>
<context>
    <name>OpenLP.SlideController</name>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Previous Slide</source>
        <translation>Edellinen dia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Move to previous.</source>
        <translation>Siirry edelliseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Next Slide</source>
        <translation>Seuraava dia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Move to next.</source>
        <translation>Siirry seuraavaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="266"/>
        <source>Hide</source>
        <translation>Piilota</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="268"/>
        <source>Move to preview.</source>
        <translation>Siirry esikatseluun</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="272"/>
        <source>Show Desktop</source>
        <translation>Näytä työpöytä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="277"/>
        <source>Toggle Desktop</source>
        <translation>Näytä / piilota työpöytä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="282"/>
        <source>Toggle Blank to Theme</source>
        <translation>Pimennä teemaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="287"/>
        <source>Toggle Blank Screen</source>
        <translation>Pimennä näyttölaite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="316"/>
        <source>Play Slides</source>
        <translation>Toista diat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="336"/>
        <source>Delay between slides in seconds.</source>
        <translation>Automaattisen toiston vaihtoaika sekunteina.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="340"/>
        <source>Move to live.</source>
        <translation>Lähetä Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="343"/>
        <source>Add to Service.</source>
        <translation>Lisää Listaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="347"/>
        <source>Edit and reload song preview.</source>
        <translation>Muokkaa ja esikatsele.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="351"/>
        <source>Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="358"/>
        <source>Start playing media.</source>
        <translation>Aloita median toistaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="362"/>
        <source>Pause playing media.</source>
        <translation>Pysäytä median toistaminen.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="366"/>
        <source>Stop playing media.</source>
        <translation>Keskeytä median toistaminen. </translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="370"/>
        <source>Loop playing media.</source>
        <translation>Uudelleentoista mediaa automaattisesti.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="377"/>
        <source>Video timer.</source>
        <translation>Videon ajastin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="386"/>
        <source>Video position.</source>
        <translation>Videon kohta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="397"/>
        <source>Audio Volume.</source>
        <translation>Äänenvoimakkuus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="414"/>
        <source>Go To</source>
        <translation>Valitse säe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="442"/>
        <source>Go to &quot;Verse&quot;</source>
        <translation>Siirry &quot;Säkeistöön&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="443"/>
        <source>Go to &quot;Chorus&quot;</source>
        <translation>Siirry &quot;Kertosäkeeseen&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="444"/>
        <source>Go to &quot;Bridge&quot;</source>
        <translation>Siirry &quot;Bridgeen (C-osaan)&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="446"/>
        <source>Go to &quot;Pre-Chorus&quot;</source>
        <translation>Siirry &quot;Esi-kertosäkeeseen&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="447"/>
        <source>Go to &quot;Intro&quot;</source>
        <translation>Siirry &quot;Introon&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="448"/>
        <source>Go to &quot;Ending&quot;</source>
        <translation>Siirry &quot;Lopetukseen&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="449"/>
        <source>Go to &quot;Other&quot;</source>
        <translation>Siirry &quot;Muuhun&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="575"/>
        <source>Previous Service</source>
        <translation>Siirry edelliseen Listan kohteeseen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="580"/>
        <source>Next Service</source>
        <translation>Siirry seuraavaan Listan kohteeseen</translation>
    </message>
</context>
<context>
    <name>OpenLP.SourceSelectForm</name>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="150"/>
        <source>Ignoring current changes and return to OpenLP</source>
        <translation>Ohitetaan nykyiset muutokset ja palataan OpenLP:hen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="153"/>
        <source>Delete all user-defined text and revert to PJLink default text</source>
        <translation>Poista käyttäjän määrittämä teksti ja palauta PJLink:in oletusteksti.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="156"/>
        <source>Discard changes and reset to previous user-defined text</source>
        <translation>Hylkää muutokset ja palaa edeltäviin käyttäjän määrittelemiin teksteihin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="159"/>
        <source>Save changes and return to OpenLP</source>
        <translation>Tallenna muutokset ja palaa OpenLP:hen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="397"/>
        <source>Edit Projector Source Text</source>
        <translation>Muokkaa projektorin lähdön tekstiä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="399"/>
        <source>Select Projector Source</source>
        <translation>Valitse projektorin lähtö</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="481"/>
        <source>Delete entries for this projector</source>
        <translation>Poista tämän projektorin tiedot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="482"/>
        <source>Are you sure you want to delete ALL user-defined source input text for this projector?</source>
        <translation>Oletko varma, että haluat poistaa KAIKKI käyttäjän määrittelemät tekstit tälle projektorille?</translation>
    </message>
</context>
<context>
    <name>OpenLP.SpellTextEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="405"/>
        <source>Language:</source>
        <translation>Kieli:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="416"/>
        <source>Spelling Suggestions</source>
        <translation>Tavutus ehdotuksia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="424"/>
        <source>Formatting Tags</source>
        <translation>Tekstin muotoilu</translation>
    </message>
</context>
<context>
    <name>OpenLP.StartTimeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="70"/>
        <source>Theme Layout</source>
        <translation>Teeman ulkoasu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="71"/>
        <source>The blue box shows the main area.</source>
        <translation>Sininen laatikko on alue, jolla teksti näytetään.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="72"/>
        <source>The red box shows the footer.</source>
        <translation>Punainen laatikko näyttää alatunnisteelle varatun alueen.</translation>
    </message>
</context>
<context>
    <name>OpenLP.StartTime_form</name>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="116"/>
        <source>Item Start and Finish Time</source>
        <translation>Rivin alku- ja loppuaika</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="123"/>
        <source>Hours:</source>
        <translation>Tunnit:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="124"/>
        <source>Minutes:</source>
        <translation>Minuutit:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="125"/>
        <source>Seconds:</source>
        <translation>Sekunnit:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="126"/>
        <source>Start</source>
        <translation>Alku</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="127"/>
        <source>Finish</source>
        <translation>Loppu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="128"/>
        <source>Length</source>
        <translation>Pituus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Time Validation Error</source>
        <translation>Aikamääreessä on virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="72"/>
        <source>Finish time is set after the end of the media item</source>
        <translation>Loppuaika on asetettu median kokonaispituuden yli.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Start time is after the finish time of the media item</source>
        <translation>Alkuaika on asetettu median kokonaispituuden yli.</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="167"/>
        <source>(approximately %d lines per slide)</source>
        <translation>Näytölle mahtuu %d riviä tekstiä tällä koolla ja nykyisillä näyttöasetuksilla.</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeManager</name>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="68"/>
        <source>Create a new theme.</source>
        <translation>Luo uusi teema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit Theme</source>
        <translation>Muokkaa teemaa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit a theme.</source>
        <translation>Muokkaa teemaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete Theme</source>
        <translation>Poista teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete a theme.</source>
        <translation>Poista teema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import Theme</source>
        <translation>Tuo teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import a theme.</source>
        <translation>Tuo teema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export Theme</source>
        <translation>Vie teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export a theme.</source>
        <translation>Vie teema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="108"/>
        <source>&amp;Edit Theme</source>
        <translation>&amp;Muokkaa teemaa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="111"/>
        <source>&amp;Copy Theme</source>
        <translation>&amp;Luo kopio</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="114"/>
        <source>&amp;Rename Theme</source>
        <translation>&amp;Uudelleennimeä teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="117"/>
        <source>&amp;Delete Theme</source>
        <translation>&amp;Poista teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="121"/>
        <source>Set As &amp;Global Default</source>
        <translation>Aseta &amp;Yleiseksi teemaksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="125"/>
        <source>&amp;Export Theme</source>
        <translation>&amp;Vie teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="260"/>
        <source>{text} (default)</source>
        <translation>{text} (Oletus)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>You must select a theme to rename.</source>
        <translation>Sinun pitää valita uudelleennimettävä teema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename Confirmation</source>
        <translation>Nimeämisen vahvistaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename {theme_name} theme?</source>
        <translation>Uudelleennimeä {theme_name} teema?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="310"/>
        <source>Copy of {name}</source>
        <comment>Copy of &lt;theme name&gt;</comment>
        <translation>Kopio {name}:sta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="340"/>
        <source>You must select a theme to edit.</source>
        <translation>Sinun pitää valita muokattava teema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>You must select a theme to delete.</source>
        <translation>Sinun pitää valita poistettava teema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete Confirmation</source>
        <translation>Poistaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete {theme_name} theme?</source>
        <translation>Poista {theme_name} teema?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="394"/>
        <source>You have not selected a theme.</source>
        <translation>Teemaa ei ole valittu.

Ole hyvä ja valitse haluttu
teema teemojen listasta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="398"/>
        <source>Save Theme - ({name})</source>
        <translation>Tallenna teema - ({name})</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>OpenLP Themes (*.otz)</source>
        <translation>OpenLP Teema (*.otz)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Theme Exported</source>
        <translation>Teema viety</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Your theme has been successfully exported.</source>
        <translation>Valitsemasi teeman vienti onnistui.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>Theme Export Failed</source>
        <translation>Teeman vienti epäonnistui</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>The {theme_name} export failed because this error occurred: {err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>Select Theme Import File</source>
        <translation>Valitse tuotava OpenLP:n teematiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="505"/>
        <source>{name} (default)</source>
        <translation>{name} (oletus)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme Already Exists</source>
        <translation>Teema on jo olemassa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme {name} already exists. Do you want to replace it?</source>
        <translation>Teema &quot;{name}&quot; on jo olemassa, haluatko korvata sen?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>Import Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>There was a problem importing {file_name}.

It is corrupt, inaccessible or not a valid theme.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>Validation Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>A theme with this name already exists.</source>
        <translation>Teema tällä nimellä on jo olemassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="773"/>
        <source>You are unable to delete the default theme.</source>
        <translation>Et voi poistaa &quot;Yleiseksi&quot; asetettua teemaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="782"/>
        <source>{count} time(s) by {plugin}</source>
        <translation>{count} kpl seuraavien moduulien toimesta:
{plugin}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Unable to delete theme</source>
        <translation>Teemaa ei voi poistaa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Theme is currently used 

{text}</source>
        <translation>Teemaa käytetään seuraavissa kohteissa:

 {text}</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>Background Image Empty</source>
        <translation>Taustakuva tyhjä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>You have not selected a background image. Please select one before continuing.</source>
        <translation>Taustakuvaa ei ole valittu.

Sinun on valittava kuva ennen
kuin voit jatkaa eteenpäin.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="301"/>
        <source>Edit Theme - {name}</source>
        <translation>Muokkaa Teemaa - {name}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>Theme Name Missing</source>
        <translation>Teeman nimikenttä on tyhjä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>There is no name for this theme. Please enter one.</source>
        <translation>&lt;font size=&quot;4&quot;&gt;Teema tarvitsee nimen, anna teemalle nimi ennen tallentamista. &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Theme Name Invalid</source>
        <translation>Teeman nimi on virheellinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Invalid theme name. Please enter one.</source>
        <translation>Teeman nimi on kelvoton. Ole hyvä ja anna uusi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="128"/>
        <source>Select Image</source>
        <translation>Valitse kuva</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="153"/>
        <source>Select Video</source>
        <translation>Valitse video</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="411"/>
        <source>Theme Wizard</source>
        <translation>Teemojen ohjattu toiminto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="412"/>
        <source>Welcome to the Theme Wizard</source>
        <translation>Uuden teeman luonti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="414"/>
        <source>This wizard will help you to create and edit your themes. Click the next button below to start the process by setting up your background.</source>
        <translation>&lt;font size=&quot;4&quot;&gt;Tämän työkalun avulla voit luoda uuden teeman.&lt;br&gt;
Aloita teeman luonti painamalla &apos;Seuraava&apos;.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="417"/>
        <source>Set Up Background</source>
        <translation>Taustan asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="418"/>
        <source>Set up your theme&apos;s background according to the parameters below.</source>
        <translation>Valitse listasta haluttu tausta. Läpinäkyvä tausta mahdollistaa esimerkiksi 
PowerPoint tai videotaustojen käyttämisen erillisellä ohjelmalla.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="420"/>
        <source>Background type:</source>
        <translation>Valitse haluttu tausta:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="421"/>
        <source>Solid color</source>
        <translation>Yhtenäinen väri</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="422"/>
        <source>Gradient</source>
        <translation>Liukuväri</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="425"/>
        <source>Transparent</source>
        <translation>Läpinäkyvä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="427"/>
        <source>Live Stream</source>
        <translation>Suoratoisto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="468"/>
        <source>color:</source>
        <translation>Väri:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="430"/>
        <source>Starting color:</source>
        <translation>Aloittava väri:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="431"/>
        <source>Ending color:</source>
        <translation>Lopettava väri:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="432"/>
        <source>Gradient:</source>
        <translation>Kuvio:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="433"/>
        <source>Horizontal</source>
        <translation>Vaakasuora</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="435"/>
        <source>Vertical</source>
        <translation>Pystysuora</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="437"/>
        <source>Circular</source>
        <translation>Säteittäinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="439"/>
        <source>Top Left - Bottom Right</source>
        <translation>Ylhäältä vasemmalta - alas oikealle</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="441"/>
        <source>Bottom Left - Top Right</source>
        <translation>Alhaalta vasemmalta - ylhäälle oikealle</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="445"/>
        <source>Background color:</source>
        <translation>Taustan väri:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="447"/>
        <source>Main Area Font Details</source>
        <translation>Fontin asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="448"/>
        <source>Define the font and display characteristics for the Display text</source>
        <translation>Valitse käytettävä fontti ja muotoilut.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="467"/>
        <source>Font:</source>
        <translation>Fontti:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="469"/>
        <source>Size:</source>
        <translation>Koko:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="454"/>
        <source>Line Spacing:</source>
        <translation>Riviväli:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="456"/>
        <source>&amp;Outline:</source>
        <translation>&amp;Ääriviivat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="459"/>
        <source>&amp;Shadow:</source>
        <translation>&amp;Varjostus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="462"/>
        <source>Bold</source>
        <translation>Lihavointi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="463"/>
        <source>Italic</source>
        <translation>Kursiivi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="464"/>
        <source>Footer Area Font Details</source>
        <translation>Alatunnisteen fontti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="465"/>
        <source>Define the font and display characteristics for the Footer text</source>
        <translation>Valitse käytettävä fontti ja muotoilut alatunnisteessa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="471"/>
        <source>Text Formatting Details</source>
        <translation>Tekstin lisäasetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="472"/>
        <source>Allows additional display formatting information to be defined</source>
        <translation>Valitse tekstin tasauksen ja siirtymäefektien asetukset.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="474"/>
        <source>Horizontal Align:</source>
        <translation>Vaakasuora tasaus:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="475"/>
        <source>Left</source>
        <translation>Vasempaan reunaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="476"/>
        <source>Right</source>
        <translation>Oikeaan reunaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="477"/>
        <source>Center</source>
        <translation>Keskitetty</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="478"/>
        <source>Justify</source>
        <translation>Tasaa molemmat reunat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="479"/>
        <source>Transitions:</source>
        <translation>Näytä siirtymäefekti diaa vaihtaessa:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="480"/>
        <source>Fade</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="481"/>
        <source>Slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="482"/>
        <source>Concave</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="483"/>
        <source>Convex</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="484"/>
        <source>Zoom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="485"/>
        <source>Speed:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="486"/>
        <source>Normal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="487"/>
        <source>Fast</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="488"/>
        <source>Slow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="489"/>
        <source>Output Area Locations</source>
        <translation>Näyttöalueen asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="490"/>
        <source>Allows you to change and move the Main and Footer areas.</source>
        <translation>Voit halutessasi muuttaa käytettävissä olevaa näytön 
resoluutiota sekä tekstin sisennystä reunoilta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="492"/>
        <source>&amp;Main Area</source>
        <translation>&amp;Tekstialue</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="493"/>
        <source>&amp;Use default location</source>
        <translation>&amp;Määritä automaattisesti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="503"/>
        <source>X position:</source>
        <translation>Vaakatason sisennys:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="510"/>
        <source>px</source>
        <translation> pikseliä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="505"/>
        <source>Y position:</source>
        <translation>Pystytason sisennys:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="507"/>
        <source>Width:</source>
        <translation>Leveys:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="509"/>
        <source>Height:</source>
        <translation>Korkeus:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="502"/>
        <source>&amp;Footer Area</source>
        <translation>&amp;Alatunniste</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="511"/>
        <source>Use default location</source>
        <translation>Määritä automaattisesti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="513"/>
        <source>Layout Preview</source>
        <translation>Ulkoasun esikatselu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="514"/>
        <source>Preview and Save</source>
        <translation>Teeman yhteenveto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="515"/>
        <source>Preview the theme and save it.</source>
        <translation>Voit nyt tallentaa teeman tai palata muokkaukseen. Alapuolella on malli teeman ulkoasusta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="516"/>
        <source>Theme name:</source>
        <translation>Teeman nimi:</translation>
    </message>
</context>
<context>
    <name>OpenLP.Themes</name>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="74"/>
        <source>Recreating Theme Thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="75"/>
        <source>TextLabel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ThemesTab</name>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="44"/>
        <source>Themes</source>
        <translation>Teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="116"/>
        <source>Global Theme</source>
        <translation>Yleinen teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="117"/>
        <source>Universal Settings</source>
        <translation>Yleiset asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="118"/>
        <source>&amp;Wrap footer text</source>
        <translation>&amp;Rivitä alatunnisteen teksti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="119"/>
        <source>Theme Level</source>
        <translation>Teemojen toiminta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="120"/>
        <source>S&amp;ong Level</source>
        <translation>&amp;Kohdekohtainen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="121"/>
        <source>Use the theme from each song in the database. If a song doesn&apos;t have a theme associated with it, then use the service&apos;s theme. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation>Käytä kohdekohtaisia teemoja. 
Voit valita eri lauluille ja tekstidioille 
omat teemansa. Raamatut käyttävät 
Raamatuille määriteltyä teemaa. 

Jos kohteelle ei ole määritetty 
omaa teemaa, käytetään Listan 
teemaa, jos Listalla ei ole teemaa 
käytetään Yleistä teemaa.

</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="125"/>
        <source>&amp;Service Level</source>
        <translation>&amp;Listan taso</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="126"/>
        <source>Use the theme from the service, overriding any of the individual songs&apos; themes. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation>Käytä Listan teemaa ja korvaa 
sillä kohdekohtaiset teemat. 

Jos Listalla ei ole teemaa, 
käytetään Yleistä teemaa sen sijaan.

</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="130"/>
        <source>&amp;Global Level</source>
        <translation>&amp;Yleinen taso</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="131"/>
        <source>Use the global theme, overriding any themes associated with either the service or the songs.</source>
        <translation>Käytä Yleistä teemaa ja korvaa 
sillä kohdekohtaiset ja Listan teemat.</translation>
    </message>
</context>
<context>
    <name>OpenLP.Ui</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="337"/>
        <source>About</source>
        <translation>Tietoja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="338"/>
        <source>&amp;Add</source>
        <translation>&amp;Lisää</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="339"/>
        <source>Add group</source>
        <translation>Luo ryhmä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="340"/>
        <source>Add group.</source>
        <translation>Lisää ryhmä.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="341"/>
        <source>Advanced</source>
        <translation>Lisäasetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="342"/>
        <source>All Files</source>
        <translation>Kaikki tiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="343"/>
        <source>Automatic</source>
        <translation>Automaattinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="344"/>
        <source>Background Color</source>
        <translation>Taustaväri</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="345"/>
        <source>Background color:</source>
        <translation>Taustan väri:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="346"/>
        <source>Search is Empty or too Short</source>
        <translation>Haku on tyhjä tai liian lyhyt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="347"/>
        <source>&lt;strong&gt;The search you have entered is empty or shorter than 3 characters long.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please try again with a longer search.</source>
        <translation>&lt;strong&gt;Syöttämäsi hakusana on tyhjä tai lyhyempi kuin 3 merkkiä pitkä.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Ole hyvä ja yritä uudestaan pidemmällä hakusanalla.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="350"/>
        <source>No Bibles Available</source>
        <translation>Raamattuja ei ole saatavilla</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="351"/>
        <source>&lt;strong&gt;There are no Bibles currently installed.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please use the Import Wizard to install one or more Bibles.</source>
        <translation>&lt;strong&gt;Yhtään Raamattua ei ole asennettu.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;
Ole hyvä ja tuo Raamattuja &quot;Tiedosto &gt; Tuo&quot; valikon kautta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="353"/>
        <source>Bottom</source>
        <translation>Alareunaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="354"/>
        <source>Browse...</source>
        <translation>Selaa...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="355"/>
        <source>Cancel</source>
        <translation>Peruuta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="356"/>
        <source>CCLI number:</source>
        <translation>CCLI numero:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="357"/>
        <source>CCLI song number:</source>
        <translation>CCLI laulun numero:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="358"/>
        <source>Create a new service.</source>
        <translation>Luo uusi Lista.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="359"/>
        <source>Confirm Delete</source>
        <translation>Vahvista poisto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="360"/>
        <source>Continuous</source>
        <translation>Jatkuva</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="361"/>
        <source>Default</source>
        <translation>Oletusarvo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="362"/>
        <source>Default Color:</source>
        <translation>Taustan väri:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="363"/>
        <source>Service %Y-%m-%d %H-%M</source>
        <comment>This may not contain any of the following characters: /\?*|&lt;&gt;[]&quot;:+
See http://docs.python.org/library/datetime.html#strftime-strptime-behavior for more information.</comment>
        <translation>Lista-%d.%m.%Y luotu %H.%M</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="367"/>
        <source>&amp;Delete</source>
        <translation>&amp;Poista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="368"/>
        <source>Display style:</source>
        <translation>Numeroiden sulut:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="369"/>
        <source>Duplicate Error</source>
        <translation>Päällekkäisyys virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="370"/>
        <source>&amp;Edit</source>
        <translation>&amp;Muokkaa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="371"/>
        <source>Empty Field</source>
        <translation>Tyhjä kenttä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="372"/>
        <source>Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="373"/>
        <source>Export</source>
        <translation>Vienti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="374"/>
        <source>File</source>
        <translation>Tiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="375"/>
        <source>File appears to be corrupt.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="376"/>
        <source>pt</source>
        <comment>Abbreviated font point size unit</comment>
        <translation> pt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="377"/>
        <source>Help</source>
        <translation>Ohjeet</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="378"/>
        <source>h</source>
        <comment>The abbreviated unit for hours</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="379"/>
        <source>Invalid Folder Selected</source>
        <comment>Singular</comment>
        <translation>Valittu tiedostosijainti on virheellinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="380"/>
        <source>Invalid File Selected</source>
        <comment>Singular</comment>
        <translation>Virheellinen tiedosto on valittu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="381"/>
        <source>Invalid Files Selected</source>
        <comment>Plural</comment>
        <translation>Virheelliset tiedostot on valittu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="382"/>
        <source>Image</source>
        <translation>Kuvat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="383"/>
        <source>Import</source>
        <translation>Tuo tiedosta…</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="384"/>
        <source>Layout style:</source>
        <translation>Rivitys:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="385"/>
        <source>Live</source>
        <translation>Esitys</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="386"/>
        <source>Live Stream</source>
        <translation>Suoratoisto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="387"/>
        <source>Live Background Error</source>
        <translation>Esityksen taustan virhe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="388"/>
        <source>Live Toolbar</source>
        <translation>Esityksen-työkalurivi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="389"/>
        <source>Load</source>
        <translation>Lataa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="390"/>
        <source>Manufacturer</source>
        <comment>Singular</comment>
        <translation>Valmistaja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="391"/>
        <source>Manufacturers</source>
        <comment>Plural</comment>
        <translation>Valmistajat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="392"/>
        <source>Model</source>
        <comment>Singular</comment>
        <translation>Malli</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="393"/>
        <source>Models</source>
        <comment>Plural</comment>
        <translation>Mallit</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="394"/>
        <source>m</source>
        <comment>The abbreviated unit for minutes</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="395"/>
        <source>Middle</source>
        <translation>Keskitetty</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="396"/>
        <source>New</source>
        <translation>Uusi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="397"/>
        <source>New Service</source>
        <translation>Uusi Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="398"/>
        <source>New Theme</source>
        <translation>Uusi teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="399"/>
        <source>Next Track</source>
        <translation>Seuraava kappale</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="400"/>
        <source>No Folder Selected</source>
        <comment>Singular</comment>
        <translation>Kansiota ei ole valittu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="401"/>
        <source>No File Selected</source>
        <comment>Singular</comment>
        <translation>Yhtään tiedostoa ei ole valittu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="402"/>
        <source>No Files Selected</source>
        <comment>Plural</comment>
        <translation>Tiedostoja ei ole valittu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="403"/>
        <source>No Item Selected</source>
        <comment>Singular</comment>
        <translation>Tyhjä kenttä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="404"/>
        <source>No Items Selected</source>
        <comment>Plural</comment>
        <translation>Valinta on tyhjä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="405"/>
        <source>No Search Results</source>
        <translation>Ei hakutuloksia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="406"/>
        <source>OpenLP</source>
        <translation>OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="407"/>
        <source>OpenLP 2.0 and up</source>
        <translation>OpenLP 2.0 ja uudemmat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="408"/>
        <source>OpenLP is already running on this machine. 
Closing this instance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="409"/>
        <source>Open service.</source>
        <translation>Avaa Lista.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="410"/>
        <source>Optional, this will be displayed in footer.</source>
        <translation>Valinnainen, tämä näytetään alatunnisteessa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="411"/>
        <source>Optional, this won&apos;t be displayed in footer.</source>
        <translation>Valinnainen, tätä ei näytetä alatunnisteessa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="412"/>
        <source>Play Slides in Loop</source>
        <translation>Toista loputtomasti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="413"/>
        <source>Play Slides to End</source>
        <translation>Toista loppuun</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="414"/>
        <source>Preview</source>
        <translation>Esikatselu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="415"/>
        <source>Preview Toolbar</source>
        <translation>Esikatselun työkalurivi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="416"/>
        <source>Print Service</source>
        <translation>Tulosta Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="417"/>
        <source>Projector</source>
        <comment>Singular</comment>
        <translation>Projektori</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="418"/>
        <source>Projectors</source>
        <comment>Plural</comment>
        <translation>Projektorit</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="419"/>
        <source>Replace Background</source>
        <translation>Korvaa tausta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="420"/>
        <source>Replace live background.</source>
        <translation>Korvaa Esityksen tausta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="421"/>
        <source>Replace live background is not available when the WebKit player is disabled.</source>
        <translation>Taustakuvan korvaaminen ei ole käytettävissä jos WebKit
mediasoitinta ei ole otettu käyttöön asetuksista.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="423"/>
        <source>Reset Background</source>
        <translation>Nollaa tausta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="424"/>
        <source>Reset live background.</source>
        <translation>Palauta Esityksen tausta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="425"/>
        <source>Required, this will be displayed in footer.</source>
        <translation>Pakollinen, tämä näytetään alatunnisteessa.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="426"/>
        <source>s</source>
        <comment>The abbreviated unit for seconds</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="428"/>
        <source>Save &amp;&amp; Preview</source>
        <translation>Tallenna &amp;&amp; Esikatsele</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="429"/>
        <source>Search</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="430"/>
        <source>Search Themes...</source>
        <comment>Search bar place holder text </comment>
        <translation>Hae teeman mukaan...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="431"/>
        <source>You must select an item to delete.</source>
        <translation>Sinun pitää valita poistettava kohta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="432"/>
        <source>You must select an item to edit.</source>
        <translation>Siniun pitää valita muokattava kohta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="433"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="434"/>
        <source>Save Service</source>
        <translation>Listan tallentaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="435"/>
        <source>Service</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="436"/>
        <source>Please type more text to use &apos;Search As You Type&apos;</source>
        <translation>Ole hyvä ja kirjoita enemmän käyttääksesi &apos;Etsi kirjoittaessa&apos; -toimintoa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="437"/>
        <source>Optional &amp;Split</source>
        <translation>&amp;Puolituskohta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="438"/>
        <source>Split a slide into two only if it does not fit on the screen as one slide.</source>
        <translation>Jos teksti ei mahdu yhdelle dialle, voit lisätä haluamasi jakokohdan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="64"/>
        <source>Starting import...</source>
        <translation>Aloitetaan tuontia...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="441"/>
        <source>Stop Play Slides in Loop</source>
        <translation>Keskeytä loputon toisto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="442"/>
        <source>Stop Play Slides to End</source>
        <translation>Keskeytä loppuun asti toistaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="443"/>
        <source>Theme</source>
        <comment>Singular</comment>
        <translation>Teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="444"/>
        <source>Themes</source>
        <comment>Plural</comment>
        <translation>Teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="445"/>
        <source>Tools</source>
        <translation>Työkalut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="446"/>
        <source>Top</source>
        <translation>Yläreunaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="447"/>
        <source>Unsupported File</source>
        <translation>Tiedostomuotoa ei tueta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="448"/>
        <source>Verse Per Slide</source>
        <translation>Jae per dia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="449"/>
        <source>Verse Per Line</source>
        <translation>Jae per rivi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="450"/>
        <source>Version</source>
        <translation>Käännös</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="451"/>
        <source>View</source>
        <translation>Näytä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="452"/>
        <source>View Mode</source>
        <translation>Näyttötila</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="453"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="454"/>
        <source>Web Interface, Download and Install latest Version</source>
        <translation>Selainkäyttöliittymä, lataa ja asenna viimeisin versio</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="455"/>
        <source>Book Chapter</source>
        <translation>Kirja Luku</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="456"/>
        <source>Chapter</source>
        <translation>Luku</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="457"/>
        <source>Verse</source>
        <translation>Säe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="459"/>
        <source>Psalm</source>
        <translation>Psalmi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="460"/>
        <source>Book names may be shortened from full names, for an example Ps 23 = Psalm 23</source>
        <translation>Kirjan nimiä voidaan myös lyhentää, esimerkiksi Ps 23 = Psalmi 23</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="706"/>
        <source>Written by</source>
        <translation>Sanat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="163"/>
        <source>Delete the selected item.</source>
        <translation>Poista valittu kohta.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="166"/>
        <source>Move selection up one position.</source>
        <translation>Siirrä valintaa ylöspäin yhden pykälän.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="169"/>
        <source>Move selection down one position.</source>
        <translation>Siirrä valintaa alaspäin yhden pykälän.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="310"/>
        <source>&amp;Vertical Align:</source>
        <translation>Pystysuuntainen tasaus:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="53"/>
        <source>Finished import.</source>
        <translation>Tuominen valmis.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="54"/>
        <source>Format:</source>
        <translation>Tiedostomuoto:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="56"/>
        <source>Importing</source>
        <translation>Tuominen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="57"/>
        <source>Importing &quot;{source}&quot;...</source>
        <translation>Tuodaan &quot;{source}&quot;...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="58"/>
        <source>Select Import Source</source>
        <translation>Valitse tuonnin lähde</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="59"/>
        <source>Select the import format and the location to import from.</source>
        <translation>Valitse tuotavan tiedoston muoto ja sijainti.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="60"/>
        <source>Open {file_type} File</source>
        <translation>Avaa {file_type} tiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="61"/>
        <source>Open {folder_name} Folder</source>
        <translation>Avaa {folder_name} hakemisto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="62"/>
        <source>%p%</source>
        <translation>%p%</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="63"/>
        <source>Ready.</source>
        <translation>Valmis.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="65"/>
        <source>You need to specify one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>Sinun tulee määritellä ainakin yksi %s tiedosto tuotavaksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="67"/>
        <source>You need to specify at least one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>Sinun pitää määritellä vähintää yksi %s tiedosto tuotavaksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="69"/>
        <source>You need to specify one %s folder to import from.</source>
        <comment>A song format e.g. PowerSong</comment>
        <translation>Sinun on määritettävä vähintään yksi %s kansio, josta haluat tuoda tiedostoja.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="372"/>
        <source>Welcome to the Bible Import Wizard</source>
        <translation>Raamattujen tuonti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="137"/>
        <source>Welcome to the Duplicate Song Removal Wizard</source>
        <translation>Päällekkäisten laulujen haku</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="144"/>
        <source>Welcome to the Song Export Wizard</source>
        <translation>Laulujen vienti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="135"/>
        <source>Welcome to the Song Import Wizard</source>
        <translation>Laulujen tuonti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="33"/>
        <source>Author</source>
        <comment>Singular</comment>
        <translation>Tekijä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="34"/>
        <source>Authors</source>
        <comment>Plural</comment>
        <translation>Tekijät</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="35"/>
        <source>Author Unknown</source>
        <translation>Tekijä tuntematon</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="37"/>
        <source>Songbook</source>
        <comment>Singular</comment>
        <translation>Laulukirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="38"/>
        <source>Songbooks</source>
        <comment>Plural</comment>
        <translation>Laulukirjat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="39"/>
        <source>Title and/or verses not found</source>
        <translation>Otsikko ja / tai jakeet eivät löytyneet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="40"/>
        <source>Song Maintenance</source>
        <translation>Laulujen ylläpito</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="41"/>
        <source>Topic</source>
        <comment>Singular</comment>
        <translation>Aihe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="42"/>
        <source>Topics</source>
        <comment>Plural</comment>
        <translation>Aiheet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="43"/>
        <source>XML syntax error</source>
        <translation>XML kielioppivirhe</translation>
    </message>
</context>
<context>
    <name>OpenLP.core.lib</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="400"/>
        <source>{one} and {two}</source>
        <translation>{one} ja {two}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="402"/>
        <source>{first} and {last}</source>
        <translation>{first} ja {last}
</translation>
    </message>
</context>
<context>
    <name>OpenPL.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="499"/>
        <source>Other</source>
        <translation>Muu</translation>
    </message>
</context>
<context>
    <name>Openlp.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="119"/>
        <source>Source select dialog interface</source>
        <translation>Lähdön valinnan käyttöliittymä</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin</name>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="168"/>
        <source>&lt;strong&gt;Presentation Plugin&lt;/strong&gt;&lt;br /&gt;The presentation plugin provides the ability to show presentations using a number of different programs. The choice of available presentation programs is available to the user in a drop down box.</source>
        <translation>&lt;strong&gt;Presentaatiot&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;

Tämä moduuli mahdollistaa presentaatioiden, kuten 
diaesityksien ja .pdf tiedostojen näyttämisen.&lt;br/&gt;&lt;br/&gt;

Presentaatiot tarvitsevat avukseen sopivat ohjaimet.
Diaesityksiin tarvitaan esimerkiksi joko Power Point
tai Libre Office. &lt;br/&gt;&lt;br/&gt;

Ohjaimia on mahdollista tarkkailla&lt;br/&gt;
”Asetukset” valikon kautta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="181"/>
        <source>Presentation</source>
        <comment>name singular</comment>
        <translation>Presentaatiot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="182"/>
        <source>Presentations</source>
        <comment>name plural</comment>
        <translation>Presentaatiot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="186"/>
        <source>Presentations</source>
        <comment>container title</comment>
        <translation>Presentaatiot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="190"/>
        <source>Load a new presentation.</source>
        <translation>Tuo presentaatio.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="194"/>
        <source>Delete the selected presentation.</source>
        <translation>Poista valittu presentaatio.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="195"/>
        <source>Preview the selected presentation.</source>
        <translation>Esikatsele valittua presentaatiota.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="196"/>
        <source>Send the selected presentation live.</source>
        <translation>Lähetä valittu presentaatio Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="197"/>
        <source>Add the selected presentation to the service.</source>
        <translation>Lisää valittu presentaatio Listaan.</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="62"/>
        <source>Select Presentation(s)</source>
        <translation>Valitse Presentaatio(t)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="63"/>
        <source>Automatic</source>
        <translation>Automaattinen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="64"/>
        <source>Present using:</source>
        <translation>Esitä käyttäen:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="92"/>
        <source>Presentations ({text})</source>
        <translation>Presentaatiot ({text})</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>File Exists</source>
        <translation>Tiedosto on jo olemassa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>A presentation with that filename already exists.</source>
        <translation>Tällä nimellä löytyy jo toinen presentaatio.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="207"/>
        <source>This type of presentation is not supported.</source>
        <translation>Tämän muotoista esitysgrafiikkaa ei tueta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>Missing Presentation</source>
        <translation>Presentaatiota ei löydy</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>The presentation {name} no longer exists.</source>
        <translation>Presentaatiota {name} ei enää ole olemassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="369"/>
        <source>The presentation {name} is incomplete, please reload.</source>
        <translation>Tiedosto: {name}
on vioittunut, ole hyvä ja yritä uudestaan.</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin.PowerpointDocument</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/powerpointcontroller.py" line="536"/>
        <source>An error occurred in the PowerPoint integration and the presentation will be stopped. Restart the presentation if you wish to present it.</source>
        <translation>PowerPointin kanssa kommunikoinnissa tapahtui virhe ja esitys pysäytetään.

Voit halutessasi lähettää tiedoston esitettäväksi uudestaan.</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin.PresentationTab</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="105"/>
        <source>Available Controllers</source>
        <translation>Käytettävissä olevat sovellukset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="111"/>
        <source>PDF options</source>
        <translation>PDF asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="112"/>
        <source>PowerPoint options</source>
        <translation>PowerPoint:in asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="113"/>
        <source>Allow presentation application to be overridden</source>
        <translation>Salli esityssovelluksen valinta Presentaatioille</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="115"/>
        <source>Clicking on the current slide advances to the next effect</source>
        <translation>Toista diaesityksen efektit, kuten animaatiot ja siirtymät klikkaamalla.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="118"/>
        <source>Let PowerPoint control the size and monitor of the presentations
(This may fix PowerPoint scaling issues in Windows 8 and 10)</source>
        <translation>Anna PowerPointin määritellä käytettävät näyttöasetukset
(Voi korjata Windows 8:n ja 10:n liityviä PowePoint ongelmia)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="122"/>
        <source>Use given full path for mudraw or ghostscript binary:</source>
        <translation>Määritä polku mudraw tai ghostscript ohjelmaan.
(OpenLP käyttää oletuksena sisäänrakennettua)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="124"/>
        <source>Select mudraw or ghostscript binary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="131"/>
        <source>{name} (unavailable)</source>
        <translation>{name} (ei saatavilla)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="228"/>
        <source>The program is not ghostscript or mudraw which is required.</source>
        <translation>Ohjelma ei ole ghostscript eikä mudraw, mitä tarvitaan.</translation>
    </message>
</context>
<context>
    <name>RemotePlugin</name>
    <message>
        <location filename="../../openlp/core/api/http/server.py" line="159"/>
        <source>Importing Website</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RemotePlugin.Mobile</name>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="57"/>
        <source>Remote</source>
        <translation>Etähallinta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="58"/>
        <source>Stage View</source>
        <translation>Lavanäyttö</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="59"/>
        <source>Live View</source>
        <translation>Esitysnäkymä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="60"/>
        <source>Chords View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="67"/>
        <source>Service Manager</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="68"/>
        <source>Slide Controller</source>
        <translation>Esitys</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="69"/>
        <source>Alerts</source>
        <translation>Huomioviestit</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="70"/>
        <source>Search</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="71"/>
        <source>Home</source>
        <translation>Koti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="72"/>
        <source>Refresh</source>
        <translation>Päivitä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="73"/>
        <source>Blank</source>
        <translation>Pimennä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="74"/>
        <source>Theme</source>
        <translation>Teema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="75"/>
        <source>Desktop</source>
        <translation>Työpöytä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="76"/>
        <source>Show</source>
        <translation>Näytä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="77"/>
        <source>Prev</source>
        <translation>Edellinen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="78"/>
        <source>Next</source>
        <translation>Seuraava</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="79"/>
        <source>Text</source>
        <translation>Teksti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="80"/>
        <source>Show Alert</source>
        <translation>Näytä huomioviesti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="81"/>
        <source>Go Live</source>
        <translation>Lähetä Esitykseen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="82"/>
        <source>Add to Service</source>
        <translation>Lisää Listaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="83"/>
        <source>Add &amp;amp; Go to Service</source>
        <translation>Lisää &amp;amp;Siirry Listaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="84"/>
        <source>No Results</source>
        <translation>Ei tuloksia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="85"/>
        <source>Options</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="86"/>
        <source>Service</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="87"/>
        <source>Slides</source>
        <translation>Esitys</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="88"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
</context>
<context>
    <name>RemotePlugin.RemoteTab</name>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="158"/>
        <source>Remote Interface</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="159"/>
        <source>Server Settings</source>
        <translation>Palvelimen asetukset</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="160"/>
        <source>Serve on IP address:</source>
        <translation>IP osoite:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="161"/>
        <source>Port number:</source>
        <translation>Portti:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="162"/>
        <source>Remote URL:</source>
        <translation>Etähallinnan osoite:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="163"/>
        <source>Stage view URL:</source>
        <translation>Lavanäytön osoite:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="164"/>
        <source>Live view URL:</source>
        <translation>Esitysnäkymän osoite:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="165"/>
        <source>Chords view URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="166"/>
        <source>Display stage time in 12h format</source>
        <translation>Näytä aika 12-tunnin muodossa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="167"/>
        <source>Show thumbnails of non-text slides in remote and stage view.</source>
        <translation>Näytä pienet kuvat tekstittömistä dioista
etähallinnassa ja lavanäkymässä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="169"/>
        <source>Remote App</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="170"/>
        <source>Scan the QR code or click &lt;a href=&quot;{qr}&quot;&gt;download&lt;/a&gt; to download an app for your mobile device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="174"/>
        <source>User Authentication</source>
        <translation>Käyttäjän todentaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="177"/>
        <source>User id:</source>
        <translation>Käyttäjän id:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="178"/>
        <source>Password:</source>
        <translation>Salasana:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="179"/>
        <source>Current Version number:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="180"/>
        <source>Latest Version number:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongPlugin.ReportSongList</name>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>Save File</source>
        <translation>Tallenna</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>song_extract.csv</source>
        <translation>song_extract.csv</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>CSV format (*.csv)</source>
        <translation>CSV muoto (*.csv)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report Creation</source>
        <translation>Raportin luominen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report 
{name} 
has been successfully created. </source>
        <translation>Tiedosto: {name}
luotiin onnistuneesti.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>Song Extraction Failed</source>
        <translation>Laulun purkaminen epäonnistui</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>An error occurred while extracting: {error}</source>
        <translation>Purkaminen päättyi virhetilanteeseen: {error}</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="90"/>
        <source>&amp;Song Usage Tracking</source>
        <translation>&amp;Laulujen tilastointi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>&amp;Delete Tracking Data</source>
        <translation>&amp;Poista tilastot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>Delete song usage data up to a specified date.</source>
        <translation>Poista laulujen tilastotiedot tiettyyn päivään saakka.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>&amp;Extract Tracking Data</source>
        <translation>Tallenna nimellä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>Generate a report on song usage.</source>
        <translation>Tee raportti laulujen käytöstä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="104"/>
        <source>Toggle Tracking</source>
        <translation>Tilastointi päälle/pois</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="118"/>
        <source>Toggle the tracking of song usage.</source>
        <translation>Laulujen käyttötilastointi päälle / pois.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="154"/>
        <source>Song Usage</source>
        <translation>Laulun käyttötilasto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="178"/>
        <source>Song usage tracking is active.</source>
        <translation>Laulujen käyttötilastointi on päällä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="183"/>
        <source>Song usage tracking is inactive.</source>
        <translation>Laulujen käyttötilastointi ei ole päällä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="193"/>
        <source>display</source>
        <translation>näyttö</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="201"/>
        <source>printed</source>
        <translation>tulostettu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="238"/>
        <source>&lt;strong&gt;SongUsage Plugin&lt;/strong&gt;&lt;br /&gt;This plugin tracks the usage of songs in services.</source>
        <translation>&lt;strong&gt;Laulujen käyttötilastointi&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;

Tämän moduulin avulla voidaan&lt;br/&gt;
tilastoida käytettyjä lauluja.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="249"/>
        <source>SongUsage</source>
        <comment>name singular</comment>
        <translation>Laulujen käyttötilastointi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="250"/>
        <source>SongUsage</source>
        <comment>name plural</comment>
        <translation>Laulujen käyttötilastointi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="254"/>
        <source>SongUsage</source>
        <comment>container title</comment>
        <translation>Laulujen käyttötilastointi</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDeleteForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="64"/>
        <source>Delete Song Usage Data</source>
        <translation>Poista laulujen käyttötilastoinnin tiedot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="66"/>
        <source>Select the date up to which the song usage data should be deleted. 
All data recorded before this date will be permanently deleted.</source>
        <translation>Valitse päivämäärä, mihin saakka laulujen käyttötilastot poistetaan.
Kaikki tallennetut tiedot ennen tätä päivää poistetaan pysyvästi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Delete Selected Song Usage Events?</source>
        <translation>Poistetaanko laulujen käyttötilastoinnin tiedot?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Are you sure you want to delete selected Song Usage data?</source>
        <translation>Oletko varma, että haluat poistaa laulujen käyttötilastoinnin tiedot?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>Deletion Successful</source>
        <translation>Poisto onnistui</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>All requested data has been deleted successfully.</source>
        <translation>Kaikki pyydetyt tiedot on poistettu onnistuneesti.</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDetailForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="85"/>
        <source>Song Usage Extraction</source>
        <translation>Käyttötilaston purkaminen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="87"/>
        <source>Select Date Range</source>
        <translation>Valitse päivämääräväli</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="88"/>
        <source>to</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="89"/>
        <source>Report Location</source>
        <translation>Raportin sijainti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>Output Path Not Selected</source>
        <translation>Kohdetiedostoa ei ole valittu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>You have not set a valid output location for your song usage report.
Please select an existing path on your computer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="91"/>
        <source>usage_detail_{old}_{new}.txt</source>
        <translation>usage_detail_{old}_{new}.txt</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report Creation</source>
        <translation>Raportin luominen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report
{name}
has been successfully created.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>Report Creation Failed</source>
        <translation>Raportin luominen epäonnistui</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>An error occurred while creating the report: {error}</source>
        <translation>Raporttia luodessa tapahtui virhe:  {error}</translation>
    </message>
</context>
<context>
    <name>SongsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="301"/>
        <source>Arabic (CP-1256)</source>
        <translation>Arabialainen (CP-1256)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="302"/>
        <source>Baltic (CP-1257)</source>
        <translation>Balttia (CP-1257)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="303"/>
        <source>Central European (CP-1250)</source>
        <translation>Keski-Eurooppa (CP-1250)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="304"/>
        <source>Cyrillic (CP-1251)</source>
        <translation>Kyrillinen (CP-1251)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="305"/>
        <source>Greek (CP-1253)</source>
        <translation>Kreikka (CP-1253)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="306"/>
        <source>Hebrew (CP-1255)</source>
        <translation>Heprea (CP-1255)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="307"/>
        <source>Japanese (CP-932)</source>
        <translation>Japani (CP-932)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="308"/>
        <source>Korean (CP-949)</source>
        <translation>Korea (CP-949)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="309"/>
        <source>Simplified Chinese (CP-936)</source>
        <translation>Kiina Simplified (CP-936)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="310"/>
        <source>Thai (CP-874)</source>
        <translation>Thai (CP-874)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="311"/>
        <source>Traditional Chinese (CP-950)</source>
        <translation>Kiina Traditional (CP-950)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="312"/>
        <source>Turkish (CP-1254)</source>
        <translation>Turkki (CP-1254)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="313"/>
        <source>Vietnam (CP-1258)</source>
        <translation>Vietnam (CP-1258)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="314"/>
        <source>Western European (CP-1252)</source>
        <translation>Läntinen Eurooppa (CP-1252)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Character Encoding</source>
        <translation>Merkistö</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="323"/>
        <source>The codepage setting is responsible
for the correct character representation.
Usually you are fine with the preselected choice.</source>
        <translation>Koodisivuasetus määrittelee tekstille
oikean merkistöesityksen.
Yleensä esivalittu merkistö on varsin toimiva valinta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Please choose the character encoding.
The encoding is responsible for the correct character representation.</source>
        <translation>Ole hyvä ja valitse merkistön enkoodaus.
Enkoodaus määrittelee tekstille oikean merkistöesityksen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>&amp;Song</source>
        <translation>&amp;Laulut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="186"/>
        <source>Import songs using the import wizard.</source>
        <translation>Tuo lauluja käyttäen tuonnin ohjattua toimintoa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>CCLI SongSelect</source>
        <translation>CCLI SongSelect</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>Import songs from CCLI&apos;s SongSelect service.</source>
        <translation>Tuo laulut CCLI SongSelect Listatiedostosta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>Exports songs using the export wizard.</source>
        <translation>Vie lauluja käyttäen tuonnin ohjattua toimintoa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="223"/>
        <source>Songs</source>
        <translation>Laulut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>&amp;Re-index Songs</source>
        <translation>&amp;Uudelleen-indeksoi laulut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>Re-index the songs database to improve searching and ordering.</source>
        <translation>Laulujen uudelleen-indeksointi parantaa tietokannan nopeutta etsimisessä ja järjestämisessä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find &amp;Duplicate Songs</source>
        <translation>&amp;Päällekkäisten laulujen haku</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find and remove duplicate songs in the song database.</source>
        <translation>Hae ja poista päällekkäisiä lauluja.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Song List Report</source>
        <translation>Laululuettelo raportti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Produce a CSV file of all the songs in the database.</source>
        <translation>Tee CSV tiedosto kaikista tietokannan lauluista.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="259"/>
        <source>Reindexing songs...</source>
        <translation>Laulutietokantaa järjestellään…</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="261"/>
        <source>Reindexing songs</source>
        <translation>Uudelleenindeksoidaan lauluja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="304"/>
        <source>&lt;strong&gt;Songs Plugin&lt;/strong&gt;&lt;br /&gt;The songs plugin provides the ability to display and manage songs.</source>
        <translation>&lt;strong&gt;Laulut&lt;/strong&gt;&lt;br/&gt;&lt;br/&gt;

Tämä moduuli mahdollistaa laulujen&lt;br/&gt; 
näyttämisen ja hallinnoinnin.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="347"/>
        <source>Song</source>
        <comment>name singular</comment>
        <translation>Laulut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="348"/>
        <source>Songs</source>
        <comment>name plural</comment>
        <translation>Laulut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="352"/>
        <source>Songs</source>
        <comment>container title</comment>
        <translation>Laulut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="358"/>
        <source>Add a new song.</source>
        <translation>Lisää uusi laulu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="359"/>
        <source>Edit the selected song.</source>
        <translation>Muokkaa valittua laulua.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="360"/>
        <source>Delete the selected song.</source>
        <translation>Poista valittu laulu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="361"/>
        <source>Preview the selected song.</source>
        <translation>Esikatsele valittua laulua.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="362"/>
        <source>Send the selected song live.</source>
        <translation>Lähetä valittu laulu Esitykseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="363"/>
        <source>Add the selected song to the service.</source>
        <translation>Lisää valittu laulu Listaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="388"/>
        <source>Importing Songs</source>
        <translation>Tuodaan lauluja</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="67"/>
        <source>Words</source>
        <comment>Author who wrote the lyrics of a song</comment>
        <translation>Sanat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="68"/>
        <source>Music</source>
        <comment>Author who wrote the music of a song</comment>
        <translation>Sävellys</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="69"/>
        <source>Words and Music</source>
        <comment>Author who wrote both lyrics and music of a song</comment>
        <translation>Sanat ja Sävellys</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="71"/>
        <source>Translation</source>
        <comment>Author who translated the song</comment>
        <translation>Käännös</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="74"/>
        <source>Author Maintenance</source>
        <translation>Tekijöiden ylläpito</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="75"/>
        <source>Display name:</source>
        <translation>Näytön nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="76"/>
        <source>First name:</source>
        <translation>Etunimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="77"/>
        <source>Last name:</source>
        <translation>Sukunimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="92"/>
        <source>You need to type in the first name of the author.</source>
        <translation>Sinun pitää syöttää tekijän etunimi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="97"/>
        <source>You need to type in the last name of the author.</source>
        <translation>Sinun pitää syöttää tekijän sukunimi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="102"/>
        <source>You have not set a display name for the author, combine the first and last names?</source>
        <translation>Et ole asettanut näyttönimeä tekijälle, yhdistetäänkö etu- ja sukunimet?</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.CCLIFileImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/cclifile.py" line="84"/>
        <source>The file does not have a valid extension.</source>
        <translation>Tiedostolla ei ole kelvollista päätettä.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.DreamBeamImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/dreambeam.py" line="101"/>
        <source>Invalid DreamBeam song file_path. Missing DreamSong tag.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.EasyWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="316"/>
        <source>Administered by {admin}</source>
        <translation>Ylläpitäjä {admin}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="393"/>
        <source>&quot;{title}&quot; could not be imported. {entry}</source>
        <translation>&quot;{title}&quot; ei voitu tuoda. {entry}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="206"/>
        <source>This file does not exist.</source>
        <translation>Tätä tiedostoa ei ole olemassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="210"/>
        <source>Could not find the &quot;Songs.MB&quot; file. It must be in the same folder as the &quot;Songs.DB&quot; file.</source>
        <translation>Tiedostoa &quot;Songs.MB&quot; ei löydetty.
Varmista, että se on tallennettuna samaan kansioon kuin ”Songs.DB” tiedosto.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="225"/>
        <source>This file is not a valid EasyWorship database.</source>
        <translation>Tämä tiedosto ei ole kelvollinen EasyWorship tietokanta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="254"/>
        <source>Could not retrieve encoding.</source>
        <translation>Enkoodattua sisältöä ei voida vastaanottaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="337"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation>&quot;{title}&quot; ei voitu tuoda. {error}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="350"/>
        <source>This does not appear to be a valid Easy Worship 6 database directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="352"/>
        <source>This is not a valid Easy Worship 6 database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="426"/>
        <source>Unexpected data formatting.</source>
        <translation>Odottamaton tiedostomuoto.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="431"/>
        <source>No song text found.</source>
        <translation>Laulun sanoja ei löydy.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="471"/>
        <source>
[above are Song Tags with notes imported from EasyWorship]</source>
        <translation>
[yllä on laulun tagit muistiinpanoineen, jotka on tuotu EasyWorshipistä]</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="134"/>
        <source>Meta Data</source>
        <translation>Tiedot - Kieli</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="155"/>
        <source>Custom Book Names</source>
        <translation>Kirjojen nimet</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditSongForm</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="427"/>
        <source>&amp;Save &amp;&amp; Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="301"/>
        <source>Song Editor</source>
        <translation>Laulun muokkaus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="302"/>
        <source>&amp;Title:</source>
        <translation>&amp;Nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="303"/>
        <source>Alt&amp;ernate title:</source>
        <translation>&amp;Vaihtoehtoinen nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="304"/>
        <source>&amp;Lyrics:</source>
        <translation>&amp;Sanat:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="305"/>
        <source>&amp;Verse order:</source>
        <translation>&amp;Säkeiden järjestys:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="308"/>
        <source>Ed&amp;it All</source>
        <translation>Muokkaa &amp;kaikkia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="310"/>
        <source>Title &amp;&amp; Lyrics</source>
        <translation>Nimi - Sanat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="313"/>
        <source>&amp;Add to Song</source>
        <translation>&amp;Lisää tekijä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="314"/>
        <source>&amp;Edit Author Type</source>
        <translation>&amp;Muokkaa tekijän tyyppiä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="337"/>
        <source>&amp;Remove</source>
        <translation>&amp;Poista</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="316"/>
        <source>&amp;Manage Authors, Topics, Songbooks</source>
        <translation>&amp;Muokkaa Tekijöitä, Aiheita ja Laulukirjoja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="318"/>
        <source>A&amp;dd to Song</source>
        <translation>&amp;Lisää aihe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="319"/>
        <source>R&amp;emove</source>
        <translation>&amp;Poista</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="321"/>
        <source>Add &amp;to Song</source>
        <translation>Lisää &amp;Lauluun</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="322"/>
        <source>Re&amp;move</source>
        <translation>&amp;Poista</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="323"/>
        <source>Authors, Topics &amp;&amp; Songbooks</source>
        <translation>Tekijät - Aiheet - Laulukirjat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="326"/>
        <source>New &amp;Theme</source>
        <translation>Uusi &amp;Teema</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="327"/>
        <source>Copyright Information</source>
        <translation>Tekijäinoikeudet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="330"/>
        <source>Comments</source>
        <translation>Kommentit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="331"/>
        <source>Theme, Copyright Info &amp;&amp; Comments</source>
        <translation>Teema - Tekijäinoikeudet - Kommentit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="333"/>
        <source>Linked Audio</source>
        <translation>Taustamusiikki</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="335"/>
        <source>Add &amp;File(s)</source>
        <translation>Lisää &amp;Tiedosto(t)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="336"/>
        <source>Add &amp;Media</source>
        <translation>Lisää &amp;Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="338"/>
        <source>Remove &amp;All</source>
        <translation>Poista &amp;Kaikki</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="340"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; Not all of the verses are in use.</source>
        <translation>&lt;strong&gt;Varoitus:&lt;/strong&gt; Puuttuvia säkeitä ei voida näyttää jos niitä ei lisätä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="342"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; You have not entered a verse order.</source>
        <translation>&lt;strong&gt;Huom:&lt;/strong&gt; Voit halutessasi muuttaa säkeiden järjestystä kirjaamalla lyhenteet kenttään.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="202"/>
        <source>There are no verses corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation>Vastaavaa säettä &quot;{invalid}&quot; ei lötynyt.
Kelvollisia säkeitä ovat: {valid}.

Käytäthän välilyöntiä säkeiden erottamiseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="207"/>
        <source>There is no verse corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation>Vastaavaa säettä &quot;{invalid}&quot; ei lötynyt.
Kelvollisia säkeitä ovat: {valid}.

Käytäthän välilyöntiä säkeiden erottamiseen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="211"/>
        <source>Invalid Verse Order</source>
        <translation>Säkeiden järjestys on virheellinen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="225"/>
        <source>You need to type in a song title.</source>
        <translation>Laulun ”Nimi” ei voi olla tyhjä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="231"/>
        <source>You need to type in at least one verse.</source>
        <translation>Sinun pitää syöttää ainakin yksi jae.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="237"/>
        <source>You need to have an author for this song.</source>
        <translation>Sinun pitää määritellä tekijä tälle laululle.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="256"/>
        <source>There are misplaced formatting tags in the following verses:

{tag}

Please correct these tags before continuing.</source>
        <translation>Seuraavissa säkeissä on käytetty virheellisiä muotoilutunnuksia:

{tag}

Ole hyvä ja korjaa tilanne muokkaamalla säkeitä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="265"/>
        <source>You have {count} verses named {name} {number}. You can have at most 26 verses with the same name</source>
        <translation>Sinulla on käytössäsi {count} kpl säettä {name} {number}.
Voit käyttää samaa säettä enintään 26 kertaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>Add Author</source>
        <translation>Lisää tekijä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>This author does not exist, do you want to add them?</source>
        <translation>Tätä tekijää ei ole olemassa, haluatko lisätä sen?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="603"/>
        <source>This author is already in the list.</source>
        <translation>Tämä tekijä on jo luettelossa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="610"/>
        <source>You have not selected a valid author. Either select an author from the list, or type in a new author and click the &quot;Add Author to Song&quot; button to add the new author.</source>
        <translation>Sinun pitää valita kelvollinen tekijä. Valitse tekijä joko luettelosta tai kirjoita uusi tekijä ja paina &quot;Lisää tekijä lauluun&quot; -painiketta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Edit Author Type</source>
        <translation>Muokkaa tekijän tyyppiä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Choose type for this author</source>
        <translation>Valitse tyyppi tällä tekijälle</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>Add Topic</source>
        <translation>Lisää aihe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>This topic does not exist, do you want to add it?</source>
        <translation>Tätä aihetta ei ole olemassa, tahdotko sinä lisätä sen?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="679"/>
        <source>This topic is already in the list.</source>
        <translation>Tämä aihe on jo luettelossa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="688"/>
        <source>You have not selected a valid topic. Either select a topic from the list, or type in a new topic and click the &quot;Add Topic to Song&quot; button to add the new topic.</source>
        <translation>&lt;font size=&quot;4&quot;&gt;Aihe ei voi olla tyhjä, kirjoita haluttu aihe tai&lt;br&gt; valitse jo olemassa oleva aihe listasta.&lt;font size=&quot;4&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>Add Songbook</source>
        <translation>Lisää Laulukirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>This Songbook does not exist, do you want to add it?</source>
        <translation>Laulukirjaa ei ole olemassa, haluatko luoda sen?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="724"/>
        <source>This Songbook is already in the list.</source>
        <translation>Laulukirja on jo listassa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="732"/>
        <source>You have not selected a valid Songbook. Either select a Songbook from the list, or type in a new Songbook and click the &quot;Add to Song&quot; button to add the new Songbook.</source>
        <translation>Et ole valinnut kelvollista laulukirjaa.  Valitse laulukirja listasta tai lisää 
uusi laulukirja painamalla ”Lisää lauluun” painiketta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="932"/>
        <source>Open File(s)</source>
        <translation>Avaa tiedosto(t)</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="91"/>
        <source>Edit Verse</source>
        <translation>Sanojen muokkaus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="92"/>
        <source>&amp;Verse type:</source>
        <translation>&amp;Säkeen tyyppi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="102"/>
        <source>&amp;Forced Split</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="103"/>
        <source>Split the verse when displayed regardless of the screen size.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="105"/>
        <source>&amp;Insert</source>
        <translation>&amp;Lisää</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="106"/>
        <source>Split a slide into two by inserting a verse splitter.</source>
        <translation>Jaa dia kahteen osaan lisäämällä jakeen jakaja.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="109"/>
        <source>Transpose:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="110"/>
        <source>Up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="111"/>
        <source>Down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="146"/>
        <source>Transposing failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="240"/>
        <source>Invalid Chord</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.ExportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="128"/>
        <source>Select Destination Folder</source>
        <translation>Valitse tiedostosijainti.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="143"/>
        <source>Song Export Wizard</source>
        <translation>Laulujen viennin ohjattu toiminto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="146"/>
        <source>This wizard will help to export your songs to the open and free &lt;strong&gt;OpenLyrics &lt;/strong&gt; worship song format.</source>
        <translation>&lt;font size=&quot;4&quot;&gt;Tämä toiminto auttaa sinua tallentamaan&lt;br&gt; laulusi &lt;strong&gt;OpenLyrics&lt;/strong&gt; muotoon. &lt;br&gt;&lt;br&gt;Näin OpenLyrics tiedostomuotoa tukevat &lt;br&gt;
sovellukset pystyvät käyttämään laulujasi.&lt;br&gt;&lt;br&gt;
Paina &quot;Seuraava&quot; aloittaaksesi&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="149"/>
        <source>Select Songs</source>
        <translation>Laulujen valinta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="150"/>
        <source>Check the songs you want to export.</source>
        <translation>Valitse laulut, jotka haluat tallentaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="153"/>
        <source>Uncheck All</source>
        <translation>Poista valinnat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="154"/>
        <source>Check All</source>
        <translation>Valitse kaikki</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="155"/>
        <source>Select Directory</source>
        <translation>Valitse tiedostosijainti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="156"/>
        <source>Select the directory where you want the songs to be saved.</source>
        <translation>Valitse tiedostosijainti, jonne haluat tallentaa laulut.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="158"/>
        <source>Directory:</source>
        <translation>Tiedostosijainti:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="159"/>
        <source>Exporting</source>
        <translation>Tallennetaan...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="160"/>
        <source>Please wait while your songs are exported.</source>
        <translation>Lauluja tallennetaan, ole hyvä ja odota hetki.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="176"/>
        <source>You need to add at least one Song to export.</source>
        <translation>Sinun pitää lisätä ainakin yksi laulu vietäväksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>No Save Location specified</source>
        <translation>Sijaintia tallennukselle ei ole määritelty</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>You need to specify a directory.</source>
        <translation>Sinun täytyy määritellä tiedostosijainti.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="239"/>
        <source>Starting export...</source>
        <translation>Aloitetaan vienti...</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.FoilPresenterSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/foilpresenter.py" line="387"/>
        <source>Invalid Foilpresenter song file. No verses found.</source>
        <translation>Virheellinen Foilpresenter -laulutiedosto. Jakeita ei löydy.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="339"/>
        <source>Enable search as you type</source>
        <translation>Etsi jo kirjoitettaessa</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="134"/>
        <source>Song Import Wizard</source>
        <translation>Laulujen tuonti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="137"/>
        <source>This wizard will help you to import songs from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>&lt;font size=&quot;4&quot;&gt;Tämä toiminto auttaa sinua tuomaan ohjelmaan lauluja erilaisista tiedostoista. &lt;br&gt;&lt;br&gt;
Paina ”Seuraava” aloittaaksesi.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="152"/>
        <source>Add Files...</source>
        <translation>Lisää tiedostoja...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="154"/>
        <source>Remove File(s)</source>
        <translation>Poista tiedosto(ja)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="165"/>
        <source>Please wait while your songs are imported.</source>
        <translation>Ole hyvä ja odota, kunnes laulut on tuotu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="169"/>
        <source>Copy</source>
        <translation>Kopioi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="170"/>
        <source>Save to File</source>
        <translation>Tallenna tiedostoksi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="336"/>
        <source>Your Song import failed. {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="197"/>
        <source>This importer has been disabled.</source>
        <translation>Tämä tuontitoiminto on estetty.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="209"/>
        <source>OpenLyrics Files</source>
        <translation>OpenLyrics tiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="210"/>
        <source>OpenLyrics or OpenLP 2 Exported Song</source>
        <translation>OpenLyrics tai OpenLP 2:sta tuotu laulu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="217"/>
        <source>OpenLP 2 Databases</source>
        <translation>OpenLP 2:n tietokannat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="220"/>
        <source>Generic Document/Presentation</source>
        <translation>Geneerinen asiakirja/esitysgrafiikka</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="223"/>
        <source>The generic document/presentation importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation>Geneerinen asiakirjan/esitysgrafiikan tuonti on poissa käytöstä, koska OpenLP ei havaitse asennettua OpenOffice tai LiberOffice -ohjelmistoa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="226"/>
        <source>Select Document/Presentation Files</source>
        <translation>Valitse asiakirja/esitysgrafiikka tiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="232"/>
        <source>CCLI SongSelect Files</source>
        <translation>CCLI SongSelect tiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="239"/>
        <source>ChordPro Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="246"/>
        <source>DreamBeam Song Files</source>
        <translation>DreamBeam laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="253"/>
        <source>EasySlides XML File</source>
        <translation>EasySlides XML-tiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="260"/>
        <source>EasyWorship Song Database</source>
        <translation>EasyWorship tietokanta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="268"/>
        <source>EasyWorship 6 Song Data Directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="276"/>
        <source>EasyWorship Service File</source>
        <translation>EasyWorship Lista</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="283"/>
        <source>Foilpresenter Song Files</source>
        <translation>Foilpresenter -laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="291"/>
        <source>LiveWorship Database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="298"/>
        <source>LyriX Files</source>
        <translation>LyriX Tiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="299"/>
        <source>LyriX (Exported TXT-files)</source>
        <translation>Viety LyriX tiedosto (.TXT)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="306"/>
        <source>MediaShout Database</source>
        <translation>MediaShout tietokanta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="307"/>
        <source>The MediaShout importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation>MediaShout tuonti on tuettu ainoastaan Windowsissa. Se on estetty, koska tarvittavaa Python-moduulia ei ole. Jos tahdot käyttää tätä tuontia, sinun pitää asentaa &quot;pyodbc&quot; moduuli.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="323"/>
        <source>OPS Pro database</source>
        <translation>OPS Pro Tietokanta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="324"/>
        <source>The OPS Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation>OPS Pro tuontia tuetaan ainoastaan Windows käyttöjärjestelmässä.
Se on poistettu käytöstä puuttuvan Python moduulin vuoksi.
Jos haluat käyttää tätä tuontimuotoa, sinun on asennettava Pythonin 
&quot;pyodbc&quot; moduuli.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="333"/>
        <source>PowerPraise Song Files</source>
        <translation>PowerPraise laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="340"/>
        <source>You need to specify a valid PowerSong 1.0 database folder.</source>
        <translation>Annetun tiedostosijainnin pitää sisältää toimiva PowerSong 1.0 tietokanta.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="347"/>
        <source>PresentationManager Song Files</source>
        <translation>PresentationManager laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="354"/>
        <source>ProPresenter Song Files</source>
        <translation>ProPresenterin Laulutiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="361"/>
        <source>Singing The Faith Exported Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="363"/>
        <source>First use Singing The Faith Electonic edition to export the song(s) in Text format.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="371"/>
        <source>SongBeamer Files</source>
        <translation>SongBeamer -laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="379"/>
        <source>SongPro Text Files</source>
        <translation>SongPro tekstitiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="380"/>
        <source>SongPro (Export File)</source>
        <translation>SongPro (vientitiedosto)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="381"/>
        <source>In SongPro, export your songs using the File -&gt; Export menu</source>
        <translation>SongProssa, vie ensin laulut käyttäen File-&gt;Export valikkoa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="388"/>
        <source>SongShow Plus Song Files</source>
        <translation>SongShow Plus -laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="395"/>
        <source>Songs Of Fellowship Song Files</source>
        <translation>Songs Of Fellowship -laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="397"/>
        <source>The Songs of Fellowship importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation>Songs of Fellowship tuonti on pois käytöstä, koska OpenLP ei havaitse OpenOffice tai LibreOffice -ohjelmisto asennettuna.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="405"/>
        <source>SundayPlus Song Files</source>
        <translation>SundayPlus laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="412"/>
        <source>VideoPsalm Files</source>
        <translation>VideoPsalm:in laulutiedosto</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="413"/>
        <source>VideoPsalm</source>
        <translation>VideoPsalm</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="414"/>
        <source>The VideoPsalm songbooks are normally located in {path}</source>
        <translation>VideoPsalmin laulukirjat sijaitsevat yleensä seuraavassa kansiossa:
{path}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="422"/>
        <source>Words Of Worship Song Files</source>
        <translation>Words Of Worship -laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="430"/>
        <source>Worship Assistant Files</source>
        <translation>Worship Assistant tiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="432"/>
        <source>Worship Assistant (CSV)</source>
        <translation>Worship Assistant (CSV)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="433"/>
        <source>In Worship Assistant, export your Database to a CSV file.</source>
        <translation>Worship Assistantista tuo tietokantasi CSV tiedostona.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="441"/>
        <source>WorshipCenter Pro Song Files</source>
        <translation>WorshipCenter Pro laulutiedostot</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="443"/>
        <source>The WorshipCenter Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation>WorshipCenter Pro tuonti on tuettu ainoastaan Windowsissa. Se on estetty, koska tarvittavaa Python-moduulia ei ole. Jos tahdot käyttää tätä tuontia, sinun pitää asentaa &quot;pyodbc&quot; moduuli.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="453"/>
        <source>ZionWorx (CSV)</source>
        <translation>ZionWorx (CSV)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="454"/>
        <source>First convert your ZionWorx database to a CSV text file, as explained in the &lt;a href=&quot;http://manual.openlp.org/songs.html#importing-from-zionworx&quot;&gt;User Manual&lt;/a&gt;.</source>
        <translation>Ensiksi muunna ZionWorx tietokanta CSV-tekstiksi niin kuin on selitetty &lt;a href=&quot;http://manual.openlp.org/songs.html#importing-from-zionworx&quot;&gt;Käyttöohjeessa&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.LiveWorshipImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="87"/>
        <source>Extracting data from database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="133"/>
        <source>Could not find Valentina DB ADK libraries </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="161"/>
        <source>Loading the extracting data</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.LyrixImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>File {name}</source>
        <translation>Tiedosto {nimi}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>Error: {error}</source>
        <translation>Virhe: {error}</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaFilesForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="65"/>
        <source>Select Media File(s)</source>
        <translation>Valitse mediatiedosto(t)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="66"/>
        <source>Select one or more audio files from the list below, and click OK to import them into this song.</source>
        <translation>Valitse yksi tai useampia äänitiedosto alla olevasta luettelosta ja paina OK liittääksesi ne tähän lauluun.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="721"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Titles</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Titles...</source>
        <translation>Hae nimellä...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="140"/>
        <source>Maintain the lists of authors, topics and books.</source>
        <translation>Ylläpidä luetteloa tekijöistä, kappaleista ja laulukirjoista.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Entire Song</source>
        <translation>Nimi tai sanat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Entire Song...</source>
        <translation>Hae nimellä tai sanoilla...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Lyrics</source>
        <translation>Sanat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Lyrics...</source>
        <translation>Hae sanoilla...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Authors...</source>
        <translation>Hae tekijöitä...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Topics...</source>
        <translation>Hae Aiheita...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Songbooks...</source>
        <translation>Hae Laulukirjan mukaan...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Copyright</source>
        <translation>Tekijäinoikeudet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Copyright...</source>
        <translation>Hae Tekijänoikeuksia...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>CCLI number</source>
        <translation>CCLI numero</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search CCLI number...</source>
        <translation>Hae CCLI numerolla...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="512"/>
        <source>Are you sure you want to delete the following songs?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="540"/>
        <source>copy</source>
        <comment>For song cloning</comment>
        <translation>kopioi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="642"/>
        <source>Media</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="699"/>
        <source>CCLI License: </source>
        <translation>CCLI Lisenssi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="729"/>
        <source>Failed to render Song footer html.
See log for details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaShoutImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/mediashout.py" line="62"/>
        <source>Unable to open the MediaShout database.</source>
        <translation>Ei voi avata MediaShout tietokantaa.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.OPSProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="65"/>
        <source>Unable to connect the OPS Pro database.</source>
        <translation>Ei voitu yhdistää OPS Pro tiedostokantaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="87"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation>&quot;{title}&quot; ei voitu tuoda. {error}</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLPSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openlp.py" line="109"/>
        <source>Not a valid OpenLP 2 song database.</source>
        <translation>Valittu tiedosto ei ole yhteensopiva OpenLP 2:n tietokanta</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLyricsExport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsexport.py" line="68"/>
        <source>Exporting &quot;{title}&quot;...</source>
        <translation>Viedään &quot;{title}&quot;...</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opensong.py" line="139"/>
        <source>Invalid OpenSong song file. Missing song tag.</source>
        <translation>Virheellinen OpenSong laulutiedosto. Laulun tunniste puuttuu.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.PowerSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No songs to import.</source>
        <translation>Ei lauluja tuotavaksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No {text} files found.</source>
        <translation>Tiedostoja {text} ei löytynyt.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="118"/>
        <source>Invalid {text} file. Unexpected byte value.</source>
        <translation>Tiedosto on virheellinen:
{text}

Odottamaton tavun arvo.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="136"/>
        <source>Invalid {text} file. Missing &quot;TITLE&quot; header.</source>
        <translation>Tiedosto on virheellinen: 
{text}

Puuttuva &quot;TITLE&quot; header.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="142"/>
        <source>Invalid {text} file. Missing &quot;COPYRIGHTLINE&quot; header.</source>
        <translation>Tiedosto on virheellinen: 
{text}

Puuttuva &quot;COPYRIGHTLINE&quot; header.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="148"/>
        <source>Verses not found. Missing &quot;PART&quot; header.</source>
        <translation>Jakeita ei löytynyt. Puuttuva &quot;PART&quot; tunniste.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.PresentationManagerImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/presentationmanager.py" line="57"/>
        <source>File is not in XML-format, which is the only format supported.</source>
        <translation>Tiedosto ei ole XML-muodossa, muita tiedostomuotoja ei tueta.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SingingTheFaithImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="192"/>
        <source>Unknown hint {hint}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>File {file}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>Error: {error}</source>
        <translation>Virhe: {error}</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongBookForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="66"/>
        <source>Songbook Maintenance</source>
        <translation>Laulukirjan ylläpito</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="67"/>
        <source>&amp;Name:</source>
        <translation>&amp;Nimi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="68"/>
        <source>&amp;Publisher:</source>
        <translation>&amp;Kustantaja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookform.py" line="61"/>
        <source>You need to type in a name for the book.</source>
        <translation>Sinun pitää antaa kirjalle nimi.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongExportForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="253"/>
        <source>Finished export. To import these files use the &lt;strong&gt;OpenLyrics&lt;/strong&gt; importer.</source>
        <translation>Valmista! Voit avata tiedoston toisessa asennuksessa &lt;strong&gt;OpenLyrics&lt;/strong&gt; tuonnin avulla.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="257"/>
        <source>Your song export failed.</source>
        <translation>Laulun vienti epäonnistui.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="259"/>
        <source>Your song export failed because this error occurred: {error}</source>
        <translation>Laulujen vienti epäonnistui.
Virhe: {error}</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="67"/>
        <source>Cannot access OpenOffice or LibreOffice</source>
        <translation>OpenOffice tai LibreOffice -ohjelmistoa ei löydy.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="82"/>
        <source>Unable to open file</source>
        <translation>Ei voida avata tiedostoa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="84"/>
        <source>File not found</source>
        <translation>Tiedostoa ei löydy</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="104"/>
        <source>copyright</source>
        <translation>tekijäinoikeudet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="118"/>
        <source>The following songs could not be imported:</source>
        <translation>Seuraavia lauluja ei voi tuoda:</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongMaintenanceForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="248"/>
        <source>Could not add your author.</source>
        <translation>Tekijää ei voida lisätä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="251"/>
        <source>This author already exists.</source>
        <translation>Tämä tekijä on jo olemassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="264"/>
        <source>Could not add your topic.</source>
        <translation>Aihetta ei voida lisätä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="267"/>
        <source>This topic already exists.</source>
        <translation>Aihe on jo olemassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="281"/>
        <source>Could not add your book.</source>
        <translation>Kirjaa ei voida lisätä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="284"/>
        <source>This book already exists.</source>
        <translation>Tämä kirja on jo olemassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="389"/>
        <source>Could not save your changes.</source>
        <translation>Muutoksia ei voida tallentaa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="316"/>
        <source>The author {original} already exists. Would you like to make songs with author {new} use the existing author {original}?</source>
        <translation>Tekijä {original} on jo olemassa.
Haluatko käyttää {original} tekijää
uuden tekijän sijaan niissä lauluissa 
joissa uusi tekijä {new} esiintyy?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="329"/>
        <source>Could not save your modified author, because the author already exists.</source>
        <translation>Ei voida tallentaa muokattua tekijää, koska tekijä on jo olemassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="352"/>
        <source>The topic {original} already exists. Would you like to make songs with topic {new} use the existing topic {original}?</source>
        <translation>Aihe {original} on jo olemassa.
Haluatko käyttää {original} aihetta
uuden aiheen sijaan niissä lauluissa 
joissa uusi aihe {new} esiintyy?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="362"/>
        <source>Could not save your modified topic, because it already exists.</source>
        <translation>Ei voida tallentaa muokattua aihetta, koska se on jo olemassa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="391"/>
        <source>The book {original} already exists. Would you like to make songs with book {new} use the existing book {original}?</source>
        <translation>Laulukirja {original} on jo olemassa.
Haluatko käyttää {original} kirjaa
uuden kirjan sijaan niissä lauluissa 
joissa uusi kirja {new} esiintyy?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Delete Author</source>
        <translation>Poista tekijä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Are you sure you want to delete the selected author?</source>
        <translation>Oletko varma, että haluat poistaa valitun tekijän?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>This author cannot be deleted, they are currently assigned to at least one song.</source>
        <translation>Tätä tekijää ei voida poistaa, koska sitä käytetään ainakin yhdessä laulussa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Delete Topic</source>
        <translation>Poista aihe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Are you sure you want to delete the selected topic?</source>
        <translation>Oletko varma, että haluat poistaa valitun aiheen?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>This topic cannot be deleted, it is currently assigned to at least one song.</source>
        <translation>Tätä aihetta ei voi poistaa, koska se on käytössä ainakin yhdessä laulussa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Delete Book</source>
        <translation>Poista kirja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Are you sure you want to delete the selected book?</source>
        <translation>Haluatko varmasti poistaa valitun kirjan?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>This book cannot be deleted, it is currently assigned to at least one song.</source>
        <translation>Tätä kirjaa ei voi poistaa, koska sitä käytetään ainakin yhdessä laulussa.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongSelectForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="233"/>
        <source>CCLI SongSelect Importer</source>
        <translation>CCLI SongSelect tuonti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="234"/>
        <source>&lt;strong&gt;Note:&lt;/strong&gt; An Internet connection is required in order to import songs from CCLI SongSelect.</source>
        <translation>&lt;strong&gt;Huomaa:&lt;/strong&gt; Internet yhteys vaaditaan, jotta voidaan tuoda lauluja CCLI SongSelectistä.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="238"/>
        <source>Username:</source>
        <translation>Käyttäjätunnus:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="239"/>
        <source>Password:</source>
        <translation>Salasana:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="240"/>
        <source>Save username and password</source>
        <translation>Tallenna käyttäjätunnus ja salasana</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="241"/>
        <source>Login</source>
        <translation>Kirjautuminen</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="242"/>
        <source>Search Text:</source>
        <translation>Hae tekstiä:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="243"/>
        <source>Search</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="244"/>
        <source>Stop</source>
        <translation>Lopeta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="346"/>
        <source>Found {count:d} song(s)</source>
        <translation>{count:d} laulu(a) löydettiin</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="247"/>
        <source>Logout</source>
        <translation>Kirjaudu ulos</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="248"/>
        <source>View</source>
        <translation>Näytä</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="249"/>
        <source>Title:</source>
        <translation>Nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="250"/>
        <source>Author(s):</source>
        <translation>Tekijä(t):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="251"/>
        <source>Copyright:</source>
        <translation>Tekijäinoikeudet:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="252"/>
        <source>CCLI Number:</source>
        <translation>CCLI numero:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="253"/>
        <source>Lyrics:</source>
        <translation>Sanoitus:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="254"/>
        <source>Back</source>
        <translation>Takaisin</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="255"/>
        <source>Import</source>
        <translation>Tuo tiedosta…</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>More than 1000 results</source>
        <translation>Yli 1000 hakuosumaa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>Your search has returned more than 1000 results, it has been stopped. Please refine your search to fetch better results.</source>
        <translation>Haku palautti enemmän kuin 1000 osumaa, joten se pysäytettiin. Ole hyvä ja tarkenna hakua saadaksesi tarkemmat tulokset.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="139"/>
        <source>Logging out...</source>
        <translation>Kirjaudutaan ulos...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>Incomplete song</source>
        <translation>Keskeneräinen laulu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>This song is missing some information, like the lyrics, and cannot be imported.</source>
        <translation>Tästä laulusta puuttuu osa tiedoista, kuten sanoitus ja sitä ei voi täten tuoda.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>Save Username and Password</source>
        <translation>Tallenna käyttäjänimi ja salasana</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>WARNING: Saving your username and password is INSECURE, your password is stored in PLAIN TEXT. Click Yes to save your password or No to cancel this.</source>
        <translation>VAROITUS: Käyttäjätunnuksen ja salasanan tallentaminen on TURVATONTA, salasana tallennetaan tekstinä. Klikkaa Kyllä tallentaaksesi salasana ja Ei peruaksesi sen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>Error Logging In</source>
        <translation>Virhe kirjautuessa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>There was a problem logging in, perhaps your username or password is incorrect?</source>
        <translation>Kirjautumisessa oli ongelmia, ehkä käyttäjätunnus tai salasana on virheellinen?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>Free user</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>You logged in with a free account, the search will be limited to songs in the public domain.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Song Imported</source>
        <translation>Laulu on tuotu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Your song has been imported, would you like to import more songs?</source>
        <translation>Laulusi on tuotu, haluaisitko tuoda lisää lauluja?</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongsTab</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="116"/>
        <source>Song related settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="117"/>
        <source>Enable &quot;Go to verse&quot; button in Live panel</source>
        <translation> Näytä &quot;Valitse säe&quot;  Esityksen paneelissa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="119"/>
        <source>Update service from song edit</source>
        <translation>Päivitä Lista laulun muokkauksesta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="120"/>
        <source>Import missing songs from Service files</source>
        <translation>Tuo puuttuvat laulut Listatiedostoista</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="122"/>
        <source>Add Songbooks as first slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="124"/>
        <source>If enabled all text between &quot;[&quot; and &quot;]&quot; will be regarded as chords.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="126"/>
        <source>Chords</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="127"/>
        <source>Display chords in the main view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="128"/>
        <source>Ignore chords when importing songs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="130"/>
        <source>Chord notation to use:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="131"/>
        <source>English</source>
        <translation>Suomi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="132"/>
        <source>German</source>
        <translation>Saksa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="133"/>
        <source>Neo-Latin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="135"/>
        <source>Footer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="140"/>
        <source>Song Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="141"/>
        <source>Alternate Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="142"/>
        <source>Written By</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="143"/>
        <source>Authors when type is not set</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="145"/>
        <source>Authors (Type &quot;Words&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="147"/>
        <source>Authors (Type &quot;Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="149"/>
        <source>Authors (Type &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="151"/>
        <source>Authors (Type &quot;Translation&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="152"/>
        <source>Authors (Type &quot;Words&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="154"/>
        <source>Authors (Type &quot;Music&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="156"/>
        <source>Copyright information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="157"/>
        <source>Songbook Entries</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="159"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="160"/>
        <source>Song CCLI Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="161"/>
        <source>Topics</source>
        <translation>Aiheet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Placeholder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Description</source>
        <translation>Efekti</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="172"/>
        <source>can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="173"/>
        <source>list of entries, can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="177"/>
        <source>How to use Footers:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Footer Template</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Mako Syntax</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="181"/>
        <source>Reset Template</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.TopicsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="60"/>
        <source>Topic Maintenance</source>
        <translation>Aiheiden ylläpito</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="61"/>
        <source>Topic name:</source>
        <translation>Aiheen nimi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsform.py" line="58"/>
        <source>You need to type in a topic name.</source>
        <translation>Sinun pitää syöttää nimi aiheelle.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.VerseType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="149"/>
        <source>Verse</source>
        <translation>Säe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="150"/>
        <source>Chorus</source>
        <translation>Kertosäe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="151"/>
        <source>Bridge</source>
        <translation>Bridge (C-osa)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="152"/>
        <source>Pre-Chorus</source>
        <translation>Esi-kertosäe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="153"/>
        <source>Intro</source>
        <translation>Intro</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="154"/>
        <source>Ending</source>
        <translation>Lopetus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="155"/>
        <source>Other</source>
        <translation>Muu</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.VideoPsalmImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/videopsalm.py" line="134"/>
        <source>Error: {error}</source>
        <translation>Virhe: {error}</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.WordsofWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/wordsofworship.py" line="177"/>
        <source>Invalid Words of Worship song file. Missing {text!r} header.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipAssistantImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Error reading CSV file.</source>
        <translation>Virhe luettaessa CSV-tiedostoa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Line {number:d}: {error}</source>
        <translation>Rivi {number:d}: {error}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="182"/>
        <source>Record {count:d}</source>
        <translation>Löytyi {count:d} kappaletta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="122"/>
        <source>Decoding error: {error}</source>
        <translation>Virhe tiedoston lukemisessa: {error} 
(decoding error)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="127"/>
        <source>File not valid WorshipAssistant CSV format.</source>
        <translation>Tiedosto ei ole kelvollista WorshipAssistant CSV muotoa.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipCenterProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipcenterpro.py" line="59"/>
        <source>Unable to connect the WorshipCenter Pro database.</source>
        <translation>Yhdistäminen WorshipCenter Pro tietokantaan ei onnistu.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.ZionWorxImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Error reading CSV file.</source>
        <translation>Virhe luettaessa CSV-tiedostoa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Line {number:d}: {error}</source>
        <translation>Rivi {number:d}: {virhe}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Record {index}</source>
        <translation>Tietue {index}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Decoding error: {error}</source>
        <translation>Virhe tiedoston lukemisessa: {error} 
(decoding error)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="106"/>
        <source>File not valid ZionWorx CSV format.</source>
        <translation>Tiedosto ei ole kelvollista  ZionWorx CSV -muotoa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="120"/>
        <source>Record %d</source>
        <translation>Tietue %d</translation>
    </message>
</context>
<context>
    <name>Wizard</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="136"/>
        <source>Wizard</source>
        <translation>Avustaja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="140"/>
        <source>This wizard will help you to remove duplicate songs from the song database. You will have a chance to review every potential duplicate song before it is deleted. So no songs will be deleted without your explicit approval.</source>
        <translation>Tämä avustaja auttaa päällekkäisten laulujen löytämisessä. 

Avustaja hakee päällekkäisiä lauluja, voit itse valita poistatko ne vai et.
Avustaja ei siis poista lauluja ilman hyväksyntääsi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="145"/>
        <source>Searching for duplicate songs.</source>
        <translation>Etsitään päällekkäisiä lauluja...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="146"/>
        <source>Please wait while your songs database is analyzed.</source>
        <translation>Ole hyvä ja odota, kunnes laulutietokanta on analysoitu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="148"/>
        <source>Here you can decide which songs to remove and which ones to keep.</source>
        <translation>Täällä voit päättää, mitkä laulut poistetaan ja mitkä säilytetään.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="155"/>
        <source>Review duplicate songs ({current}/{total})</source>
        <translation>Päällekkäisten laulujen tarkistus ({current}/{total})</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>Information</source>
        <translation>Tietoa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>No duplicate songs have been found in the database.</source>
        <translation>Päällekkäisiä lauluja ei löytynyt.</translation>
    </message>
</context>
<context>
    <name>common.languages</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>(Afan) Oromo</source>
        <comment>Language code: om</comment>
        <translation>oromo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Abkhazian</source>
        <comment>Language code: ab</comment>
        <translation>abhaasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afar</source>
        <comment>Language code: aa</comment>
        <translation>afar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afrikaans</source>
        <comment>Language code: af</comment>
        <translation>afrikaans</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Albanian</source>
        <comment>Language code: sq</comment>
        <translation>Albania</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amharic</source>
        <comment>Language code: am</comment>
        <translation>amhara</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amuzgo</source>
        <comment>Language code: amu</comment>
        <translation>amuzgo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ancient Greek</source>
        <comment>Language code: grc</comment>
        <translation>muinaiskreikka</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Arabic</source>
        <comment>Language code: ar</comment>
        <translation>arabia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Armenian</source>
        <comment>Language code: hy</comment>
        <translation>armenia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Assamese</source>
        <comment>Language code: as</comment>
        <translation>assami</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Aymara</source>
        <comment>Language code: ay</comment>
        <translation>aimara</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Azerbaijani</source>
        <comment>Language code: az</comment>
        <translation>azerbaidžani</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bashkir</source>
        <comment>Language code: ba</comment>
        <translation>baškiiri</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Basque</source>
        <comment>Language code: eu</comment>
        <translation>baski</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bengali</source>
        <comment>Language code: bn</comment>
        <translation>bengali</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bhutani</source>
        <comment>Language code: dz</comment>
        <translation>bhutan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bihari</source>
        <comment>Language code: bh</comment>
        <translation>bihari</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bislama</source>
        <comment>Language code: bi</comment>
        <translation>bislama</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Breton</source>
        <comment>Language code: br</comment>
        <translation>bretoni</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bulgarian</source>
        <comment>Language code: bg</comment>
        <translation>bulgaria</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Burmese</source>
        <comment>Language code: my</comment>
        <translation>burma</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Byelorussian</source>
        <comment>Language code: be</comment>
        <translation>valkovenäjä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cakchiquel</source>
        <comment>Language code: cak</comment>
        <translation>caksiquel</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cambodian</source>
        <comment>Language code: km</comment>
        <translation>kambodza</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Catalan</source>
        <comment>Language code: ca</comment>
        <translation>katalaani</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Chinese</source>
        <comment>Language code: zh</comment>
        <translation>kiina</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Comaltepec Chinantec</source>
        <comment>Language code: cco</comment>
        <translation>Comaltepec Chinantec</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Corsican</source>
        <comment>Language code: co</comment>
        <translation>korsika</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Croatian</source>
        <comment>Language code: hr</comment>
        <translation>kroatia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Czech</source>
        <comment>Language code: cs</comment>
        <translation> tšekki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Danish</source>
        <comment>Language code: da</comment>
        <translation>tanska</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Dutch</source>
        <comment>Language code: nl</comment>
        <translation>hollanti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>English</source>
        <comment>Language code: en</comment>
        <translation>Suomi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Esperanto</source>
        <comment>Language code: eo</comment>
        <translation>esperanto</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Estonian</source>
        <comment>Language code: et</comment>
        <translation>viro</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Faeroese</source>
        <comment>Language code: fo</comment>
        <translation>fääri</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Fiji</source>
        <comment>Language code: fj</comment>
        <translation> fidži</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Finnish</source>
        <comment>Language code: fi</comment>
        <translation>suomi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>French</source>
        <comment>Language code: fr</comment>
        <translation>ranska</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Frisian</source>
        <comment>Language code: fy</comment>
        <translation>friisi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Galician</source>
        <comment>Language code: gl</comment>
        <translation>gaeli</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Georgian</source>
        <comment>Language code: ka</comment>
        <translation>georgia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>German</source>
        <comment>Language code: de</comment>
        <translation>Saksa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greek</source>
        <comment>Language code: el</comment>
        <translation>kreikka</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greenlandic</source>
        <comment>Language code: kl</comment>
        <translation>grönlanti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Guarani</source>
        <comment>Language code: gn</comment>
        <translation>guarani</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Gujarati</source>
        <comment>Language code: gu</comment>
        <translation>gudžarati</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Haitian Creole</source>
        <comment>Language code: ht</comment>
        <translation>haiti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hausa</source>
        <comment>Language code: ha</comment>
        <translation>hausa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hebrew (former iw)</source>
        <comment>Language code: he</comment>
        <translation>heprea</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hiligaynon</source>
        <comment>Language code: hil</comment>
        <translation>hiligaino</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hindi</source>
        <comment>Language code: hi</comment>
        <translation>hindi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hungarian</source>
        <comment>Language code: hu</comment>
        <translation>Unkari</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Icelandic</source>
        <comment>Language code: is</comment>
        <translation>islanti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Indonesian (former in)</source>
        <comment>Language code: id</comment>
        <translation>indonesia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingua</source>
        <comment>Language code: ia</comment>
        <translation>Interlingua</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingue</source>
        <comment>Language code: ie</comment>
        <translation>Interlingue</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inuktitut (Eskimo)</source>
        <comment>Language code: iu</comment>
        <translation>inuktitut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inupiak</source>
        <comment>Language code: ik</comment>
        <translation>inupiatun</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Irish</source>
        <comment>Language code: ga</comment>
        <translation>irlanti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Italian</source>
        <comment>Language code: it</comment>
        <translation>italia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Jakalteko</source>
        <comment>Language code: jac</comment>
        <translation>jakalteko</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Japanese</source>
        <comment>Language code: ja</comment>
        <translation>japani</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Javanese</source>
        <comment>Language code: jw</comment>
        <translation>jaava</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>K&apos;iche&apos;</source>
        <comment>Language code: quc</comment>
        <translation>k&apos;iche&apos;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kannada</source>
        <comment>Language code: kn</comment>
        <translation>kannada</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kashmiri</source>
        <comment>Language code: ks</comment>
        <translation> kašmiri</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kazakh</source>
        <comment>Language code: kk</comment>
        <translation>kazakki</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kekchí </source>
        <comment>Language code: kek</comment>
        <translation>Kekchí</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kinyarwanda</source>
        <comment>Language code: rw</comment>
        <translation>ruanda</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirghiz</source>
        <comment>Language code: ky</comment>
        <translation>Kirgiisi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirundi</source>
        <comment>Language code: rn</comment>
        <translation>Kirundi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Korean</source>
        <comment>Language code: ko</comment>
        <translation>Korea</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kurdish</source>
        <comment>Language code: ku</comment>
        <translation>Kurdi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Laothian</source>
        <comment>Language code: lo</comment>
        <translation>Lao</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latin</source>
        <comment>Language code: la</comment>
        <translation>Latina</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latvian, Lettish</source>
        <comment>Language code: lv</comment>
        <translation>latvia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lingala</source>
        <comment>Language code: ln</comment>
        <translation>Lingala</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lithuanian</source>
        <comment>Language code: lt</comment>
        <translation>Liettua</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Macedonian</source>
        <comment>Language code: mk</comment>
        <translation>makedonia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malagasy</source>
        <comment>Language code: mg</comment>
        <translation>Madagaskarin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malay</source>
        <comment>Language code: ms</comment>
        <translation>Malaiji</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malayalam</source>
        <comment>Language code: ml</comment>
        <translation>Malajalam</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maltese</source>
        <comment>Language code: mt</comment>
        <translation>malta</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mam</source>
        <comment>Language code: mam</comment>
        <translation>Mam</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mi</comment>
        <translation>maori</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mri</comment>
        <translation>maori</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Marathi</source>
        <comment>Language code: mr</comment>
        <translation>Marathi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Moldavian</source>
        <comment>Language code: mo</comment>
        <translation>moldova</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mongolian</source>
        <comment>Language code: mn</comment>
        <translation>mongolia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nahuatl</source>
        <comment>Language code: nah</comment>
        <translation>Nahuatl</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nauru</source>
        <comment>Language code: na</comment>
        <translation>Nauru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nepali</source>
        <comment>Language code: ne</comment>
        <translation>nepal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Norwegian</source>
        <comment>Language code: no</comment>
        <translation>norja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Occitan</source>
        <comment>Language code: oc</comment>
        <translation>oksitaani</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Oriya</source>
        <comment>Language code: or</comment>
        <translation>Oriya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Pashto, Pushto</source>
        <comment>Language code: ps</comment>
        <translation>pashtu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Persian</source>
        <comment>Language code: fa</comment>
        <translation>persia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Plautdietsch</source>
        <comment>Language code: pdt</comment>
        <translation>Plautdietsch</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Polish</source>
        <comment>Language code: pl</comment>
        <translation>puola</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Portuguese</source>
        <comment>Language code: pt</comment>
        <translation>portugali</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Punjabi</source>
        <comment>Language code: pa</comment>
        <translation>punjabi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Quechua</source>
        <comment>Language code: qu</comment>
        <translation>Quechua</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Rhaeto-Romance</source>
        <comment>Language code: rm</comment>
        <translation>rheto-romania</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Romanian</source>
        <comment>Language code: ro</comment>
        <translation>romania</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Russian</source>
        <comment>Language code: ru</comment>
        <translation>venäjä</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Samoan</source>
        <comment>Language code: sm</comment>
        <translation>samoa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sangro</source>
        <comment>Language code: sg</comment>
        <translation>Sangro</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sanskrit</source>
        <comment>Language code: sa</comment>
        <translation>sanskriitti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Scots Gaelic</source>
        <comment>Language code: gd</comment>
        <translation>Gaeli</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbian</source>
        <comment>Language code: sr</comment>
        <translation>serbia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbo-Croatian</source>
        <comment>Language code: sh</comment>
        <translation>Serbokroaatti</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sesotho</source>
        <comment>Language code: st</comment>
        <translation>Sesotho</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Setswana</source>
        <comment>Language code: tn</comment>
        <translation>Setswana</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Shona</source>
        <comment>Language code: sn</comment>
        <translation>Shona</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sindhi</source>
        <comment>Language code: sd</comment>
        <translation>Sindhi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Singhalese</source>
        <comment>Language code: si</comment>
        <translation>Sinhalese</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Siswati</source>
        <comment>Language code: ss</comment>
        <translation>Swati</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovak</source>
        <comment>Language code: sk</comment>
        <translation>slovakia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovenian</source>
        <comment>Language code: sl</comment>
        <translation>Slovenia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Somali</source>
        <comment>Language code: so</comment>
        <translation>somalia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Spanish</source>
        <comment>Language code: es</comment>
        <translation>espanja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sudanese</source>
        <comment>Language code: su</comment>
        <translation>sudan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swahili</source>
        <comment>Language code: sw</comment>
        <translation>swahili</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swedish</source>
        <comment>Language code: sv</comment>
        <translation>ruotsi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tagalog</source>
        <comment>Language code: tl</comment>
        <translation>Tagalog</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tajik</source>
        <comment>Language code: tg</comment>
        <translation>Tadžikki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tamil</source>
        <comment>Language code: ta</comment>
        <translation>Tamili</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tatar</source>
        <comment>Language code: tt</comment>
        <translation>tataari</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tegulu</source>
        <comment>Language code: te</comment>
        <translation>Tegulu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Thai</source>
        <comment>Language code: th</comment>
        <translation>thai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tibetan</source>
        <comment>Language code: bo</comment>
        <translation>tiibet</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tigrinya</source>
        <comment>Language code: ti</comment>
        <translation>Tigrinya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tonga</source>
        <comment>Language code: to</comment>
        <translation>Tonga</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tsonga</source>
        <comment>Language code: ts</comment>
        <translation>Tsonga</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkish</source>
        <comment>Language code: tr</comment>
        <translation>turkki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkmen</source>
        <comment>Language code: tk</comment>
        <translation>turkmen</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Twi</source>
        <comment>Language code: tw</comment>
        <translation>Twi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uigur</source>
        <comment>Language code: ug</comment>
        <translation>Uiguuri</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ukrainian</source>
        <comment>Language code: uk</comment>
        <translation>ukraina</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Urdu</source>
        <comment>Language code: ur</comment>
        <translation>urdu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uspanteco</source>
        <comment>Language code: usp</comment>
        <translation>Uspanteco</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uzbek</source>
        <comment>Language code: uz</comment>
        <translation>uzbek</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Vietnamese</source>
        <comment>Language code: vi</comment>
        <translation>vietnam</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Volapuk</source>
        <comment>Language code: vo</comment>
        <translation>Volapük</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Welch</source>
        <comment>Language code: cy</comment>
        <translation>Welch</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Wolof</source>
        <comment>Language code: wo</comment>
        <translation>Wolof</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Xhosa</source>
        <comment>Language code: xh</comment>
        <translation>Xhosa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yiddish (former ji)</source>
        <comment>Language code: yi</comment>
        <translation>Jiddiš (ennen ji)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yoruba</source>
        <comment>Language code: yo</comment>
        <translation>Joruba</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zhuang</source>
        <comment>Language code: za</comment>
        <translation>Zhuang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zulu</source>
        <comment>Language code: zu</comment>
        <translation>zulu</translation>
    </message>
</context>
</TS>
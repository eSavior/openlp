<?xml version="1.0" ?><!DOCTYPE TS><TS language="id" version="2.0">
<context>
    <name>AlertsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>&amp;Alert</source>
        <translation>&amp;Peringatan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>Show an alert message.</source>
        <translation>Menampilkan suatu pesan peringatan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="212"/>
        <source>&lt;strong&gt;Alerts Plugin&lt;/strong&gt;&lt;br /&gt;The alert plugin controls the displaying of alerts on the display screen.</source>
        <translation>&lt;strong&gt;Plugin Peringatan&lt;/strong&gt;&lt;br&gt;Plugin Peringatan mengendalikan penampilan peringatan di layar tampilan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="222"/>
        <source>Alert</source>
        <comment>name singular</comment>
        <translation>Peringatan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="223"/>
        <source>Alerts</source>
        <comment>name plural</comment>
        <translation>Peringatan-Peringatan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="227"/>
        <source>Alerts</source>
        <comment>container title</comment>
        <translation>Peringatan-Peringatan</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertForm</name>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="93"/>
        <source>Alert Message</source>
        <translation>Pesan Peringatan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="94"/>
        <source>Alert &amp;text:</source>
        <translation>Teks &amp;peringatan:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="95"/>
        <source>&amp;Parameter:</source>
        <translation>&amp;Parameter:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="96"/>
        <source>&amp;New</source>
        <translation>&amp;Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="97"/>
        <source>&amp;Save</source>
        <translation>&amp;Simpan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="98"/>
        <source>Displ&amp;ay</source>
        <translation>&amp;Tampilkan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="99"/>
        <source>Display &amp;&amp; Cl&amp;ose</source>
        <translation>Tampilkan dan &amp;Tutup</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>New Alert</source>
        <translation>Peringatan Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>You haven't specified any text for your alert. 
Please type in some text before clicking New.</source>
        <translation>Teks isi peringatan belum ditentukan. 
Silakan masukkan teks sebelum memilih Baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>No Parameter Found</source>
        <translation>Parameter Tidak Ditemukan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>You have not entered a parameter to be replaced.
Do you want to continue anyway?</source>
        <translation>Anda belum memasukkan parameter baru.
Tetap lanjutkan?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>No Placeholder Found</source>
        <translation>Tempat Penampung Tidak Ditemukan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>The alert text does not contain '&lt;&gt;'.
Do you want to continue anyway?</source>
        <translation>Teks peringatan tidak berisikan &apos;&lt;&gt;&apos;.
Tetap lanjutkan?</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsManager</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertsmanager.py" line="73"/>
        <source>Alert message created and displayed.</source>
        <translation>Pesan peringatan telah dibuat dan ditampilkan.</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsTab</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="119"/>
        <source>Font Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="120"/>
        <source>Font name:</source>
        <translation>Nama fon:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="121"/>
        <source>Font color:</source>
        <translation>Warna fon:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="123"/>
        <source>Font size:</source>
        <translation>Ukuran fon:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="125"/>
        <source>Background Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="126"/>
        <source>Other Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="127"/>
        <source>Alert timeout:</source>
        <translation>Batas-waktu peringatan:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="129"/>
        <source>Repeat (no. of times):</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="130"/>
        <source>Enable Scrolling</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin</name>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="125"/>
        <source>&amp;Bible</source>
        <translation>&amp;Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="141"/>
        <source>&lt;strong&gt;Bible Plugin&lt;/strong&gt;&lt;br /&gt;The Bible plugin provides the ability to display Bible verses from different sources during the service.</source>
        <translation>&lt;strong&gt;Plugin Alkitab&lt;/strong&gt;&lt;br /&gt;Plugin Alkitab menyediakan kemampuan untuk menampilkan ayat Alkitab dari sumber yang berbeda selama Layanan dijalankan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="174"/>
        <source>Bible</source>
        <comment>name singular</comment>
        <translation>Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="175"/>
        <source>Bibles</source>
        <comment>name plural</comment>
        <translation>Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="179"/>
        <source>Bibles</source>
        <comment>container title</comment>
        <translation>Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="184"/>
        <source>Import a Bible.</source>
        <translation>Impor Alkitab.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="185"/>
        <source>Add a new Bible.</source>
        <translation>Tambahkan Alkitab baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="186"/>
        <source>Edit the selected Bible.</source>
        <translation>Sunting Alkitab terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="187"/>
        <source>Delete the selected Bible.</source>
        <translation>Hapus Alkitab terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="188"/>
        <source>Preview the selected Bible.</source>
        <translation>Pratinjau Alkitab terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="189"/>
        <source>Send the selected Bible live.</source>
        <translation>Tayangkan Alkitab terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="190"/>
        <source>Add the selected Bible to the service.</source>
        <translation>Tambahkan Alkitab terpilih ke Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="76"/>
        <source>Genesis</source>
        <translation>Kejadian</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="77"/>
        <source>Exodus</source>
        <translation>Keluaran</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="78"/>
        <source>Leviticus</source>
        <translation>Imamat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="79"/>
        <source>Numbers</source>
        <translation>Bilangan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="80"/>
        <source>Deuteronomy</source>
        <translation>Ulangan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="81"/>
        <source>Joshua</source>
        <translation>Yosua</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="82"/>
        <source>Judges</source>
        <translation>Hakim Hakim</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="83"/>
        <source>Ruth</source>
        <translation>Rut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="84"/>
        <source>1 Samuel</source>
        <translation>1 Samuel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="85"/>
        <source>2 Samuel</source>
        <translation>2 Samuel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="86"/>
        <source>1 Kings</source>
        <translation>1 Raja Raja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="87"/>
        <source>2 Kings</source>
        <translation>2 Raja Raja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="88"/>
        <source>1 Chronicles</source>
        <translation>1 Tawarikh</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="89"/>
        <source>2 Chronicles</source>
        <translation>2 Tawarikh</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="90"/>
        <source>Ezra</source>
        <translation>Ezra</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="91"/>
        <source>Nehemiah</source>
        <translation>Nehemia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="92"/>
        <source>Esther</source>
        <translation>Ester</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="93"/>
        <source>Job</source>
        <translation>Ayub</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="94"/>
        <source>Psalms</source>
        <translation>Mazmur</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="95"/>
        <source>Proverbs</source>
        <translation>Amsal</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="96"/>
        <source>Ecclesiastes</source>
        <translation>Pengkotbah</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="97"/>
        <source>Song of Solomon</source>
        <translation>Kidung Agung</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="98"/>
        <source>Isaiah</source>
        <translation>Yesaya</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="99"/>
        <source>Jeremiah</source>
        <translation>Yeremia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="100"/>
        <source>Lamentations</source>
        <translation>Ratapan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="101"/>
        <source>Ezekiel</source>
        <translation>Yehezkiel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="102"/>
        <source>Daniel</source>
        <translation>Daniel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="103"/>
        <source>Hosea</source>
        <translation>Hosea</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="104"/>
        <source>Joel</source>
        <translation>Yoel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="105"/>
        <source>Amos</source>
        <translation>Amos</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="106"/>
        <source>Obadiah</source>
        <translation>Obaja</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="107"/>
        <source>Jonah</source>
        <translation>Yunus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="108"/>
        <source>Micah</source>
        <translation>Mikha</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="109"/>
        <source>Nahum</source>
        <translation>Nahum</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="110"/>
        <source>Habakkuk</source>
        <translation>Habakuk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="111"/>
        <source>Zephaniah</source>
        <translation>Zefanya</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="112"/>
        <source>Haggai</source>
        <translation>Hagai</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="113"/>
        <source>Zechariah</source>
        <translation>Zakharia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="114"/>
        <source>Malachi</source>
        <translation>Maleakhi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="115"/>
        <source>Matthew</source>
        <translation>Matius</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="116"/>
        <source>Mark</source>
        <translation>Markus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="117"/>
        <source>Luke</source>
        <translation>Lukas</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="118"/>
        <source>John</source>
        <translation>Yohanes</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="119"/>
        <source>Acts</source>
        <translation>Kisah Para Rasul</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="120"/>
        <source>Romans</source>
        <translation>Roma</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="121"/>
        <source>1 Corinthians</source>
        <translation>1 Korintus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="122"/>
        <source>2 Corinthians</source>
        <translation>2 Korintus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="123"/>
        <source>Galatians</source>
        <translation>Galatia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="124"/>
        <source>Ephesians</source>
        <translation>Efesus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="125"/>
        <source>Philippians</source>
        <translation>Filipi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="126"/>
        <source>Colossians</source>
        <translation>Kolose</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="127"/>
        <source>1 Thessalonians</source>
        <translation>1 Tesalonika</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="128"/>
        <source>2 Thessalonians</source>
        <translation>2 Tesalonika</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="129"/>
        <source>1 Timothy</source>
        <translation>1 Timotius</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="130"/>
        <source>2 Timothy</source>
        <translation>2 Timotius</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="131"/>
        <source>Titus</source>
        <translation>Titus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="132"/>
        <source>Philemon</source>
        <translation>Filemon</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="133"/>
        <source>Hebrews</source>
        <translation>Ibrani</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="134"/>
        <source>James</source>
        <translation>Yakobus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="135"/>
        <source>1 Peter</source>
        <translation>1 Petrus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="136"/>
        <source>2 Peter</source>
        <translation>2 Petrus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="137"/>
        <source>1 John</source>
        <translation>1 Yohanes</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="138"/>
        <source>2 John</source>
        <translation>2 Yohanes</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="139"/>
        <source>3 John</source>
        <translation>3 Yohanes</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="140"/>
        <source>Jude</source>
        <translation>Yudas</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="141"/>
        <source>Revelation</source>
        <translation>Wahyu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="142"/>
        <source>Judith</source>
        <translation>Yudit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="143"/>
        <source>Wisdom</source>
        <translation>Kebijaksanaan Salomo</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="144"/>
        <source>Tobit</source>
        <translation>Tobit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="145"/>
        <source>Sirach</source>
        <translation>Sirakh</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="146"/>
        <source>Baruch</source>
        <translation>Barukh</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="147"/>
        <source>1 Maccabees</source>
        <translation>1 Makabe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="148"/>
        <source>2 Maccabees</source>
        <translation>2 Makabe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="149"/>
        <source>3 Maccabees</source>
        <translation>3 Maccabees</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="150"/>
        <source>4 Maccabees</source>
        <translation>4 Maccabees</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="151"/>
        <source>Rest of Daniel</source>
        <translation>Tamb. Daniel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="152"/>
        <source>Rest of Esther</source>
        <translation>Tamb. Ester</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="153"/>
        <source>Prayer of Manasses</source>
        <translation>Prayer of Manasses</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="154"/>
        <source>Letter of Jeremiah</source>
        <translation>Letter of Jeremiah</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="155"/>
        <source>Prayer of Azariah</source>
        <translation>Prayer of Azariah</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="156"/>
        <source>Susanna</source>
        <translation>Susanna</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="157"/>
        <source>Bel</source>
        <translation>Bel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="158"/>
        <source>1 Esdras</source>
        <translation>1 Esdras</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="159"/>
        <source>2 Esdras</source>
        <translation>2 Esdras</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>:</source>
        <comment>Verse identifier e.g. Genesis 1 : 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>v</source>
        <comment>Verse identifier e.g. Genesis 1 v 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>v</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>V</source>
        <comment>Verse identifier e.g. Genesis 1 V 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verse</source>
        <comment>Verse identifier e.g. Genesis 1 verse 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>ayat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verses</source>
        <comment>Verse identifier e.g. Genesis 1 verses 1 - 2 = Genesis Chapter 1 Verses 1 to 2</comment>
        <translation>ayat-ayat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>-</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>to</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>ke</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>,</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2, 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>,</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>and</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2 and 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>dan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="185"/>
        <source>end</source>
        <comment>ending identifier e.g. Genesis 1 verse 1 - end = Genesis Chapter 1 Verses 1 To The Last Verse</comment>
        <translation>akhir</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No Book Found</source>
        <translation>Kitab Tidak Ditemukan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No matching book could be found in this Bible. Check that you have spelled the name of the book correctly.</source>
        <translation>Kitab tidak ditemukan dalam Alkitab ini. Periksa apakah Anda telah mengeja nama kitab dengan benar.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="64"/>
        <source>The proxy server {proxy} was found in the bible {name}.&lt;br&gt;Would you like to set it as the proxy for OpenLP?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="69"/>
        <source>both</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleEditForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="162"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>Anda harus menentukan suatu nama versi untuk Alkitab.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="168"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>Anda harus menetapkan hak cipta untuk Alkitab Anda. Alkitab di Domain Publik harus ditandai seperti itu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>Bible Exists</source>
        <translation>Alkitab Sudah Ada</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>Alkitab sudah ada. Silakan impor Alkitab lain atau hapus dahulu yang sudah ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="190"/>
        <source>You need to specify a book name for &quot;{text}&quot;.</source>
        <translation>Anda harus menentukan suatu nama kitab untuk &quot;{text}&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="197"/>
        <source>The book name &quot;{name}&quot; is not correct.
Numbers can only be used at the beginning and must
be followed by one or more non-numeric characters.</source>
        <translation>Nama kitab &quot;{name}&quot; salah.
Angka hanya dapat digunakan di awal dan harus
diikuti satu atau lebih karakter non-numerik.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>Duplicate Book Name</source>
        <translation>Duplikasi Nama Kitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>The Book Name &quot;{name}&quot; has been entered more than once.</source>
        <translation>Nama kitab &quot;{name}&quot; telah dimasukkan lebih dari satu kali.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="53"/>
        <source>The file &quot;{file}&quot; you supplied is compressed. You must decompress it before import.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="224"/>
        <source>unknown type of</source>
        <comment>This looks like an unknown type of XML bible.</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleManager</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Web Bible cannot be used in Text Search</source>
        <translation>Alkitab Web tidak dapat digunakan dalam Pencarian Teks</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Text Search is not available with Web Bibles.
Please use the Scripture Reference Search instead.

This means that the currently selected Bible is a Web Bible.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>Scripture Reference Error</source>
        <translation>Kesalahan Referensi Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>&lt;strong&gt;The reference you typed is invalid!&lt;br&gt;&lt;br&gt;Please make sure that your reference follows one of these patterns:&lt;/strong&gt;&lt;br&gt;&lt;br&gt;%s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BiblesTab</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="178"/>
        <source>Verse Display</source>
        <translation>Tampilan Ayat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="179"/>
        <source>Show verse numbers</source>
        <translation>Tampilkan nomor ayat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="180"/>
        <source>Only show new chapter numbers</source>
        <translation>Hanya tampilkan nomor bab baru</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="183"/>
        <source>Bible theme:</source>
        <translation>Tema Alkitab:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="187"/>
        <source>No Brackets</source>
        <translation>Tanpa Tanda Kurung</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="189"/>
        <source>( And )</source>
        <translation>( Dan )</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="191"/>
        <source>{ And }</source>
        <translation>{ Dan }</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="193"/>
        <source>[ And ]</source>
        <translation>[ Dan ]</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="195"/>
        <source>Note: Changes do not affect verses in the Service</source>
        <translation>Catatan: Perubahan tidak mempengaruhi ayat-ayat dalam Kebaktian</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="197"/>
        <source>Display second Bible verses</source>
        <translation>Tampilkan ayat Alkitab kedua (beda versi)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="198"/>
        <source>Custom Scripture References</source>
        <translation>Referensi Alkitab Kustom</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="199"/>
        <source>Verse separator:</source>
        <translation>Pemisah ayat:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="200"/>
        <source>Range separator:</source>
        <translation>Pemisah jarak:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="201"/>
        <source>List separator:</source>
        <translation>Pemisah daftar:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="202"/>
        <source>End mark:</source>
        <translation>Tanda akhir:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="203"/>
        <source>Multiple alternative verse separators may be defined.
They have to be separated by a vertical bar &quot;|&quot;.
Please clear this edit line to use the default value.</source>
        <translation>Beberapa alternatif pemisah ayat dapat ditetapkan.
Semuanya harus dipisahkan oleh sebuah palang vertikal &quot;|&quot;.
Silakan bersihkan baris penyuntingan ini untuk menggunakan nilai bawaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="210"/>
        <source>Default Bible Language</source>
        <translation>Bahasa Alkitab Bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="211"/>
        <source>Book name language in search field,
search results and on display:</source>
        <translation>Bahasa Kitab di bidang penelusuran,
hasil penelusuran, dan tampilan:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="213"/>
        <source>Bible Language</source>
        <translation>Bahasa Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="215"/>
        <source>Application Language</source>
        <translation>Bahasa Aplikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="217"/>
        <source>English</source>
        <translation>Bahasa Inggris</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="219"/>
        <source>Quick Search Settings</source>
        <translation>Pengaturan Pencarian Cepat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="220"/>
        <source>Reset search type to &quot;Text or Scripture Reference&quot; on startup</source>
        <translation>Me-reset tipe pencarian ke &quot;Referensi Teks atau Firman&quot; pada startup</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="223"/>
        <source>Don&apos;t show error if nothing is found in &quot;Text or Scripture Reference&quot;</source>
        <translation>Jangan tampilkan error jika tak ada yang ditemukan dalam &quot;Referensi Teks atau Firman&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="226"/>
        <source>Search automatically while typing (Text search must contain a
minimum of {count} characters and a space for performance reasons)</source>
        <translation>Cari otomatis selagi diketik (Pencarian teks harus mengandung
minimal {count} karakter dan sebuah spasi unutk alasan performa)</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="84"/>
        <source>Select Book Name</source>
        <translation>Pilih Nama Kitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="85"/>
        <source>The following book name cannot be matched up internally. Please select the corresponding name from the list.</source>
        <translation>Nama kitab berikut tidak dapat dicocokkan dengan sistem. Silakan pilih yang sesuai dari daftar.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="88"/>
        <source>Current name:</source>
        <translation>Nama saat ini:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="89"/>
        <source>Corresponding name:</source>
        <translation>Nama yang sesuai:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="90"/>
        <source>Show Books From</source>
        <translation>Tampilkan Kitab Dari</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="91"/>
        <source>Old Testament</source>
        <translation>Perjanjian Lama</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="92"/>
        <source>New Testament</source>
        <translation>Perjanjian Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="93"/>
        <source>Apocrypha</source>
        <translation>Apokrif</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknameform.py" line="109"/>
        <source>You need to select a book.</source>
        <translation>Anda harus memilih sebuah kitab.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.CSVBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="123"/>
        <source>Importing books... {book}</source>
        <translation>Mengimpo kitab-kitab... {book}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="145"/>
        <source>Importing verses from {book}...</source>
        <comment>Importing verses from &lt;book name&gt;...</comment>
        <translation>Mengimpor ayat-ayat dari (book)...</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="132"/>
        <source>Bible Editor</source>
        <translation>Penyunting Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="136"/>
        <source>License Details</source>
        <translation>Rincian Lisensi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="137"/>
        <source>Version name:</source>
        <translation>Nama versi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="138"/>
        <source>Copyright:</source>
        <translation>Hak Cipta:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="139"/>
        <source>Permissions:</source>
        <translation>Izin:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="140"/>
        <source>Full license:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="141"/>
        <source>Default Bible Language</source>
        <translation>Bahasa Alkitab Bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="142"/>
        <source>Book name language in search field, search results and on display:</source>
        <translation>Bahasa Kitab di bidang penelusuran, hasil penelusuran, dan tampilan:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="145"/>
        <source>Global Settings</source>
        <translation>Setelan Global</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="146"/>
        <source>Bible Language</source>
        <translation>Bahasa Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="149"/>
        <source>Application Language</source>
        <translation>Bahasa Aplikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="151"/>
        <source>English</source>
        <translation>Bahasa Inggris</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="93"/>
        <source>This is a Web Download Bible.
It is not possible to customize the Book Names.</source>
        <translation>Ini suatu Alkitab Unduhan dari Web.
Tidak mungkin untuk mengubahsuaikan Nama Kitab.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="98"/>
        <source>To use the customized book names, &quot;Bible language&quot; must be selected on the Meta Data tab or, if &quot;Global settings&quot; is selected, on the Bible page in Configure OpenLP.</source>
        <translation>Untuk menggunakan nama kitab yang diubahsuaikan, &quot;Bahasa Alkitab&quot; harus dipilih pada tab Meta Data atau, jika &quot;Setelan Global&quot; dipilih, pada halaman Alkitab dalam Mengkonfigurasi OpenLP.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.HTTPBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="607"/>
        <source>Registering Bible and loading books...</source>
        <translation>Mendaftarkan Alkitab dan memuat kitab...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="623"/>
        <source>Registering Language...</source>
        <translation>Mendaftarkan bahasa...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="630"/>
        <source>Importing {book}...</source>
        <comment>Importing &lt;book name&gt;...</comment>
        <translation>Mengimpor {book}...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>Download Error</source>
        <translation>Kesalahan Unduhan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>There was a problem downloading your verse selection. Please check your Internet connection, and if this error continues to occur please consider reporting a bug.</source>
        <translation>Ada masalah dalam mengunduh ayat yang terpilih. Silakan periksa sambungan internet Anda dan jika kesalahan ini berlanjut, pertimbangkan untuk melaporkan hal ini sebagai bug.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>Parse Error</source>
        <translation>Kesalahan Penguraian</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>There was a problem extracting your verse selection. If this error continues to occur please consider reporting a bug.</source>
        <translation>Ada masalah dalam mengekstrak ayat yang terpilih. Jika kesalahan ini berlanjut, silakan pertimbangkan untuk melaporkan hal ini sebagai bug.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="192"/>
        <source>CSV File</source>
        <translation>Berkas CSV</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="371"/>
        <source>Bible Import Wizard</source>
        <translation>Wisaya Impor Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="374"/>
        <source>This wizard will help you to import Bibles from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>Wisaya ini akan membantu Anda mengimpor Alkitab dari berbagai format. Klik tombol Selanjutnya di bawah untuk memulai proses dengan memilih format untuk diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="386"/>
        <source>Web Download</source>
        <translation>Unduhan dari Web</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="396"/>
        <source>Bible file:</source>
        <translation>Berkas Alkitab:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="391"/>
        <source>Books file:</source>
        <translation>Berkas kitab:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="392"/>
        <source>Verses file:</source>
        <translation>Berkas ayat:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="394"/>
        <source>Location:</source>
        <translation>Lokasi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="397"/>
        <source>Click to download bible list</source>
        <translation>Klik untuk mengunduh daftar Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="398"/>
        <source>Download bible list</source>
        <translation>Pengunduhan daftar Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="399"/>
        <source>Crosswalk</source>
        <translation>Crosswalk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="401"/>
        <source>BibleGateway</source>
        <translation>BibleGateway</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="403"/>
        <source>Bibleserver</source>
        <translation>Bibleserver</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="405"/>
        <source>Bible:</source>
        <translation>Alkitab:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="409"/>
        <source>Bibles:</source>
        <translation>Alkitab-Alkitab:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="407"/>
        <source>SWORD data folder:</source>
        <translation>Folder data SWORD:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="408"/>
        <source>SWORD zip-file:</source>
        <translation>File zip SWORD:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="410"/>
        <source>Import from folder</source>
        <translation>Impor dari folder</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="412"/>
        <source>Import from Zip-file</source>
        <translation>Impor dari file Zip</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="417"/>
        <source>To import SWORD bibles the pysword python module must be installed. Please read the manual for instructions.</source>
        <translation>Unutk mengimpor Alkitab SWORD modul pysword python harus sudah ter-install. Silakan Silakan baca manualnya untuk instruksi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="420"/>
        <source>License Details</source>
        <translation>Rincian Lisensi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="422"/>
        <source>Set up the Bible&apos;s license details.</source>
        <translation>Siapkan rincian lisensi Alkitab.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="424"/>
        <source>Version name:</source>
        <translation>Nama versi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="425"/>
        <source>Copyright:</source>
        <translation>Hak Cipta:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="426"/>
        <source>Permissions:</source>
        <translation>Izin:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="427"/>
        <source>Full license:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="429"/>
        <source>Please wait while your Bible is imported.</source>
        <translation>Silakan tunggu selama Alkitab diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="461"/>
        <source>You need to specify a file with books of the Bible to use in the import.</source>
        <translation>Anda harus menentukan suatu berkas yang berisi kitab-kitab Alkitab untuk digunakan dalam impor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="468"/>
        <source>You need to specify a file of Bible verses to import.</source>
        <translation>Anda harus menentukan suatu berkas ayat Alkitab untuk diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="526"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>Anda harus menentukan suatu nama versi untuk Alkitab.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="532"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>Anda harus menetapkan hak cipta untuk Alkitab Anda. Alkitab di Domain Publik harus ditandai seperti itu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>Bible Exists</source>
        <translation>Alkitab Sudah Ada</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>Alkitab sudah ada. Silakan impor Alkitab lain atau hapus dahulu yang sudah ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>Error during download</source>
        <translation>Terjadi kesalahan saat pengunduhan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>An error occurred while downloading the list of bibles from %s.</source>
        <translation>Terjadi kesalahan saat mengunduh daftar Alkitab dari %s.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="691"/>
        <source>Registering Bible...</source>
        <translation>Mendaftarkan Alkitab...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="757"/>
        <source>Registered Bible. Please note, that verses will be downloaded on demand and thus an internet connection is required.</source>
        <translation>Alkitab telah terdaftar. Perlu diketahui bahwa ayat-ayat akan diunduh sesuai permintaan dan membutuhkan sambungan internet.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="767"/>
        <source>Your Bible import failed.</source>
        <translation>Impor Alkitab gagal.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="66"/>
        <source>Select Language</source>
        <translation>Pilih Bahasa</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="68"/>
        <source>OpenLP is unable to determine the language of this translation of the Bible. Please select the language from the list below.</source>
        <translation>OpenLP tidak dapat menentukan bahasa untuk terjemahan Alkitab ini. Silakan pilih bahasa dari daftar di bawah.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="72"/>
        <source>Language:</source>
        <translation>Bahasa:</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languageform.py" line="62"/>
        <source>You need to choose a language.</source>
        <translation>Anda harus memilih sebuah bahasa.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="161"/>
        <source>Find</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="164"/>
        <source>Find:</source>
        <translation>Temukan:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="170"/>
        <source>Select</source>
        <translation>Pilih</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="178"/>
        <source>Sort bible books alphabetically.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="180"/>
        <source>Book:</source>
        <translation>Buku Lagu:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="192"/>
        <source>From:</source>
        <translation>Dari:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="198"/>
        <source>To:</source>
        <translation>Sampai:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="204"/>
        <source>Options</source>
        <translation>Opsi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="209"/>
        <source>Second:</source>
        <translation>Kedua:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="273"/>
        <source>Chapter:</source>
        <translation>Bab:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="274"/>
        <source>Verse:</source>
        <translation>Ayat:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="278"/>
        <source>Clear the results on the current tab.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="279"/>
        <source>Add the search results to the saved list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference</source>
        <translation>Teks atau Referensi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference...</source>
        <translation>Teks atau Referensi...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Scripture Reference</source>
        <translation>Referensi Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Scripture Reference...</source>
        <translation>Telusuri Referensi Alkitab...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text Search</source>
        <translation>Penelusuran Teks</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Text...</source>
        <translation>Telusuri Teks...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="458"/>
        <source>Are you sure you want to completely delete &quot;{bible}&quot; Bible from OpenLP?

You will need to re-import this Bible to use it again.</source>
        <translation>Anda yakin ingin menghapus Alkitab &quot;{bible}&quot; dari OpenLP?

Anda harus mengimpor ulang Alkitab ini unutk menggunakannya lagi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="506"/>
        <source>Saved ({result_count})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="509"/>
        <source>Results ({result_count})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="588"/>
        <source>OpenLP cannot combine single and dual Bible verse search results. Do you want to clear your saved results?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="618"/>
        <source>Bible not fully loaded.</source>
        <translation>Alkitab belum termuat seluruhnya.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>Verses not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>The second Bible &quot;{second_name}&quot; does not contain all the verses that are in the main Bible &quot;{name}&quot;.
Only verses found in both Bibles will be shown.

{count:d} verses have not been included in the results.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.OsisImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="171"/>
        <source>Removing unused tags (this may take a few minutes)...</source>
        <translation>Menghapus label yang tidak digunakan ( mungkin butuh waktu beberapa menit)...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="200"/>
        <source>Importing {book} {chapter}...</source>
        <translation>Mengimpor {book} {chapter}...</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.Sword</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="88"/>
        <source>Importing {name}...</source>
        <translation>Mengimpor {name}...</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.SwordImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="93"/>
        <source>An unexpected error happened while importing the SWORD bible, please report this to the OpenLP developers.
{error}</source>
        <translation>Suatu error yang tak terduga telah terjadi selagi mengimpor Alkitab SWORD, silakan laporkan ini ke pengembang OpenLP.
{error}</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.ZefaniaImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="90"/>
        <source>Incorrect Bible file type supplied. Zefania Bibles may be compressed. You must decompress them before import.</source>
        <translation>Jenis berkas Alkitab tidak benar. Alkitab Zefania mungkin dikompresi. Anda harus lakukan dekompresi sebelum mengimpor. </translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.Zefnia</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="83"/>
        <source>Importing {book} {chapter}...</source>
        <translation>Mengimpor {book} {chapter}...</translation>
    </message>
</context>
<context>
    <name>CustomPlugin</name>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="77"/>
        <source>&lt;strong&gt;Custom Slide Plugin &lt;/strong&gt;&lt;br /&gt;The custom slide plugin provides the ability to set up custom text slides that can be displayed on the screen the same way songs are. This plugin provides greater freedom over the songs plugin.</source>
        <translation>&lt;strong&gt;Plugin Salindia Kustom &lt;/strong&gt;&lt;br /&gt;Plugin salindia kustom menyediakan kemampuan untuk menyiapkan salindia teks kustom yang dapat ditampilkan pada layar dengan cara yang sama seperti lagu. Plugin ini memberikan kebebasan lebih ketimbang plugin lagu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="109"/>
        <source>Custom Slide</source>
        <comment>name singular</comment>
        <translation>Salindia Kustom</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="110"/>
        <source>Custom Slides</source>
        <comment>name plural</comment>
        <translation>Salindia Kustom</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="114"/>
        <source>Custom Slides</source>
        <comment>container title</comment>
        <translation>Salindia Kustom</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="118"/>
        <source>Load a new custom slide.</source>
        <translation>Muat sebuah salindia kustom.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="119"/>
        <source>Import a custom slide.</source>
        <translation>Impor sebuah salindia kustom.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="120"/>
        <source>Add a new custom slide.</source>
        <translation>Tambahkan sebuah salindia kustom.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="121"/>
        <source>Edit the selected custom slide.</source>
        <translation>Sunting salindia kustom terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="122"/>
        <source>Delete the selected custom slide.</source>
        <translation>Hapus salindia kustom terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="123"/>
        <source>Preview the selected custom slide.</source>
        <translation>Pratinjau salindia kustom terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="124"/>
        <source>Send the selected custom slide live.</source>
        <translation>Tayangkan salindia kustom terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="125"/>
        <source>Add the selected custom slide to the service.</source>
        <translation>Tambahkan salindia kustom terpilih ke Layanan.</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.CustomTab</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="56"/>
        <source>Custom Display</source>
        <translation>Tampilan Kustom</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="57"/>
        <source>Display footer</source>
        <translation>Tampilkan Catatan Kaki</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="58"/>
        <source>Import missing custom slides from service files</source>
        <translation>Impor salindia kustom yang hilang dari berkas Layanan</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditCustomForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="104"/>
        <source>Edit Custom Slides</source>
        <translation>Sunting Salindia Kustom</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="105"/>
        <source>&amp;Title:</source>
        <translation>&amp;Judul:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="107"/>
        <source>Add a new slide at bottom.</source>
        <translation>Tambahkan suatu salindia baru di dasar.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="109"/>
        <source>Edit the selected slide.</source>
        <translation>Sunting salindia terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="110"/>
        <source>Ed&amp;it All</source>
        <translation>Sunting &amp;Semua</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="111"/>
        <source>Edit all the slides at once.</source>
        <translation>Sunting seluruh salindia bersamaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="112"/>
        <source>The&amp;me:</source>
        <translation>Te&amp;ma:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="113"/>
        <source>&amp;Credits:</source>
        <translation>&amp;Kredit:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="239"/>
        <source>You need to type in a title.</source>
        <translation>Anda harus mengetikkan judul.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="243"/>
        <source>You need to add at least one slide.</source>
        <translation>Anda harus masukkan setidaknya satu salindia.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="50"/>
        <source>Insert Slide</source>
        <translation>Sisipkan Salindia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="51"/>
        <source>Split a slide into two by inserting a slide splitter.</source>
        <translation>Pisah salindia menjadi dua menggunakan pemisah salindia.</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="47"/>
        <source>Edit Slide</source>
        <translation>Sunting Salindia</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="200"/>
        <source>Are you sure you want to delete the &quot;{items:d}&quot; selected custom slide(s)?</source>
        <translation>Anda yakin ingin menghapus slide(-slide) kustom  &quot;{items:d}&quot; yang diilih?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="261"/>
        <source>copy</source>
        <comment>For item cloning</comment>
        <translation>salin</translation>
    </message>
</context>
<context>
    <name>ImagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="70"/>
        <source>&lt;strong&gt;Image Plugin&lt;/strong&gt;&lt;br /&gt;The image plugin provides displaying of images.&lt;br /&gt;One of the distinguishing features of this plugin is the ability to group a number of images together in the service manager, making the displaying of multiple images easier. This plugin can also make use of OpenLP&apos;s &quot;timed looping&quot; feature to create a slide show that runs automatically. In addition to this, images from the plugin can be used to override the current theme&apos;s background, which renders text-based items like songs with the selected image as a background instead of the background provided by the theme.</source>
        <translation>&lt;strong&gt;Plugin Gambar&lt;/strong&gt;&lt;br /&gt;Plugin gambar memungkinkan penayangan gambar.&lt;br /&gt;Salah satu keunggulan fitur ini adalah kemampuan untuk menggabungkan beberapa gambar pada Manajer Layanan, yang menjadikan penayangan beberapa gambar lebih mudah. Plugin ini juga dapat menggunakan &quot;pengulangan terwaktu&quot; dari OpenLP untuk membuat penayangan salindia berjalan otomatis. Sebagai tambahan, gambar dari plugin dapat digunakan untuk menggantikan latar tema yang sedang digunakan, yang mana menjadikan butir berbasis teks seperti lagu dengan gambar pilihan sebagai latar bukan dari latar yang disediakan oleh tema.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="89"/>
        <source>Image</source>
        <comment>name singular</comment>
        <translation>Gambar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="90"/>
        <source>Images</source>
        <comment>name plural</comment>
        <translation>Gambar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="93"/>
        <source>Images</source>
        <comment>container title</comment>
        <translation>Gambar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="96"/>
        <source>Add new image(s).</source>
        <translation>Tambahkan gambar(-gambar) baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="98"/>
        <source>Add a new image.</source>
        <translation>Tambahkan suatu gambar baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="99"/>
        <source>Edit the selected image.</source>
        <translation>Sunting gambar terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="100"/>
        <source>Delete the selected image.</source>
        <translation>Hapus gambar terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="101"/>
        <source>Preview the selected image.</source>
        <translation>Pratinjau gambar terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="102"/>
        <source>Send the selected image live.</source>
        <translation>Tayangkan gambar terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="103"/>
        <source>Add the selected image to the service.</source>
        <translation>Tambahkan gambar terpilih ke Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="191"/>
        <source>Add new image(s)</source>
        <translation>Tambahkan gambar(-gambar) baru.</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.AddGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="54"/>
        <source>Add group</source>
        <translation>Tambahkan grup</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="55"/>
        <source>Parent group:</source>
        <translation>Grup induk:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="56"/>
        <source>Group name:</source>
        <translation>Nama grup:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="67"/>
        <source>You need to type in a group name.</source>
        <translation>Anda harus mengetikkan nama grup.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="655"/>
        <source>Could not add the new group.</source>
        <translation>Tidak dapat menambahkan grup tersebut.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="658"/>
        <source>This group already exists.</source>
        <translation>Grup ini sudah ada.</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ChooseGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="82"/>
        <source>Select Image Group</source>
        <translation>Pilih Grup Gambar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="83"/>
        <source>Add images to group:</source>
        <translation>Tambahkan gambar ke grup:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="84"/>
        <source>No group</source>
        <translation>Tanpa grup</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="85"/>
        <source>Existing group</source>
        <translation>Grup yang Ada</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="86"/>
        <source>New group</source>
        <translation>Grup baru</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="170"/>
        <source>Select Attachment</source>
        <translation>Pilih Lampiran</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="54"/>
        <source>-- Top-level group --</source>
        <translation>-- Grup tingkatan-atas --</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="81"/>
        <source>Select Image(s)</source>
        <translation>Pilih (beberapa) Gambar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="243"/>
        <source>You must select an image or group to delete.</source>
        <translation>Anda harus memilih suatu gambar atau grup untuk menghapusnya.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Remove group</source>
        <translation>Hapus grup</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Are you sure you want to remove &quot;{name}&quot; and everything in it?</source>
        <translation>Anda yakin ingin menghapus &quot;{name}&quot; dan semua isinya?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>Missing Image(s)</source>
        <translation>(Beberapa) Gambar Hilang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="596"/>
        <source>The following image(s) no longer exist: {names}</source>
        <translation>Gambar(-gambar) berikut tidak ada lagi: {names}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>The following image(s) no longer exist: {names}
Do you want to add the other images anyway?</source>
        <translation>Gambar(-gambar) berikut tidak ada lagit: {names}
Apa Anda ingin tetap menambahkan gambar-gambar selebihnya?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="679"/>
        <source>You must select an image to replace the background with.</source>
        <translation>Anda harus memilih suatu gambar untuk menggantikan latar.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="693"/>
        <source>There was no display item to amend.</source>
        <translation>Tidak ada butir tampilan untuk diubah.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="697"/>
        <source>There was a problem replacing your background, the image file &quot;{name}&quot; no longer exists.</source>
        <translation>Terjadi masalah dalam mengganti latar, file gambar &quot;{name}&quot; tidak ada lagi.</translation>
    </message>
</context>
<context>
    <name>ImagesPlugin.ImageTab</name>
    <message>
        <location filename="../../openlp/plugins/images/lib/imagetab.py" line="63"/>
        <source>Visible background for images with aspect ratio different to screen.</source>
        <translation>Latar yang dapat terlihat untuk gambar dengan rasio aspek yang berbeda dengan layar.</translation>
    </message>
</context>
<context>
    <name>MediaPlugin</name>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="81"/>
        <source>&lt;strong&gt;Media Plugin&lt;/strong&gt;&lt;br /&gt;The media plugin provides playback of audio and video.</source>
        <translation>&lt;strong&gt;Plugin Media&lt;/strong&gt;&lt;br /&gt;Plugin Media mampu memainkan audio dan video.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="91"/>
        <source>Media</source>
        <comment>name singular</comment>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="92"/>
        <source>Media</source>
        <comment>name plural</comment>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="96"/>
        <source>Media</source>
        <comment>container title</comment>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="100"/>
        <source>Load new media.</source>
        <translation>Muat media baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="102"/>
        <source>Add new media.</source>
        <translation>Tambahkan media baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="103"/>
        <source>Edit the selected media.</source>
        <translation>Sunting media terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="104"/>
        <source>Delete the selected media.</source>
        <translation>Hapus media terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="105"/>
        <source>Preview the selected media.</source>
        <translation>Pratinjau media terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="106"/>
        <source>Send the selected media live.</source>
        <translation>Tayangkan media terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="107"/>
        <source>Add the selected media to the service.</source>
        <translation>Tambahkan media terpilih ke Layanan.</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelector</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="184"/>
        <source>Select Media Clip</source>
        <translation>Pilih Klip Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="185"/>
        <source>Source</source>
        <translation>Sumber</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="186"/>
        <source>Media path:</source>
        <translation>Jalur media:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="187"/>
        <source>Select drive from list</source>
        <translation>Pilih penggerak dari daftar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="189"/>
        <source>Load disc</source>
        <translation>Muat disk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="190"/>
        <source>Track Details</source>
        <translation>Rincian Trek</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="191"/>
        <source>Title:</source>
        <translation>Judul</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="192"/>
        <source>Audio track:</source>
        <translation>Trek audio:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="193"/>
        <source>Subtitle track:</source>
        <translation>Trek sub-judul:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="201"/>
        <source>HH:mm:ss.z</source>
        <translation>HH:mm:ss.z</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="195"/>
        <source>Clip Range</source>
        <translation>Kisaran Klip</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="196"/>
        <source>Start point:</source>
        <translation>Titik mulai:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="198"/>
        <source>Set start point</source>
        <translation>Setel titik mulai</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="199"/>
        <source>Jump to start point</source>
        <translation>Pindah ke titik mulai</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="200"/>
        <source>End point:</source>
        <translation>Titik akhir:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="202"/>
        <source>Set end point</source>
        <translation>Setel titik akhir</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="203"/>
        <source>Jump to end point</source>
        <translation>Pindah ke titik akhir</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelectorForm</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="195"/>
        <source>No path was given</source>
        <translation>Jalur yang dipilih belum ada</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="201"/>
        <source>Given path does not exists</source>
        <translation>Jalur yang dipilih tidak ada</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="218"/>
        <source>An error happened during initialization of VLC player</source>
        <translation>Terjadi kesalahan saat inisialisasi pemutar VLC</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="239"/>
        <source>VLC player failed playing the media</source>
        <translation>Pemutar VLC gagal memainkan media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>CD not loaded correctly</source>
        <translation>CD tidak termuat dengan benar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>The CD was not loaded correctly, please re-load and try again.</source>
        <translation>CD tidak termuat dengan benar, silakan muat-ulang dan coba lagi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>DVD not loaded correctly</source>
        <translation>DVD tidak termuat dengan benar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>The DVD was not loaded correctly, please re-load and try again.</source>
        <translation>DVD tidak termuat dengan benar, silakan muat-ulang dan coba lagi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Set name of mediaclip</source>
        <translation>Tentukan nama klip media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Name of mediaclip:</source>
        <translation>Nama klip media:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="602"/>
        <source>Enter a valid name or cancel</source>
        <translation>Masukkan nama yang valid atau batal</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>Invalid character</source>
        <translation>Karakter tidak valid</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>The name of the mediaclip must not contain the character &quot;:&quot;</source>
        <translation>Nama klip media tidak dapat berisikan karakter &quot;:&quot;</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="290"/>
        <source>Unsupported File</source>
        <translation>Tidak Ada Dukungan untuk Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="93"/>
        <source>Select Media</source>
        <translation>Pilih Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="123"/>
        <source>Load CD/DVD</source>
        <translation>Muat CD/DVD</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>Missing Media File</source>
        <translation>Berkas Media Hilang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="155"/>
        <source>The optical disc {name} is no longer available.</source>
        <translation>Disk optik {name} tidak lagi tersedia.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>The file {name} no longer exists.</source>
        <translation>File {name} tidak ada lagi.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="206"/>
        <source>Videos ({video});;Audio ({audio});;{files} (*)</source>
        <translation>Video ({video});;Audio ({audio});;{files} (*)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="216"/>
        <source>You must select a media file to delete.</source>
        <translation>Anda harus memilih sebuah berkas media untuk dihapus.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="233"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="237"/>
        <source>Show Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>Mediaclip already saved</source>
        <translation>Klip media telah tersimpan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>This mediaclip has already been saved</source>
        <translation>Klip media ini telah tersimpan sebelumnya</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="74"/>
        <source>Video:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="76"/>
        <source>Audio:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="95"/>
        <source>Live Media</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="96"/>
        <source>Stream Media Command</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="97"/>
        <source>VLC arguments</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="98"/>
        <source>Start Live items automatically</source>
        <translation>Mulai butir Tayang secara otomatis</translation>
    </message>
</context>
<context>
    <name>OpenLP</name>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>Data Directory Error</source>
        <translation>Kesalahan Direktori Data</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>OpenLP data folder was not found in:

{path}

The location of the data folder was previously changed from the OpenLP's default location. If the data was stored on removable device, that device needs to be made available.

You may reset the data location back to the default location, or you can try to make the current location available.

Do you want to reset to the default data location? If not, OpenLP will be closed so you can try to fix the the problem.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="238"/>
        <source>Backup</source>
        <translation>Pencadangan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="221"/>
        <source>OpenLP has been upgraded, do you want to create
a backup of the old data folder?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="232"/>
        <source>Backup of the data folder failed!</source>
        <translation>Pencadangan folder data gagal!</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="235"/>
        <source>A backup of the data folder has been created at:

{text}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings Upgrade</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="426"/>
        <source>Your settings are about to be upgraded. A backup will be created at {back_up_path}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings back up failed.

Continuining to upgrade.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/__init__.py" line="415"/>
        <source>Image Files</source>
        <translation>Berkas Gambar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Open</source>
        <translation>Buka</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="79"/>
        <source>Video Files</source>
        <translation>File-file Video</translation>
    </message>
</context>
<context>
    <name>OpenLP.AboutForm</name>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="103"/>
        <source>&lt;p&gt;OpenLP {{version}}{{revision}} - Open Source Lyrics Projection&lt;br&gt;Copyright {crs} 2004-{yr} OpenLP Developers&lt;/p&gt;&lt;p&gt;Find out more about OpenLP: &lt;a href=&quot;https://openlp.org/&quot;&gt;https://openlp.org/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with this program.  If not, see &lt;a href=&quot;https://www.gnu.org/licenses/&quot;&gt;https://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="118"/>
        <source>OpenLP is written and maintained by volunteers all over the world in their spare time. If you would like to see this project succeed, please consider contributing to it by clicking the &quot;contribute&quot; button below.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="122"/>
        <source>OpenLP would not be possible without the following software libraries:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="155"/>
        <source>&lt;h3&gt;Final credit:&lt;/h3&gt;&lt;blockquote&gt;&lt;p&gt;For God so loved the world that He gave His one and only Son, so that whoever believes in Him will not perish but inherit eternal life.&lt;/p&gt;&lt;p&gt;John 3:16&lt;/p&gt;&lt;/blockquote&gt;&lt;p&gt;And last but not least, final credit goes to God our Father, for sending His Son to die on the cross, setting us free from sin. We bring this software to you for free because He has set us free.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="164"/>
        <source>Credits</source>
        <translation>Kredit</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="744"/>
        <source>License</source>
        <translation>Lisensi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="746"/>
        <source>Contribute</source>
        <translation>Berkontribusi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutform.py" line="57"/>
        <source> build {version}</source>
        <translation>versi {version}</translation>
    </message>
</context>
<context>
    <name>OpenLP.AdvancedTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="59"/>
        <source>Advanced</source>
        <translation>Lanjutan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="252"/>
        <source>UI Settings</source>
        <translation>Setelan Antarmuka</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="253"/>
        <source>Data Location</source>
        <translation>Lokasi Data</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="254"/>
        <source>Number of recent service files to display:</source>
        <translation>Jumlah file Kebaktian terbaru untuk ditampilkan:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="255"/>
        <source>Open the last used Library tab on startup</source>
        <translation>Buka tab Library yang terakhir digunakan pada startup</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="257"/>
        <source>Double-click to send items straight to Live</source>
        <translation>Klik-ganda untuk langsung menayangkan butir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="259"/>
        <source>Preview items when clicked in Library</source>
        <translation>Pratinjau butir saat diklik di Perpustakaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="261"/>
        <source>Preview items when clicked in Service</source>
        <translation>Pratinjau butir saat diklik di Kebaktian</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="263"/>
        <source>Expand new service items on creation</source>
        <translation>Kembangkan butir Layanan baru saat dibuat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="265"/>
        <source>Max height for non-text slides
in slide controller:</source>
        <translation>Tinggi maksimal untuk slide non-teks
pada pengendali slide:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="267"/>
        <source>Disabled</source>
        <translation>Dimatikan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="268"/>
        <source>Automatic</source>
        <translation>Otomatis</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="269"/>
        <source>When changing slides:</source>
        <translation>Saat berganti slide:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="271"/>
        <source>Do not auto-scroll</source>
        <translation>Jangan auto-scroll</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="272"/>
        <source>Auto-scroll the previous slide into view</source>
        <translation>Auto-scroll slide sebelumnya ke bidang pandang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="274"/>
        <source>Auto-scroll the previous slide to top</source>
        <translation>Auto-scroll slide sebelumnya ke paling atas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="276"/>
        <source>Auto-scroll the previous slide to middle</source>
        <translation>Auto-scroll slide sebelumnya ke tengah
</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="278"/>
        <source>Auto-scroll the current slide into view</source>
        <translation>Auto-scroll slide ini ke bidang pandang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="280"/>
        <source>Auto-scroll the current slide to top</source>
        <translation>Auto-scroll slide ini ke paling atas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="282"/>
        <source>Auto-scroll the current slide to middle</source>
        <translation>Auto-scroll slide ini ke tengah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="284"/>
        <source>Auto-scroll the current slide to bottom</source>
        <translation>Auto-scroll slide ini ke paling bawah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="286"/>
        <source>Auto-scroll the next slide into view</source>
        <translation>Auto-scroll slide berikut ke bidang pandang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="288"/>
        <source>Auto-scroll the next slide to top</source>
        <translation>Auto-scroll slide berikut ke paling atas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="290"/>
        <source>Auto-scroll the next slide to middle</source>
        <translation>Auto-scroll slide berikut ke tengah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="292"/>
        <source>Auto-scroll the next slide to bottom</source>
        <translation>Auto-scroll slide berikut ke paling bawah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="294"/>
        <source>Enable application exit confirmation</source>
        <translation>Gunakan konfirmasi aplikasi keluar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="299"/>
        <source>Use dark style (needs restart)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="300"/>
        <source>Default Service Name</source>
        <translation>Nama Layanan Bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="301"/>
        <source>Enable default service name</source>
        <translation>Gunakan nama Layanan bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="302"/>
        <source>Date and Time:</source>
        <translation>Tanggal dan Waktu:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="303"/>
        <source>Monday</source>
        <translation>Senin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="304"/>
        <source>Tuesday</source>
        <translation>Selasa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="305"/>
        <source>Wednesday</source>
        <translation>Rabu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="306"/>
        <source>Thursday</source>
        <translation>Kamis</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="307"/>
        <source>Friday</source>
        <translation>Jumat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="308"/>
        <source>Saturday</source>
        <translation>Sabtu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="309"/>
        <source>Sunday</source>
        <translation>Minggu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="310"/>
        <source>Now</source>
        <translation>Sekarang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="311"/>
        <source>Time when usual service starts.</source>
        <translation>Saat Layanan biasa dimulai.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="312"/>
        <source>Name:</source>
        <translation>Nama:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="313"/>
        <source>Consult the OpenLP manual for usage.</source>
        <translation>Lihat panduan OpenLP untuk penggunaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="314"/>
        <source>Revert to the default service name &quot;{name}&quot;.</source>
        <translation>Kembalikan ke nama Layanan bawaan &quot;{name}&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="317"/>
        <source>Example:</source>
        <translation>Contoh:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="318"/>
        <source>Hide mouse cursor when over display window</source>
        <translation>Sembunyikan kursor tetikus saat melewati jendela tampilan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="319"/>
        <source>Path:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="320"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="321"/>
        <source>Cancel OpenLP data directory location change.</source>
        <translation>Batalkan perubahan lokasi direktori data OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="323"/>
        <source>Copy data to new location.</source>
        <translation>Salin data ke lokasi baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="324"/>
        <source>Copy the OpenLP data files to the new location.</source>
        <translation>Salin berkas data OpenLP ke lokasi baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="326"/>
        <source>&lt;strong&gt;WARNING:&lt;/strong&gt; New data directory location contains OpenLP data files.  These files WILL be replaced during a copy.</source>
        <translation>&lt;strong&gt;PERINGATAN:&lt;/strong&gt; Lokasi direktori data baru berisi berkas data OpenLP.  Berkas ini AKAN digantikan saat penyalinan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="329"/>
        <source>Display Workarounds</source>
        <translation>Tampilkan Solusi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="330"/>
        <source>Ignore Aspect Ratio</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="331"/>
        <source>Bypass X11 Window Manager</source>
        <translation>Abaikan Window Manager X11</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="332"/>
        <source>Use alternating row colours in lists</source>
        <translation>Gunakan warna baris dalam daftar secara bergantian</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="490"/>
        <source>Syntax error.</source>
        <translation>Kesalahan sintaks.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Confirm Data Directory Change</source>
        <translation>Konfirmasi Perubahan Direktori Data</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Are you sure you want to change the location of the OpenLP data directory to:

{path}

The data directory will be changed when OpenLP is closed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="559"/>
        <source>Overwrite Existing Data</source>
        <translation>Tulis-Timpa Data yang Ada</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>Restart Required</source>
        <translation>Butuh Dimulai-ulang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>This change will only take effect once OpenLP has been restarted.</source>
        <translation>Perubahan ini hanya akan diterapkan setelah OpenLP dimulai-ulang.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="186"/>
        <source>Select Logo File</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ColorButton</name>
    <message>
        <location filename="../../openlp/core/widgets/buttons.py" line="44"/>
        <source>Click to select a color.</source>
        <translation>Klik untuk memilih warna.</translation>
    </message>
</context>
<context>
    <name>OpenLP.DB</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="542"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="543"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="544"/>
        <source>Digital</source>
        <translation>Digital</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="545"/>
        <source>Storage</source>
        <translation>Penyimpanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="546"/>
        <source>Network</source>
        <translation>Jaringan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="547"/>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="551"/>
        <source>1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="552"/>
        <source>2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="553"/>
        <source>3</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="554"/>
        <source>4</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="555"/>
        <source>5</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="556"/>
        <source>6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="557"/>
        <source>7</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="558"/>
        <source>8</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="559"/>
        <source>9</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="560"/>
        <source>A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="561"/>
        <source>B</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="562"/>
        <source>C</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="563"/>
        <source>D</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="564"/>
        <source>E</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="565"/>
        <source>F</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="566"/>
        <source>G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="567"/>
        <source>H</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="568"/>
        <source>I</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="569"/>
        <source>J</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="570"/>
        <source>K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="571"/>
        <source>L</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="572"/>
        <source>M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="573"/>
        <source>N</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="574"/>
        <source>O</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="575"/>
        <source>P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="576"/>
        <source>Q</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="577"/>
        <source>R</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="578"/>
        <source>S</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="579"/>
        <source>T</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="580"/>
        <source>U</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="581"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="582"/>
        <source>W</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="583"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="584"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="585"/>
        <source>Z</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.DisplayWindow</name>
    <message>
        <location filename="../../openlp/core/display/window.py" line="121"/>
        <source>Display Window</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="94"/>
        <source>Error Occurred</source>
        <translation>Terjadi Kesalahan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="96"/>
        <source>&lt;strong&gt;Please describe what you were trying to do.&lt;/strong&gt; &amp;nbsp;If possible, write in English.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="99"/>
        <source>&lt;strong&gt;Oops, OpenLP hit a problem and couldn&apos;t recover!&lt;br&gt;&lt;br&gt;You can help &lt;/strong&gt; the OpenLP developers to &lt;strong&gt;fix this&lt;/strong&gt; by&lt;br&gt; sending them a &lt;strong&gt;bug report to {email}&lt;/strong&gt;{newlines}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="105"/>
        <source>{first_part}&lt;strong&gt;No email app? &lt;/strong&gt; You can &lt;strong&gt;save&lt;/strong&gt; this information to a &lt;strong&gt;file&lt;/strong&gt; and&lt;br&gt;send it from your &lt;strong&gt;mail on browser&lt;/strong&gt; via an &lt;strong&gt;attachment.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;&lt;strong&gt;Thank you&lt;/strong&gt; for being part of making OpenLP better!&lt;br&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="112"/>
        <source>Send E-Mail</source>
        <translation>Kirim Email</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="113"/>
        <source>Save to File</source>
        <translation>Simpan jadi Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="114"/>
        <source>Attach File</source>
        <translation>Lampirkan Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>Failed to Save Report</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>The following error occured when saving the report.

{exception}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="153"/>
        <source>&lt;strong&gt;Thank you for your description!&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="157"/>
        <source>&lt;strong&gt;Tell us what you were doing when this happened.&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="161"/>
        <source>&lt;strong&gt;Please enter a more detailed description of the situation&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionForm</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="77"/>
        <source>Platform: {platform}
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Save Crash Report</source>
        <translation>Simpan Laporan Crash</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Text files (*.txt *.log *.text)</source>
        <translation>Berkas teks (*.txt *.log *.text)</translation>
    </message>
</context>
<context>
    <name>OpenLP.FileRenameForm</name>
    <message>
        <location filename="../../openlp/core/ui/filerenamedialog.py" line="60"/>
        <source>New File Name:</source>
        <translation>Nama Berkas Baru:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="55"/>
        <source>File Copy</source>
        <translation>Salin Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="57"/>
        <source>File Rename</source>
        <translation>Penamaan-ulang Berkas</translation>
    </message>
</context>
<context>
    <name>OpenLP.FirstTimeLanguageForm</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="68"/>
        <source>Select Translation</source>
        <translation>Pilih Terjemahan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="69"/>
        <source>Choose the translation you&apos;d like to use in OpenLP.</source>
        <translation>Pilih terjemahan yang ingin Anda gunakan di OpenLP.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="71"/>
        <source>Translation:</source>
        <translation>Terjemahan:</translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>OpenLP.FirstTimeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="520"/>
        <source>Network Error</source>
        <translation>Kesalahan Jaringan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="177"/>
        <source>There was a network error attempting to connect to retrieve initial configuration information</source>
        <translation>Ada kesalahan jaringan saat mencoba menyambungkan untuk mengambil informasi konfigurasi awal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="184"/>
        <source>Downloading {name}...</source>
        <translation>Mengunduh {name}...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>Invalid index file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>OpenLP was unable to read the resource index file. Please try again later.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>Download Error</source>
        <translation>Kesalahan Unduhan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="417"/>
        <source>There was a connection problem during download, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation>Ada masalah sambungan saat pengunduhan, jadi unduhan berikutnya akan dilewatkan. Cobalah untuk menjalankan Wisaya Kali Pertama beberapa saat lagi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="429"/>
        <source>Setting Up And Downloading</source>
        <translation>Persiapan dan Pengunduhan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="430"/>
        <source>Please wait while OpenLP is set up and your data is downloaded.</source>
        <translation>Silakan tunggu selama OpenLP dipersiapkan dan data Anda diunduh.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="434"/>
        <source>Setting Up</source>
        <translation>Persiapan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="448"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="451"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="455"/>
        <source>Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="457"/>
        <source>Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>There was a connection problem while downloading, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation>Ada masalah sambungan saat mengunduh, jadi unduhan berikutnya akan dilewatkan. Cobalah untuk menjalankan Wisaya Kali Pertama beberapa saat lagi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="521"/>
        <source>Unable to download some files</source>
        <translation>Tidak dapat mengunduh beberapa berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="262"/>
        <source>First Time Wizard</source>
        <translation>Wisaya Kali Pertama</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="263"/>
        <source>Welcome to the First Time Wizard</source>
        <translation>Selamat datang di Wisaya Kali Pertama</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="265"/>
        <source>This wizard will help you to configure OpenLP for initial use. Click the &apos;{next_button}&apos; button below to start.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="269"/>
        <source>Internet Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="271"/>
        <source>Downloading Resource Index</source>
        <translation>Mengunduh Indeks Sumber</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="272"/>
        <source>Please wait while the resource index is downloaded.</source>
        <translation>Silakan tunggu selama indeks sumber diunduh.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="274"/>
        <source>Please wait while OpenLP downloads the resource index file...</source>
        <translation>Silakan tunggu selama OpenLP mengunduh berkas indeks sumber...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="276"/>
        <source>Select parts of the program you wish to use</source>
        <translation>Pilih bagian program yang ingin Anda gunakan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="277"/>
        <source>You can also change these settings after the Wizard.</source>
        <translation>Anda juga dapat mengubah pengaturan-pengaturan ini setelah Wizard ini.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="279"/>
        <source>Displays</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="280"/>
        <source>Choose the main display screen for OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="282"/>
        <source>Songs</source>
        <translation>Lagu</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="283"/>
        <source>Custom Slides – Easier to manage than songs and they have their own list of slides</source>
        <translation>Slide Kustom – lebih mudah dikelola dari lagu-lagu dan mereka punya daftar slide-nya sendiri</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="286"/>
        <source>Bibles – Import and show Bibles</source>
        <translation>Alkitab - Impor dan tampilkan Alkitab</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="287"/>
        <source>Images – Show images or replace background with them</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="289"/>
        <source>Presentations – Show .ppt, .odp and .pdf files</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="291"/>
        <source>Media – Playback of Audio and Video files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="292"/>
        <source>Song Usage Monitor</source>
        <translation>Pantauan Penggunaan Lagu</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="293"/>
        <source>Alerts – Display informative messages while showing other slides</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="295"/>
        <source>Resource Data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="296"/>
        <source>Can OpenLP download some resource data?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="297"/>
        <source>OpenLP has collected some resources that we have permission to distribute.

If you would like to download some of these resources click the &apos;{next_button}&apos; button, otherwise click the &apos;{finish_button}&apos; button.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="303"/>
        <source>No Internet Connection</source>
        <translation>Tidak Tersambung ke Internet</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="304"/>
        <source>Cannot connect to the internet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="305"/>
        <source>OpenLP could not connect to the internet to get information about the sample data available.

Please check your internet connection. If your church uses a proxy server click the 'Internet Settings' button below and enter the server details there.

Click the '{back_button}' button to try again.

If you click the &apos;{finish_button}&apos; button you can download the data at a later time by selecting &apos;Re-run First Time Wizard&apos; from the &apos;Tools&apos; menu in OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="314"/>
        <source>Sample Songs</source>
        <translation>Contoh Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="315"/>
        <source>Select and download public domain songs.</source>
        <translation>Pilih dan unduh lagu domain publik.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="316"/>
        <source>Sample Bibles</source>
        <translation>Contoh Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="317"/>
        <source>Select and download free Bibles.</source>
        <translation>Pilih dan unduh Alkitab gratis.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="319"/>
        <source>Sample Themes</source>
        <translation>Contoh Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="320"/>
        <source>Select and download sample themes.</source>
        <translation>Pilih dan unduh contoh tema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="321"/>
        <source>Default theme:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="322"/>
        <source>Select all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="323"/>
        <source>Deselect all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="324"/>
        <source>Downloading and Configuring</source>
        <translation>Pengunduhan dan Pengkonfigurasian</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="325"/>
        <source>Please wait while resources are downloaded and OpenLP is configured.</source>
        <translation>Silakan tunggu selama sumber-sumber diunduh dan OpenLP dikonfigurasikan.</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagDialog</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="111"/>
        <source>Configure Formatting Tags</source>
        <translation>Mengkonfigurasi Label Pemformatan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="114"/>
        <source>Default Formatting</source>
        <translation>Pemformatan Bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="125"/>
        <source>Description</source>
        <translation>Deskripsi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="126"/>
        <source>Tag</source>
        <translation>Label</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="127"/>
        <source>Start HTML</source>
        <translation>Mulai HTML</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="128"/>
        <source>End HTML</source>
        <translation>Akhiri HTML</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="124"/>
        <source>Custom Formatting</source>
        <translation>Pemformatan Kustom</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagForm</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="79"/>
        <source>Tag {tag} already defined.</source>
        <translation>Label {tag} telah terdefinisi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="81"/>
        <source>Description {tag} already defined.</source>
        <translation>Deskripsi {tag} telah terdefinisi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="148"/>
        <source>Start tag {tag} is not valid HTML</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="168"/>
        <source>End tag {end} does not match end tag for start tag {start}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="92"/>
        <source>New Tag {row:d}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="96"/>
        <source>&lt;HTML here&gt;</source>
        <translation>&lt;HTML here&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="199"/>
        <source>Validation Error</source>
        <translation>Kesalahan Validasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="179"/>
        <source>Description is missing</source>
        <translation>Deskripsi hilang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="182"/>
        <source>Tag is missing</source>
        <translation>Label hilang</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTags</name>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="63"/>
        <source>Red</source>
        <translation>Merah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="69"/>
        <source>Black</source>
        <translation>Hitam</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="75"/>
        <source>Blue</source>
        <translation>Biru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="81"/>
        <source>Yellow</source>
        <translation>Kuning</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="87"/>
        <source>Green</source>
        <translation>Hijau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="93"/>
        <source>Pink</source>
        <translation>Merah Muda</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="99"/>
        <source>Orange</source>
        <translation>Oranye</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="105"/>
        <source>Purple</source>
        <translation>Ungu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="111"/>
        <source>White</source>
        <translation>Putih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="117"/>
        <source>Superscript</source>
        <translation>Tulis-atas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="122"/>
        <source>Subscript</source>
        <translation>Tulis-bawah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="127"/>
        <source>Paragraph</source>
        <translation>Paragraf</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="132"/>
        <source>Bold</source>
        <translation>Tebal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="137"/>
        <source>Italics</source>
        <translation>Miring</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="141"/>
        <source>Underline</source>
        <translation>Garis Bawah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="147"/>
        <source>Break</source>
        <translation>Pisah</translation>
    </message>
</context>
<context>
    <name>OpenLP.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="296"/>
        <source>Experimental features (use at your own risk)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="334"/>
        <source>Service Item Slide Limits</source>
        <translation>Batasan Salindia Butir Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="335"/>
        <source>Behavior of next/previous on the last/first slide:</source>
        <translation>Aturan selanjutnya/sebelumnya dari salindia terakhir/pertama:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="336"/>
        <source>&amp;Remain on Slide</source>
        <translation>&amp;Tetap pada Salindia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="337"/>
        <source>&amp;Wrap around</source>
        <translation>&amp;Pindah ke Salindia selanjutnya/sebelumnya pada butir Layanan yang sama</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="338"/>
        <source>&amp;Move to next/previous service item</source>
        <translation>&amp;Pindah ke butir Layanan selanjutnya/sebelumnya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="160"/>
        <source>General</source>
        <translation>Umum</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="161"/>
        <source>Application Startup</source>
        <translation>Awal Mulai Aplikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="162"/>
        <source>Show blank screen warning</source>
        <translation>Tampilkan peringatan layar kosong</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="163"/>
        <source>Automatically open the previous service file</source>
        <translation>Buka file kebaktian terakhir otomatis</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="164"/>
        <source>Show the splash screen</source>
        <translation>Tampilkan layar pembuka</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="165"/>
        <source>Logo</source>
        <translation>Logo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="167"/>
        <source>Logo file:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="168"/>
        <source>Don&apos;t show logo on startup</source>
        <translation>Jangan tampilkan logo saat memulai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="169"/>
        <source>Check for updates to OpenLP</source>
        <translation>Periksa pembaruan untuk OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="170"/>
        <source>Application Settings</source>
        <translation>Setelan Aplikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="171"/>
        <source>Prompt to save before starting a new service</source>
        <translation>Sarankan untuk menyimpan sebelum memulai Layanan baru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="173"/>
        <source>Unblank display when changing slide in Live</source>
        <translation>Jangan kosongkan layar saat mengganti slide di Tayang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="175"/>
        <source>Unblank display when sending items to Live</source>
        <translation>Jangan kosongkan layar saat mengirim butir ke Tayang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="177"/>
        <source>Automatically preview the next item in service</source>
        <translation>Pratinjau butir berikut dari Layanan otomatis</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="179"/>
        <source>Timed slide interval:</source>
        <translation>Selang waktu salindia:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="180"/>
        <source> sec</source>
        <translation> dtk</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="181"/>
        <source>CCLI Details</source>
        <translation>Rincian CCLI</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="183"/>
        <source>SongSelect username:</source>
        <translation>Nama pengguna SongSelect:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="184"/>
        <source>SongSelect password:</source>
        <translation>Kata sandi SongSelect:</translation>
    </message>
</context>
<context>
    <name>OpenLP.LanguageManager</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Language</source>
        <translation>Bahasa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Please restart OpenLP to use your new language setting.</source>
        <translation>Silakan memulai-ulang OpenLP untuk menggunakan setelan bahasa baru.</translation>
    </message>
</context>
<context>
    <name>OpenLP.MainWindow</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="261"/>
        <source>English</source>
        <comment>Please add the name of your language here</comment>
        <translation>Bahasa Inggris</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="304"/>
        <source>General</source>
        <translation>Umum</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="361"/>
        <source>&amp;File</source>
        <translation>&amp;Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="362"/>
        <source>&amp;Import</source>
        <translation>&amp;Impor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="363"/>
        <source>&amp;Export</source>
        <translation>&amp;Ekspor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="364"/>
        <source>&amp;Recent Services</source>
        <translation>&amp;Layanan Sekarang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="365"/>
        <source>&amp;View</source>
        <translation>&amp;Tinjau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="366"/>
        <source>&amp;Layout Presets</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="367"/>
        <source>&amp;Tools</source>
        <translation>&amp;Alat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="368"/>
        <source>&amp;Settings</source>
        <translation>&amp;Setelan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="395"/>
        <source>&amp;Language</source>
        <translation>&amp;Bahasa</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="370"/>
        <source>&amp;Help</source>
        <translation>&amp;Bantuan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="371"/>
        <source>Library</source>
        <translation>Pustaka</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="372"/>
        <source>Service</source>
        <translation>Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="373"/>
        <source>Themes</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="374"/>
        <source>Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="375"/>
        <source>&amp;New Service</source>
        <translation>&amp;Layanan Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="378"/>
        <source>&amp;Open Service</source>
        <translation>&amp;Buka Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="380"/>
        <source>Open an existing service.</source>
        <translation>Buka Layanan yang ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="381"/>
        <source>&amp;Save Service</source>
        <translation>&amp;Simpan Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="383"/>
        <source>Save the current service to disk.</source>
        <translation>Simpan Layanan saat ini ke dalam disk.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="384"/>
        <source>Save Service &amp;As...</source>
        <translation>Simpan Layanan &amp;Sebagai...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="385"/>
        <source>Save Service As</source>
        <translation>Simpan Layanan Sebagai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="386"/>
        <source>Save the current service under a new name.</source>
        <translation>Simpan Layanan saat ini dengan nama baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="389"/>
        <source>Print the current service.</source>
        <translation>Cetak Layanan sekarang.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="390"/>
        <source>E&amp;xit</source>
        <translation>Kelua&amp;r</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="391"/>
        <source>Close OpenLP - Shut down the program.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="394"/>
        <source>&amp;Theme</source>
        <translation>&amp;Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="396"/>
        <source>Configure &amp;Shortcuts...</source>
        <translation>Mengkonfigurasi &amp;Pintasan...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="397"/>
        <source>Configure &amp;Formatting Tags...</source>
        <translation>Mengkonfigurasi Label Pem&amp;formatan...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="398"/>
        <source>&amp;Configure OpenLP...</source>
        <translation>&amp;Mengkonfigurasi OpenLP...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="399"/>
        <source>Export settings to a *.config file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="405"/>
        <source>Settings</source>
        <translation>Setelan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="402"/>
        <source>Import settings from a *.config file previously exported from this or another machine.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="406"/>
        <source>&amp;Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="407"/>
        <source>Hide or show Projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="408"/>
        <source>Toggle visibility of the Projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="410"/>
        <source>L&amp;ibrary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="411"/>
        <source>Hide or show the Library.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="412"/>
        <source>Toggle the visibility of the Library.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="414"/>
        <source>&amp;Themes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="415"/>
        <source>Hide or show themes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="416"/>
        <source>Toggle visibility of the Themes.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="418"/>
        <source>&amp;Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="419"/>
        <source>Hide or show Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="420"/>
        <source>Toggle visibility of the Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="422"/>
        <source>&amp;Preview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="423"/>
        <source>Hide or show Preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="424"/>
        <source>Toggle visibility of the Preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="426"/>
        <source>Li&amp;ve</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="427"/>
        <source>Hide or show Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="428"/>
        <source>L&amp;ock visibility of the panels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="429"/>
        <source>Lock visibility of the panels.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="430"/>
        <source>Toggle visibility of the Live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="431"/>
        <source>&amp;Manage Plugins</source>
        <translation>&amp;Kelola Plugin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="432"/>
        <source>You can enable and disable plugins from here.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="434"/>
        <source>&amp;About</source>
        <translation>Tent&amp;ang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="435"/>
        <source>More information about OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="436"/>
        <source>&amp;User Manual</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="438"/>
        <source>Jump to the search box of the current active plugin.</source>
        <translation>Pindah ke kotak pencarian plugin yang aktif saat ini.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="440"/>
        <source>&amp;Web Site</source>
        <translation>Situs &amp;Web</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="443"/>
        <source>Set the interface language to {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="445"/>
        <source>&amp;Autodetect</source>
        <translation>&amp;Deteksi Otomatis</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="446"/>
        <source>Use the system language, if available.</source>
        <translation>Gunakan bahasa sistem, jika tersedia.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="447"/>
        <source>Add &amp;Tool...</source>
        <translation>Tambahkan &amp;Alat...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="448"/>
        <source>Add an application to the list of tools.</source>
        <translation>Tambahkan sebuah aplikasi ke daftar alat.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="450"/>
        <source>Open &amp;Data Folder...</source>
        <translation>Buka &amp;Folder Data...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="451"/>
        <source>Open the folder where songs, bibles and other data resides.</source>
        <translation>Buka folder tempat lagu, Alkitab, dan data lain disimpan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="453"/>
        <source>Re-run First Time Wizard</source>
        <translation>Jalankan lagi Wisaya Kali Pertama</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="454"/>
        <source>Re-run the First Time Wizard, importing songs, Bibles and themes.</source>
        <translation>Jalankan lagi Wisaya Kali Pertama, mengimpor lagu, Alkitab, dan tema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="456"/>
        <source>Update Theme Images</source>
        <translation>Perbarui Gambar Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="457"/>
        <source>Update the preview images for all themes.</source>
        <translation>Perbarui gambar pratinjau untuk semua tema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="459"/>
        <source>&amp;Show all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="460"/>
        <source>Reset the interface back to the default layout and show all the panels.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="462"/>
        <source>&amp;Setup</source>
        <translation>&amp;Persiapan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="463"/>
        <source>Use layout that focuses on setting up the Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="465"/>
        <source>&amp;Live</source>
        <translation>&amp;Tayang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="466"/>
        <source>Use layout that focuses on Live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="548"/>
        <source>Waiting for some things to finish...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="550"/>
        <source>Please Wait</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="631"/>
        <source>Version {new} of OpenLP is now available for download (you are currently running version {current}). 

You can download the latest version from https://openlp.org/.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="634"/>
        <source>OpenLP Version Updated</source>
        <translation>Versi OpenLP Terbarui</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Re-run First Time Wizard?</source>
        <translation>Jalankan lagi Wisaya Kali Pertama?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Are you sure you want to re-run the First Time Wizard?

Re-running this wizard may make changes to your current OpenLP configuration and possibly add songs to your existing songs list and change your default theme.</source>
        <translation>Anda yakin ingin menjalankan lagi Wisaya Kali Pertama?

Menjalankan lagi wisaya ini mungkin akan mengubah konfigurasi OpenLP saat ini dan mungkin menambah lagu ke dalam daftar lagu dan mengubah tema bawaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>OpenLP Main Display Blanked</source>
        <translation>Tampilan Utama OpenLP Kosong</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>The Main Display has been blanked out</source>
        <translation>Tampilan Utama telah dikosongkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Import settings?</source>
        <translation>Impor setelan?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Are you sure you want to import settings?

 Importing settings will make permanent changes to your current OpenLP configuration.

 Importing incorrect settings may cause erratic behaviour or OpenLP to terminate abnormally.</source>
        <translation>Anda yakin ingin meng-impor setelan?

 Mengimpor setelan akan membuat perubahan permanen pada konfigurasi OpenLP Anda saat ini.

 Mengimpor setelan yang salah dapat menyebabkan perilaku tak menentu atau OpenLP berakhir secara tidak wajar.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>Import settings</source>
        <translation>Impor setelan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>OpenLP Settings (*.conf)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="889"/>
        <source>The file you have selected does not appear to be a valid OpenLP settings file.

Processing has terminated and no changes have been made.</source>
        <translation>Berkas yang Anda pilih nampaknya berkas setelan OpenLP yang valid.

Proses telah dihentikan dan tidak ada perubahan yang telah dibuat.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>OpenLP will now close.  Imported settings will be applied the next time you start OpenLP.</source>
        <translation>OpenLP sekarang akan ditutup.  Setelan yang di-impor akan diterapkan saat berikutnya Anda memulai OpenLP.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>Export Settings File</source>
        <translation>Ekspor Berkas Setelan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>Export setting error</source>
        <translation>Kesalahan setelan ekspor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>An error occurred while exporting the settings: {err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Exit OpenLP</source>
        <translation>Keluar dari OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Are you sure you want to exit OpenLP?</source>
        <translation>Anda yakin ingin keluar dari OpenLP?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1042"/>
        <source>&amp;Exit OpenLP</source>
        <translation>&amp;Keluar dari OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1108"/>
        <source>Default Theme: {theme}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear List</source>
        <comment>Clear List of recent files</comment>
        <translation>Bersihkan Daftar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear the list of recent files.</source>
        <translation>Bersihkan daftar berkas terbaru.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1345"/>
        <source>Copying OpenLP data to new data directory location - {path} - Please wait for copy to finish</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1353"/>
        <source>OpenLP Data directory copy failed

{err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1355"/>
        <source>New Data Directory Error</source>
        <translation>Kesalahan Direktori Data Baru</translation>
    </message>
</context>
<context>
    <name>OpenLP.Manager</name>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>Database Error</source>
        <translation>Kesalahan Basis-Data</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="157"/>
        <source>OpenLP cannot load your database.

Database: {db}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>The database being loaded was created in a more recent version of OpenLP. The database is version {db_ver}, while OpenLP expects version {db_up}. The database will not be loaded.

Database: {db_name}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.MediaController</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="100"/>
        <source>The media integration library is missing (python - vlc is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="102"/>
        <source>The media integration library is missing (python - pymediainfo is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="122"/>
        <source>No Displays have been configured, so Live Media has been disabled</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.MediaManagerItem</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="382"/>
        <source>No Items Selected</source>
        <translation>Tidak Ada Satupun Butir Terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="161"/>
        <source>&amp;Add to selected Service Item</source>
        <translation>&amp;Tambahkan ke Butir Layanan terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File Type</source>
        <translation>Jenis Berkas Tidak Valid</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File {file_path}.
File extension not supported</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="391"/>
        <source>Duplicate files were found on import and were ignored.</source>
        <translation>Berkas duplikat ditemukan saat impor dan diabaikan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="496"/>
        <source>You must select one or more items to preview.</source>
        <translation>Anda harus memilih satu atau beberapa butir untuk dipratinjau.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="514"/>
        <source>You must select one or more items to send live.</source>
        <translation>Anda harus memilih satu atau beberapa butir untuk ditayangkan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="563"/>
        <source>You must select one or more items to add.</source>
        <translation>Anda harus memilih satu atau lebih butir untuk menambahkan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="607"/>
        <source>You must select one or more items.</source>
        <translation>Anda harus memilih satu atau beberapa butir.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="614"/>
        <source>You must select an existing service item to add to.</source>
        <translation>Anda harus memilih sebuah Butir Layanan yang ada untuk ditambahkan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>Invalid Service Item</source>
        <translation>Butir Layanan Tidak Valid</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>You must select a {title} service item.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="119"/>
        <source>&amp;Clone</source>
        <translation>&amp;Kloning</translation>
    </message>
</context>
<context>
    <name>OpenLP.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="51"/>
        <source>Media</source>
        <translation>Media</translation>
    </message>
</context>
<context>
    <name>OpenLP.OpenLyricsImportError</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="707"/>
        <source>&lt;lyrics&gt; tag is missing.</source>
        <translation>Label &lt;lyrics&gt; hilang.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="712"/>
        <source>&lt;verse&gt; tag is missing.</source>
        <translation>Label &lt;verse&gt; hilang.</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="494"/>
        <source>Fan</source>
        <translation>Kipas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="495"/>
        <source>Lamp</source>
        <translation>Lampu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="496"/>
        <source>Temperature</source>
        <translation>Suhu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="497"/>
        <source>Cover</source>
        <translation>Penutup</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="498"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="430"/>
        <source>No message</source>
        <translation>Tak ada pesan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="759"/>
        <source>Error while sending data to projector</source>
        <translation>Terjadi kesalahan saat mengirim data ke proyektor</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLinkConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="166"/>
        <source>Acknowledge a PJLink SRCH command - returns MAC address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="171"/>
        <source>Blank/unblank video and/or mute audio.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="176"/>
        <source>Query projector PJLink class support.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="181"/>
        <source>Query error status from projector. Returns fan/lamp/temp/cover/filter/other error status.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="187"/>
        <source>Query number of hours on filter.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="192"/>
        <source>Freeze or unfreeze current image being projected.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="197"/>
        <source>Query projector manufacturer name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="202"/>
        <source>Query projector product name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="207"/>
        <source>Query projector for other information set by manufacturer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="212"/>
        <source>Query specified input source name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="217"/>
        <source>Switch to specified video source.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="222"/>
        <source>Query available input sources.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="227"/>
        <source>Query current input resolution.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="232"/>
        <source>Query lamp time and on/off status. Multiple lamps supported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="237"/>
        <source>UDP Status - Projector is now available on network. Includes MAC address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="242"/>
        <source>Adjust microphone volume by 1 step.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="247"/>
        <source>Query customer-set projector name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="252"/>
        <source>Initial connection with authentication/no authentication request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="257"/>
        <source>Turn lamp on or off/standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="262"/>
        <source>Query replacement air filter model number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="267"/>
        <source>Query replacement lamp model number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="272"/>
        <source>Query recommended resolution.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="277"/>
        <source>Query projector serial number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="282"/>
        <source>UDP broadcast search request for available projectors. Reply is ACKN.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="287"/>
        <source>Query projector software version number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="292"/>
        <source>Adjust speaker volume by 1 step.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PathEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="299"/>
        <source>Browse for directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="300"/>
        <source>Revert to default directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="302"/>
        <source>Browse for file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="303"/>
        <source>Revert to default file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="317"/>
        <source>Select Directory</source>
        <translation>Pilih Direktori</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="321"/>
        <source>Select File</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PluginForm</name>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="74"/>
        <source>Manage Plugins</source>
        <translation>Kelola Plugin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="75"/>
        <source>Plugin Details</source>
        <translation>Rincian Plugin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="77"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="78"/>
        <source>Active</source>
        <translation>Aktif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="149"/>
        <source>{name} (Disabled)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="145"/>
        <source>{name} (Active)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="147"/>
        <source>{name} (Inactive)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PluginManager</name>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="173"/>
        <source>Unable to initialise the following plugins:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="179"/>
        <source>See the log file for more details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceDialog</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Page</source>
        <translation>Pas dengan Halaman</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Width</source>
        <translation>Pas dengan Lebar</translation>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceForm</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="61"/>
        <source>Print</source>
        <translation>Cetak</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="69"/>
        <source>Copy as Text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="71"/>
        <source>Copy as HTML</source>
        <translation>Salin sebagai HTML</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="137"/>
        <source>Zoom Out</source>
        <translation>Perkecil</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="138"/>
        <source>Zoom Original</source>
        <translation>Kembalikan Ukuran</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="139"/>
        <source>Zoom In</source>
        <translation>Perbesar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="140"/>
        <source>Options</source>
        <translation>Opsi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="141"/>
        <source>Title:</source>
        <translation>Judul</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="142"/>
        <source>Service Note Text:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="143"/>
        <source>Other Options</source>
        <translation>Opsi Lain</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="144"/>
        <source>Include slide text if available</source>
        <translation>Sertakan teks salindia jika tersedia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="145"/>
        <source>Add page break before each text item</source>
        <translation>Tambahkan pemisah sebelum tiap butir teks</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="146"/>
        <source>Include service item notes</source>
        <translation>Masukkan catatan butir Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="147"/>
        <source>Include play length of media items</source>
        <translation>Masukkan durasi permainan butir media</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="148"/>
        <source>Show chords</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="149"/>
        <source>Service Sheet</source>
        <translation>Lembar Layanan</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="401"/>
        <source>The address specified with socket.bind() is already in use and was set to be exclusive</source>
        <translation>Alamat yang ditentukan dengan socket.bind() sedang digunakan dan telah disetel eksklusif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="404"/>
        <source>PJLink returned &quot;ERRA: Authentication Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="405"/>
        <source>The connection was refused by the peer (or timed out)</source>
        <translation>Sambungan ditolak oleh peer (atau melampaui batas waktu)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="407"/>
        <source>Projector cover open detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="408"/>
        <source>PJLink class not supported</source>
        <translation>Tidak ada dukungan untuk class PJLink</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="409"/>
        <source>The datagram was larger than the operating system&apos;s limit</source>
        <translation>Datagram lebih besar dari batas yang dimiliki sistem operasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="411"/>
        <source>Error condition detected</source>
        <translation>Kondisi kesalahan terdeteksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="412"/>
        <source>Projector fan error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="413"/>
        <source>Projector check filter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="414"/>
        <source>General projector error</source>
        <translation>Kesalahan proyektor secara umum</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="415"/>
        <source>The host address was not found</source>
        <translation>Alamat host tidak ditemukan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="416"/>
        <source>PJLink invalid packet received</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="417"/>
        <source>Projector lamp error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="418"/>
        <source>An error occurred with the network (Possibly someone pulled the plug?)</source>
        <translation>Terjadi kesalahan pada jaringan (Mungkin seseorang mencabut sambungannya?)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="420"/>
        <source>PJlink authentication Mismatch Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="421"/>
        <source>Projector not connected error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="422"/>
        <source>PJLink returned &quot;ERR2: Invalid Parameter&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="423"/>
        <source>PJLink Invalid prefix character</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="424"/>
        <source>PJLink returned &quot;ERR4: Projector/Display Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="425"/>
        <source>The socket is using a proxy, and the proxy requires authentication</source>
        <translation>Socket tersebut menggunakan sebuah proxy yang membutuhkan otentikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="428"/>
        <source>The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established)</source>
        <translation>Sambungan ke server proxy ditutup secara tak terduga (sebelum sambungan ke peer akhir terjadi)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="431"/>
        <source>Could not contact the proxy server because the connection to that server was denied</source>
        <translation>Tidak dapat menghubungi server proxy karena sambungan ke server tersebut ditolak</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="434"/>
        <source>The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase.</source>
        <translation>Sambungan ke server proxy telah melampaui batas waktu atau server proxy berhenti merespon saat tahapan otentikasi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="437"/>
        <source>The proxy address set with setProxy() was not found</source>
        <translation>Alamat proxy yang telah ditentukan dengan setProxy() tidak ditemukan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="439"/>
        <source>The connection negotiation with the proxy server failed because the response from the proxy server could not be understood</source>
        <translation>Negosiasi sambungan dengan server proxy gagal karena respon dari server proxy tidak dapat dipahami</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="442"/>
        <source>The remote host closed the connection</source>
        <translation>Host remote telah menutup sambungan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="444"/>
        <source>The SSL/TLS handshake failed</source>
        <translation>Proses negosiasi SSL/TLS gagal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="446"/>
        <source>The address specified to socket.bind() does not belong to the host</source>
        <translation>Alamat yang ditentukan dengan socket.bind() bukanlah milik host</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="449"/>
        <source>The socket operation failed because the application lacked the required privileges</source>
        <translation>Pengoperasian socket gagal karena aplikasi tidak memiliki hak khusus yang dibutuhkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="452"/>
        <source>The local system ran out of resources (e.g., too many sockets)</source>
        <translation>Sistem lokal telah kehabisan sumber daya (mis. terlalu banyak socket)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="454"/>
        <source>The socket operation timed out</source>
        <translation>Pengoperasian socket telah melampaui batas waktu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="456"/>
        <source>Projector high temperature detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="457"/>
        <source>PJLink returned &quot;ERR3: Busy&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="458"/>
        <source>PJLink returned &quot;ERR1: Undefined Command&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="459"/>
        <source>The last operation attempted has not finished yet (still in progress in the background)</source>
        <translation>Pengoperasian terakhir masih diupayakan dan belum selesai (masih berlangsung di latar sistem)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="462"/>
        <source>Unknown condiction detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="463"/>
        <source>An unidentified socket error occurred</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="464"/>
        <source>The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support)</source>
        <translation>Pengoperasian socket yang diminta tidak didukung oleh sistem operasi lokal (mis. tiada dukungan IPv6)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="467"/>
        <source>Warning condition detected</source>
        <translation>Kondisi yang perlu diperhatikan terdeteksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="468"/>
        <source>Connection initializing with pin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="469"/>
        <source>Socket is bount to an address or port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="470"/>
        <source>Connection initializing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="471"/>
        <source>Socket is about to close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="472"/>
        <source>Connected</source>
        <translation>Tersambung</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="473"/>
        <source>Connecting</source>
        <translation>Menyambungkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="474"/>
        <source>Cooldown in progress</source>
        <translation>Pendinginan sedang berlangsung</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="475"/>
        <source>Command returned with OK</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="476"/>
        <source>Performing a host name lookup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="477"/>
        <source>Projector Information available</source>
        <translation>Informasi Proyektor tersedia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="478"/>
        <source>Initialize in progress</source>
        <translation>Inisialisasi sedang berlangsung</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="479"/>
        <source>Socket it listening (internal use only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="480"/>
        <source>No network activity at this time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="481"/>
        <source>Received data</source>
        <translation>Data yang diterima</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="482"/>
        <source>Sending data</source>
        <translation>Mengirim data</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="483"/>
        <source>Not Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="484"/>
        <source>Off</source>
        <translation>Padam</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="485"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="486"/>
        <source>Power is on</source>
        <translation>Daya sedang hidup</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="487"/>
        <source>Power in standby</source>
        <translation>Daya sedang siaga</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="488"/>
        <source>Getting status</source>
        <translation>Mendapatkan status</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="489"/>
        <source>Warmup in progress</source>
        <translation>Pemanasan sedang berlangsung</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEdit</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>Name Not Set</source>
        <translation>Nama Belum Ditetapkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>You must enter a name for this entry.&lt;br /&gt;Please enter a new name for this entry.</source>
        <translation>Anda harus masukkan sebuah nama untuk entri ini.&lt;br /&gt;Silakan masukkan sebuah nama untuk entri ini.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="196"/>
        <source>Duplicate Name</source>
        <translation>Duplikasi Nama</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEditForm</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="115"/>
        <source>Add New Projector</source>
        <translation>Tambahkan Proyektor Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="118"/>
        <source>Edit Projector</source>
        <translation>Sunting Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="120"/>
        <source>IP Address</source>
        <translation>Alamat IP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="123"/>
        <source>Port Number</source>
        <translation>Nomor Port</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="125"/>
        <source>PIN</source>
        <translation>PIN</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="127"/>
        <source>Name</source>
        <translation>Nama</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="129"/>
        <source>Location</source>
        <translation>Lokasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="131"/>
        <source>Notes</source>
        <translation>Catatan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>Database Error</source>
        <translation>Kesalahan Basis-Data</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>There was an error saving projector information. See the log for the error</source>
        <translation>Terjadi kesalahan saat menyimpan informasi proyektor. Lihatlah log untuk kesalahan tersebut</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorManager</name>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add Projector</source>
        <translation>Tambahkan Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add a new projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit Projector</source>
        <translation>Sunting Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete Projector</source>
        <translation>Hapus Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Select Input Source</source>
        <translation>Pilih Sumber Masukan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Choose input source on selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View Projector</source>
        <translation>Tinjau Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View selected projector information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="100"/>
        <source>Connect to selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors</source>
        <translation>Sambungkan ke proyektor-proyektor terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projectors</source>
        <translation>Putus sambungan dari proyektor-proyektor terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projector</source>
        <translation>Putus sambungan dari proyektor terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projector</source>
        <translation>Hidupkan proyektor terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="129"/>
        <source>Power on selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Standby selected projector</source>
        <translation>Siagakan proyektor terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="143"/>
        <source>Put selected projector in standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Put selected projectors in standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="156"/>
        <source>Blank selected projector screen</source>
        <translation>Kosongkan layar proyektor terplih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projector screen</source>
        <translation>Tampilkan layar proyektor terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="170"/>
        <source>Show selected projector screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projectors screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="199"/>
        <source>&amp;View Projector Information</source>
        <translation>&amp;Tinjau Informasi Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="204"/>
        <source>&amp;Edit Projector</source>
        <translation>&amp;Sunting Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="210"/>
        <source>&amp;Connect Projector</source>
        <translation>&amp;Sambungkan Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="215"/>
        <source>D&amp;isconnect Projector</source>
        <translation>&amp;Putus-sambungan Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="221"/>
        <source>Power &amp;On Projector</source>
        <translation>Hidupkan &amp;Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="226"/>
        <source>Power O&amp;ff Projector</source>
        <translation>Padamkan &amp;Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="232"/>
        <source>Select &amp;Input</source>
        <translation>Pilih &amp;Masukan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="237"/>
        <source>Edit Input Source</source>
        <translation>Sunting Sumber Masukan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="242"/>
        <source>&amp;Blank Projector Screen</source>
        <translation>&amp;Kosongkan Layar Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="247"/>
        <source>&amp;Show Projector Screen</source>
        <translation>&amp;Tampilkan Layar Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="253"/>
        <source>&amp;Delete Projector</source>
        <translation>&amp;Hapus Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="503"/>
        <source>Are you sure you want to delete this projector?</source>
        <translation>Anda yakin ingin menghapus proyektor ini?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="648"/>
        <source>Name</source>
        <translation>Nama</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="650"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="652"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="654"/>
        <source>Notes</source>
        <translation>Catatan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="658"/>
        <source>Projector information not available at this time.</source>
        <translation>Informasi proyektor saat ini tidak tersedia.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="660"/>
        <source>Projector Name</source>
        <translation>Nama Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="663"/>
        <source>Manufacturer</source>
        <translation>Pembuat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="665"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="667"/>
        <source>PJLink Class</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="670"/>
        <source>Software Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="673"/>
        <source>Serial Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="676"/>
        <source>Lamp Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="679"/>
        <source>Filter Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="682"/>
        <source>Other info</source>
        <translation>Info lainnya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="685"/>
        <source>Power status</source>
        <translation>Status daya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Shutter is</source>
        <translation>Posisi shutter:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Closed</source>
        <translation>Tutup</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="691"/>
        <source>Current source input is</source>
        <translation>Sumber masukan saat ini adalah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="698"/>
        <source>Unavailable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="700"/>
        <source>ON</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="702"/>
        <source>OFF</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="703"/>
        <source>Lamp</source>
        <translation>Lampu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="708"/>
        <source>Hours</source>
        <translation>Hitungan jam</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="713"/>
        <source>No current errors or warnings</source>
        <translation>Tidak ada kesalahan atau peringatan pada saat ini</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="715"/>
        <source>Current errors/warnings</source>
        <translation>Kesalahan/peringatan saat ini</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="718"/>
        <source>Projector Information</source>
        <translation>Informasi Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="941"/>
        <source>Authentication Error</source>
        <translation>Kesalahan Otentikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="956"/>
        <source>No Authentication Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="998"/>
        <source>Not Implemented Yet</source>
        <translation>Belum Diimplementasikan</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="52"/>
        <source>Projector</source>
        <translation>Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="111"/>
        <source>Communication Options</source>
        <translation>Opsi Komunikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="113"/>
        <source>Connect to projectors on startup</source>
        <translation>Sambungkan ke proyektor saat memulai OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="115"/>
        <source>Socket timeout (seconds)</source>
        <translation>Batas waktu socket (detik)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="117"/>
        <source>Poll time (seconds)</source>
        <translation>Waktu Poll (detik)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="121"/>
        <source>Tabbed dialog box</source>
        <translation>Kotak dialog multi-tab</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="123"/>
        <source>Single dialog box</source>
        <translation>Kotak dialog tunggal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="125"/>
        <source>Connect to projector when LINKUP received (v2 only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="127"/>
        <source>Enable listening for PJLink2 broadcast messages</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorWizard</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="216"/>
        <source>Duplicate IP Address</source>
        <translation>Duplikasi Alamat IP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="224"/>
        <source>Invalid IP Address</source>
        <translation>Alamat IP Tidak Valid</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="233"/>
        <source>Invalid Port Number</source>
        <translation>Nomor Port Tidak Valid</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProxyDialog</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="177"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProxyWidget</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="116"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="117"/>
        <source>No prox&amp;y</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="118"/>
        <source>&amp;Use system proxy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="119"/>
        <source>&amp;Manual proxy configuration</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="120"/>
        <source>e.g. proxy_server_address:port_no</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="121"/>
        <source>HTTP:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="123"/>
        <source>HTTPS:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="125"/>
        <source>Username:</source>
        <translation>Nama Pengguna:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="126"/>
        <source>Password:</source>
        <translation>Kata sandi:</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreenList</name>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="254"/>
        <source>Screen</source>
        <translation>Layar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="257"/>
        <source>primary</source>
        <translation>Utama</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreensTab</name>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="44"/>
        <source>Screens</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="69"/>
        <source>Generic screen settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="70"/>
        <source>Display if a single screen</source>
        <translation>Tampilkan jika layar tunggal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="297"/>
        <source>F&amp;ull screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="298"/>
        <source>Width:</source>
        <translation>Lebar:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="299"/>
        <source>Use this screen as a display</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="302"/>
        <source>Left:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="303"/>
        <source>Custom &amp;geometry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="304"/>
        <source>Top:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="305"/>
        <source>Height:</source>
        <translation>Tinggi:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="306"/>
        <source>Identify Screens</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItem</name>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="289"/>
        <source>[slide {frame:d}]</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="590"/>
        <source>&lt;strong&gt;Start&lt;/strong&gt;: {start}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="594"/>
        <source>&lt;strong&gt;Length&lt;/strong&gt;: {length}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItemEditForm</name>
    <message>
        <location filename="../../openlp/core/ui/serviceitemeditdialog.py" line="70"/>
        <source>Reorder Service Item</source>
        <translation>Susun-Ulang Butir Layanan</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceManager</name>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="199"/>
        <source>Service Notes: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="246"/>
        <source>Notes: </source>
        <translation>Catatan :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="254"/>
        <source>Playing time: </source>
        <translation>Waktu permainan:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="148"/>
        <source>Load an existing service.</source>
        <translation>Muat Layanan yang ada saat ini.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="152"/>
        <source>Save this service.</source>
        <translation>Simpan Layanan ini.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="162"/>
        <source>Select a theme for the service.</source>
        <translation>Pilih suatu tema untuk Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move to &amp;top</source>
        <translation>Pindahkan ke &amp;puncak</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move item to the top of the service.</source>
        <translation>Pindahkan butir ke puncak daftar Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move &amp;up</source>
        <translation>Naikkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move item up one position in the service.</source>
        <translation>Naikkan butir satu posisi pada daftar Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move &amp;down</source>
        <translation>Turunkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move item down one position in the service.</source>
        <translation>Turunkan butir satu posisi pada daftar Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move to &amp;bottom</source>
        <translation>Pindahkan ke &amp;dasar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move item to the end of the service.</source>
        <translation>Pindahkan butir ke akhir daftar Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>&amp;Delete From Service</source>
        <translation>&amp;Hapus dari Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>Delete the selected item from the service.</source>
        <translation>Hapus butir terpilih dari Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>&amp;Expand all</source>
        <translation>&amp;Kembangkan semua</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>Expand all the service items.</source>
        <translation>Kembangkan seluruh butir Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>&amp;Collapse all</source>
        <translation>&amp;Kempiskan semua</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>Collapse all the service items.</source>
        <translation>Kempiskan seluruh butir Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Go Live</source>
        <translation>Tayangkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Send the selected item to Live.</source>
        <translation>Tayangkan butir terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="241"/>
        <source>&amp;Add New Item</source>
        <translation>&amp;Tambahkan Butir Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="243"/>
        <source>&amp;Add to Selected Item</source>
        <translation>&amp;Tambahkan ke Butir Terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="247"/>
        <source>&amp;Edit Item</source>
        <translation>&amp;Sunting Butir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="249"/>
        <source>&amp;Rename...</source>
        <translation>&amp;Namai-ulang...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="252"/>
        <source>&amp;Reorder Item</source>
        <translation>&amp;Susun-Ulang Butir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="255"/>
        <source>&amp;Notes</source>
        <translation>&amp;Catatan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="258"/>
        <source>&amp;Start Time</source>
        <translation>&amp;Waktu Mulai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="265"/>
        <source>Create New &amp;Custom Slide</source>
        <translation>Buat Salindia &amp;Kustom Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="272"/>
        <source>&amp;Auto play slides</source>
        <translation>&amp;Mainkan otomatis salindia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="276"/>
        <source>Auto play slides &amp;Loop</source>
        <translation>Mainkan otomatis salindia &amp;Berulang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="281"/>
        <source>Auto play slides &amp;Once</source>
        <translation>Mainkan otomatis salindia &amp;Sekali</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="812"/>
        <source>&amp;Delay between slides</source>
        <translation>&amp;Penundaan antar salindia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="292"/>
        <source>Show &amp;Preview</source>
        <translation>Tampilkan &amp;Pratinjau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="297"/>
        <source>&amp;Change Item Theme</source>
        <translation>&amp;Ubah Tema Butir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="371"/>
        <source>Untitled Service</source>
        <translation>Layanan Tanpa Judul</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>Open File</source>
        <translation>Membuka Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>OpenLP Service Files (*.osz *.oszl)</source>
        <translation>Berkas Layanan OpenLP (*.osz *.oszl)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>Modified Service</source>
        <translation>Layanan Termodifikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>The current service has been modified. Would you like to save this service?</source>
        <translation>Layanan sekarang telah termodifikasi. Ingin menyimpan Layanan ini?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="572"/>
        <source>Service File(s) Missing</source>
        <translation>(Beberapa) Berkas Layanan Hilang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="573"/>
        <source>The following file(s) in the service are missing: {name}

These files will be removed if you continue to save.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>Error Saving File</source>
        <translation>Kesalahan Saat Menyimpan Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>There was an error saving your file.

{error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="653"/>
        <source>OpenLP Service Files - lite (*.oszl)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="654"/>
        <source>OpenLP Service Files (*.osz)</source>
        <translation>Berkas Layanan OpenLP (*.osz)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="726"/>
        <source>The service file {file_path} could not be loaded because it is either corrupt, inaccessible, or not a valid OpenLP 2 or OpenLP 3 service file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="823"/>
        <source>&amp;Auto Start - active</source>
        <translation>&amp;Mulai Otomatis - aktif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="827"/>
        <source>&amp;Auto Start - inactive</source>
        <translation>&amp;Mulai Otomatis - nonaktif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Input delay</source>
        <translation>Penundaan masukan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Delay between slides in seconds.</source>
        <translation>Penundaan antar salindia dalam hitungan detik.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1220"/>
        <source>Edit</source>
        <translation>Sunting</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1221"/>
        <source>Service copy only</source>
        <translation>Salinan Layanan saja</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1224"/>
        <source>Slide theme</source>
        <translation>Tema salindia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1228"/>
        <source>Notes</source>
        <translation>Catatan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Missing Display Handler</source>
        <translation>Penangan Tayang Hilang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1399"/>
        <source>Your item cannot be displayed as there is no handler to display it</source>
        <translation>Butir tidak dapat ditayangkan karena tidak ada penangan untuk menayangkannya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Your item cannot be displayed as the plugin required to display it is missing or inactive</source>
        <translation>Butir ini tidak dapat ditampilkan karena plugin yang dibutuhkan untuk menampilkannya hilang atau nonaktif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Rename item title</source>
        <translation>Namai-ulang judul butir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Title:</source>
        <translation>Judul</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceNoteForm</name>
    <message>
        <location filename="../../openlp/core/ui/servicenoteform.py" line="72"/>
        <source>Service Item Notes</source>
        <translation>Catatan Butir Layanan</translation>
    </message>
</context>
<context>
    <name>OpenLP.SettingsForm</name>
    <message>
        <location filename="../../openlp/core/ui/settingsdialog.py" line="62"/>
        <source>Configure OpenLP</source>
        <translation>Mengkonfigurasi OpenLP</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListDialog</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="138"/>
        <source>Configure Shortcuts</source>
        <translation>Mengkonfigurasi Pintasan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="139"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>Pilih suatu aksi dan klik salah satu tombol di bawah ini untuk menentukan, berturut-turut, pintasan utama atau pintasan alternatif yang baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Action</source>
        <translation>Aksi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Shortcut</source>
        <translation>Pintasan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Alternate</source>
        <translation>Alternatif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="145"/>
        <source>Default</source>
        <translation>Bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="146"/>
        <source>Custom</source>
        <translation>Kustom</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="148"/>
        <source>Capture shortcut.</source>
        <translation>Menentukan pintasan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="151"/>
        <source>Restore the default shortcut of this action.</source>
        <translation>Mengembalikan pintasan bawaan untuk aksi ini.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Restore Default Shortcuts</source>
        <translation>Mengembalikan Pintasan Bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Do you want to restore all shortcuts to their defaults?</source>
        <translation>Apakah Anda ingin mengembalikan semua pintasan ke bawaan-nya masing-masing?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="437"/>
        <source>The shortcut &quot;{key}&quot; is already assigned to another action,
please use a different shortcut.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="441"/>
        <source>Duplicate Shortcut</source>
        <translation>Duplikasi Pintasan</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListForm</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an Action</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>Pilih suatu aksi dan klik salah satu tombol di bawah ini untuk menentukan, berturut-turut, pintasan utama atau pintasan alternatif yang baru.</translation>
    </message>
</context>
<context>
    <name>OpenLP.SlideController</name>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Previous Slide</source>
        <translation>Salindia Sebelumnya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Move to previous.</source>
        <translation>Pindah ke sebelumnya.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Next Slide</source>
        <translation>Salindia Selanjutnya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Move to next.</source>
        <translation>Pindah ke selanjutnya.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="266"/>
        <source>Hide</source>
        <translation>Sembunyikan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="268"/>
        <source>Move to preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="272"/>
        <source>Show Desktop</source>
        <translation>Tampilkan Desktop</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="277"/>
        <source>Toggle Desktop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="282"/>
        <source>Toggle Blank to Theme</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="287"/>
        <source>Toggle Blank Screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="316"/>
        <source>Play Slides</source>
        <translation>Mainkan Salindia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="336"/>
        <source>Delay between slides in seconds.</source>
        <translation>Penundaan antar salindia dalam hitungan detik.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="340"/>
        <source>Move to live.</source>
        <translation>Pindah ke Tayang.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="343"/>
        <source>Add to Service.</source>
        <translation>Tambahkan ke Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="347"/>
        <source>Edit and reload song preview.</source>
        <translation>Sunting dan muat-ulang pratinjau lagu.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="351"/>
        <source>Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="358"/>
        <source>Start playing media.</source>
        <translation>Mulai mainkan media.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="362"/>
        <source>Pause playing media.</source>
        <translation>Sela media yang sedang dimainkan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="366"/>
        <source>Stop playing media.</source>
        <translation>Stop media yang sedang dimainkan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="370"/>
        <source>Loop playing media.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="377"/>
        <source>Video timer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="386"/>
        <source>Video position.</source>
        <translation>Posisi video.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="397"/>
        <source>Audio Volume.</source>
        <translation>Volume Audio.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="414"/>
        <source>Go To</source>
        <translation>Tuju Ke</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="442"/>
        <source>Go to &quot;Verse&quot;</source>
        <translation>Tuju ke &quot;Bait&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="443"/>
        <source>Go to &quot;Chorus&quot;</source>
        <translation>Tuju ke &quot;Refrain&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="444"/>
        <source>Go to &quot;Bridge&quot;</source>
        <translation>Tuju ke &quot;Bridge&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="446"/>
        <source>Go to &quot;Pre-Chorus&quot;</source>
        <translation>Tuju ke &quot;Pra-Refrain&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="447"/>
        <source>Go to &quot;Intro&quot;</source>
        <translation>Tuju ke &quot;Intro&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="448"/>
        <source>Go to &quot;Ending&quot;</source>
        <translation>Tuju ke &quot;Ending&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="449"/>
        <source>Go to &quot;Other&quot;</source>
        <translation>Tuju ke &quot;Lainnya&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="575"/>
        <source>Previous Service</source>
        <translation>Layanan Sebelumnya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="580"/>
        <source>Next Service</source>
        <translation>Layanan Selanjutnya</translation>
    </message>
</context>
<context>
    <name>OpenLP.SourceSelectForm</name>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="150"/>
        <source>Ignoring current changes and return to OpenLP</source>
        <translation>Abaikan perubahan saat ini dan kembali ke OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="153"/>
        <source>Delete all user-defined text and revert to PJLink default text</source>
        <translation>Hapus semua teks yang-ditetapkan-pengguna dan kembalikan ke teks bawaan PJLink</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="156"/>
        <source>Discard changes and reset to previous user-defined text</source>
        <translation>Batalkan perubahan dan setel-ulang ke teks yang-ditetapkan-pengguna sebelumnya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="159"/>
        <source>Save changes and return to OpenLP</source>
        <translation>Simpan perubahan dan kembali ke OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="397"/>
        <source>Edit Projector Source Text</source>
        <translation>Sunting Teks Sumber Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="399"/>
        <source>Select Projector Source</source>
        <translation>Pilih Sumber Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="481"/>
        <source>Delete entries for this projector</source>
        <translation>Hapus entri untuk proyektor ini</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="482"/>
        <source>Are you sure you want to delete ALL user-defined source input text for this projector?</source>
        <translation>Anda yakin ingin menghapus SEMUA teks sumber masukan yang-ditetapkan-pengguna untuk proyektor ini?</translation>
    </message>
</context>
<context>
    <name>OpenLP.SpellTextEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="405"/>
        <source>Language:</source>
        <translation>Bahasa:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="416"/>
        <source>Spelling Suggestions</source>
        <translation>Saran Ejaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="424"/>
        <source>Formatting Tags</source>
        <translation>Label Pemformatan</translation>
    </message>
</context>
<context>
    <name>OpenLP.StartTimeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="70"/>
        <source>Theme Layout</source>
        <translation>Tata-Letak Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="71"/>
        <source>The blue box shows the main area.</source>
        <translation>Kotak biru menunjukkan area utama.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="72"/>
        <source>The red box shows the footer.</source>
        <translation>Kotak merah menunjukkan Catatan Kaki.</translation>
    </message>
</context>
<context>
    <name>OpenLP.StartTime_form</name>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="116"/>
        <source>Item Start and Finish Time</source>
        <translation>Butir Waktu Mulai &amp; Selesai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="123"/>
        <source>Hours:</source>
        <translation>Hitungan jam:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="124"/>
        <source>Minutes:</source>
        <translation>Hitungan menit:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="125"/>
        <source>Seconds:</source>
        <translation>Hitungan detik:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="126"/>
        <source>Start</source>
        <translation>Mulai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="127"/>
        <source>Finish</source>
        <translation>Selesai</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="128"/>
        <source>Length</source>
        <translation>Durasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Time Validation Error</source>
        <translation>Kesalahan Validasi Waktu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="72"/>
        <source>Finish time is set after the end of the media item</source>
        <translation>Waktu selesai disetel setelah akhir butir media</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Start time is after the finish time of the media item</source>
        <translation>Waktu mulai diatur setelah waktu selesai butir media</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="167"/>
        <source>(approximately %d lines per slide)</source>
        <translation>(sekitar %d baris per salindia)</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeManager</name>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="68"/>
        <source>Create a new theme.</source>
        <translation>Buat suatu tema baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit Theme</source>
        <translation>Sunting Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit a theme.</source>
        <translation>Sunting suatu tema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete Theme</source>
        <translation>Hapus Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete a theme.</source>
        <translation>Hapus suatu tema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import Theme</source>
        <translation>Impor Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import a theme.</source>
        <translation>Impor suatu tema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export Theme</source>
        <translation>Ekspor Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export a theme.</source>
        <translation>Ekspor suatu tema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="108"/>
        <source>&amp;Edit Theme</source>
        <translation>&amp;Sunting Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="111"/>
        <source>&amp;Copy Theme</source>
        <translation>&amp;Salin Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="114"/>
        <source>&amp;Rename Theme</source>
        <translation>&amp;Namai-ulang Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="117"/>
        <source>&amp;Delete Theme</source>
        <translation>&amp;Hapus Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="121"/>
        <source>Set As &amp;Global Default</source>
        <translation>Setel Sebagai &amp;Bawaan Global</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="125"/>
        <source>&amp;Export Theme</source>
        <translation>&amp;Ekspor Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="260"/>
        <source>{text} (default)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>You must select a theme to rename.</source>
        <translation>Anda harus memilih suatu tema untuk dinamai-ulang.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename Confirmation</source>
        <translation>Konfirmasi Penamaan-ulang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename {theme_name} theme?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="310"/>
        <source>Copy of {name}</source>
        <comment>Copy of &lt;theme name&gt;</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="340"/>
        <source>You must select a theme to edit.</source>
        <translation>Anda harus memilih suatu tema untuk disunting.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>You must select a theme to delete.</source>
        <translation>Anda harus memilih suatu tema untuk dihapus.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete Confirmation</source>
        <translation>Konfirmasi Penghapusan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete {theme_name} theme?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="394"/>
        <source>You have not selected a theme.</source>
        <translation>Anda belum memilih suatu tema.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="398"/>
        <source>Save Theme - ({name})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>OpenLP Themes (*.otz)</source>
        <translation>Tema OpenLP (*.otz)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Theme Exported</source>
        <translation>Tema Telah Diekspor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Your theme has been successfully exported.</source>
        <translation>Tema pilihan Anda berhasil diekspor.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>Theme Export Failed</source>
        <translation>Ekspor Tema Gagal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>The {theme_name} export failed because this error occurred: {err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>Select Theme Import File</source>
        <translation>Pilih Berkas Tema Untuk Diimpor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="505"/>
        <source>{name} (default)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme Already Exists</source>
        <translation>Tema Sudah Ada</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme {name} already exists. Do you want to replace it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>Import Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>There was a problem importing {file_name}.

It is corrupt, inaccessible or not a valid theme.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>Validation Error</source>
        <translation>Kesalahan Validasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>A theme with this name already exists.</source>
        <translation>Suatu tema dengan nama ini sudah ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="773"/>
        <source>You are unable to delete the default theme.</source>
        <translation>Anda tidak dapat menghapus tema bawaan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="782"/>
        <source>{count} time(s) by {plugin}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Unable to delete theme</source>
        <translation>Tidak dapat menghapus tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Theme is currently used 

{text}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ThemeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>Background Image Empty</source>
        <translation>Gambar Latar Kosong</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>You have not selected a background image. Please select one before continuing.</source>
        <translation>Anda belum memilih suatu gambar latar. Silakan pilih satu sebelum melanjutkan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="301"/>
        <source>Edit Theme - {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>Theme Name Missing</source>
        <translation>Nama Tema Hilang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>There is no name for this theme. Please enter one.</source>
        <translation>Tidak ada nama untuk tema ini. Silakan masukkan suatu nama.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Theme Name Invalid</source>
        <translation>Nama Tema Tidak Valid</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Invalid theme name. Please enter one.</source>
        <translation>Nama Tema Tidak Valid. Silakan masukkan suatu nama yang valid.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="128"/>
        <source>Select Image</source>
        <translation>Pilih Gambar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="153"/>
        <source>Select Video</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="411"/>
        <source>Theme Wizard</source>
        <translation>Wisaya Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="412"/>
        <source>Welcome to the Theme Wizard</source>
        <translation>Selamat Datang di Wisaya Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="414"/>
        <source>This wizard will help you to create and edit your themes. Click the next button below to start the process by setting up your background.</source>
        <translation>Wisaya ini akan membantu Anda untuk membuat dan menyunting tema Anda. Klik tombol Selanjutnya di bawah ini untuk memulai proses dengan menyiapkan latar Anda.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="417"/>
        <source>Set Up Background</source>
        <translation>Siapkan Latar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="418"/>
        <source>Set up your theme&apos;s background according to the parameters below.</source>
        <translation>Siapkan latar tema Anda berdasarkan parameter di bawah ini.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="420"/>
        <source>Background type:</source>
        <translation>Jenis latar:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="421"/>
        <source>Solid color</source>
        <translation>Warna pekat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="422"/>
        <source>Gradient</source>
        <translation>Gradien</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="425"/>
        <source>Transparent</source>
        <translation>Transparan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="427"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="468"/>
        <source>color:</source>
        <translation>warna:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="430"/>
        <source>Starting color:</source>
        <translation>Warna mulai:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="431"/>
        <source>Ending color:</source>
        <translation>Warna akhir:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="432"/>
        <source>Gradient:</source>
        <translation>Gradien:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="433"/>
        <source>Horizontal</source>
        <translation>Horisontal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="435"/>
        <source>Vertical</source>
        <translation>Vertikal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="437"/>
        <source>Circular</source>
        <translation>Sirkular</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="439"/>
        <source>Top Left - Bottom Right</source>
        <translation>Kiri Atas - Kanan Bawah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="441"/>
        <source>Bottom Left - Top Right</source>
        <translation>Kiri Bawah - Kanan Atas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="445"/>
        <source>Background color:</source>
        <translation>Warna latar:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="447"/>
        <source>Main Area Font Details</source>
        <translation>Rincian Fon di Area Utama</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="448"/>
        <source>Define the font and display characteristics for the Display text</source>
        <translation>Tetapkan karakteristik fon dan tampilan untuk teks Tampilan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="467"/>
        <source>Font:</source>
        <translation>Fon:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="469"/>
        <source>Size:</source>
        <translation>Ukuran:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="454"/>
        <source>Line Spacing:</source>
        <translation>Jarak antar Baris:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="456"/>
        <source>&amp;Outline:</source>
        <translation>&amp;Garis Besar:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="459"/>
        <source>&amp;Shadow:</source>
        <translation>&amp;Bayangan:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="462"/>
        <source>Bold</source>
        <translation>Tebal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="463"/>
        <source>Italic</source>
        <translation>Miring</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="464"/>
        <source>Footer Area Font Details</source>
        <translation>Rincian Fon di Catatan Kaki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="465"/>
        <source>Define the font and display characteristics for the Footer text</source>
        <translation>Tetapkan karakteristik fon dan tampilan untuk teks Catatan Kaki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="471"/>
        <source>Text Formatting Details</source>
        <translation>Rincian Pemformatan Teks</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="472"/>
        <source>Allows additional display formatting information to be defined</source>
        <translation>Izinkan informasi dari format tampilan tambahan untuk ditetapkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="474"/>
        <source>Horizontal Align:</source>
        <translation>Sejajarkan Horisontal:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="475"/>
        <source>Left</source>
        <translation>Kiri</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="476"/>
        <source>Right</source>
        <translation>Kanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="477"/>
        <source>Center</source>
        <translation>Tengah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="478"/>
        <source>Justify</source>
        <translation>Justifikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="479"/>
        <source>Transitions:</source>
        <translation>Transisi:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="480"/>
        <source>Fade</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="481"/>
        <source>Slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="482"/>
        <source>Concave</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="483"/>
        <source>Convex</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="484"/>
        <source>Zoom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="485"/>
        <source>Speed:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="486"/>
        <source>Normal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="487"/>
        <source>Fast</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="488"/>
        <source>Slow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="489"/>
        <source>Output Area Locations</source>
        <translation>Lokasi Area Keluaran</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="490"/>
        <source>Allows you to change and move the Main and Footer areas.</source>
        <translation>Izinkan Anda untuk mengubah dan memindahkan area Utama dan Catatan Kaki.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="492"/>
        <source>&amp;Main Area</source>
        <translation>&amp;Area Utama</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="493"/>
        <source>&amp;Use default location</source>
        <translation>&amp;Gunakan lokasi bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="503"/>
        <source>X position:</source>
        <translation>Posisi X:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="510"/>
        <source>px</source>
        <translation> pks</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="505"/>
        <source>Y position:</source>
        <translation>Posisi Y:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="507"/>
        <source>Width:</source>
        <translation>Lebar:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="509"/>
        <source>Height:</source>
        <translation>Tinggi:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="502"/>
        <source>&amp;Footer Area</source>
        <translation>&amp;Area Catatan Kaki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="511"/>
        <source>Use default location</source>
        <translation>Gunakan lokasi bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="513"/>
        <source>Layout Preview</source>
        <translation>Pratinjau Tata-Letak</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="514"/>
        <source>Preview and Save</source>
        <translation>Pratinjau dan Simpan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="515"/>
        <source>Preview the theme and save it.</source>
        <translation>Pratinjau tema dan simpan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="516"/>
        <source>Theme name:</source>
        <translation>Nama tema:</translation>
    </message>
</context>
<context>
    <name>OpenLP.Themes</name>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="74"/>
        <source>Recreating Theme Thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="75"/>
        <source>TextLabel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ThemesTab</name>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="44"/>
        <source>Themes</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="116"/>
        <source>Global Theme</source>
        <translation>Tema Global</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="117"/>
        <source>Universal Settings</source>
        <translation>Setelan Universal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="118"/>
        <source>&amp;Wrap footer text</source>
        <translation>&amp;Sembunyikan teks Catatan Kaki</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="119"/>
        <source>Theme Level</source>
        <translation>Tingkatan Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="120"/>
        <source>S&amp;ong Level</source>
        <translation>Tingkatan Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="121"/>
        <source>Use the theme from each song in the database. If a song doesn&apos;t have a theme associated with it, then use the service&apos;s theme. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation>Gunakan tema dari setiap lagu dalam basis-data. Jika sebuah lagu tidak memiliki tema yang terkait dengannya, tema Layanan akan digunakan. Jika Layanan tidak memiliki tema, tema global akan digunakan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="125"/>
        <source>&amp;Service Level</source>
        <translation>&amp;Tingkatan Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="126"/>
        <source>Use the theme from the service, overriding any of the individual songs&apos; themes. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation>Gunakan tema dari Layanan, mengesampingkan apapun tema lagu individual. Jika Layanan tidak memiliki tema, tema global akan digunakan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="130"/>
        <source>&amp;Global Level</source>
        <translation>&amp;Tingkatan Global</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="131"/>
        <source>Use the global theme, overriding any themes associated with either the service or the songs.</source>
        <translation>Gunakan tema global, mengesampingkan apapun tema yang terkait dengan Layanan atau lagu.</translation>
    </message>
</context>
<context>
    <name>OpenLP.Ui</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="337"/>
        <source>About</source>
        <translation>Tentang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="338"/>
        <source>&amp;Add</source>
        <translation>&amp;Tambahkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="339"/>
        <source>Add group</source>
        <translation>Tambahkan grup</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="340"/>
        <source>Add group.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="341"/>
        <source>Advanced</source>
        <translation>Lanjutan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="342"/>
        <source>All Files</source>
        <translation>Semua Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="343"/>
        <source>Automatic</source>
        <translation>Otomatis</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="344"/>
        <source>Background Color</source>
        <translation>Warna Latar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="345"/>
        <source>Background color:</source>
        <translation>Warna latar:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="346"/>
        <source>Search is Empty or too Short</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="347"/>
        <source>&lt;strong&gt;The search you have entered is empty or shorter than 3 characters long.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please try again with a longer search.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="350"/>
        <source>No Bibles Available</source>
        <translation>Alkitab tidak tersedia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="351"/>
        <source>&lt;strong&gt;There are no Bibles currently installed.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please use the Import Wizard to install one or more Bibles.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="353"/>
        <source>Bottom</source>
        <translation>Dasar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="354"/>
        <source>Browse...</source>
        <translation>Menelusuri...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="355"/>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="356"/>
        <source>CCLI number:</source>
        <translation>Nomor CCLI:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="357"/>
        <source>CCLI song number:</source>
        <translation>Nomor lagu CCLI:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="358"/>
        <source>Create a new service.</source>
        <translation>Buat suatu Layanan baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="359"/>
        <source>Confirm Delete</source>
        <translation>Konfirmasi Penghapusan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="360"/>
        <source>Continuous</source>
        <translation>Kontinu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="361"/>
        <source>Default</source>
        <translation>Bawaan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="362"/>
        <source>Default Color:</source>
        <translation>Warna Bawaan:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="363"/>
        <source>Service %Y-%m-%d %H-%M</source>
        <comment>This may not contain any of the following characters: /\?*|&lt;&gt;[]&quot;:+
See http://docs.python.org/library/datetime.html#strftime-strptime-behavior for more information.</comment>
        <translation>Layanan %d-%m-%Y %H-%M</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="367"/>
        <source>&amp;Delete</source>
        <translation>&amp;Hapus</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="368"/>
        <source>Display style:</source>
        <translation>Gaya tampilan:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="369"/>
        <source>Duplicate Error</source>
        <translation>Kesalahan Duplikasi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="370"/>
        <source>&amp;Edit</source>
        <translation>&amp;Sunting</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="371"/>
        <source>Empty Field</source>
        <translation>Bidang Kosong</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="372"/>
        <source>Error</source>
        <translation>Kesalahan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="373"/>
        <source>Export</source>
        <translation>Ekspor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="374"/>
        <source>File</source>
        <translation>Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="375"/>
        <source>File appears to be corrupt.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="376"/>
        <source>pt</source>
        <comment>Abbreviated font point size unit</comment>
        <translation> pn</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="377"/>
        <source>Help</source>
        <translation>Bantuan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="378"/>
        <source>h</source>
        <comment>The abbreviated unit for hours</comment>
        <translation> jam</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="379"/>
        <source>Invalid Folder Selected</source>
        <comment>Singular</comment>
        <translation>Folder Terpilih Tidak Valid</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="380"/>
        <source>Invalid File Selected</source>
        <comment>Singular</comment>
        <translation>Berkas Terpilih Tidak Valid</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="381"/>
        <source>Invalid Files Selected</source>
        <comment>Plural</comment>
        <translation>Beberapa Berkas Terpilih Tidak Valid</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="382"/>
        <source>Image</source>
        <translation>Gambar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="383"/>
        <source>Import</source>
        <translation>Impor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="384"/>
        <source>Layout style:</source>
        <translation>Gaya tata-letak:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="385"/>
        <source>Live</source>
        <translation>Tayang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="386"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="387"/>
        <source>Live Background Error</source>
        <translation>Kesalahan Latar Tayang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="388"/>
        <source>Live Toolbar</source>
        <translation>Bilah Alat Tayang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="389"/>
        <source>Load</source>
        <translation>Muat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="390"/>
        <source>Manufacturer</source>
        <comment>Singular</comment>
        <translation>Pembuat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="391"/>
        <source>Manufacturers</source>
        <comment>Plural</comment>
        <translation>Para Pembuat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="392"/>
        <source>Model</source>
        <comment>Singular</comment>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="393"/>
        <source>Models</source>
        <comment>Plural</comment>
        <translation>Model-model</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="394"/>
        <source>m</source>
        <comment>The abbreviated unit for minutes</comment>
        <translation> mnt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="395"/>
        <source>Middle</source>
        <translation>Tengah</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="396"/>
        <source>New</source>
        <translation>Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="397"/>
        <source>New Service</source>
        <translation>Layanan Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="398"/>
        <source>New Theme</source>
        <translation>Tema Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="399"/>
        <source>Next Track</source>
        <translation>Trek Selanjutnya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="400"/>
        <source>No Folder Selected</source>
        <comment>Singular</comment>
        <translation>Tidak Ada Folder Terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="401"/>
        <source>No File Selected</source>
        <comment>Singular</comment>
        <translation>Tidak Ada Berkas Terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="402"/>
        <source>No Files Selected</source>
        <comment>Plural</comment>
        <translation>Tidak Ada Satupun Berkas Terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="403"/>
        <source>No Item Selected</source>
        <comment>Singular</comment>
        <translation>Tidak Ada Butir Terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="404"/>
        <source>No Items Selected</source>
        <comment>Plural</comment>
        <translation>Tidak Ada Satupun Butir Terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="405"/>
        <source>No Search Results</source>
        <translation>Tidak Ada Hasil Penelusuran</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="406"/>
        <source>OpenLP</source>
        <translation>OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="407"/>
        <source>OpenLP 2.0 and up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="408"/>
        <source>OpenLP is already running on this machine. 
Closing this instance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="409"/>
        <source>Open service.</source>
        <translation>Buka Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="410"/>
        <source>Optional, this will be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="411"/>
        <source>Optional, this won&apos;t be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="412"/>
        <source>Play Slides in Loop</source>
        <translation>Mainkan Semua Salindia Berulang-ulang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="413"/>
        <source>Play Slides to End</source>
        <translation>Mainkan Semua Salindia sampai Akhir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="414"/>
        <source>Preview</source>
        <translation>Pratinjau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="415"/>
        <source>Preview Toolbar</source>
        <translation>Bilah Alat Pratinjau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="416"/>
        <source>Print Service</source>
        <translation>Cetak Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="417"/>
        <source>Projector</source>
        <comment>Singular</comment>
        <translation>Proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="418"/>
        <source>Projectors</source>
        <comment>Plural</comment>
        <translation>Proyektor-proyektor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="419"/>
        <source>Replace Background</source>
        <translation>Ganti Latar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="420"/>
        <source>Replace live background.</source>
        <translation>Ganti Latar Tayang.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="421"/>
        <source>Replace live background is not available when the WebKit player is disabled.</source>
        <translation>Ganti latar tayang tidak tersedia jika pemutar WebKit dinonaktifkan.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="423"/>
        <source>Reset Background</source>
        <translation>Setel-Ulang Latar</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="424"/>
        <source>Reset live background.</source>
        <translation>Setel-Ulang latar Tayang.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="425"/>
        <source>Required, this will be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="426"/>
        <source>s</source>
        <comment>The abbreviated unit for seconds</comment>
        <translation> dtk</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="428"/>
        <source>Save &amp;&amp; Preview</source>
        <translation>Simpan &amp;&amp; Pratinjau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="429"/>
        <source>Search</source>
        <translation>Penelusuran</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="430"/>
        <source>Search Themes...</source>
        <comment>Search bar place holder text </comment>
        <translation>Telusuri Tema...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="431"/>
        <source>You must select an item to delete.</source>
        <translation>Anda harus memilih suatu butir untuk dihapus.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="432"/>
        <source>You must select an item to edit.</source>
        <translation>Anda harus memilih suatu butir untuk disunting.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="433"/>
        <source>Settings</source>
        <translation>Setelan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="434"/>
        <source>Save Service</source>
        <translation>Simpan Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="435"/>
        <source>Service</source>
        <translation>Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="436"/>
        <source>Please type more text to use &apos;Search As You Type&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="437"/>
        <source>Optional &amp;Split</source>
        <translation>Pisah &amp;Opsional</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="438"/>
        <source>Split a slide into two only if it does not fit on the screen as one slide.</source>
        <translation>Pisah salindia menjadi dua jika tidak muat pada layar sebagai satu salindia.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="64"/>
        <source>Starting import...</source>
        <translation>Memulai impor...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="441"/>
        <source>Stop Play Slides in Loop</source>
        <translation>Stop Mainkan Semua Salindia Berulang-ulang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="442"/>
        <source>Stop Play Slides to End</source>
        <translation>Stop Mainkan Semua Salindia sampai Akhir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="443"/>
        <source>Theme</source>
        <comment>Singular</comment>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="444"/>
        <source>Themes</source>
        <comment>Plural</comment>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="445"/>
        <source>Tools</source>
        <translation>Alat</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="446"/>
        <source>Top</source>
        <translation>Puncak</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="447"/>
        <source>Unsupported File</source>
        <translation>Tidak Ada Dukungan untuk Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="448"/>
        <source>Verse Per Slide</source>
        <translation>Ayat per Salindia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="449"/>
        <source>Verse Per Line</source>
        <translation>Ayat per Baris</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="450"/>
        <source>Version</source>
        <translation>Versi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="451"/>
        <source>View</source>
        <translation>Tinjau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="452"/>
        <source>View Mode</source>
        <translation>Mode Tinjauan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="453"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="454"/>
        <source>Web Interface, Download and Install latest Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="455"/>
        <source>Book Chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="456"/>
        <source>Chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="457"/>
        <source>Verse</source>
        <translation>Bait</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="459"/>
        <source>Psalm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="460"/>
        <source>Book names may be shortened from full names, for an example Ps 23 = Psalm 23</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="706"/>
        <source>Written by</source>
        <translation>Ditulis oleh</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="163"/>
        <source>Delete the selected item.</source>
        <translation>Hapus butir terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="166"/>
        <source>Move selection up one position.</source>
        <translation>Naikkan pilihan satu posisi. </translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="169"/>
        <source>Move selection down one position.</source>
        <translation>Turunkan pilihan satu posisi.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="310"/>
        <source>&amp;Vertical Align:</source>
        <translation>&amp;Sejajarkan Vertikal:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="53"/>
        <source>Finished import.</source>
        <translation>Impor selesai.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="54"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="56"/>
        <source>Importing</source>
        <translation>Mengimpor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="57"/>
        <source>Importing &quot;{source}&quot;...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="58"/>
        <source>Select Import Source</source>
        <translation>Pilih Sumber Impor</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="59"/>
        <source>Select the import format and the location to import from.</source>
        <translation>Pilih format impor dan lokasi sumber.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="60"/>
        <source>Open {file_type} File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="61"/>
        <source>Open {folder_name} Folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="62"/>
        <source>%p%</source>
        <translation>%p%</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="63"/>
        <source>Ready.</source>
        <translation>Siap.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="65"/>
        <source>You need to specify one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>Anda harus menentukan sebuah berkas %s untuk diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="67"/>
        <source>You need to specify at least one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>Anda harus menentukan setidaknya satu berkas %s untuk diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="69"/>
        <source>You need to specify one %s folder to import from.</source>
        <comment>A song format e.g. PowerSong</comment>
        <translation>Anda harus menentukan sebuah folder %s untuk diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="372"/>
        <source>Welcome to the Bible Import Wizard</source>
        <translation>Selamat Datang di Wisaya Impor Alkitab</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="137"/>
        <source>Welcome to the Duplicate Song Removal Wizard</source>
        <translation>Selamat datang di Wisaya Penghapus Lagu Duplikat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="144"/>
        <source>Welcome to the Song Export Wizard</source>
        <translation>Selamat Datang di Wisaya Ekspor Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="135"/>
        <source>Welcome to the Song Import Wizard</source>
        <translation>Selamat Datang di Wisaya Impor Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="33"/>
        <source>Author</source>
        <comment>Singular</comment>
        <translation>Pengarang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="34"/>
        <source>Authors</source>
        <comment>Plural</comment>
        <translation>Pengarang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="35"/>
        <source>Author Unknown</source>
        <translation>Pengarang Tak Diketahui</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="37"/>
        <source>Songbook</source>
        <comment>Singular</comment>
        <translation>Buku Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="38"/>
        <source>Songbooks</source>
        <comment>Plural</comment>
        <translation>Buku-buku Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="39"/>
        <source>Title and/or verses not found</source>
        <translation>Judul dan/atau bait-bait tidak ditemukan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="40"/>
        <source>Song Maintenance</source>
        <translation>Pengelolaan Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="41"/>
        <source>Topic</source>
        <comment>Singular</comment>
        <translation>Topik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="42"/>
        <source>Topics</source>
        <comment>Plural</comment>
        <translation>Topik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="43"/>
        <source>XML syntax error</source>
        <translation>Kesalahan sintaks XML</translation>
    </message>
</context>
<context>
    <name>OpenLP.core.lib</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="400"/>
        <source>{one} and {two}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="402"/>
        <source>{first} and {last}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenPL.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="499"/>
        <source>Other</source>
        <translation>Lainnya</translation>
    </message>
</context>
<context>
    <name>Openlp.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="119"/>
        <source>Source select dialog interface</source>
        <translation>Antarmuka pemilihan sumber</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin</name>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="168"/>
        <source>&lt;strong&gt;Presentation Plugin&lt;/strong&gt;&lt;br /&gt;The presentation plugin provides the ability to show presentations using a number of different programs. The choice of available presentation programs is available to the user in a drop down box.</source>
        <translation>&lt;strong&gt;Plugin Presentasi&lt;/strong&gt;&lt;br /&gt;Plugin presentasi menyediakan kemampuan untuk menampilkan presentasi dengan sejumlah program berbeda. Pemilihan program presentasi yang ada tersedia untuk pengguna dalam kotak tarik-turun.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="181"/>
        <source>Presentation</source>
        <comment>name singular</comment>
        <translation>Presentasi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="182"/>
        <source>Presentations</source>
        <comment>name plural</comment>
        <translation>Presentasi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="186"/>
        <source>Presentations</source>
        <comment>container title</comment>
        <translation>Presentasi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="190"/>
        <source>Load a new presentation.</source>
        <translation>Muat suatu presentasi baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="194"/>
        <source>Delete the selected presentation.</source>
        <translation>Hapus presentasi terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="195"/>
        <source>Preview the selected presentation.</source>
        <translation>Pratinjau presentasi terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="196"/>
        <source>Send the selected presentation live.</source>
        <translation>Tayangkan presentasi terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="197"/>
        <source>Add the selected presentation to the service.</source>
        <translation>Tambahkan presentasi terpilih ke Layanan.</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="62"/>
        <source>Select Presentation(s)</source>
        <translation>Pilih (beberapa) Presentasi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="63"/>
        <source>Automatic</source>
        <translation>Otomatis</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="64"/>
        <source>Present using:</source>
        <translation>Tampilkan dengan:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="92"/>
        <source>Presentations ({text})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>File Exists</source>
        <translation>Berkas Sudah Ada</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>A presentation with that filename already exists.</source>
        <translation>Suatu presentasi dengan nama itu sudah ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="207"/>
        <source>This type of presentation is not supported.</source>
        <translation>Tidak ada dukungan untuk presentasi jenis ini.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>Missing Presentation</source>
        <translation>Presentasi Hilang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>The presentation {name} no longer exists.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="369"/>
        <source>The presentation {name} is incomplete, please reload.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PresentationPlugin.PowerpointDocument</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/powerpointcontroller.py" line="536"/>
        <source>An error occurred in the PowerPoint integration and the presentation will be stopped. Restart the presentation if you wish to present it.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PresentationPlugin.PresentationTab</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="105"/>
        <source>Available Controllers</source>
        <translation>Pengontrol yang Tersedia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="111"/>
        <source>PDF options</source>
        <translation>Opsi PDF</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="112"/>
        <source>PowerPoint options</source>
        <translation>Opsi PowerPoint</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="113"/>
        <source>Allow presentation application to be overridden</source>
        <translation>Izinkan aplikasi presentasi untuk digantikan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="115"/>
        <source>Clicking on the current slide advances to the next effect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="118"/>
        <source>Let PowerPoint control the size and monitor of the presentations
(This may fix PowerPoint scaling issues in Windows 8 and 10)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="122"/>
        <source>Use given full path for mudraw or ghostscript binary:</source>
        <translation>Gunakan sepenuhnya jalur yang diberikan untuk biner mudraw atau ghostscript:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="124"/>
        <source>Select mudraw or ghostscript binary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="131"/>
        <source>{name} (unavailable)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="228"/>
        <source>The program is not ghostscript or mudraw which is required.</source>
        <translation>Program tersebut bukanlah ghostscript ataupun mudraw yang dibutuhkan.</translation>
    </message>
</context>
<context>
    <name>RemotePlugin</name>
    <message>
        <location filename="../../openlp/core/api/http/server.py" line="159"/>
        <source>Importing Website</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RemotePlugin.Mobile</name>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="57"/>
        <source>Remote</source>
        <translation>Remote</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="58"/>
        <source>Stage View</source>
        <translation>Tinjuan Bertahap</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="59"/>
        <source>Live View</source>
        <translation>Tinjauan Tayang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="60"/>
        <source>Chords View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="67"/>
        <source>Service Manager</source>
        <translation>Manajer Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="68"/>
        <source>Slide Controller</source>
        <translation>Pengontrol Salindia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="69"/>
        <source>Alerts</source>
        <translation>Peringatan-Peringatan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="70"/>
        <source>Search</source>
        <translation>Penelusuran</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="71"/>
        <source>Home</source>
        <translation>Beranda</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="72"/>
        <source>Refresh</source>
        <translation>Segarkan-ulang</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="73"/>
        <source>Blank</source>
        <translation>Kosong</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="74"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="75"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="76"/>
        <source>Show</source>
        <translation>Tampilkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="77"/>
        <source>Prev</source>
        <translation>Sebelumnya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="78"/>
        <source>Next</source>
        <translation>Selanjutnya</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="79"/>
        <source>Text</source>
        <translation>Teks</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="80"/>
        <source>Show Alert</source>
        <translation>Tampilkan Peringatan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="81"/>
        <source>Go Live</source>
        <translation>Tayangkan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="82"/>
        <source>Add to Service</source>
        <translation>Tambahkan ke Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="83"/>
        <source>Add &amp;amp; Go to Service</source>
        <translation>Tambahkan &amp;amp; Tuju ke Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="84"/>
        <source>No Results</source>
        <translation>Tidak Ada Hasil</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="85"/>
        <source>Options</source>
        <translation>Opsi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="86"/>
        <source>Service</source>
        <translation>Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="87"/>
        <source>Slides</source>
        <translation>Salindia</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="88"/>
        <source>Settings</source>
        <translation>Setelan</translation>
    </message>
</context>
<context>
    <name>RemotePlugin.RemoteTab</name>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="158"/>
        <source>Remote Interface</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="159"/>
        <source>Server Settings</source>
        <translation>Setelan Server</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="160"/>
        <source>Serve on IP address:</source>
        <translation>Tugaskan di alamat IP:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="161"/>
        <source>Port number:</source>
        <translation>Nomor port:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="162"/>
        <source>Remote URL:</source>
        <translation>URL remote:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="163"/>
        <source>Stage view URL:</source>
        <translation>URL tinjauan bertahap:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="164"/>
        <source>Live view URL:</source>
        <translation>URL tinjauan Tayang:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="165"/>
        <source>Chords view URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="166"/>
        <source>Display stage time in 12h format</source>
        <translation>Tampilkan waktu bertahap dalam format 12 jam</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="167"/>
        <source>Show thumbnails of non-text slides in remote and stage view.</source>
        <translation>Tampilkan thumbnail dari salindia yang bukan teks pada remote dan tinjauan bertahap.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="169"/>
        <source>Remote App</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="170"/>
        <source>Scan the QR code or click &lt;a href=&quot;{qr}&quot;&gt;download&lt;/a&gt; to download an app for your mobile device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="174"/>
        <source>User Authentication</source>
        <translation>Otentikasi Pengguna</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="177"/>
        <source>User id:</source>
        <translation>ID Pengguna:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="178"/>
        <source>Password:</source>
        <translation>Kata sandi:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="179"/>
        <source>Current Version number:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="180"/>
        <source>Latest Version number:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongPlugin.ReportSongList</name>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>Save File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>song_extract.csv</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>CSV format (*.csv)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report Creation</source>
        <translation>Pembuatan Laporan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report 
{name} 
has been successfully created. </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>Song Extraction Failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>An error occurred while extracting: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongUsagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="90"/>
        <source>&amp;Song Usage Tracking</source>
        <translation>&amp;Pelacakan Penggunaan Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>&amp;Delete Tracking Data</source>
        <translation>&amp;Hapus Data Pelacakan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>Delete song usage data up to a specified date.</source>
        <translation>Hapus data penggunaan lagu sampai tanggal tertentu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>&amp;Extract Tracking Data</source>
        <translation>&amp;Ekstrak Data Pelacakan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>Generate a report on song usage.</source>
        <translation>Hasilkan suatu laporan mengenai penggunaan lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="104"/>
        <source>Toggle Tracking</source>
        <translation>Ganti Pelacakan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="118"/>
        <source>Toggle the tracking of song usage.</source>
        <translation>Ganti pelacakan penggunaan lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="154"/>
        <source>Song Usage</source>
        <translation>Penggunaan Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="178"/>
        <source>Song usage tracking is active.</source>
        <translation>Pelacakan penggunaan lagu aktif.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="183"/>
        <source>Song usage tracking is inactive.</source>
        <translation>Pelacakan penggunaan lagu nonaktif.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="193"/>
        <source>display</source>
        <translation>tampilkan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="201"/>
        <source>printed</source>
        <translation>tercetak</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="238"/>
        <source>&lt;strong&gt;SongUsage Plugin&lt;/strong&gt;&lt;br /&gt;This plugin tracks the usage of songs in services.</source>
        <translation>&lt;strong&gt;Plugin PenggunaanLagu&lt;/strong&gt;&lt;br /&gt;Plugin ini melacak penggunaan lagu dalam Layanan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="249"/>
        <source>SongUsage</source>
        <comment>name singular</comment>
        <translation>PenggunaanLagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="250"/>
        <source>SongUsage</source>
        <comment>name plural</comment>
        <translation>PenggunaanLagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="254"/>
        <source>SongUsage</source>
        <comment>container title</comment>
        <translation>PenggunaanLagu</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDeleteForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="64"/>
        <source>Delete Song Usage Data</source>
        <translation>Hapus Data Penggunaan Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="66"/>
        <source>Select the date up to which the song usage data should be deleted. 
All data recorded before this date will be permanently deleted.</source>
        <translation>Pilih tanggal sampai dengan mana data penggunaan lagu akan dihapus. 
Semua data terekam sebelum tanggal ini akan dihapus secara permanen.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Delete Selected Song Usage Events?</source>
        <translation>Hapus Event Penggunaan Lagu Terpilih?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Are you sure you want to delete selected Song Usage data?</source>
        <translation>Anda yakin ingin menghapus data Penggunaan Lagu terpilih?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>Deletion Successful</source>
        <translation>Penghapusan Berhasil</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>All requested data has been deleted successfully.</source>
        <translation>Semua data yang diminta telah berhasil dihapus.</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDetailForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="85"/>
        <source>Song Usage Extraction</source>
        <translation>Ekstraksi Penggunaan Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="87"/>
        <source>Select Date Range</source>
        <translation>Pilih Kisaran Tanggal</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="88"/>
        <source>to</source>
        <translation>ke</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="89"/>
        <source>Report Location</source>
        <translation>Laporkan Lokasi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>Output Path Not Selected</source>
        <translation>Jalur Keluaran Belum Terpilih</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>You have not set a valid output location for your song usage report.
Please select an existing path on your computer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="91"/>
        <source>usage_detail_{old}_{new}.txt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report Creation</source>
        <translation>Pembuatan Laporan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report
{name}
has been successfully created.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>Report Creation Failed</source>
        <translation>Pembuatan Laporan Gagal</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>An error occurred while creating the report: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="301"/>
        <source>Arabic (CP-1256)</source>
        <translation>Arab (CP-1256)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="302"/>
        <source>Baltic (CP-1257)</source>
        <translation>Baltik (CP-1257)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="303"/>
        <source>Central European (CP-1250)</source>
        <translation>Eropa Tengah (CP-1250)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="304"/>
        <source>Cyrillic (CP-1251)</source>
        <translation>Cyrillic (CP-1251)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="305"/>
        <source>Greek (CP-1253)</source>
        <translation>Yunani (CP-1253)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="306"/>
        <source>Hebrew (CP-1255)</source>
        <translation>Ibrani (CP-1255)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="307"/>
        <source>Japanese (CP-932)</source>
        <translation>Jepang (CP-932)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="308"/>
        <source>Korean (CP-949)</source>
        <translation>Korea (CP-949)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="309"/>
        <source>Simplified Chinese (CP-936)</source>
        <translation>Mandarin - Sederhana (CP-936)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="310"/>
        <source>Thai (CP-874)</source>
        <translation>Thailand (CP-874)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="311"/>
        <source>Traditional Chinese (CP-950)</source>
        <translation>Mandarin - Tradisional (CP-950)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="312"/>
        <source>Turkish (CP-1254)</source>
        <translation>Turki (CP-1254)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="313"/>
        <source>Vietnam (CP-1258)</source>
        <translation>Vietnam (CP-1258)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="314"/>
        <source>Western European (CP-1252)</source>
        <translation>Eropa Barat (CP-1252)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Character Encoding</source>
        <translation>Pengodean Karakter</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="323"/>
        <source>The codepage setting is responsible
for the correct character representation.
Usually you are fine with the preselected choice.</source>
        <translation>Setelan halaman-kode ini bertanggung-jawab
atas representasi karakter yang benar.
Biasanya pilihan yang dipilih sebelumnya sudah baik.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Please choose the character encoding.
The encoding is responsible for the correct character representation.</source>
        <translation>Silakan pilih pengodean karakter.
Pengodean ini bertanggung-jawab atas representasi karakter yang benar.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>&amp;Song</source>
        <translation>&amp;Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="186"/>
        <source>Import songs using the import wizard.</source>
        <translation>Impor lagu dengan wisaya impor</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>CCLI SongSelect</source>
        <translation>CCLI SongSelect</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>Import songs from CCLI&apos;s SongSelect service.</source>
        <translation>Impor lagu dari layanan CCLI SongSelect.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>Exports songs using the export wizard.</source>
        <translation>Ekspor lagu dengan wisaya ekspor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="223"/>
        <source>Songs</source>
        <translation>Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>&amp;Re-index Songs</source>
        <translation>&amp;Indeks-ulang Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>Re-index the songs database to improve searching and ordering.</source>
        <translation>Indeks-ulang basis-data lagu untuk mempercepat penelusuran dan penyusunan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find &amp;Duplicate Songs</source>
        <translation>Temukan &amp;Lagu Duplikat</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find and remove duplicate songs in the song database.</source>
        <translation>Temukan dan hapus lagu duplikat di basis-data lagu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Song List Report</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Produce a CSV file of all the songs in the database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="259"/>
        <source>Reindexing songs...</source>
        <translation>Mengindeks-ulang Lagu...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="261"/>
        <source>Reindexing songs</source>
        <translation>Mengindeks-ulang lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="304"/>
        <source>&lt;strong&gt;Songs Plugin&lt;/strong&gt;&lt;br /&gt;The songs plugin provides the ability to display and manage songs.</source>
        <translation>&lt;strong&gt;Plugin Lagu&lt;/strong&gt;&lt;br /&gt;Plugin Lagu menyediakan kemampuan untuk menampilkan dan mengelola lagu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="347"/>
        <source>Song</source>
        <comment>name singular</comment>
        <translation>Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="348"/>
        <source>Songs</source>
        <comment>name plural</comment>
        <translation>Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="352"/>
        <source>Songs</source>
        <comment>container title</comment>
        <translation>Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="358"/>
        <source>Add a new song.</source>
        <translation>Tambahkan sebuah lagu baru.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="359"/>
        <source>Edit the selected song.</source>
        <translation>Sunting lagu terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="360"/>
        <source>Delete the selected song.</source>
        <translation>Hapus lagu terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="361"/>
        <source>Preview the selected song.</source>
        <translation>Pratinjau lagu terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="362"/>
        <source>Send the selected song live.</source>
        <translation>Tayangkan lagu terpilih.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="363"/>
        <source>Add the selected song to the service.</source>
        <translation>Tambahkan lagu terpilih ke Layanan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="388"/>
        <source>Importing Songs</source>
        <translation>Mengimpor Lagu</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="67"/>
        <source>Words</source>
        <comment>Author who wrote the lyrics of a song</comment>
        <translation>Teks</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="68"/>
        <source>Music</source>
        <comment>Author who wrote the music of a song</comment>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="69"/>
        <source>Words and Music</source>
        <comment>Author who wrote both lyrics and music of a song</comment>
        <translation>Teks dan Musik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="71"/>
        <source>Translation</source>
        <comment>Author who translated the song</comment>
        <translation>Terjemahan</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="74"/>
        <source>Author Maintenance</source>
        <translation>Pengelolaan Pengarang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="75"/>
        <source>Display name:</source>
        <translation>Nama tampilan:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="76"/>
        <source>First name:</source>
        <translation>Nama depan:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="77"/>
        <source>Last name:</source>
        <translation>Nama belakang:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="92"/>
        <source>You need to type in the first name of the author.</source>
        <translation>Anda harus masukkan nama depan pengarang.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="97"/>
        <source>You need to type in the last name of the author.</source>
        <translation>Anda harus masukkan nama belakang pengarang.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="102"/>
        <source>You have not set a display name for the author, combine the first and last names?</source>
        <translation>Anda belum menetapkan nama tampilan pengarang. Kombinasikan nama depan dan belakang?</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.CCLIFileImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/cclifile.py" line="84"/>
        <source>The file does not have a valid extension.</source>
        <translation>Berkas tidak memiliki ekstensi yang valid.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.DreamBeamImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/dreambeam.py" line="101"/>
        <source>Invalid DreamBeam song file_path. Missing DreamSong tag.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.EasyWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="316"/>
        <source>Administered by {admin}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="393"/>
        <source>&quot;{title}&quot; could not be imported. {entry}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="206"/>
        <source>This file does not exist.</source>
        <translation>Berkas ini tidak ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="210"/>
        <source>Could not find the &quot;Songs.MB&quot; file. It must be in the same folder as the &quot;Songs.DB&quot; file.</source>
        <translation>Tidak dapat menemukan berkas &quot;Songs.MB&quot;. Berkas tersebut harus berada pada folder yang sama dengan &quot;Songs.DB&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="225"/>
        <source>This file is not a valid EasyWorship database.</source>
        <translation>Berkas ini bukanlah sebuah basis-data EasyWorship yang valid.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="254"/>
        <source>Could not retrieve encoding.</source>
        <translation>Pengodean tak dapat diperoleh kembali.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="337"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="350"/>
        <source>This does not appear to be a valid Easy Worship 6 database directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="352"/>
        <source>This is not a valid Easy Worship 6 database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="426"/>
        <source>Unexpected data formatting.</source>
        <translation>Pemformatan data tak terduga.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="431"/>
        <source>No song text found.</source>
        <translation>Teks lagu tidak ditemukan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="471"/>
        <source>
[above are Song Tags with notes imported from EasyWorship]</source>
        <translation>
[di atas adalah Label Lagu beserta catatannya yang diimpor dari EasyWorship]</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="134"/>
        <source>Meta Data</source>
        <translation>Meta Data</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="155"/>
        <source>Custom Book Names</source>
        <translation>Nama Kitab Kustom</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditSongForm</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="427"/>
        <source>&amp;Save &amp;&amp; Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="301"/>
        <source>Song Editor</source>
        <translation>Penyunting Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="302"/>
        <source>&amp;Title:</source>
        <translation>&amp;Judul:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="303"/>
        <source>Alt&amp;ernate title:</source>
        <translation>Judul &amp;alternatif:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="304"/>
        <source>&amp;Lyrics:</source>
        <translation>&amp;Lirik:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="305"/>
        <source>&amp;Verse order:</source>
        <translation>&amp;Susunan bait:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="308"/>
        <source>Ed&amp;it All</source>
        <translation>Sunting &amp;Semua</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="310"/>
        <source>Title &amp;&amp; Lyrics</source>
        <translation>Judul &amp;&amp; Lirik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="313"/>
        <source>&amp;Add to Song</source>
        <translation>&amp;Tambahkan ke Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="314"/>
        <source>&amp;Edit Author Type</source>
        <translation>&amp;Sunting Jenis Pengarang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="337"/>
        <source>&amp;Remove</source>
        <translation>&amp;Hapus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="316"/>
        <source>&amp;Manage Authors, Topics, Songbooks</source>
        <translation>&amp;Kelola Pengarang, Topik, Buku Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="318"/>
        <source>A&amp;dd to Song</source>
        <translation>Tambahkan ke Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="319"/>
        <source>R&amp;emove</source>
        <translation>Singkirkan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="321"/>
        <source>Add &amp;to Song</source>
        <translation>Tambahkan &amp;ke Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="322"/>
        <source>Re&amp;move</source>
        <translation>Ha&amp;pus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="323"/>
        <source>Authors, Topics &amp;&amp; Songbooks</source>
        <translation>Pengarang, Topik, &amp;&amp; Buku Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="326"/>
        <source>New &amp;Theme</source>
        <translation>Tema &amp;Baru</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="327"/>
        <source>Copyright Information</source>
        <translation>Informasi Hak Cipta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="330"/>
        <source>Comments</source>
        <translation>Komentar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="331"/>
        <source>Theme, Copyright Info &amp;&amp; Comments</source>
        <translation>Tema, Info Hak Cipta, &amp;&amp; Komentar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="333"/>
        <source>Linked Audio</source>
        <translation>Audio Terhubung</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="335"/>
        <source>Add &amp;File(s)</source>
        <translation>Tambahkan &amp;(beberapa) Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="336"/>
        <source>Add &amp;Media</source>
        <translation>Tambahkan &amp;Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="338"/>
        <source>Remove &amp;All</source>
        <translation>Hapus &amp;Semua</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="340"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; Not all of the verses are in use.</source>
        <translation>&lt;strong&gt;Peringatan:&lt;/strong&gt; Tidak semua bait tersebut digunakan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="342"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; You have not entered a verse order.</source>
        <translation>&lt;strong&gt;Peringatan:&lt;/strong&gt; Anda belum memasukkan susunan bait.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="202"/>
        <source>There are no verses corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="207"/>
        <source>There is no verse corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="211"/>
        <source>Invalid Verse Order</source>
        <translation>Susunan Bait Tidak Valid</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="225"/>
        <source>You need to type in a song title.</source>
        <translation>Anda harus mengetikkan judul lagu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="231"/>
        <source>You need to type in at least one verse.</source>
        <translation>Anda harus mengetikkan setidaknya satu bait.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="237"/>
        <source>You need to have an author for this song.</source>
        <translation>Anda harus masukkan suatu pengarang untuk lagu ini.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="256"/>
        <source>There are misplaced formatting tags in the following verses:

{tag}

Please correct these tags before continuing.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="265"/>
        <source>You have {count} verses named {name} {number}. You can have at most 26 verses with the same name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>Add Author</source>
        <translation>Tambahkan Pengarang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>This author does not exist, do you want to add them?</source>
        <translation>Pengarang ini tidak ada, Anda ingin menambahkannya?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="603"/>
        <source>This author is already in the list.</source>
        <translation>Pengarang ini sudah ada dalam daftar.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="610"/>
        <source>You have not selected a valid author. Either select an author from the list, or type in a new author and click the &quot;Add Author to Song&quot; button to add the new author.</source>
        <translation>Anda belum memilih pengarang yang valid. Pilihlah suatu pengarang dari daftar, atau ketik suatu pengarang baru dan klik tombol &quot;Tambahkan Pengarang ke Lagu&quot; untuk menambahkan pengarang baru tersebut.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Edit Author Type</source>
        <translation>Sunting Jenis Pengarang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Choose type for this author</source>
        <translation>Pilih jenis untuk pengarang ini</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>Add Topic</source>
        <translation>Tambahkan Topik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>This topic does not exist, do you want to add it?</source>
        <translation>Topik ini tidak ada, Anda ingin menambahkannya?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="679"/>
        <source>This topic is already in the list.</source>
        <translation>Topik ini sudah ada dalam daftar.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="688"/>
        <source>You have not selected a valid topic. Either select a topic from the list, or type in a new topic and click the &quot;Add Topic to Song&quot; button to add the new topic.</source>
        <translation>Anda belum memilih topik yang valid. Pilihlah suatu topik dari daftar, atau ketik sebuah topik baru dan klik tombol &quot;Tambahkan Topik ke Lagu&quot; untuk menambahkan topik baru tersebut.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>Add Songbook</source>
        <translation>Tambahkan Buku Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>This Songbook does not exist, do you want to add it?</source>
        <translation>Buku lagu ini tidak ada, Anda ingin menambahkannya?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="724"/>
        <source>This Songbook is already in the list.</source>
        <translation>Buku Lagu ini sudah ada dalam daftar.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="732"/>
        <source>You have not selected a valid Songbook. Either select a Songbook from the list, or type in a new Songbook and click the &quot;Add to Song&quot; button to add the new Songbook.</source>
        <translation>Anda belum memilih Buku Lagu yang valid. Pilihlah sebuah Buku Lagu dari daftar, atau ketik sebuah Buku Lagu baru dan klik tombol &quot;Tambahkan ke Lagu&quot; untuk menambahkan Buku Lagu baru tersebut.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="932"/>
        <source>Open File(s)</source>
        <translation>Buka (beberapa) Berkas</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="91"/>
        <source>Edit Verse</source>
        <translation>Sunting Bait</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="92"/>
        <source>&amp;Verse type:</source>
        <translation>&amp;Jenis bait:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="102"/>
        <source>&amp;Forced Split</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="103"/>
        <source>Split the verse when displayed regardless of the screen size.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="105"/>
        <source>&amp;Insert</source>
        <translation>&amp;Sisipkan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="106"/>
        <source>Split a slide into two by inserting a verse splitter.</source>
        <translation>Pisah salindia menggunakan pemisah bait.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="109"/>
        <source>Transpose:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="110"/>
        <source>Up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="111"/>
        <source>Down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="146"/>
        <source>Transposing failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="240"/>
        <source>Invalid Chord</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.ExportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="128"/>
        <source>Select Destination Folder</source>
        <translation>Pilih Folder Tujuan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="143"/>
        <source>Song Export Wizard</source>
        <translation>Wisaya Ekspor Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="146"/>
        <source>This wizard will help to export your songs to the open and free &lt;strong&gt;OpenLyrics &lt;/strong&gt; worship song format.</source>
        <translation>Wisaya ini akan membantu Anda mengekspor lagu ke format lagu penyembahan &lt;strong&gt;OpenLyrics&lt;/strong&gt; yang terbuka dan gratis.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="149"/>
        <source>Select Songs</source>
        <translation>Pilih Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="150"/>
        <source>Check the songs you want to export.</source>
        <translation>Centang lagu-lagu yang ingin Anda ekspor:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="153"/>
        <source>Uncheck All</source>
        <translation>Hapus Semua Centang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="154"/>
        <source>Check All</source>
        <translation>Centang Semua</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="155"/>
        <source>Select Directory</source>
        <translation>Pilih Direktori</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="156"/>
        <source>Select the directory where you want the songs to be saved.</source>
        <translation>Pilih direktori untuk menyimpan lagu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="158"/>
        <source>Directory:</source>
        <translation>Direktori:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="159"/>
        <source>Exporting</source>
        <translation>Mengekspor</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="160"/>
        <source>Please wait while your songs are exported.</source>
        <translation>Silakan tunggu selama lagu diekspor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="176"/>
        <source>You need to add at least one Song to export.</source>
        <translation>Anda harus masukkan setidaknya satu lagu untuk diekspor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>No Save Location specified</source>
        <translation>Lokasi Penyimpanan Belum Ditentukan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>You need to specify a directory.</source>
        <translation>Anda harus menentukan sebuah direktori.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="239"/>
        <source>Starting export...</source>
        <translation>Memulai ekspor...</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.FoilPresenterSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/foilpresenter.py" line="387"/>
        <source>Invalid Foilpresenter song file. No verses found.</source>
        <translation>Berkas lagu Foilpresenter tidak valid. Bait tidak ditemukan.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="339"/>
        <source>Enable search as you type</source>
        <translation>Gunakan penelusuran saat Anda mengetikkannya</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="134"/>
        <source>Song Import Wizard</source>
        <translation>Wisaya Impor Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="137"/>
        <source>This wizard will help you to import songs from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>Wisaya ini akan membantu Anda mengimpor lagu dari berbagai format. Klik tombol Selanjutnya di bawah untuk memulai proses dengan memilih format untuk diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="152"/>
        <source>Add Files...</source>
        <translation>Tambahkan Berkas...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="154"/>
        <source>Remove File(s)</source>
        <translation>Hapus (beberapa) Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="165"/>
        <source>Please wait while your songs are imported.</source>
        <translation>Silakan tunggu selama lagu diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="169"/>
        <source>Copy</source>
        <translation>Salin</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="170"/>
        <source>Save to File</source>
        <translation>Simpan jadi Berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="336"/>
        <source>Your Song import failed. {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="197"/>
        <source>This importer has been disabled.</source>
        <translation>Pengimpor telah dinonaktifkan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="209"/>
        <source>OpenLyrics Files</source>
        <translation>Berkas OpenLyrics</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="210"/>
        <source>OpenLyrics or OpenLP 2 Exported Song</source>
        <translation>Lagu OpenLyrics atau OpenLP 2.0 yang telah diekspor</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="217"/>
        <source>OpenLP 2 Databases</source>
        <translation>Basis-data OpenLP 2</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="220"/>
        <source>Generic Document/Presentation</source>
        <translation>Dokumen / Presentasi Generik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="223"/>
        <source>The generic document/presentation importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation>Pengimpor dokumen / presentasi generik telah dinonaktifkan karena OpenLP tidak dapat mengakses OpenOffice atau LibreOffice.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="226"/>
        <source>Select Document/Presentation Files</source>
        <translation>Pilih Dokumen / Berkas Presentasi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="232"/>
        <source>CCLI SongSelect Files</source>
        <translation>Berkas CCLI SongSelect</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="239"/>
        <source>ChordPro Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="246"/>
        <source>DreamBeam Song Files</source>
        <translation>Berkas Lagu DreamBeam</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="253"/>
        <source>EasySlides XML File</source>
        <translation>Berkas XML EasySlides</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="260"/>
        <source>EasyWorship Song Database</source>
        <translation>Basis-Data Lagu EasyWorship</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="268"/>
        <source>EasyWorship 6 Song Data Directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="276"/>
        <source>EasyWorship Service File</source>
        <translation>Berkas Layanan EasyWorship</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="283"/>
        <source>Foilpresenter Song Files</source>
        <translation>Berkas Lagu Foilpresenter</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="291"/>
        <source>LiveWorship Database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="298"/>
        <source>LyriX Files</source>
        <translation>Berkas LyriX</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="299"/>
        <source>LyriX (Exported TXT-files)</source>
        <translation>Lyrix (berkas TXT yang telah diekspor)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="306"/>
        <source>MediaShout Database</source>
        <translation>Basis-data MediaShout</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="307"/>
        <source>The MediaShout importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation>Pengimpor MediaShout hanya didukung dalam Windows. Pengimpor telah dinonaktifkan karena ada modul Python yang hilang. Jika Anda ingin menggunakan pengimpor ini, Anda harus memasang modul &quot;pyodbc&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="323"/>
        <source>OPS Pro database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="324"/>
        <source>The OPS Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="333"/>
        <source>PowerPraise Song Files</source>
        <translation>Berkas Lagu PowerPraise</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="340"/>
        <source>You need to specify a valid PowerSong 1.0 database folder.</source>
        <translation>Anda harus menentukan folder basis-data PowerSong 1.0 yang valid.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="347"/>
        <source>PresentationManager Song Files</source>
        <translation>Berkas Lagu PresentationManager</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="354"/>
        <source>ProPresenter Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="361"/>
        <source>Singing The Faith Exported Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="363"/>
        <source>First use Singing The Faith Electonic edition to export the song(s) in Text format.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="371"/>
        <source>SongBeamer Files</source>
        <translation>Berkas SongBeamer</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="379"/>
        <source>SongPro Text Files</source>
        <translation>Berkas Teks SongPro</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="380"/>
        <source>SongPro (Export File)</source>
        <translation>SongPro (Berkas Ekspor)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="381"/>
        <source>In SongPro, export your songs using the File -&gt; Export menu</source>
        <translation>Pada SongPro, ekspor lagu menggunakan menu Berkas -&gt; Ekspor</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="388"/>
        <source>SongShow Plus Song Files</source>
        <translation>Berkas Lagu SongShow Plus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="395"/>
        <source>Songs Of Fellowship Song Files</source>
        <translation>Berkas Lagu Song Of Fellowship</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="397"/>
        <source>The Songs of Fellowship importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation>Pengimpor Songs of Fellowhip telah dinonaktifkan karena OpenLP tidak dapat mengakses OpenOffice atau LibreOffice.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="405"/>
        <source>SundayPlus Song Files</source>
        <translation>Berkas Lagu SundayPlus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="412"/>
        <source>VideoPsalm Files</source>
        <translation>Berkas VideoPsalm</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="413"/>
        <source>VideoPsalm</source>
        <translation>VideoPsalm</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="414"/>
        <source>The VideoPsalm songbooks are normally located in {path}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="422"/>
        <source>Words Of Worship Song Files</source>
        <translation>Berkas Lagu Words of Worship</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="430"/>
        <source>Worship Assistant Files</source>
        <translation>Berkas Worship Assistant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="432"/>
        <source>Worship Assistant (CSV)</source>
        <translation>Worship Assistant (CSV)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="433"/>
        <source>In Worship Assistant, export your Database to a CSV file.</source>
        <translation>Dalam Worship Assistant, ekspor Basis-Data Anda ke sebuah berkas CSV.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="441"/>
        <source>WorshipCenter Pro Song Files</source>
        <translation>Berkas Lagu WorshipCenter Pro</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="443"/>
        <source>The WorshipCenter Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation>Pengimpor WorshipCenter Pro hanya didukung dalam Windows. Pengimpor telah dinonaktifkan karena ada modul Python yang hilang. Jika Anda ingin menggunakan pengimpor ini, Anda harus memasang modul &quot;pyodbc&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="453"/>
        <source>ZionWorx (CSV)</source>
        <translation>ZionWorx (CSV)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="454"/>
        <source>First convert your ZionWorx database to a CSV text file, as explained in the &lt;a href=&quot;http://manual.openlp.org/songs.html#importing-from-zionworx&quot;&gt;User Manual&lt;/a&gt;.</source>
        <translation>Pertama konversi dahulu basis-data ZionWorx ke berkas teks CSV, seperti yang dijelaskan pada &lt;a href=&quot;http://manual.openlp.org/songs.html#importing-from-zionworx&quot;&gt;Panduan Pengguna&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.LiveWorshipImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="87"/>
        <source>Extracting data from database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="133"/>
        <source>Could not find Valentina DB ADK libraries </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="161"/>
        <source>Loading the extracting data</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.LyrixImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>File {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>Error: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaFilesForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="65"/>
        <source>Select Media File(s)</source>
        <translation>Pilih (beberapa) Berkas Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="66"/>
        <source>Select one or more audio files from the list below, and click OK to import them into this song.</source>
        <translation>Pilih satu atau lebih berkas audio dari daftar di bawah, dan klik OK untuk mengimpornya ke lagu ini.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="721"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Titles</source>
        <translation>Judul</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Titles...</source>
        <translation>Telusuri Judul...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="140"/>
        <source>Maintain the lists of authors, topics and books.</source>
        <translation>Kelola daftar pengarang, topik, dan buku.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Entire Song</source>
        <translation>Keseluruhan Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Entire Song...</source>
        <translation>Telusuri Seluruh Lagu...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Lyrics</source>
        <translation>Lirik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Lyrics...</source>
        <translation>Telusuri Lirik...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Authors...</source>
        <translation>Telusuri Pengarang...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Topics...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Songbooks...</source>
        <translation>Telusuri Buku Lagu...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Copyright</source>
        <translation>Hak Cipta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Copyright...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>CCLI number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search CCLI number...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="512"/>
        <source>Are you sure you want to delete the following songs?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="540"/>
        <source>copy</source>
        <comment>For song cloning</comment>
        <translation>salin</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="642"/>
        <source>Media</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="699"/>
        <source>CCLI License: </source>
        <translation>Lisensi CCLI:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="729"/>
        <source>Failed to render Song footer html.
See log for details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaShoutImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/mediashout.py" line="62"/>
        <source>Unable to open the MediaShout database.</source>
        <translation>Tidak dapat membuka basis-data MediaShout</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.OPSProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="65"/>
        <source>Unable to connect the OPS Pro database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="87"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLPSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openlp.py" line="109"/>
        <source>Not a valid OpenLP 2 song database.</source>
        <translation>Bukan basis-data lagu OpenLP 2.0 yang valid.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLyricsExport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsexport.py" line="68"/>
        <source>Exporting &quot;{title}&quot;...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opensong.py" line="139"/>
        <source>Invalid OpenSong song file. Missing song tag.</source>
        <translation>Berkas lagu OpenSong tidak valid. Label lagu hilang.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.PowerSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No songs to import.</source>
        <translation>Tidak ada lagu untuk diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No {text} files found.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="118"/>
        <source>Invalid {text} file. Unexpected byte value.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="136"/>
        <source>Invalid {text} file. Missing &quot;TITLE&quot; header.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="142"/>
        <source>Invalid {text} file. Missing &quot;COPYRIGHTLINE&quot; header.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="148"/>
        <source>Verses not found. Missing &quot;PART&quot; header.</source>
        <translation>Bait tidak ditemukan. Header &quot;PART&quot; hilang.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.PresentationManagerImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/presentationmanager.py" line="57"/>
        <source>File is not in XML-format, which is the only format supported.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SingingTheFaithImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="192"/>
        <source>Unknown hint {hint}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>File {file}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>Error: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongBookForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="66"/>
        <source>Songbook Maintenance</source>
        <translation>Pengelolaan Buku Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="67"/>
        <source>&amp;Name:</source>
        <translation>&amp;Nama:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="68"/>
        <source>&amp;Publisher:</source>
        <translation>&amp;Penerbit:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookform.py" line="61"/>
        <source>You need to type in a name for the book.</source>
        <translation>Anda harus mengetikkan nama untuk buku lagu.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongExportForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="253"/>
        <source>Finished export. To import these files use the &lt;strong&gt;OpenLyrics&lt;/strong&gt; importer.</source>
        <translation>Ekspor selesai. Untuk mengimpor berkas ini gunakan pengimpor &lt;strong&gt;OpenLyrics&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="257"/>
        <source>Your song export failed.</source>
        <translation>Ekspor lagu gagal.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="259"/>
        <source>Your song export failed because this error occurred: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="67"/>
        <source>Cannot access OpenOffice or LibreOffice</source>
        <translation>Tidak dapat mengakses OpenOffice atau LibreOffice</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="82"/>
        <source>Unable to open file</source>
        <translation>Tidak dapat membuka berkas</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="84"/>
        <source>File not found</source>
        <translation>Berkas tidak ditemukan</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="104"/>
        <source>copyright</source>
        <translation>hak cipta</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="118"/>
        <source>The following songs could not be imported:</source>
        <translation>Lagu berikut tidak dapat diimpor:</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongMaintenanceForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="248"/>
        <source>Could not add your author.</source>
        <translation>Tidak dapat menambahkan pengarang.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="251"/>
        <source>This author already exists.</source>
        <translation>Pengarang sudah ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="264"/>
        <source>Could not add your topic.</source>
        <translation>Tidak dapat menambahkan topik.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="267"/>
        <source>This topic already exists.</source>
        <translation>Topik sudah ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="281"/>
        <source>Could not add your book.</source>
        <translation>Tidak dapat menambahkan buku lagu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="284"/>
        <source>This book already exists.</source>
        <translation>Buku lagu sudah ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="389"/>
        <source>Could not save your changes.</source>
        <translation>Tidak dapat menyimpan perubahan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="316"/>
        <source>The author {original} already exists. Would you like to make songs with author {new} use the existing author {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="329"/>
        <source>Could not save your modified author, because the author already exists.</source>
        <translation>Tidak dapat menyimpan pengarang yang telah dimodifikasi, karena sudah ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="352"/>
        <source>The topic {original} already exists. Would you like to make songs with topic {new} use the existing topic {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="362"/>
        <source>Could not save your modified topic, because it already exists.</source>
        <translation>Tidak dapat menyimpan topik yang telah dimodifikasi, karena sudah ada.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="391"/>
        <source>The book {original} already exists. Would you like to make songs with book {new} use the existing book {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Delete Author</source>
        <translation>Hapus Pengarang</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Are you sure you want to delete the selected author?</source>
        <translation>Anda yakin ingin menghapus pengarang terpilih?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>This author cannot be deleted, they are currently assigned to at least one song.</source>
        <translation>Pengarang tidak dapat dihapus, karena masih terkait dengan setidaknya satu lagu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Delete Topic</source>
        <translation>Hapus Topik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Are you sure you want to delete the selected topic?</source>
        <translation>Anda yakin ingin menghapus topik terpilih?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>This topic cannot be deleted, it is currently assigned to at least one song.</source>
        <translation>Topik tidak dapat dihapus, karena masih terkait dengan setidaknya satu lagu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Delete Book</source>
        <translation>Hapus Buku Lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Are you sure you want to delete the selected book?</source>
        <translation>Anda yakin ingin menghapus buku lagu terpilih?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>This book cannot be deleted, it is currently assigned to at least one song.</source>
        <translation>Buku lagu tidak dapat dihapus, karena masih terkait dengan setidaknya satu lagu.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongSelectForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="233"/>
        <source>CCLI SongSelect Importer</source>
        <translation>Pengimpor CCLI SongSelect</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="234"/>
        <source>&lt;strong&gt;Note:&lt;/strong&gt; An Internet connection is required in order to import songs from CCLI SongSelect.</source>
        <translation>&lt;strong&gt;Catatan:&lt;/strong&gt; Sambungan internet dibutuhkan untuk mengimpor lagu-lagu dari CCLI SongSelect.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="238"/>
        <source>Username:</source>
        <translation>Nama Pengguna:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="239"/>
        <source>Password:</source>
        <translation>Kata sandi:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="240"/>
        <source>Save username and password</source>
        <translation>Simpan nama pengguna dan kata sandi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="241"/>
        <source>Login</source>
        <translation>Masuk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="242"/>
        <source>Search Text:</source>
        <translation>Telusuri Teks:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="243"/>
        <source>Search</source>
        <translation>Penelusuran</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="244"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="346"/>
        <source>Found {count:d} song(s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="247"/>
        <source>Logout</source>
        <translation>Keluar</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="248"/>
        <source>View</source>
        <translation>Tinjau</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="249"/>
        <source>Title:</source>
        <translation>Judul</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="250"/>
        <source>Author(s):</source>
        <translation>Pengarang (- pengarang) :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="251"/>
        <source>Copyright:</source>
        <translation>Hak Cipta:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="252"/>
        <source>CCLI Number:</source>
        <translation>Nomor CCLI:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="253"/>
        <source>Lyrics:</source>
        <translation>Lirik:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="254"/>
        <source>Back</source>
        <translation>Kembali</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="255"/>
        <source>Import</source>
        <translation>Impor</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>More than 1000 results</source>
        <translation>Lebih dari 1000 hasil</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>Your search has returned more than 1000 results, it has been stopped. Please refine your search to fetch better results.</source>
        <translation>Penelusuran Anda menghasilkan lebih dari 1000 hasil sehingga dihentikan. Silakan perbaiki penelusuran Anda untuk mendapatkan hasil yang lebih baik.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="139"/>
        <source>Logging out...</source>
        <translation>Menuju keluar...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>Incomplete song</source>
        <translation>Lagu yang tidak lengkap</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>This song is missing some information, like the lyrics, and cannot be imported.</source>
        <translation>Lagu ini kehilangan beberapa informasi, kemungkinan liriknya, dan tidak dapat diimpor.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>Save Username and Password</source>
        <translation>Simpan Nama Pengguna dan Kata Sandi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>WARNING: Saving your username and password is INSECURE, your password is stored in PLAIN TEXT. Click Yes to save your password or No to cancel this.</source>
        <translation>PERINGATAN: Menyimpan nama pengguna dan kata sandi Anda TIDAKLAH AMAN, kata sandi Anda akan tersimpan dalam TEKS BIASA. Klik Ya untuk menyimpannya atau Tidak untuk membatalkannya.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>Error Logging In</source>
        <translation>Kesalahan Saat Hendak Masuk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>There was a problem logging in, perhaps your username or password is incorrect?</source>
        <translation>Terjadi kesalahan saat hendak masuk, mungkin nama pengguna atau kata sandi Anda salah?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>Free user</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>You logged in with a free account, the search will be limited to songs in the public domain.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Song Imported</source>
        <translation>Lagu Telah Diimpor</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Your song has been imported, would you like to import more songs?</source>
        <translation>Lagu Anda telah diimpor, Anda ingin mengimpor lagu-lagu lain?</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongsTab</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="116"/>
        <source>Song related settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="117"/>
        <source>Enable &quot;Go to verse&quot; button in Live panel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="119"/>
        <source>Update service from song edit</source>
        <translation>Perbarui Layanan dari penyuntingan lagu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="120"/>
        <source>Import missing songs from Service files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="122"/>
        <source>Add Songbooks as first slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="124"/>
        <source>If enabled all text between &quot;[&quot; and &quot;]&quot; will be regarded as chords.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="126"/>
        <source>Chords</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="127"/>
        <source>Display chords in the main view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="128"/>
        <source>Ignore chords when importing songs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="130"/>
        <source>Chord notation to use:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="131"/>
        <source>English</source>
        <translation>Bahasa Inggris</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="132"/>
        <source>German</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="133"/>
        <source>Neo-Latin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="135"/>
        <source>Footer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="140"/>
        <source>Song Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="141"/>
        <source>Alternate Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="142"/>
        <source>Written By</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="143"/>
        <source>Authors when type is not set</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="145"/>
        <source>Authors (Type &quot;Words&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="147"/>
        <source>Authors (Type &quot;Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="149"/>
        <source>Authors (Type &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="151"/>
        <source>Authors (Type &quot;Translation&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="152"/>
        <source>Authors (Type &quot;Words&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="154"/>
        <source>Authors (Type &quot;Music&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="156"/>
        <source>Copyright information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="157"/>
        <source>Songbook Entries</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="159"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="160"/>
        <source>Song CCLI Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="161"/>
        <source>Topics</source>
        <translation>Topik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Placeholder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Description</source>
        <translation>Deskripsi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="172"/>
        <source>can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="173"/>
        <source>list of entries, can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="177"/>
        <source>How to use Footers:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Footer Template</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Mako Syntax</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="181"/>
        <source>Reset Template</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.TopicsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="60"/>
        <source>Topic Maintenance</source>
        <translation>Pengelolaan Topik</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="61"/>
        <source>Topic name:</source>
        <translation>Nama topik:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsform.py" line="58"/>
        <source>You need to type in a topic name.</source>
        <translation>Anda harus mengetikkan nama topik.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.VerseType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="149"/>
        <source>Verse</source>
        <translation>Bait</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="150"/>
        <source>Chorus</source>
        <translation>Refrain</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="151"/>
        <source>Bridge</source>
        <translation>Bridge</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="152"/>
        <source>Pre-Chorus</source>
        <translation>Pra-Refrain</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="153"/>
        <source>Intro</source>
        <translation>Intro</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="154"/>
        <source>Ending</source>
        <translation>Ending</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="155"/>
        <source>Other</source>
        <translation>Lainnya</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.VideoPsalmImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/videopsalm.py" line="134"/>
        <source>Error: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.WordsofWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/wordsofworship.py" line="177"/>
        <source>Invalid Words of Worship song file. Missing {text!r} header.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipAssistantImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Error reading CSV file.</source>
        <translation>Kesalahan pembacaan berkas CSV.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Line {number:d}: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="182"/>
        <source>Record {count:d}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="122"/>
        <source>Decoding error: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="127"/>
        <source>File not valid WorshipAssistant CSV format.</source>
        <translation>Berkas bukan berupa format CSV Worship Assistant yang valid.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipCenterProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipcenterpro.py" line="59"/>
        <source>Unable to connect the WorshipCenter Pro database.</source>
        <translation>Tidak dapat tersambung dengan basis-data WorshipCenter Pro.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.ZionWorxImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Error reading CSV file.</source>
        <translation>Kesalahan pembacaan berkas CSV.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Line {number:d}: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Record {index}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Decoding error: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="106"/>
        <source>File not valid ZionWorx CSV format.</source>
        <translation>Berkas bukan berupa format CSV ZionWorx yang valid.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="120"/>
        <source>Record %d</source>
        <translation>Rekaman %d</translation>
    </message>
</context>
<context>
    <name>Wizard</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="136"/>
        <source>Wizard</source>
        <translation>Wisaya</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="140"/>
        <source>This wizard will help you to remove duplicate songs from the song database. You will have a chance to review every potential duplicate song before it is deleted. So no songs will be deleted without your explicit approval.</source>
        <translation>Wisaya ini akan membantu Anda untuk menghapus lagu duplikat dari basis-data lagu. Anda akan memiliki kesempatan untuk meninjau setiap lagu yang berpotensi duplikat sebelum dihapus. Jadi tidak ada lagu yang akan dihapus tanpa persetujuan eksplisit Anda.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="145"/>
        <source>Searching for duplicate songs.</source>
        <translation>Menelusuri lagu-lagu duplikat.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="146"/>
        <source>Please wait while your songs database is analyzed.</source>
        <translation>Silakan tunggu selama basis-data lagu Anda dianalisa.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="148"/>
        <source>Here you can decide which songs to remove and which ones to keep.</source>
        <translation>Di sini Anda dapat menentukan lagu yang ingin dihapus ataupun disimpan.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="155"/>
        <source>Review duplicate songs ({current}/{total})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>Information</source>
        <translation>Informasi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>No duplicate songs have been found in the database.</source>
        <translation>Lagu duplikat tidak ditemukan di basis-data.</translation>
    </message>
</context>
<context>
    <name>common.languages</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>(Afan) Oromo</source>
        <comment>Language code: om</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Abkhazian</source>
        <comment>Language code: ab</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afar</source>
        <comment>Language code: aa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afrikaans</source>
        <comment>Language code: af</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Albanian</source>
        <comment>Language code: sq</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amharic</source>
        <comment>Language code: am</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amuzgo</source>
        <comment>Language code: amu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ancient Greek</source>
        <comment>Language code: grc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Arabic</source>
        <comment>Language code: ar</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Armenian</source>
        <comment>Language code: hy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Assamese</source>
        <comment>Language code: as</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Aymara</source>
        <comment>Language code: ay</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Azerbaijani</source>
        <comment>Language code: az</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bashkir</source>
        <comment>Language code: ba</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Basque</source>
        <comment>Language code: eu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bengali</source>
        <comment>Language code: bn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bhutani</source>
        <comment>Language code: dz</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bihari</source>
        <comment>Language code: bh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bislama</source>
        <comment>Language code: bi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Breton</source>
        <comment>Language code: br</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bulgarian</source>
        <comment>Language code: bg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Burmese</source>
        <comment>Language code: my</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Byelorussian</source>
        <comment>Language code: be</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cakchiquel</source>
        <comment>Language code: cak</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cambodian</source>
        <comment>Language code: km</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Catalan</source>
        <comment>Language code: ca</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Chinese</source>
        <comment>Language code: zh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Comaltepec Chinantec</source>
        <comment>Language code: cco</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Corsican</source>
        <comment>Language code: co</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Croatian</source>
        <comment>Language code: hr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Czech</source>
        <comment>Language code: cs</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Danish</source>
        <comment>Language code: da</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Dutch</source>
        <comment>Language code: nl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>English</source>
        <comment>Language code: en</comment>
        <translation>Bahasa Inggris</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Esperanto</source>
        <comment>Language code: eo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Estonian</source>
        <comment>Language code: et</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Faeroese</source>
        <comment>Language code: fo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Fiji</source>
        <comment>Language code: fj</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Finnish</source>
        <comment>Language code: fi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>French</source>
        <comment>Language code: fr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Frisian</source>
        <comment>Language code: fy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Galician</source>
        <comment>Language code: gl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Georgian</source>
        <comment>Language code: ka</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>German</source>
        <comment>Language code: de</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greek</source>
        <comment>Language code: el</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greenlandic</source>
        <comment>Language code: kl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Guarani</source>
        <comment>Language code: gn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Gujarati</source>
        <comment>Language code: gu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Haitian Creole</source>
        <comment>Language code: ht</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hausa</source>
        <comment>Language code: ha</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hebrew (former iw)</source>
        <comment>Language code: he</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hiligaynon</source>
        <comment>Language code: hil</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hindi</source>
        <comment>Language code: hi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hungarian</source>
        <comment>Language code: hu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Icelandic</source>
        <comment>Language code: is</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Indonesian (former in)</source>
        <comment>Language code: id</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingua</source>
        <comment>Language code: ia</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingue</source>
        <comment>Language code: ie</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inuktitut (Eskimo)</source>
        <comment>Language code: iu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inupiak</source>
        <comment>Language code: ik</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Irish</source>
        <comment>Language code: ga</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Italian</source>
        <comment>Language code: it</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Jakalteko</source>
        <comment>Language code: jac</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Japanese</source>
        <comment>Language code: ja</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Javanese</source>
        <comment>Language code: jw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>K&apos;iche&apos;</source>
        <comment>Language code: quc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kannada</source>
        <comment>Language code: kn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kashmiri</source>
        <comment>Language code: ks</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kazakh</source>
        <comment>Language code: kk</comment>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kekchí </source>
        <comment>Language code: kek</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kinyarwanda</source>
        <comment>Language code: rw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirghiz</source>
        <comment>Language code: ky</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirundi</source>
        <comment>Language code: rn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Korean</source>
        <comment>Language code: ko</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kurdish</source>
        <comment>Language code: ku</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Laothian</source>
        <comment>Language code: lo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latin</source>
        <comment>Language code: la</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latvian, Lettish</source>
        <comment>Language code: lv</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lingala</source>
        <comment>Language code: ln</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lithuanian</source>
        <comment>Language code: lt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Macedonian</source>
        <comment>Language code: mk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malagasy</source>
        <comment>Language code: mg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malay</source>
        <comment>Language code: ms</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malayalam</source>
        <comment>Language code: ml</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maltese</source>
        <comment>Language code: mt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mam</source>
        <comment>Language code: mam</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mri</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Marathi</source>
        <comment>Language code: mr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Moldavian</source>
        <comment>Language code: mo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mongolian</source>
        <comment>Language code: mn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nahuatl</source>
        <comment>Language code: nah</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nauru</source>
        <comment>Language code: na</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nepali</source>
        <comment>Language code: ne</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Norwegian</source>
        <comment>Language code: no</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Occitan</source>
        <comment>Language code: oc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Oriya</source>
        <comment>Language code: or</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Pashto, Pushto</source>
        <comment>Language code: ps</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Persian</source>
        <comment>Language code: fa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Plautdietsch</source>
        <comment>Language code: pdt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Polish</source>
        <comment>Language code: pl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Portuguese</source>
        <comment>Language code: pt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Punjabi</source>
        <comment>Language code: pa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Quechua</source>
        <comment>Language code: qu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Rhaeto-Romance</source>
        <comment>Language code: rm</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Romanian</source>
        <comment>Language code: ro</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Russian</source>
        <comment>Language code: ru</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Samoan</source>
        <comment>Language code: sm</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sangro</source>
        <comment>Language code: sg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sanskrit</source>
        <comment>Language code: sa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Scots Gaelic</source>
        <comment>Language code: gd</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbian</source>
        <comment>Language code: sr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbo-Croatian</source>
        <comment>Language code: sh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sesotho</source>
        <comment>Language code: st</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Setswana</source>
        <comment>Language code: tn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Shona</source>
        <comment>Language code: sn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sindhi</source>
        <comment>Language code: sd</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Singhalese</source>
        <comment>Language code: si</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Siswati</source>
        <comment>Language code: ss</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovak</source>
        <comment>Language code: sk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovenian</source>
        <comment>Language code: sl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Somali</source>
        <comment>Language code: so</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Spanish</source>
        <comment>Language code: es</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sudanese</source>
        <comment>Language code: su</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swahili</source>
        <comment>Language code: sw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swedish</source>
        <comment>Language code: sv</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tagalog</source>
        <comment>Language code: tl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tajik</source>
        <comment>Language code: tg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tamil</source>
        <comment>Language code: ta</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tatar</source>
        <comment>Language code: tt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tegulu</source>
        <comment>Language code: te</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Thai</source>
        <comment>Language code: th</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tibetan</source>
        <comment>Language code: bo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tigrinya</source>
        <comment>Language code: ti</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tonga</source>
        <comment>Language code: to</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tsonga</source>
        <comment>Language code: ts</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkish</source>
        <comment>Language code: tr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkmen</source>
        <comment>Language code: tk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Twi</source>
        <comment>Language code: tw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uigur</source>
        <comment>Language code: ug</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ukrainian</source>
        <comment>Language code: uk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Urdu</source>
        <comment>Language code: ur</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uspanteco</source>
        <comment>Language code: usp</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uzbek</source>
        <comment>Language code: uz</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Vietnamese</source>
        <comment>Language code: vi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Volapuk</source>
        <comment>Language code: vo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Welch</source>
        <comment>Language code: cy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Wolof</source>
        <comment>Language code: wo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Xhosa</source>
        <comment>Language code: xh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yiddish (former ji)</source>
        <comment>Language code: yi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yoruba</source>
        <comment>Language code: yo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zhuang</source>
        <comment>Language code: za</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zulu</source>
        <comment>Language code: zu</comment>
        <translation type="unfinished"/>
    </message>
</context>
</TS>
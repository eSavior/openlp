<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.0">
<context>
    <name>AlertsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>&amp;Alert</source>
        <translation>&amp;Alerte</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>Show an alert message.</source>
        <translation>Afficher un message d&apos;alerte.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="212"/>
        <source>&lt;strong&gt;Alerts Plugin&lt;/strong&gt;&lt;br /&gt;The alert plugin controls the displaying of alerts on the display screen.</source>
        <translation>&lt;strong&gt;Module Alertes&lt;/strong&gt;&lt;br /&gt;Le module d&apos;alerte permet l&apos;affichage de messages d&apos;avertissements sur l&apos;écran de présentation.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="222"/>
        <source>Alert</source>
        <comment>name singular</comment>
        <translation>Alerte</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="223"/>
        <source>Alerts</source>
        <comment>name plural</comment>
        <translation>Alertes</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="227"/>
        <source>Alerts</source>
        <comment>container title</comment>
        <translation>Alertes</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertForm</name>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="93"/>
        <source>Alert Message</source>
        <translation>Message d&apos;alerte</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="94"/>
        <source>Alert &amp;text:</source>
        <translation>&amp;Texte d&apos;Alerte :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="95"/>
        <source>&amp;Parameter:</source>
        <translation>&amp;Paramètre :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="96"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="97"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="98"/>
        <source>Displ&amp;ay</source>
        <translation>A&amp;fficher</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="99"/>
        <source>Display &amp;&amp; Cl&amp;ose</source>
        <translation>&amp;Afficher &amp;&amp; Fermer</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>New Alert</source>
        <translation>Nouvelle alerte</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>You haven't specified any text for your alert. 
Please type in some text before clicking New.</source>
        <translation>Vous n&apos;avez pas spécifié de texte pour votre alerte. 
Veuillez entrer votre message puis cliquer sur Nouveau.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>No Parameter Found</source>
        <translation>Aucun paramètre n&apos;a été trouvé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>You have not entered a parameter to be replaced.
Do you want to continue anyway?</source>
        <translation>Vous n&apos;avez entré aucun paramètre à remplacer.
Voulez-vous tout de même continuer ?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>No Placeholder Found</source>
        <translation>Aucun espace réservé trouvé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>The alert text does not contain '&lt;&gt;'.
Do you want to continue anyway?</source>
        <translation>Le texte d&apos;alerte ne contient pas &apos;&lt;&gt;&apos;.
Voulez-vous tout de même continuer ?</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsManager</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertsmanager.py" line="73"/>
        <source>Alert message created and displayed.</source>
        <translation>Message d&apos;alerte créé et affiché.</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsTab</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="119"/>
        <source>Font Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="120"/>
        <source>Font name:</source>
        <translation>Nom de la police :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="121"/>
        <source>Font color:</source>
        <translation>Couleur de la police :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="123"/>
        <source>Font size:</source>
        <translation>Taille de la police :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="125"/>
        <source>Background Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="126"/>
        <source>Other Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="127"/>
        <source>Alert timeout:</source>
        <translation>Temps d&apos;alerte :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="129"/>
        <source>Repeat (no. of times):</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="130"/>
        <source>Enable Scrolling</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin</name>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="125"/>
        <source>&amp;Bible</source>
        <translation>&amp;Bible</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="141"/>
        <source>&lt;strong&gt;Bible Plugin&lt;/strong&gt;&lt;br /&gt;The Bible plugin provides the ability to display Bible verses from different sources during the service.</source>
        <translation>&lt;strong&gt;Module Bible&lt;/strong&gt;&lt;br /&gt;Le Module Bible permet d&apos;afficher des versets bibliques de différentes sources pendant le service.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="174"/>
        <source>Bible</source>
        <comment>name singular</comment>
        <translation>Bible</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="175"/>
        <source>Bibles</source>
        <comment>name plural</comment>
        <translation>Bibles</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="179"/>
        <source>Bibles</source>
        <comment>container title</comment>
        <translation>Bibles</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="184"/>
        <source>Import a Bible.</source>
        <translation>Importer une Bible.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="185"/>
        <source>Add a new Bible.</source>
        <translation>Ajoute une nouvelle Bible.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="186"/>
        <source>Edit the selected Bible.</source>
        <translation>Édite la bible sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="187"/>
        <source>Delete the selected Bible.</source>
        <translation>Supprime la Bible sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="188"/>
        <source>Preview the selected Bible.</source>
        <translation>Prévisualiser la Bible sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="189"/>
        <source>Send the selected Bible live.</source>
        <translation>Envoie la Bible sélectionnée au direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="190"/>
        <source>Add the selected Bible to the service.</source>
        <translation>Ajoute la Bible sélectionnée au service.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="76"/>
        <source>Genesis</source>
        <translation>Genèse</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="77"/>
        <source>Exodus</source>
        <translation>Exode</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="78"/>
        <source>Leviticus</source>
        <translation>Lévitique</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="79"/>
        <source>Numbers</source>
        <translation>Nombres</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="80"/>
        <source>Deuteronomy</source>
        <translation>Deutéronome</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="81"/>
        <source>Joshua</source>
        <translation>Josué</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="82"/>
        <source>Judges</source>
        <translation>Juges</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="83"/>
        <source>Ruth</source>
        <translation>Ruth</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="84"/>
        <source>1 Samuel</source>
        <translation>1 Samuel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="85"/>
        <source>2 Samuel</source>
        <translation>2 Samuel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="86"/>
        <source>1 Kings</source>
        <translation>1 Rois</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="87"/>
        <source>2 Kings</source>
        <translation>2 Rois</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="88"/>
        <source>1 Chronicles</source>
        <translation>1 Chroniques</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="89"/>
        <source>2 Chronicles</source>
        <translation>2 Chroniques</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="90"/>
        <source>Ezra</source>
        <translation>Esdras</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="91"/>
        <source>Nehemiah</source>
        <translation>Néhémie</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="92"/>
        <source>Esther</source>
        <translation>Esther</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="93"/>
        <source>Job</source>
        <translation>Job</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="94"/>
        <source>Psalms</source>
        <translation>Psaumes</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="95"/>
        <source>Proverbs</source>
        <translation>Proverbes</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="96"/>
        <source>Ecclesiastes</source>
        <translation>Ecclésiaste</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="97"/>
        <source>Song of Solomon</source>
        <translation>Cantique</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="98"/>
        <source>Isaiah</source>
        <translation>Esaïe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="99"/>
        <source>Jeremiah</source>
        <translation>Jérémie</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="100"/>
        <source>Lamentations</source>
        <translation>Lamentations</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="101"/>
        <source>Ezekiel</source>
        <translation>Ezéchiel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="102"/>
        <source>Daniel</source>
        <translation>Daniel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="103"/>
        <source>Hosea</source>
        <translation>Osée</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="104"/>
        <source>Joel</source>
        <translation>Joël</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="105"/>
        <source>Amos</source>
        <translation>Amos</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="106"/>
        <source>Obadiah</source>
        <translation>Abdias</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="107"/>
        <source>Jonah</source>
        <translation>Jonas</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="108"/>
        <source>Micah</source>
        <translation>Michée</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="109"/>
        <source>Nahum</source>
        <translation>Nahum</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="110"/>
        <source>Habakkuk</source>
        <translation>Habacuc</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="111"/>
        <source>Zephaniah</source>
        <translation>Sophonie</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="112"/>
        <source>Haggai</source>
        <translation>Aggée</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="113"/>
        <source>Zechariah</source>
        <translation>Zacharie</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="114"/>
        <source>Malachi</source>
        <translation>Malachie</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="115"/>
        <source>Matthew</source>
        <translation>Matthieu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="116"/>
        <source>Mark</source>
        <translation>Marc</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="117"/>
        <source>Luke</source>
        <translation>Luc</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="118"/>
        <source>John</source>
        <translation>Jean</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="119"/>
        <source>Acts</source>
        <translation>Actes</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="120"/>
        <source>Romans</source>
        <translation>Romains</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="121"/>
        <source>1 Corinthians</source>
        <translation>1 Corinthiens</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="122"/>
        <source>2 Corinthians</source>
        <translation>2 Corinthiens</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="123"/>
        <source>Galatians</source>
        <translation>Galates</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="124"/>
        <source>Ephesians</source>
        <translation>Ephésiens</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="125"/>
        <source>Philippians</source>
        <translation>Philippiens</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="126"/>
        <source>Colossians</source>
        <translation>Colossiens</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="127"/>
        <source>1 Thessalonians</source>
        <translation>1 Thessaloniciens</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="128"/>
        <source>2 Thessalonians</source>
        <translation>2 Thessaloniciens</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="129"/>
        <source>1 Timothy</source>
        <translation>1 Timothée</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="130"/>
        <source>2 Timothy</source>
        <translation>2 Timothée</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="131"/>
        <source>Titus</source>
        <translation>Tite</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="132"/>
        <source>Philemon</source>
        <translation>Philémon</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="133"/>
        <source>Hebrews</source>
        <translation>Hébreux</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="134"/>
        <source>James</source>
        <translation>Jacques</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="135"/>
        <source>1 Peter</source>
        <translation>1 Pierre</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="136"/>
        <source>2 Peter</source>
        <translation>2 Pierre</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="137"/>
        <source>1 John</source>
        <translation>1 Jean</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="138"/>
        <source>2 John</source>
        <translation>2 Jean</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="139"/>
        <source>3 John</source>
        <translation>3 Jean</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="140"/>
        <source>Jude</source>
        <translation>Jude</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="141"/>
        <source>Revelation</source>
        <translation>Apocalypse</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="142"/>
        <source>Judith</source>
        <translation>Judith</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="143"/>
        <source>Wisdom</source>
        <translation>Sagesse</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="144"/>
        <source>Tobit</source>
        <translation>Tobit</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="145"/>
        <source>Sirach</source>
        <translation>Siracide</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="146"/>
        <source>Baruch</source>
        <translation>Baruch</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="147"/>
        <source>1 Maccabees</source>
        <translation>1 Macchabées</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="148"/>
        <source>2 Maccabees</source>
        <translation>2 Macchabées</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="149"/>
        <source>3 Maccabees</source>
        <translation>3 Macchabées</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="150"/>
        <source>4 Maccabees</source>
        <translation>4 Macchabées</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="151"/>
        <source>Rest of Daniel</source>
        <translation>Suppléments de Daniel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="152"/>
        <source>Rest of Esther</source>
        <translation>Suppléments d&apos;Esther</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="153"/>
        <source>Prayer of Manasses</source>
        <translation>Prière de Manassé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="154"/>
        <source>Letter of Jeremiah</source>
        <translation>Lettre de Jérémie</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="155"/>
        <source>Prayer of Azariah</source>
        <translation>Prière d&apos;Azariah</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="156"/>
        <source>Susanna</source>
        <translation>Suzanne</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="157"/>
        <source>Bel</source>
        <translation>Bel</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="158"/>
        <source>1 Esdras</source>
        <translation>1 Esdras</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="159"/>
        <source>2 Esdras</source>
        <translation>2 Esdras</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>:</source>
        <comment>Verse identifier e.g. Genesis 1 : 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>v</source>
        <comment>Verse identifier e.g. Genesis 1 v 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>v</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>V</source>
        <comment>Verse identifier e.g. Genesis 1 V 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verse</source>
        <comment>Verse identifier e.g. Genesis 1 verse 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>verset</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verses</source>
        <comment>Verse identifier e.g. Genesis 1 verses 1 - 2 = Genesis Chapter 1 Verses 1 to 2</comment>
        <translation>versets</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>-</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>to</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>à</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>,</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2, 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>,</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>and</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2 and 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>et</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="185"/>
        <source>end</source>
        <comment>ending identifier e.g. Genesis 1 verse 1 - end = Genesis Chapter 1 Verses 1 To The Last Verse</comment>
        <translation>fin</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No Book Found</source>
        <translation>Aucun livre trouvé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No matching book could be found in this Bible. Check that you have spelled the name of the book correctly.</source>
        <translation>Aucun livre correspondant n&apos;a été trouvé dans cette Bible. Vérifiez que vous avez correctement écrit le nom du livre.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="64"/>
        <source>The proxy server {proxy} was found in the bible {name}.&lt;br&gt;Would you like to set it as the proxy for OpenLP?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="69"/>
        <source>both</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleEditForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="162"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>Vous devez spécifier le nom de la version de votre Bible.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="168"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>Vous devez définir un copyright pour votre Bible. Les Bibles dans le Domaine Publique doivent être indiquées comme telle.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>Bible Exists</source>
        <translation>Bible existante</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>Cette Bible existe déjà. Veuillez importer une autre Bible ou supprimer la Bible existante.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="190"/>
        <source>You need to specify a book name for &quot;{text}&quot;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="197"/>
        <source>The book name &quot;{name}&quot; is not correct.
Numbers can only be used at the beginning and must
be followed by one or more non-numeric characters.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>Duplicate Book Name</source>
        <translation>Dupliquer le nom du livre </translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>The Book Name &quot;{name}&quot; has been entered more than once.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="53"/>
        <source>The file &quot;{file}&quot; you supplied is compressed. You must decompress it before import.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="224"/>
        <source>unknown type of</source>
        <comment>This looks like an unknown type of XML bible.</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleManager</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Web Bible cannot be used in Text Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Text Search is not available with Web Bibles.
Please use the Scripture Reference Search instead.

This means that the currently selected Bible is a Web Bible.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>Scripture Reference Error</source>
        <translation>Écriture de référence erronée</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>&lt;strong&gt;The reference you typed is invalid!&lt;br&gt;&lt;br&gt;Please make sure that your reference follows one of these patterns:&lt;/strong&gt;&lt;br&gt;&lt;br&gt;%s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BiblesTab</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="178"/>
        <source>Verse Display</source>
        <translation>Affichage de versets</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="179"/>
        <source>Show verse numbers</source>
        <translation>Voir les numéros de versets</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="180"/>
        <source>Only show new chapter numbers</source>
        <translation>Afficher uniquement les nouveaux numéros de chapitre</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="183"/>
        <source>Bible theme:</source>
        <translation>Thème :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="187"/>
        <source>No Brackets</source>
        <translation>Pas de parenthèses</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="189"/>
        <source>( And )</source>
        <translation>( et )</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="191"/>
        <source>{ And }</source>
        <translation>{ et }</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="193"/>
        <source>[ And ]</source>
        <translation>[ et ]</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="195"/>
        <source>Note: Changes do not affect verses in the Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="197"/>
        <source>Display second Bible verses</source>
        <translation>Afficher les versets de la seconde Bible</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="198"/>
        <source>Custom Scripture References</source>
        <translation>Références bibliques personnalisées</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="199"/>
        <source>Verse separator:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="200"/>
        <source>Range separator:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="201"/>
        <source>List separator:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="202"/>
        <source>End mark:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="203"/>
        <source>Multiple alternative verse separators may be defined.
They have to be separated by a vertical bar &quot;|&quot;.
Please clear this edit line to use the default value.</source>
        <translation>Plusieurs séparateurs de versets peuvent être définis.⏎
Ils doivent être séparés par la barre verticale &quot;|&quot;.⏎
Veuillez supprimer cette ligne pour utiliser la valeur par défaut.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="210"/>
        <source>Default Bible Language</source>
        <translation>Langue de la bible par défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="211"/>
        <source>Book name language in search field,
search results and on display:</source>
        <translation>Langue du livre dans le champ de recherche,
résultats de recherche et à l&apos;affichage :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="213"/>
        <source>Bible Language</source>
        <translation>Langue de la bible</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="215"/>
        <source>Application Language</source>
        <translation>Langue de l&apos;application</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="217"/>
        <source>English</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="219"/>
        <source>Quick Search Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="220"/>
        <source>Reset search type to &quot;Text or Scripture Reference&quot; on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="223"/>
        <source>Don&apos;t show error if nothing is found in &quot;Text or Scripture Reference&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="226"/>
        <source>Search automatically while typing (Text search must contain a
minimum of {count} characters and a space for performance reasons)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="84"/>
        <source>Select Book Name</source>
        <translation>Sélectionner le nom du livre</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="85"/>
        <source>The following book name cannot be matched up internally. Please select the corresponding name from the list.</source>
        <translation>Le nom du livre suivant ne correspond pas. Veuillez sélectionner le bon nom dans la liste.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="88"/>
        <source>Current name:</source>
        <translation>Nom courant :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="89"/>
        <source>Corresponding name:</source>
        <translation>Nom correspondant :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="90"/>
        <source>Show Books From</source>
        <translation>Afficher le formulaire des livres</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="91"/>
        <source>Old Testament</source>
        <translation>Ancien testament</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="92"/>
        <source>New Testament</source>
        <translation>Nouveau testament</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="93"/>
        <source>Apocrypha</source>
        <translation>Apocryphes</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknameform.py" line="109"/>
        <source>You need to select a book.</source>
        <translation>Vous devez sélectionner un livre.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.CSVBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="123"/>
        <source>Importing books... {book}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="145"/>
        <source>Importing verses from {book}...</source>
        <comment>Importing verses from &lt;book name&gt;...</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="132"/>
        <source>Bible Editor</source>
        <translation>Éditeur de bible</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="136"/>
        <source>License Details</source>
        <translation>Détails de la licence</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="137"/>
        <source>Version name:</source>
        <translation>Nom de la version :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="138"/>
        <source>Copyright:</source>
        <translation>Droits d&apos;auteur :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="139"/>
        <source>Permissions:</source>
        <translation>Autorisations :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="140"/>
        <source>Full license:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="141"/>
        <source>Default Bible Language</source>
        <translation>Langue de la bible par défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="142"/>
        <source>Book name language in search field, search results and on display:</source>
        <translation>Langue du livre dans le champ de recherche, résultats de recherche et à l&apos;affichage :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="145"/>
        <source>Global Settings</source>
        <translation>Paramètres généraux</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="146"/>
        <source>Bible Language</source>
        <translation>Langue de la bible</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="149"/>
        <source>Application Language</source>
        <translation>Langue de l&apos;application</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="151"/>
        <source>English</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="93"/>
        <source>This is a Web Download Bible.
It is not possible to customize the Book Names.</source>
        <translation>C&apos;est une Bible Web téléchargée.
Ce n&apos;est pas possible de personnaliser le nom des livres.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="98"/>
        <source>To use the customized book names, &quot;Bible language&quot; must be selected on the Meta Data tab or, if &quot;Global settings&quot; is selected, on the Bible page in Configure OpenLP.</source>
        <translation>Pour utiliser les noms de livres personnalisés, &quot;Langue de bible&quot; doit être sélectionné à partir de l&apos;onglet Méta données ou, si &quot;Paramètres globales&quot; est sélectionné, dans la page Bible de la configuration d&apos;OpenLP.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.HTTPBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="607"/>
        <source>Registering Bible and loading books...</source>
        <translation>Enregistrement de la Bible et chargement des livres...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="623"/>
        <source>Registering Language...</source>
        <translation>Enregistrement des langues...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="630"/>
        <source>Importing {book}...</source>
        <comment>Importing &lt;book name&gt;...</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>Download Error</source>
        <translation>Erreur de téléchargement</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>There was a problem downloading your verse selection. Please check your Internet connection, and if this error continues to occur please consider reporting a bug.</source>
        <translation>Un problème de téléchargement de votre sélection de verset a été rencontré. Vérifiez votre connexion Internet et si cette erreur persiste merci de signaler ce dysfonctionnement.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>Parse Error</source>
        <translation>Erreur syntaxique</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>There was a problem extracting your verse selection. If this error continues to occur please consider reporting a bug.</source>
        <translation>Un problème a été rencontré durant l&apos;extraction de votre sélection de verset. Si cette erreur persiste merci de signaler ce dysfonctionnement.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="192"/>
        <source>CSV File</source>
        <translation>Fichier CSV</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="371"/>
        <source>Bible Import Wizard</source>
        <translation>Assistant d&apos;import de Bible</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="374"/>
        <source>This wizard will help you to import Bibles from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>Cet assistant vous permet d&apos;importer des bibles de différents formats. Cliquez sur le bouton suivant si dessous pour démarrer le processus en sélectionnant le format à importer.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="386"/>
        <source>Web Download</source>
        <translation>Téléchargement Web</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="396"/>
        <source>Bible file:</source>
        <translation>Fichier Bible :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="391"/>
        <source>Books file:</source>
        <translation>Fichier de livres :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="392"/>
        <source>Verses file:</source>
        <translation>Fichier de versets :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="394"/>
        <source>Location:</source>
        <translation>Emplacement :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="397"/>
        <source>Click to download bible list</source>
        <translation>Cliquer pour télécharger la liste des bibles</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="398"/>
        <source>Download bible list</source>
        <translation>Télécharge la liste des bibles</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="399"/>
        <source>Crosswalk</source>
        <translation>Crosswalk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="401"/>
        <source>BibleGateway</source>
        <translation>BibleGateway</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="403"/>
        <source>Bibleserver</source>
        <translation>Bibleserver</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="405"/>
        <source>Bible:</source>
        <translation>Bible :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="409"/>
        <source>Bibles:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="407"/>
        <source>SWORD data folder:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="408"/>
        <source>SWORD zip-file:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="410"/>
        <source>Import from folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="412"/>
        <source>Import from Zip-file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="417"/>
        <source>To import SWORD bibles the pysword python module must be installed. Please read the manual for instructions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="420"/>
        <source>License Details</source>
        <translation>Détails de la licence</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="422"/>
        <source>Set up the Bible&apos;s license details.</source>
        <translation>Mise en place des details de la licence de la Bible.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="424"/>
        <source>Version name:</source>
        <translation>Nom de la version :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="425"/>
        <source>Copyright:</source>
        <translation>Droits d&apos;auteur :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="426"/>
        <source>Permissions:</source>
        <translation>Autorisations :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="427"/>
        <source>Full license:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="429"/>
        <source>Please wait while your Bible is imported.</source>
        <translation>Merci de patienter durant l&apos;importation de la Bible.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="461"/>
        <source>You need to specify a file with books of the Bible to use in the import.</source>
        <translation>Veuillez sélectionner un fichier contenant les livres de la Bible à utiliser dans l&apos;import.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="468"/>
        <source>You need to specify a file of Bible verses to import.</source>
        <translation>Veuillez sélectionner un fichier de versets bibliques à importer.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="526"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>Vous devez spécifier le nom de la version de votre Bible.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="532"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>Vous devez définir un copyright pour votre Bible. Les Bibles dans le Domaine Publique doivent être indiquées comme telle.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>Bible Exists</source>
        <translation>Bible existante</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>Cette Bible existe déjà. Veuillez importer une autre Bible ou supprimer la Bible existante.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>Error during download</source>
        <translation>Erreur durant le téléchargement</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>An error occurred while downloading the list of bibles from %s.</source>
        <translation>Une erreur est survenue lors du téléchargement de la liste des bibles depuis %s.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="691"/>
        <source>Registering Bible...</source>
        <translation>Enregistrement de la Bible...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="757"/>
        <source>Registered Bible. Please note, that verses will be downloaded on demand and thus an internet connection is required.</source>
        <translation>Bible enregistrée. Veuillez noter que les versets seront téléchargés
à la demande, par conséquent une connexion Internet sera nécessaire.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="767"/>
        <source>Your Bible import failed.</source>
        <translation>L&apos;import de votre Bible à échoué.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="66"/>
        <source>Select Language</source>
        <translation>Sélectionner la langue</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="68"/>
        <source>OpenLP is unable to determine the language of this translation of the Bible. Please select the language from the list below.</source>
        <translation>OpenLP ne peut déterminer la langue de cette traduction de la Bible. Veillez sélectionner la langue dans la liste suivante.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="72"/>
        <source>Language:</source>
        <translation>Langue :</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languageform.py" line="62"/>
        <source>You need to choose a language.</source>
        <translation>Vous devez choisir une langue.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="161"/>
        <source>Find</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="164"/>
        <source>Find:</source>
        <translation>Recherche :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="170"/>
        <source>Select</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="178"/>
        <source>Sort bible books alphabetically.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="180"/>
        <source>Book:</source>
        <translation>Carnet de chants :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="192"/>
        <source>From:</source>
        <translation>De :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="198"/>
        <source>To:</source>
        <translation>A :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="204"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="209"/>
        <source>Second:</source>
        <translation>Deuxième :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="273"/>
        <source>Chapter:</source>
        <translation>Chapitre :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="274"/>
        <source>Verse:</source>
        <translation>Verset :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="278"/>
        <source>Clear the results on the current tab.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="279"/>
        <source>Add the search results to the saved list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Scripture Reference</source>
        <translation>Référence biblique</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Scripture Reference...</source>
        <translation>Recherche de référence biblique...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text Search</source>
        <translation>Recherche de texte</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Text...</source>
        <translation>Recherche dans le texte...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="458"/>
        <source>Are you sure you want to completely delete &quot;{bible}&quot; Bible from OpenLP?

You will need to re-import this Bible to use it again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="506"/>
        <source>Saved ({result_count})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="509"/>
        <source>Results ({result_count})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="588"/>
        <source>OpenLP cannot combine single and dual Bible verse search results. Do you want to clear your saved results?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="618"/>
        <source>Bible not fully loaded.</source>
        <translation>Bible partiellement chargée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>Verses not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>The second Bible &quot;{second_name}&quot; does not contain all the verses that are in the main Bible &quot;{name}&quot;.
Only verses found in both Bibles will be shown.

{count:d} verses have not been included in the results.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.OsisImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="171"/>
        <source>Removing unused tags (this may take a few minutes)...</source>
        <translation>Retirer les balises inutilisées (cela peut prendre quelques minutes)...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="200"/>
        <source>Importing {book} {chapter}...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.Sword</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="88"/>
        <source>Importing {name}...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.SwordImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="93"/>
        <source>An unexpected error happened while importing the SWORD bible, please report this to the OpenLP developers.
{error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.ZefaniaImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="90"/>
        <source>Incorrect Bible file type supplied. Zefania Bibles may be compressed. You must decompress them before import.</source>
        <translation>Fichier Bible incorrecte. Les bibles Zefania peuvent être compressées. Vous devez les décompresser avant de les importer.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.Zefnia</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="83"/>
        <source>Importing {book} {chapter}...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CustomPlugin</name>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="77"/>
        <source>&lt;strong&gt;Custom Slide Plugin &lt;/strong&gt;&lt;br /&gt;The custom slide plugin provides the ability to set up custom text slides that can be displayed on the screen the same way songs are. This plugin provides greater freedom over the songs plugin.</source>
        <translation>&lt;strong&gt;Module Diapositive personnalisé&lt;/strong&gt;&lt;br /&gt;Le module de diapositive personnalisé permet de créer des diapositives textuelles personnalisées affichées de la même manière que les chants. Ce module permet une grande liberté par rapport au module chant.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="109"/>
        <source>Custom Slide</source>
        <comment>name singular</comment>
        <translation>Diapositive personnalisée</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="110"/>
        <source>Custom Slides</source>
        <comment>name plural</comment>
        <translation>Diapositives personnalisées</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="114"/>
        <source>Custom Slides</source>
        <comment>container title</comment>
        <translation>Diapositives personnalisées</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="118"/>
        <source>Load a new custom slide.</source>
        <translation>Charge une nouvelle diapositive personnalisée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="119"/>
        <source>Import a custom slide.</source>
        <translation>Importe une diapositive personnalisée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="120"/>
        <source>Add a new custom slide.</source>
        <translation>Ajoute la diapositive personnalisée sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="121"/>
        <source>Edit the selected custom slide.</source>
        <translation>Édite la diapositive personnalisée sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="122"/>
        <source>Delete the selected custom slide.</source>
        <translation>Supprime la diapositive personnalisée sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="123"/>
        <source>Preview the selected custom slide.</source>
        <translation>Prévisualiser la diapositive personnalisée sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="124"/>
        <source>Send the selected custom slide live.</source>
        <translation>Envoie la diapositive personnalisée sélectionnée au direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="125"/>
        <source>Add the selected custom slide to the service.</source>
        <translation>Ajoute la diapositive personnalisée sélectionnée au service.</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.CustomTab</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="56"/>
        <source>Custom Display</source>
        <translation>Affichage Personnalisé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="57"/>
        <source>Display footer</source>
        <translation>Afficher le pied de page</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="58"/>
        <source>Import missing custom slides from service files</source>
        <translation>Importer les diapositives personnalisées du fichier du service</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditCustomForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="104"/>
        <source>Edit Custom Slides</source>
        <translation>Édite les diapositives personnalisées</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="105"/>
        <source>&amp;Title:</source>
        <translation>&amp;Titre :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="107"/>
        <source>Add a new slide at bottom.</source>
        <translation>Ajoute une nouvelle diapositive en bas.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="109"/>
        <source>Edit the selected slide.</source>
        <translation>Édite la diapositive sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="110"/>
        <source>Ed&amp;it All</source>
        <translation>Édite &amp;tous</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="111"/>
        <source>Edit all the slides at once.</source>
        <translation>Édite toutes les diapositives en une.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="112"/>
        <source>The&amp;me:</source>
        <translation>Thè&amp;me :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="113"/>
        <source>&amp;Credits:</source>
        <translation>&amp;Crédits :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="239"/>
        <source>You need to type in a title.</source>
        <translation>Vous devez spécifier un titre.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="243"/>
        <source>You need to add at least one slide.</source>
        <translation>Vous devez ajouter au moins une diapositive.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="50"/>
        <source>Insert Slide</source>
        <translation>Insère une diapositive</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="51"/>
        <source>Split a slide into two by inserting a slide splitter.</source>
        <translation>Sépare la diapositive en deux en insérant un séparateur de diapositive.</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="47"/>
        <source>Edit Slide</source>
        <translation>Éditer la diapo</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="200"/>
        <source>Are you sure you want to delete the &quot;{items:d}&quot; selected custom slide(s)?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="261"/>
        <source>copy</source>
        <comment>For item cloning</comment>
        <translation>copier</translation>
    </message>
</context>
<context>
    <name>ImagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="70"/>
        <source>&lt;strong&gt;Image Plugin&lt;/strong&gt;&lt;br /&gt;The image plugin provides displaying of images.&lt;br /&gt;One of the distinguishing features of this plugin is the ability to group a number of images together in the service manager, making the displaying of multiple images easier. This plugin can also make use of OpenLP&apos;s &quot;timed looping&quot; feature to create a slide show that runs automatically. In addition to this, images from the plugin can be used to override the current theme&apos;s background, which renders text-based items like songs with the selected image as a background instead of the background provided by the theme.</source>
        <translation>&lt;strong&gt;Module Image&lt;/strong&gt;&lt;br /&gt;Le module Image permet l&apos;affichage d&apos;images.&lt;br /&gt;L&apos;une des particularités de ce module est la possibilité de regrouper plusieurs images en un seul élément dans le gestionnaire de services, ce qui facilite son affichage. Ce module permet également de faire défiler les images en boucle avec une pause entre chacune d&apos;elles. Les images du module peuvent également être utilisées pour remplacer l&apos;arrière-plan du thème en cours.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="89"/>
        <source>Image</source>
        <comment>name singular</comment>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="90"/>
        <source>Images</source>
        <comment>name plural</comment>
        <translation>Images</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="93"/>
        <source>Images</source>
        <comment>container title</comment>
        <translation>Images</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="96"/>
        <source>Add new image(s).</source>
        <translation>Ajouter une/des nouvelle(s) image(s).</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="98"/>
        <source>Add a new image.</source>
        <translation>Ajouter une nouvelle image.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="99"/>
        <source>Edit the selected image.</source>
        <translation>Modifier l&apos;image sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="100"/>
        <source>Delete the selected image.</source>
        <translation>Supprimer l&apos;image sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="101"/>
        <source>Preview the selected image.</source>
        <translation>Prévisualiser l&apos;image sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="102"/>
        <source>Send the selected image live.</source>
        <translation>Envoyer l&apos;image sélectionnée au direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="103"/>
        <source>Add the selected image to the service.</source>
        <translation>Ajouter l&apos;image sélectionnée au service.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="191"/>
        <source>Add new image(s)</source>
        <translation>Ajouter une/des nouvelle(s) image(s).</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.AddGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="54"/>
        <source>Add group</source>
        <translation>Ajouter un groupe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="55"/>
        <source>Parent group:</source>
        <translation>Groupe parent :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="56"/>
        <source>Group name:</source>
        <translation>Nom du groupe :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="67"/>
        <source>You need to type in a group name.</source>
        <translation>Vous devez entrer un nom de groupe.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="655"/>
        <source>Could not add the new group.</source>
        <translation>Impossible d&apos;ajouter un nouveau groupe.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="658"/>
        <source>This group already exists.</source>
        <translation>Ce groupe existe déjà.</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ChooseGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="82"/>
        <source>Select Image Group</source>
        <translation>Sélection le groupe d&apos;images</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="83"/>
        <source>Add images to group:</source>
        <translation>Ajouter des images au groupe :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="84"/>
        <source>No group</source>
        <translation>Aucun groupe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="85"/>
        <source>Existing group</source>
        <translation>Groupe existant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="86"/>
        <source>New group</source>
        <translation>Nouveau groupe</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="170"/>
        <source>Select Attachment</source>
        <translation>Sélectionner un objet</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="54"/>
        <source>-- Top-level group --</source>
        <translation>-- Groupe principal --</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="81"/>
        <source>Select Image(s)</source>
        <translation>Sélectionner une (des) Image(s)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="243"/>
        <source>You must select an image or group to delete.</source>
        <translation>Vous devez sélectionner une image ou un groupe à supprimer.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Remove group</source>
        <translation>Retirer un groupe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Are you sure you want to remove &quot;{name}&quot; and everything in it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>Missing Image(s)</source>
        <translation>Image(s) manquante</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="596"/>
        <source>The following image(s) no longer exist: {names}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>The following image(s) no longer exist: {names}
Do you want to add the other images anyway?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="679"/>
        <source>You must select an image to replace the background with.</source>
        <translation>Vous devez sélectionner une image pour remplacer le fond.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="693"/>
        <source>There was no display item to amend.</source>
        <translation>Il n&apos;y a aucun élément d&apos;affichage à modifier.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="697"/>
        <source>There was a problem replacing your background, the image file &quot;{name}&quot; no longer exists.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ImagesPlugin.ImageTab</name>
    <message>
        <location filename="../../openlp/plugins/images/lib/imagetab.py" line="63"/>
        <source>Visible background for images with aspect ratio different to screen.</source>
        <translation>Fond d&apos;écran visible pour les images avec un ratio différent de celui de l&apos;écran.</translation>
    </message>
</context>
<context>
    <name>MediaPlugin</name>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="81"/>
        <source>&lt;strong&gt;Media Plugin&lt;/strong&gt;&lt;br /&gt;The media plugin provides playback of audio and video.</source>
        <translation>&lt;strong&gt;Module Média&lt;/strong&gt;&lt;br /&gt;Le module Média permet de lire des fichiers audio et vidéo.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="91"/>
        <source>Media</source>
        <comment>name singular</comment>
        <translation>Médias</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="92"/>
        <source>Media</source>
        <comment>name plural</comment>
        <translation>Médias</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="96"/>
        <source>Media</source>
        <comment>container title</comment>
        <translation>Médias</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="100"/>
        <source>Load new media.</source>
        <translation>Charge un nouveau média.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="102"/>
        <source>Add new media.</source>
        <translation>Ajoute un nouveau média.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="103"/>
        <source>Edit the selected media.</source>
        <translation>Édite le média sélectionné.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="104"/>
        <source>Delete the selected media.</source>
        <translation>Supprime le média sélectionné.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="105"/>
        <source>Preview the selected media.</source>
        <translation>Prévisualiser le média sélectionné.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="106"/>
        <source>Send the selected media live.</source>
        <translation>Envoie le média sélectionné au direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="107"/>
        <source>Add the selected media to the service.</source>
        <translation>Ajoute le média sélectionné au service.</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelector</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="184"/>
        <source>Select Media Clip</source>
        <translation>Choisir le clip multimédia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="185"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="186"/>
        <source>Media path:</source>
        <translation>Chemin du média :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="187"/>
        <source>Select drive from list</source>
        <translation>Choisir le disque dans la liste</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="189"/>
        <source>Load disc</source>
        <translation>Charger disque</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="190"/>
        <source>Track Details</source>
        <translation>Détails de la piste</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="191"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="192"/>
        <source>Audio track:</source>
        <translation>Piste audio :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="193"/>
        <source>Subtitle track:</source>
        <translation>Sous-titre de la piste :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="201"/>
        <source>HH:mm:ss.z</source>
        <translation>HH:mm:ss.z</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="195"/>
        <source>Clip Range</source>
        <translation>Taille du clip</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="196"/>
        <source>Start point:</source>
        <translation>Point de départ :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="198"/>
        <source>Set start point</source>
        <translation>Régler le point de départ</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="199"/>
        <source>Jump to start point</source>
        <translation>Aller au point de départ</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="200"/>
        <source>End point:</source>
        <translation>Fin :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="202"/>
        <source>Set end point</source>
        <translation>Régler la fin</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="203"/>
        <source>Jump to end point</source>
        <translation>Aller à la fin</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelectorForm</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="195"/>
        <source>No path was given</source>
        <translation>Aucun chemin spécifié</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="201"/>
        <source>Given path does not exists</source>
        <translation>Le chemin donné n&apos;existe pas</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="218"/>
        <source>An error happened during initialization of VLC player</source>
        <translation>Une erreur est survenue lors de l&apos;initialisation de VLC player</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="239"/>
        <source>VLC player failed playing the media</source>
        <translation>VLC player n&apos;arrive pas à lire le média</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>CD not loaded correctly</source>
        <translation>CD incorrectement chargé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>The CD was not loaded correctly, please re-load and try again.</source>
        <translation>Le CD ne s&apos;est pas correctement chargé, merci de le recharger.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>DVD not loaded correctly</source>
        <translation>DVD incorrectement chargé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>The DVD was not loaded correctly, please re-load and try again.</source>
        <translation>Le DVD ne s&apos;est pas correctement chargé, merci de le recharger.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Set name of mediaclip</source>
        <translation>Nommer le clip multimédia</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Name of mediaclip:</source>
        <translation>Nom du clip multimédia :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="602"/>
        <source>Enter a valid name or cancel</source>
        <translation>Entrer un nom valide ou annuler</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>Invalid character</source>
        <translation>Caractère invalide</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>The name of the mediaclip must not contain the character &quot;:&quot;</source>
        <translation>Le nom du clip multimédia ne soit pas contenir de caractère &quot;:&quot;</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="290"/>
        <source>Unsupported File</source>
        <translation>Fichier non supporté</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="93"/>
        <source>Select Media</source>
        <translation>Média sélectionné</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="123"/>
        <source>Load CD/DVD</source>
        <translation>Charger CD/DVD</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>Missing Media File</source>
        <translation>Fichier média manquant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="155"/>
        <source>The optical disc {name} is no longer available.</source>
        <translation>Le disque optique {name} n&apos;est plus disponible.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>The file {name} no longer exists.</source>
        <translation>Le fichier {name} n&apos;existe plus.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="206"/>
        <source>Videos ({video});;Audio ({audio});;{files} (*)</source>
        <translation>Vidéos ({video});;Audio ({audio});;{files} (*)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="216"/>
        <source>You must select a media file to delete.</source>
        <translation>Vous devez sélectionner un fichier média à supprimer.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="233"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="237"/>
        <source>Show Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>Mediaclip already saved</source>
        <translation>Clip multimédia déjà sauvegardé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>This mediaclip has already been saved</source>
        <translation>Ce clip multimédia a déjà été sauvegardé</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="74"/>
        <source>Video:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="76"/>
        <source>Audio:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="95"/>
        <source>Live Media</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="96"/>
        <source>Stream Media Command</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="97"/>
        <source>VLC arguments</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="98"/>
        <source>Start Live items automatically</source>
        <translation>Démarrer les articles du live automatiquement</translation>
    </message>
</context>
<context>
    <name>OpenLP</name>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>Data Directory Error</source>
        <translation>Erreur du dossier de données</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>OpenLP data folder was not found in:

{path}

The location of the data folder was previously changed from the OpenLP's default location. If the data was stored on removable device, that device needs to be made available.

You may reset the data location back to the default location, or you can try to make the current location available.

Do you want to reset to the default data location? If not, OpenLP will be closed so you can try to fix the the problem.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="238"/>
        <source>Backup</source>
        <translation>Sauvegarde</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="221"/>
        <source>OpenLP has been upgraded, do you want to create
a backup of the old data folder?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="232"/>
        <source>Backup of the data folder failed!</source>
        <translation>Sauvegarde du répertoire des données échoué !</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="235"/>
        <source>A backup of the data folder has been created at:

{text}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings Upgrade</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="426"/>
        <source>Your settings are about to be upgraded. A backup will be created at {back_up_path}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings back up failed.

Continuining to upgrade.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/__init__.py" line="415"/>
        <source>Image Files</source>
        <translation>Fichiers image</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="79"/>
        <source>Video Files</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.AboutForm</name>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="103"/>
        <source>&lt;p&gt;OpenLP {{version}}{{revision}} - Open Source Lyrics Projection&lt;br&gt;Copyright {crs} 2004-{yr} OpenLP Developers&lt;/p&gt;&lt;p&gt;Find out more about OpenLP: &lt;a href=&quot;https://openlp.org/&quot;&gt;https://openlp.org/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with this program.  If not, see &lt;a href=&quot;https://www.gnu.org/licenses/&quot;&gt;https://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="118"/>
        <source>OpenLP is written and maintained by volunteers all over the world in their spare time. If you would like to see this project succeed, please consider contributing to it by clicking the &quot;contribute&quot; button below.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="122"/>
        <source>OpenLP would not be possible without the following software libraries:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="155"/>
        <source>&lt;h3&gt;Final credit:&lt;/h3&gt;&lt;blockquote&gt;&lt;p&gt;For God so loved the world that He gave His one and only Son, so that whoever believes in Him will not perish but inherit eternal life.&lt;/p&gt;&lt;p&gt;John 3:16&lt;/p&gt;&lt;/blockquote&gt;&lt;p&gt;And last but not least, final credit goes to God our Father, for sending His Son to die on the cross, setting us free from sin. We bring this software to you for free because He has set us free.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="164"/>
        <source>Credits</source>
        <translation>Crédits</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="744"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="746"/>
        <source>Contribute</source>
        <translation>Contribuer</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutform.py" line="57"/>
        <source> build {version}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.AdvancedTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="59"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="252"/>
        <source>UI Settings</source>
        <translation>Propriétés de l&apos;interface utilisateur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="253"/>
        <source>Data Location</source>
        <translation>Emplacement des données</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="254"/>
        <source>Number of recent service files to display:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="255"/>
        <source>Open the last used Library tab on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="257"/>
        <source>Double-click to send items straight to Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="259"/>
        <source>Preview items when clicked in Library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="261"/>
        <source>Preview items when clicked in Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="263"/>
        <source>Expand new service items on creation</source>
        <translation>Étendre les nouveaux éléments du service à la création</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="265"/>
        <source>Max height for non-text slides
in slide controller:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="267"/>
        <source>Disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="268"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="269"/>
        <source>When changing slides:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="271"/>
        <source>Do not auto-scroll</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="272"/>
        <source>Auto-scroll the previous slide into view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="274"/>
        <source>Auto-scroll the previous slide to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="276"/>
        <source>Auto-scroll the previous slide to middle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="278"/>
        <source>Auto-scroll the current slide into view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="280"/>
        <source>Auto-scroll the current slide to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="282"/>
        <source>Auto-scroll the current slide to middle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="284"/>
        <source>Auto-scroll the current slide to bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="286"/>
        <source>Auto-scroll the next slide into view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="288"/>
        <source>Auto-scroll the next slide to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="290"/>
        <source>Auto-scroll the next slide to middle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="292"/>
        <source>Auto-scroll the next slide to bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="294"/>
        <source>Enable application exit confirmation</source>
        <translation>Demander une confirmation avant de quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="299"/>
        <source>Use dark style (needs restart)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="300"/>
        <source>Default Service Name</source>
        <translation>Nom de service par défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="301"/>
        <source>Enable default service name</source>
        <translation>Activer le nom de service par défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="302"/>
        <source>Date and Time:</source>
        <translation>Date et Heure :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="303"/>
        <source>Monday</source>
        <translation>Lundi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="304"/>
        <source>Tuesday</source>
        <translation>Mardi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="305"/>
        <source>Wednesday</source>
        <translation>Mercredi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="306"/>
        <source>Thursday</source>
        <translation>Jeudi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="307"/>
        <source>Friday</source>
        <translation>Vendredi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="308"/>
        <source>Saturday</source>
        <translation>Samedi</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="309"/>
        <source>Sunday</source>
        <translation>Dimanche</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="310"/>
        <source>Now</source>
        <translation>Maintenant</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="311"/>
        <source>Time when usual service starts.</source>
        <translation>Heure de début du service habituelle.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="312"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="313"/>
        <source>Consult the OpenLP manual for usage.</source>
        <translation>Veuillez consulter le manuel d&apos;OpenLP.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="314"/>
        <source>Revert to the default service name &quot;{name}&quot;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="317"/>
        <source>Example:</source>
        <translation>Exemple :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="318"/>
        <source>Hide mouse cursor when over display window</source>
        <translation>Cacher le curseur de la souris quand elle se trouve sur l&apos;écran live</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="319"/>
        <source>Path:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="320"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="321"/>
        <source>Cancel OpenLP data directory location change.</source>
        <translation>Annuler le changement d&apos;emplacement du dossier de données d&apos;OpenLP.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="323"/>
        <source>Copy data to new location.</source>
        <translation>Copier les données vers un nouvel emplacement.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="324"/>
        <source>Copy the OpenLP data files to the new location.</source>
        <translation>Copier les fichiers de données d&apos;OpenLP vers le nouvel emplacement.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="326"/>
        <source>&lt;strong&gt;WARNING:&lt;/strong&gt; New data directory location contains OpenLP data files.  These files WILL be replaced during a copy.</source>
        <translation>&lt;strong&gt;ATTENTION:&lt;/strong&gt; Le nouveau dossier de données contient des fichiers de données OpenLP. Ces fichiers seront remplacés pendant la copie.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="329"/>
        <source>Display Workarounds</source>
        <translation>Afficher contournements</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="330"/>
        <source>Ignore Aspect Ratio</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="331"/>
        <source>Bypass X11 Window Manager</source>
        <translation>Contourner le gestionnaire de fenêtres X11</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="332"/>
        <source>Use alternating row colours in lists</source>
        <translation>Utiliser une couleur de ligne alternative dans la liste</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="490"/>
        <source>Syntax error.</source>
        <translation>Erreur de syntaxe.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Confirm Data Directory Change</source>
        <translation>Confirmez le changement de dossier de données</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Are you sure you want to change the location of the OpenLP data directory to:

{path}

The data directory will be changed when OpenLP is closed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="559"/>
        <source>Overwrite Existing Data</source>
        <translation>Écraser les données existantes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>Restart Required</source>
        <translation>Redémarrage requis</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>This change will only take effect once OpenLP has been restarted.</source>
        <translation>Cette modification ne prendra effet qu&apos;une fois OpenLP redémarré.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="186"/>
        <source>Select Logo File</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ColorButton</name>
    <message>
        <location filename="../../openlp/core/widgets/buttons.py" line="44"/>
        <source>Click to select a color.</source>
        <translation>Clique pour sélectionner une couleur.</translation>
    </message>
</context>
<context>
    <name>OpenLP.DB</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="542"/>
        <source>RGB</source>
        <translation>RVB</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="543"/>
        <source>Video</source>
        <translation>Vidéo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="544"/>
        <source>Digital</source>
        <translation>Numérique</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="545"/>
        <source>Storage</source>
        <translation>Stockage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="546"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="547"/>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="551"/>
        <source>1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="552"/>
        <source>2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="553"/>
        <source>3</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="554"/>
        <source>4</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="555"/>
        <source>5</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="556"/>
        <source>6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="557"/>
        <source>7</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="558"/>
        <source>8</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="559"/>
        <source>9</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="560"/>
        <source>A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="561"/>
        <source>B</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="562"/>
        <source>C</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="563"/>
        <source>D</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="564"/>
        <source>E</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="565"/>
        <source>F</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="566"/>
        <source>G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="567"/>
        <source>H</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="568"/>
        <source>I</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="569"/>
        <source>J</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="570"/>
        <source>K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="571"/>
        <source>L</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="572"/>
        <source>M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="573"/>
        <source>N</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="574"/>
        <source>O</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="575"/>
        <source>P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="576"/>
        <source>Q</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="577"/>
        <source>R</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="578"/>
        <source>S</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="579"/>
        <source>T</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="580"/>
        <source>U</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="581"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="582"/>
        <source>W</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="583"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="584"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="585"/>
        <source>Z</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.DisplayWindow</name>
    <message>
        <location filename="../../openlp/core/display/window.py" line="121"/>
        <source>Display Window</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="94"/>
        <source>Error Occurred</source>
        <translation>Erreur survenue</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="96"/>
        <source>&lt;strong&gt;Please describe what you were trying to do.&lt;/strong&gt; &amp;nbsp;If possible, write in English.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="99"/>
        <source>&lt;strong&gt;Oops, OpenLP hit a problem and couldn&apos;t recover!&lt;br&gt;&lt;br&gt;You can help &lt;/strong&gt; the OpenLP developers to &lt;strong&gt;fix this&lt;/strong&gt; by&lt;br&gt; sending them a &lt;strong&gt;bug report to {email}&lt;/strong&gt;{newlines}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="105"/>
        <source>{first_part}&lt;strong&gt;No email app? &lt;/strong&gt; You can &lt;strong&gt;save&lt;/strong&gt; this information to a &lt;strong&gt;file&lt;/strong&gt; and&lt;br&gt;send it from your &lt;strong&gt;mail on browser&lt;/strong&gt; via an &lt;strong&gt;attachment.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;&lt;strong&gt;Thank you&lt;/strong&gt; for being part of making OpenLP better!&lt;br&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="112"/>
        <source>Send E-Mail</source>
        <translation>Envoyer un courriel</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="113"/>
        <source>Save to File</source>
        <translation>Enregistre dans un fichier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="114"/>
        <source>Attach File</source>
        <translation>Attacher un fichier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>Failed to Save Report</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>The following error occured when saving the report.

{exception}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="153"/>
        <source>&lt;strong&gt;Thank you for your description!&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="157"/>
        <source>&lt;strong&gt;Tell us what you were doing when this happened.&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="161"/>
        <source>&lt;strong&gt;Please enter a more detailed description of the situation&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionForm</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="77"/>
        <source>Platform: {platform}
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Save Crash Report</source>
        <translation>Enregistrer le rapport d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Text files (*.txt *.log *.text)</source>
        <translation>Fichiers texte (*.txt *.log *.text)</translation>
    </message>
</context>
<context>
    <name>OpenLP.FileRenameForm</name>
    <message>
        <location filename="../../openlp/core/ui/filerenamedialog.py" line="60"/>
        <source>New File Name:</source>
        <translation>Nouveau nom de fichier :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="55"/>
        <source>File Copy</source>
        <translation>Copie le fichier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="57"/>
        <source>File Rename</source>
        <translation>Renomme le fichier</translation>
    </message>
</context>
<context>
    <name>OpenLP.FirstTimeLanguageForm</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="68"/>
        <source>Select Translation</source>
        <translation>Sélectionner la traduction</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="69"/>
        <source>Choose the translation you&apos;d like to use in OpenLP.</source>
        <translation>Choisir la traduction que vous voulez utiliser dans OpenLP.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="71"/>
        <source>Translation:</source>
        <translation>Traduction :</translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>OpenLP.FirstTimeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="520"/>
        <source>Network Error</source>
        <translation>Erreur réseau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="177"/>
        <source>There was a network error attempting to connect to retrieve initial configuration information</source>
        <translation>Il y a eu une erreur réseau durant la connexion pour la récupération des informations de configuration initiales</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="184"/>
        <source>Downloading {name}...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>Invalid index file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>OpenLP was unable to read the resource index file. Please try again later.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>Download Error</source>
        <translation>Erreur de téléchargement</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="417"/>
        <source>There was a connection problem during download, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation>Il y a eu un problème de connexion durant le téléchargement, les prochains téléchargements seront ignorés. Esasyez de ré-executer l&apos;Assistant de Premier Démarrage plus tard.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="429"/>
        <source>Setting Up And Downloading</source>
        <translation>Paramétrage et téléchargement</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="430"/>
        <source>Please wait while OpenLP is set up and your data is downloaded.</source>
        <translation>Merci de patienter durant le paramétrage d&apos;OpenLP et le téléchargement de vos données.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="434"/>
        <source>Setting Up</source>
        <translation>Paramétrage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="448"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="451"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="455"/>
        <source>Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="457"/>
        <source>Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>There was a connection problem while downloading, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation>Il y a eu un problème de connexion durant le téléchargement, les prochains téléchargements seront ignorés. Esasyez de ré-executer l&apos;Assistant de Premier Démarrage plus tard.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="521"/>
        <source>Unable to download some files</source>
        <translation>Impossible de télécharger certains fichiers</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="262"/>
        <source>First Time Wizard</source>
        <translation>Assistant de démarrage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="263"/>
        <source>Welcome to the First Time Wizard</source>
        <translation>Bienvenue dans l&apos;assistant de démarrage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="265"/>
        <source>This wizard will help you to configure OpenLP for initial use. Click the &apos;{next_button}&apos; button below to start.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="269"/>
        <source>Internet Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="271"/>
        <source>Downloading Resource Index</source>
        <translation>Téléchargement de l&apos;index des ressources</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="272"/>
        <source>Please wait while the resource index is downloaded.</source>
        <translation>Merci de patienter durant le téléchargement de l&apos;index des ressources.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="274"/>
        <source>Please wait while OpenLP downloads the resource index file...</source>
        <translation>Merci de patienter pendant qu&apos;OpenLP télécharge le fichier d&apos;index des ressources...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="276"/>
        <source>Select parts of the program you wish to use</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="277"/>
        <source>You can also change these settings after the Wizard.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="279"/>
        <source>Displays</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="280"/>
        <source>Choose the main display screen for OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="282"/>
        <source>Songs</source>
        <translation>Chants</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="283"/>
        <source>Custom Slides – Easier to manage than songs and they have their own list of slides</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="286"/>
        <source>Bibles – Import and show Bibles</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="287"/>
        <source>Images – Show images or replace background with them</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="289"/>
        <source>Presentations – Show .ppt, .odp and .pdf files</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="291"/>
        <source>Media – Playback of Audio and Video files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="292"/>
        <source>Song Usage Monitor</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="293"/>
        <source>Alerts – Display informative messages while showing other slides</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="295"/>
        <source>Resource Data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="296"/>
        <source>Can OpenLP download some resource data?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="297"/>
        <source>OpenLP has collected some resources that we have permission to distribute.

If you would like to download some of these resources click the &apos;{next_button}&apos; button, otherwise click the &apos;{finish_button}&apos; button.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="303"/>
        <source>No Internet Connection</source>
        <translation>Pas de connexion à Internet</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="304"/>
        <source>Cannot connect to the internet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="305"/>
        <source>OpenLP could not connect to the internet to get information about the sample data available.

Please check your internet connection. If your church uses a proxy server click the 'Internet Settings' button below and enter the server details there.

Click the '{back_button}' button to try again.

If you click the &apos;{finish_button}&apos; button you can download the data at a later time by selecting &apos;Re-run First Time Wizard&apos; from the &apos;Tools&apos; menu in OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="314"/>
        <source>Sample Songs</source>
        <translation>Chants d&apos;exemple</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="315"/>
        <source>Select and download public domain songs.</source>
        <translation>Sélectionner et télécharger des chants du domaine public.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="316"/>
        <source>Sample Bibles</source>
        <translation>Bibles d&apos;exemple</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="317"/>
        <source>Select and download free Bibles.</source>
        <translation>Sélectionner et télécharger des Bibles gratuites.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="319"/>
        <source>Sample Themes</source>
        <translation>Exemple de Thèmes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="320"/>
        <source>Select and download sample themes.</source>
        <translation>Sélectionner et télécharger des exemples de thèmes.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="321"/>
        <source>Default theme:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="322"/>
        <source>Select all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="323"/>
        <source>Deselect all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="324"/>
        <source>Downloading and Configuring</source>
        <translation>Téléchargement et configuration en cours</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="325"/>
        <source>Please wait while resources are downloaded and OpenLP is configured.</source>
        <translation>Merci de patienter pendant le téléchargement des ressources et la configuration d&apos;OpenLP.</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagDialog</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="111"/>
        <source>Configure Formatting Tags</source>
        <translation>Configurer les balises de formatage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="114"/>
        <source>Default Formatting</source>
        <translation>Formatage par défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="125"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="126"/>
        <source>Tag</source>
        <translation>Balise</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="127"/>
        <source>Start HTML</source>
        <translation>HTML de début</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="128"/>
        <source>End HTML</source>
        <translation>HTML de fin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="124"/>
        <source>Custom Formatting</source>
        <translation>Formatage personnalisé</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagForm</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="79"/>
        <source>Tag {tag} already defined.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="81"/>
        <source>Description {tag} already defined.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="148"/>
        <source>Start tag {tag} is not valid HTML</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="168"/>
        <source>End tag {end} does not match end tag for start tag {start}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="92"/>
        <source>New Tag {row:d}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="96"/>
        <source>&lt;HTML here&gt;</source>
        <translation>&lt;HTML ici&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="199"/>
        <source>Validation Error</source>
        <translation>Erreur de validation</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="179"/>
        <source>Description is missing</source>
        <translation>La description est manquante</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="182"/>
        <source>Tag is missing</source>
        <translation>La balise est manquante</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTags</name>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="63"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="69"/>
        <source>Black</source>
        <translation>Noir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="75"/>
        <source>Blue</source>
        <translation>Bleu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="81"/>
        <source>Yellow</source>
        <translation>Jaune</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="87"/>
        <source>Green</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="93"/>
        <source>Pink</source>
        <translation>Rose</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="99"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="105"/>
        <source>Purple</source>
        <translation>Pourpre</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="111"/>
        <source>White</source>
        <translation>Blanc</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="117"/>
        <source>Superscript</source>
        <translation>Exposant</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="122"/>
        <source>Subscript</source>
        <translation>Indice</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="127"/>
        <source>Paragraph</source>
        <translation>Paragraphe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="132"/>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="137"/>
        <source>Italics</source>
        <translation>Italiques</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="141"/>
        <source>Underline</source>
        <translation>Souligner</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="147"/>
        <source>Break</source>
        <translation>Retour à la ligne</translation>
    </message>
</context>
<context>
    <name>OpenLP.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="296"/>
        <source>Experimental features (use at your own risk)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="334"/>
        <source>Service Item Slide Limits</source>
        <translation>Limites des éléments de service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="335"/>
        <source>Behavior of next/previous on the last/first slide:</source>
        <translation>Comportement suivant/précédent sur la dernière/première diapositive :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="336"/>
        <source>&amp;Remain on Slide</source>
        <translation>&amp;Rester sur la diapositive</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="337"/>
        <source>&amp;Wrap around</source>
        <translation>&amp;Retour à la ligne automatique</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="338"/>
        <source>&amp;Move to next/previous service item</source>
        <translation>&amp;Déplacer l&apos;élément du service vers suivant/précédent</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="160"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="161"/>
        <source>Application Startup</source>
        <translation>Démarrage de l&apos;application</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="162"/>
        <source>Show blank screen warning</source>
        <translation>Afficher un avertissement d&apos;écran vide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="163"/>
        <source>Automatically open the previous service file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="164"/>
        <source>Show the splash screen</source>
        <translation>Afficher l&apos;écran de démarrage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="165"/>
        <source>Logo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="167"/>
        <source>Logo file:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="168"/>
        <source>Don&apos;t show logo on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="169"/>
        <source>Check for updates to OpenLP</source>
        <translation>Vérifie si des mises à jours d&apos;OpenLP sont disponibles</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="170"/>
        <source>Application Settings</source>
        <translation>Préférence de l&apos;application</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="171"/>
        <source>Prompt to save before starting a new service</source>
        <translation>Demander d&apos;enregistrer avant de commencer un nouveau service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="173"/>
        <source>Unblank display when changing slide in Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="175"/>
        <source>Unblank display when sending items to Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="177"/>
        <source>Automatically preview the next item in service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="179"/>
        <source>Timed slide interval:</source>
        <translation>Intervalle de temps entre les diapositives :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="180"/>
        <source> sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="181"/>
        <source>CCLI Details</source>
        <translation>CCLI détails</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="183"/>
        <source>SongSelect username:</source>
        <translation>Nom d&apos;utilisateur SongSelect :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="184"/>
        <source>SongSelect password:</source>
        <translation>Mot de passe SongSelect :</translation>
    </message>
</context>
<context>
    <name>OpenLP.LanguageManager</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Language</source>
        <translation>Langage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Please restart OpenLP to use your new language setting.</source>
        <translation>Veuillez redémarrer OpenLP pour utiliser votre nouveau paramétrage de langue.</translation>
    </message>
</context>
<context>
    <name>OpenLP.MainWindow</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="261"/>
        <source>English</source>
        <comment>Please add the name of your language here</comment>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="304"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="361"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="362"/>
        <source>&amp;Import</source>
        <translation>&amp;Import</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="363"/>
        <source>&amp;Export</source>
        <translation>E&amp;xport</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="364"/>
        <source>&amp;Recent Services</source>
        <translation>Services &amp;récents</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="365"/>
        <source>&amp;View</source>
        <translation>&amp;Visualiser</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="366"/>
        <source>&amp;Layout Presets</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="367"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="368"/>
        <source>&amp;Settings</source>
        <translation>O&amp;ptions</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="395"/>
        <source>&amp;Language</source>
        <translation>&amp;Langue</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="370"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="371"/>
        <source>Library</source>
        <translation>Bibliothèque</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="372"/>
        <source>Service</source>
        <translation>Service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="373"/>
        <source>Themes</source>
        <translation>Thèmes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="374"/>
        <source>Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="375"/>
        <source>&amp;New Service</source>
        <translation>&amp;Nouveau service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="378"/>
        <source>&amp;Open Service</source>
        <translation>&amp;Ouvrir le service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="380"/>
        <source>Open an existing service.</source>
        <translation>Ouvrir un service existant.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="381"/>
        <source>&amp;Save Service</source>
        <translation>&amp;Enregistrer le service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="383"/>
        <source>Save the current service to disk.</source>
        <translation>Enregistre le service courant sur le disque.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="384"/>
        <source>Save Service &amp;As...</source>
        <translation>Enregistrer le service so&amp;us...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="385"/>
        <source>Save Service As</source>
        <translation>Enregistrer le service sous</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="386"/>
        <source>Save the current service under a new name.</source>
        <translation>Enregistre le service courant sous un nouveau nom.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="389"/>
        <source>Print the current service.</source>
        <translation>Imprimer le service courant.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="390"/>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="391"/>
        <source>Close OpenLP - Shut down the program.</source>
        <translation>Fermer OpenLP - Quitter le logiciel.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="394"/>
        <source>&amp;Theme</source>
        <translation>&amp;Thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="396"/>
        <source>Configure &amp;Shortcuts...</source>
        <translation>Personnalisation des &amp;raccourcis...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="397"/>
        <source>Configure &amp;Formatting Tags...</source>
        <translation>Configurer les &amp;balises de formatage...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="398"/>
        <source>&amp;Configure OpenLP...</source>
        <translation>&amp;Configuration d&apos;OpenLP...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="399"/>
        <source>Export settings to a *.config file.</source>
        <translation>Exporter la configuration vers un fichier *.config.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="405"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="402"/>
        <source>Import settings from a *.config file previously exported from this or another machine.</source>
        <translation>Importe la configuration d&apos;OpenLP à partir d&apos;un fichier *.config précédemment exporté depuis un autre ordinateur.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="406"/>
        <source>&amp;Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="407"/>
        <source>Hide or show Projectors.</source>
        <translation>Masquer ou afficher les projecteurs.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="408"/>
        <source>Toggle visibility of the Projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="410"/>
        <source>L&amp;ibrary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="411"/>
        <source>Hide or show the Library.</source>
        <translation>Masquer ou afficher la bibliothèque.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="412"/>
        <source>Toggle the visibility of the Library.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="414"/>
        <source>&amp;Themes</source>
        <translation>&amp;Thèmes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="415"/>
        <source>Hide or show themes</source>
        <translation>Masquer ou afficher les thèmes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="416"/>
        <source>Toggle visibility of the Themes.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="418"/>
        <source>&amp;Service</source>
        <translation>&amp;Service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="419"/>
        <source>Hide or show Service.</source>
        <translation>Masquer ou afficher le Service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="420"/>
        <source>Toggle visibility of the Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="422"/>
        <source>&amp;Preview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="423"/>
        <source>Hide or show Preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="424"/>
        <source>Toggle visibility of the Preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="426"/>
        <source>Li&amp;ve</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="427"/>
        <source>Hide or show Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="428"/>
        <source>L&amp;ock visibility of the panels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="429"/>
        <source>Lock visibility of the panels.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="430"/>
        <source>Toggle visibility of the Live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="431"/>
        <source>&amp;Manage Plugins</source>
        <translation>&amp;Gestion des extensions</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="432"/>
        <source>You can enable and disable plugins from here.</source>
        <translation>Vous pouvez activer et désactiver des modules à partir d&apos;ici.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="434"/>
        <source>&amp;About</source>
        <translation>À &amp;propos</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="435"/>
        <source>More information about OpenLP.</source>
        <translation>Plus d&apos;information à propos d&apos;OpenLP.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="436"/>
        <source>&amp;User Manual</source>
        <translation>&amp;Manuel d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="438"/>
        <source>Jump to the search box of the current active plugin.</source>
        <translation>Aller au champ de recherche du plugin actif actuel.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="440"/>
        <source>&amp;Web Site</source>
        <translation>Site &amp;Web</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="443"/>
        <source>Set the interface language to {name}</source>
        <translation>Définir la langue de l&apos;interface à {name}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="445"/>
        <source>&amp;Autodetect</source>
        <translation>&amp;Détecter automatiquement</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="446"/>
        <source>Use the system language, if available.</source>
        <translation>Utilise le langage système, si disponible.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="447"/>
        <source>Add &amp;Tool...</source>
        <translation>Ajoute un &amp;outils...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="448"/>
        <source>Add an application to the list of tools.</source>
        <translation>Ajoute une application à la liste des outils.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="450"/>
        <source>Open &amp;Data Folder...</source>
        <translation>&amp;Ouvrir le répertoire de données...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="451"/>
        <source>Open the folder where songs, bibles and other data resides.</source>
        <translation>Ouvrir le répertoire où se trouve les chants, bibles et autres données.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="453"/>
        <source>Re-run First Time Wizard</source>
        <translation>Re-démarrer l&apos;assistant de démarrage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="454"/>
        <source>Re-run the First Time Wizard, importing songs, Bibles and themes.</source>
        <translation>Re-démarrer l&apos;assistant de démarrage, importer les chants, Bibles et thèmes.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="456"/>
        <source>Update Theme Images</source>
        <translation>Mettre à jour les images de thèmes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="457"/>
        <source>Update the preview images for all themes.</source>
        <translation>Mettre à jour les images de tous les thèmes.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="459"/>
        <source>&amp;Show all</source>
        <translation>&amp;Tout afficher</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="460"/>
        <source>Reset the interface back to the default layout and show all the panels.</source>
        <translation>Ré-initialiser l&apos;interface à la configuration par défaut et afficher tous les panneaux.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="462"/>
        <source>&amp;Setup</source>
        <translation>&amp;Configuration</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="463"/>
        <source>Use layout that focuses on setting up the Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="465"/>
        <source>&amp;Live</source>
        <translation>&amp;Direct</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="466"/>
        <source>Use layout that focuses on Live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="548"/>
        <source>Waiting for some things to finish...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="550"/>
        <source>Please Wait</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="631"/>
        <source>Version {new} of OpenLP is now available for download (you are currently running version {current}). 

You can download the latest version from https://openlp.org/.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="634"/>
        <source>OpenLP Version Updated</source>
        <translation>Version d&apos;OpenLP mis à jour</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Re-run First Time Wizard?</source>
        <translation>Re-démarrer l&apos;assistant de démarrage ?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Are you sure you want to re-run the First Time Wizard?

Re-running this wizard may make changes to your current OpenLP configuration and possibly add songs to your existing songs list and change your default theme.</source>
        <translation>Êtes vous sûr de vouloir re-démarrer l&apos;assistant de démarrage ?

Re-démarrer cet assistant peut apporter des modifications à votre configuration actuelle d&apos;OpenLP, éventuellement ajouter des chants à votre liste de chants existante et changer votre thème par défaut.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>OpenLP Main Display Blanked</source>
        <translation>OpenLP affichage principal noirci</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>The Main Display has been blanked out</source>
        <translation>L&apos;affichage principal a été noirci</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Import settings?</source>
        <translation>Import de la configuration ?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Are you sure you want to import settings?

 Importing settings will make permanent changes to your current OpenLP configuration.

 Importing incorrect settings may cause erratic behaviour or OpenLP to terminate abnormally.</source>
        <translation>Êtes-vous sûr de vouloir importer la configuration ?

Importer la configuration va changer de façon permanente votre configuration d&apos;OpenLP.

Importer une configuration incorrect peut provoquer des comportements imprévisible d&apos;OpenLP et le fermer anormalement.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>Import settings</source>
        <translation>Import de la configuration</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>OpenLP Settings (*.conf)</source>
        <translation>Fichier d&apos;export de la configuration d&apos;OpenLP (*.conf)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="889"/>
        <source>The file you have selected does not appear to be a valid OpenLP settings file.

Processing has terminated and no changes have been made.</source>
        <translation>Le fichier que vous avez sélectionné ne semble pas être un fichier de configuration OpenLP valide.

L&apos;opération est terminée et aucun changement n&apos;a été fait.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>OpenLP will now close.  Imported settings will be applied the next time you start OpenLP.</source>
        <translation>OpenLP va se terminer maintenant. La Configuration importée va être appliquée au prochain démarrage.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>Export Settings File</source>
        <translation>Export de la configuration</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>Export setting error</source>
        <translation>Erreur de configuration des exports</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>An error occurred while exporting the settings: {err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Exit OpenLP</source>
        <translation>Quitter OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Are you sure you want to exit OpenLP?</source>
        <translation>Êtes vous sur de vouloir quitter OpenLP ?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1042"/>
        <source>&amp;Exit OpenLP</source>
        <translation>&amp;Quitter OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1108"/>
        <source>Default Theme: {theme}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear List</source>
        <comment>Clear List of recent files</comment>
        <translation>Vide la liste</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear the list of recent files.</source>
        <translation>Vide la liste des fichiers récents.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1345"/>
        <source>Copying OpenLP data to new data directory location - {path} - Please wait for copy to finish</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1353"/>
        <source>OpenLP Data directory copy failed

{err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1355"/>
        <source>New Data Directory Error</source>
        <translation>Erreur du nouveau dossier de données</translation>
    </message>
</context>
<context>
    <name>OpenLP.Manager</name>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>Database Error</source>
        <translation>Erreur de base de données</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="157"/>
        <source>OpenLP cannot load your database.

Database: {db}</source>
        <translation>OpenLP ne peut pas charger votre base de données.

Base de données: {db}</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>The database being loaded was created in a more recent version of OpenLP. The database is version {db_ver}, while OpenLP expects version {db_up}. The database will not be loaded.

Database: {db_name}</source>
        <translation>La base de données utilisée a été créé avec une version plus récente d&apos;OpenLP. La base de données est en version {db_ver}, tandis que OpenLP attend la version {db_up}. La base de données ne peux pas être chargée.

Base de données: {db_name}</translation>
    </message>
</context>
<context>
    <name>OpenLP.MediaController</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="100"/>
        <source>The media integration library is missing (python - vlc is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="102"/>
        <source>The media integration library is missing (python - pymediainfo is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="122"/>
        <source>No Displays have been configured, so Live Media has been disabled</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.MediaManagerItem</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="382"/>
        <source>No Items Selected</source>
        <translation>Aucun éléments sélectionnés</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="161"/>
        <source>&amp;Add to selected Service Item</source>
        <translation>&amp;Ajoute à l&apos;élément sélectionné du service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File Type</source>
        <translation>Type de fichier invalide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File {file_path}.
File extension not supported</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="391"/>
        <source>Duplicate files were found on import and were ignored.</source>
        <translation>Des fichiers dupliqués on été trouvé dans l&apos;import et ont été ignorés.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="496"/>
        <source>You must select one or more items to preview.</source>
        <translation>Vous devez sélectionner un ou plusieurs éléments à prévisualiser.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="514"/>
        <source>You must select one or more items to send live.</source>
        <translation>Vous devez sélectionner un ou plusieurs éléments pour les envoyer au direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="563"/>
        <source>You must select one or more items to add.</source>
        <translation>Vous devez sélectionner un ou plusieurs éléments à ajouter.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="607"/>
        <source>You must select one or more items.</source>
        <translation>Vous devez sélectionner un ou plusieurs éléments.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="614"/>
        <source>You must select an existing service item to add to.</source>
        <translation>Vous devez sélectionner un élément existant du service pour l&apos;ajouter.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>Invalid Service Item</source>
        <translation>Élément du service invalide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>You must select a {title} service item.</source>
        <translation>Vous devez sélectionner un élément {title} du service.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="119"/>
        <source>&amp;Clone</source>
        <translation>&amp;Clone</translation>
    </message>
</context>
<context>
    <name>OpenLP.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="51"/>
        <source>Media</source>
        <translation>Médias</translation>
    </message>
</context>
<context>
    <name>OpenLP.OpenLyricsImportError</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="707"/>
        <source>&lt;lyrics&gt; tag is missing.</source>
        <translation>La balise &lt;lyrics&gt; est manquante.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="712"/>
        <source>&lt;verse&gt; tag is missing.</source>
        <translation>La balise &lt;verse&gt; est manquante.</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="494"/>
        <source>Fan</source>
        <translation>Ventilateur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="495"/>
        <source>Lamp</source>
        <translation>Ampoule</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="496"/>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="497"/>
        <source>Cover</source>
        <translation>Couvercle</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="498"/>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="430"/>
        <source>No message</source>
        <translation>Pas de message</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="759"/>
        <source>Error while sending data to projector</source>
        <translation>Erreur lors de l&apos;envoi de données au projecteur</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLinkConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="166"/>
        <source>Acknowledge a PJLink SRCH command - returns MAC address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="171"/>
        <source>Blank/unblank video and/or mute audio.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="176"/>
        <source>Query projector PJLink class support.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="181"/>
        <source>Query error status from projector. Returns fan/lamp/temp/cover/filter/other error status.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="187"/>
        <source>Query number of hours on filter.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="192"/>
        <source>Freeze or unfreeze current image being projected.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="197"/>
        <source>Query projector manufacturer name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="202"/>
        <source>Query projector product name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="207"/>
        <source>Query projector for other information set by manufacturer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="212"/>
        <source>Query specified input source name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="217"/>
        <source>Switch to specified video source.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="222"/>
        <source>Query available input sources.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="227"/>
        <source>Query current input resolution.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="232"/>
        <source>Query lamp time and on/off status. Multiple lamps supported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="237"/>
        <source>UDP Status - Projector is now available on network. Includes MAC address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="242"/>
        <source>Adjust microphone volume by 1 step.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="247"/>
        <source>Query customer-set projector name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="252"/>
        <source>Initial connection with authentication/no authentication request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="257"/>
        <source>Turn lamp on or off/standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="262"/>
        <source>Query replacement air filter model number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="267"/>
        <source>Query replacement lamp model number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="272"/>
        <source>Query recommended resolution.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="277"/>
        <source>Query projector serial number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="282"/>
        <source>UDP broadcast search request for available projectors. Reply is ACKN.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="287"/>
        <source>Query projector software version number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="292"/>
        <source>Adjust speaker volume by 1 step.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PathEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="299"/>
        <source>Browse for directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="300"/>
        <source>Revert to default directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="302"/>
        <source>Browse for file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="303"/>
        <source>Revert to default file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="317"/>
        <source>Select Directory</source>
        <translation>Sélectionner un répertoire</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="321"/>
        <source>Select File</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PluginForm</name>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="74"/>
        <source>Manage Plugins</source>
        <translation>Gestion des extensions</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="75"/>
        <source>Plugin Details</source>
        <translation>Détails du module</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="77"/>
        <source>Status:</source>
        <translation>État : </translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="78"/>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="149"/>
        <source>{name} (Disabled)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="145"/>
        <source>{name} (Active)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="147"/>
        <source>{name} (Inactive)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PluginManager</name>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="173"/>
        <source>Unable to initialise the following plugins:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="179"/>
        <source>See the log file for more details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceDialog</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Page</source>
        <translation>Ajuster à la page</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Width</source>
        <translation>Ajuster à la largeur</translation>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceForm</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="61"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="69"/>
        <source>Copy as Text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="71"/>
        <source>Copy as HTML</source>
        <translation>Copier comme de l&apos;HTML</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="137"/>
        <source>Zoom Out</source>
        <translation>Zoom arrière</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="138"/>
        <source>Zoom Original</source>
        <translation>Zoom d&apos;origine</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="139"/>
        <source>Zoom In</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="140"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="141"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="142"/>
        <source>Service Note Text:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="143"/>
        <source>Other Options</source>
        <translation>Autres options</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="144"/>
        <source>Include slide text if available</source>
        <translation>Inclure le texte des diapositives si disponible</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="145"/>
        <source>Add page break before each text item</source>
        <translation>Ajoute des sauts de page entre chaque éléments</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="146"/>
        <source>Include service item notes</source>
        <translation>Inclure les notes des éléments du service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="147"/>
        <source>Include play length of media items</source>
        <translation>Inclure la durée des éléments média</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="148"/>
        <source>Show chords</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="149"/>
        <source>Service Sheet</source>
        <translation>Feuille de service</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="401"/>
        <source>The address specified with socket.bind() is already in use and was set to be exclusive</source>
        <translation>L&apos;adresse spécifiée avec socket.bind() est déjà utilisée et est à usage exclusive.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="404"/>
        <source>PJLink returned &quot;ERRA: Authentication Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="405"/>
        <source>The connection was refused by the peer (or timed out)</source>
        <translation>La connexion est refusée par le pair (ou délai dépassé)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="407"/>
        <source>Projector cover open detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="408"/>
        <source>PJLink class not supported</source>
        <translation>Classe PJLink non supportée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="409"/>
        <source>The datagram was larger than the operating system&apos;s limit</source>
        <translation>La trame réseau est plus longue que ne l&apos;autorise le système d&apos;exploitation</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="411"/>
        <source>Error condition detected</source>
        <translation>Erreurs détectées</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="412"/>
        <source>Projector fan error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="413"/>
        <source>Projector check filter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="414"/>
        <source>General projector error</source>
        <translation>Erreur générale de projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="415"/>
        <source>The host address was not found</source>
        <translation>L&apos;adresse de l&apos;hôte n&apos;est pas trouvée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="416"/>
        <source>PJLink invalid packet received</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="417"/>
        <source>Projector lamp error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="418"/>
        <source>An error occurred with the network (Possibly someone pulled the plug?)</source>
        <translation>Une erreur réseau est survenue (Quelqu&apos;un a débranché le câble ?)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="420"/>
        <source>PJlink authentication Mismatch Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="421"/>
        <source>Projector not connected error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="422"/>
        <source>PJLink returned &quot;ERR2: Invalid Parameter&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="423"/>
        <source>PJLink Invalid prefix character</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="424"/>
        <source>PJLink returned &quot;ERR4: Projector/Display Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="425"/>
        <source>The socket is using a proxy, and the proxy requires authentication</source>
        <translation>La connexion utilise un proxy qui demande une authentification</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="428"/>
        <source>The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established)</source>
        <translation>La connexion au serveur proxy s&apos;est coupée inopinément (avant que la connexion au destinataire final soit établie)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="431"/>
        <source>Could not contact the proxy server because the connection to that server was denied</source>
        <translation>Ne peut pas joindre le serveur proxy car la connexion au serveur est refusée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="434"/>
        <source>The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase.</source>
        <translation>La délai de la connexion au serveur proxy est dépassé ou le serveur proxy ne répond plus durant la phase d&apos;identification.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="437"/>
        <source>The proxy address set with setProxy() was not found</source>
        <translation>L&apos;adresse proxy configuré avec setProxy() n&apos;est pas trouvée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="439"/>
        <source>The connection negotiation with the proxy server failed because the response from the proxy server could not be understood</source>
        <translation>La négociation de la connexion avec le serveur proxy a échoué car la réponse du serveur n&apos;est pas compréhensible</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="442"/>
        <source>The remote host closed the connection</source>
        <translation>L&apos;hôte distant a fermé la connexion</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="444"/>
        <source>The SSL/TLS handshake failed</source>
        <translation>La validation SSL/TLS a échouée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="446"/>
        <source>The address specified to socket.bind() does not belong to the host</source>
        <translation>L&apos;adresse donnée à socket.bind() n&apos;appartient à l&apos;hôte</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="449"/>
        <source>The socket operation failed because the application lacked the required privileges</source>
        <translation>L&apos;opération de connection a échoué parce que l&apos;application n&apos;a pas les droits nécessaires</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="452"/>
        <source>The local system ran out of resources (e.g., too many sockets)</source>
        <translation>Le sytème local n&apos;a plus de ressources (ex : trop de connexions)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="454"/>
        <source>The socket operation timed out</source>
        <translation>L&apos;opération de connexion est dépassée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="456"/>
        <source>Projector high temperature detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="457"/>
        <source>PJLink returned &quot;ERR3: Busy&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="458"/>
        <source>PJLink returned &quot;ERR1: Undefined Command&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="459"/>
        <source>The last operation attempted has not finished yet (still in progress in the background)</source>
        <translation>La dernière tentative n&apos;est pas encore finie (toujours en cours en arrière plan)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="462"/>
        <source>Unknown condiction detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="463"/>
        <source>An unidentified socket error occurred</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="464"/>
        <source>The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support)</source>
        <translation>L&apos;opération de connexion n&apos;est pas supportée par le système d&apos;exploitation (ex : IPv6 non supporté)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="467"/>
        <source>Warning condition detected</source>
        <translation>Alertes détectées</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="468"/>
        <source>Connection initializing with pin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="469"/>
        <source>Socket is bount to an address or port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="470"/>
        <source>Connection initializing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="471"/>
        <source>Socket is about to close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="472"/>
        <source>Connected</source>
        <translation>Connecté</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="473"/>
        <source>Connecting</source>
        <translation>En cours de connexion</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="474"/>
        <source>Cooldown in progress</source>
        <translation>Refroidissement en cours</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="475"/>
        <source>Command returned with OK</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="476"/>
        <source>Performing a host name lookup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="477"/>
        <source>Projector Information available</source>
        <translation>Informations du projecteur disponibles</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="478"/>
        <source>Initialize in progress</source>
        <translation>Initialisation en cours</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="479"/>
        <source>Socket it listening (internal use only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="480"/>
        <source>No network activity at this time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="481"/>
        <source>Received data</source>
        <translation>Réception de données</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="482"/>
        <source>Sending data</source>
        <translation>Envoi de données</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="483"/>
        <source>Not Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="484"/>
        <source>Off</source>
        <translation>Eteind</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="485"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="486"/>
        <source>Power is on</source>
        <translation>Alimentation démarrée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="487"/>
        <source>Power in standby</source>
        <translation>Alimentation en veille</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="488"/>
        <source>Getting status</source>
        <translation>Récupération du status</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="489"/>
        <source>Warmup in progress</source>
        <translation>Préchauffage en cours</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEdit</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>Name Not Set</source>
        <translation>Nom non défini</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>You must enter a name for this entry.&lt;br /&gt;Please enter a new name for this entry.</source>
        <translation>Vous devez saisir un nom pour cette entrée.&lt;br /&gt;Merci de saisir un nouveau nom pour cette entrée.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="196"/>
        <source>Duplicate Name</source>
        <translation>Nom dupliqué</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEditForm</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="115"/>
        <source>Add New Projector</source>
        <translation>Ajouter nouveau projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="118"/>
        <source>Edit Projector</source>
        <translation>Modifier projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="120"/>
        <source>IP Address</source>
        <translation>Adresse IP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="123"/>
        <source>Port Number</source>
        <translation>Numéro de port</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="125"/>
        <source>PIN</source>
        <translation>NIP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="127"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="129"/>
        <source>Location</source>
        <translation>Localisation</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="131"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>Database Error</source>
        <translation>Erreur de base de données</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>There was an error saving projector information. See the log for the error</source>
        <translation>Erreur lors de la sauvegarde des informations du projecteur. Voir le journal pour l&apos;erreur</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorManager</name>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add Projector</source>
        <translation>Ajouter projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add a new projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit Projector</source>
        <translation>Modifier projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete Projector</source>
        <translation>Supprimer projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Select Input Source</source>
        <translation>Choisir la source en entrée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Choose input source on selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View Projector</source>
        <translation>Voir projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View selected projector information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="100"/>
        <source>Connect to selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors</source>
        <translation>Connecter les projecteurs sélectionnés</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projectors</source>
        <translation>Déconnecter les projecteurs sélectionnés</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projector</source>
        <translation>Déconnecter le projecteur sélectionné</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projector</source>
        <translation>Allumer le projecteur sélectionné</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="129"/>
        <source>Power on selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Standby selected projector</source>
        <translation>Mettre en veille le projecteur sélectionné</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="143"/>
        <source>Put selected projector in standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Put selected projectors in standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="156"/>
        <source>Blank selected projector screen</source>
        <translation>Obscurcir le projecteur sélectionner</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projector screen</source>
        <translation>Voir l&apos;écran des projecteurs sélectionnés</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="170"/>
        <source>Show selected projector screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projectors screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="199"/>
        <source>&amp;View Projector Information</source>
        <translation>&amp;Voir projecteur informations</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="204"/>
        <source>&amp;Edit Projector</source>
        <translation>Modifi&amp;er projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="210"/>
        <source>&amp;Connect Projector</source>
        <translation>&amp;Connecter le projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="215"/>
        <source>D&amp;isconnect Projector</source>
        <translation>Déconnect&amp;er le projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="221"/>
        <source>Power &amp;On Projector</source>
        <translation>Allumer projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="226"/>
        <source>Power O&amp;ff Projector</source>
        <translation>Eteindre projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="232"/>
        <source>Select &amp;Input</source>
        <translation>Cho&amp;isir entrée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="237"/>
        <source>Edit Input Source</source>
        <translation>Modifier la source d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="242"/>
        <source>&amp;Blank Projector Screen</source>
        <translation>Masquer l&apos;écran du projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="247"/>
        <source>&amp;Show Projector Screen</source>
        <translation>Afficher l&apos;écran du projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="253"/>
        <source>&amp;Delete Projector</source>
        <translation>Supprimer le projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="503"/>
        <source>Are you sure you want to delete this projector?</source>
        <translation>Êtes-vous sûr de bien vouloir supprimer ce projecteur ?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="648"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="650"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="652"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="654"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="658"/>
        <source>Projector information not available at this time.</source>
        <translation>Informations du projecteur indisponibles pour l&apos;instant.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="660"/>
        <source>Projector Name</source>
        <translation>Nom du projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="663"/>
        <source>Manufacturer</source>
        <translation>Constructeur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="665"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="667"/>
        <source>PJLink Class</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="670"/>
        <source>Software Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="673"/>
        <source>Serial Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="676"/>
        <source>Lamp Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="679"/>
        <source>Filter Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="682"/>
        <source>Other info</source>
        <translation>Autres infos</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="685"/>
        <source>Power status</source>
        <translation>Status de l&apos;alimentation</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Shutter is</source>
        <translation>Le cache est</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Closed</source>
        <translation>Fermé</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="691"/>
        <source>Current source input is</source>
        <translation>Entrée actuelle est</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="698"/>
        <source>Unavailable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="700"/>
        <source>ON</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="702"/>
        <source>OFF</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="703"/>
        <source>Lamp</source>
        <translation>Ampoule</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="708"/>
        <source>Hours</source>
        <translation>Heures</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="713"/>
        <source>No current errors or warnings</source>
        <translation>Pas d&apos;erreurs ou alertes actuellement</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="715"/>
        <source>Current errors/warnings</source>
        <translation>Erreurs/alertes en cours</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="718"/>
        <source>Projector Information</source>
        <translation>Information du projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="941"/>
        <source>Authentication Error</source>
        <translation>Erreur d&apos;identification</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="956"/>
        <source>No Authentication Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="998"/>
        <source>Not Implemented Yet</source>
        <translation>Pas encore implémenté</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="52"/>
        <source>Projector</source>
        <translation>Projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="111"/>
        <source>Communication Options</source>
        <translation>Options de communication</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="113"/>
        <source>Connect to projectors on startup</source>
        <translation>Connecter aux projecteurs au démarrage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="115"/>
        <source>Socket timeout (seconds)</source>
        <translation>Délai de connection réseau (secondes)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="117"/>
        <source>Poll time (seconds)</source>
        <translation>Temps d&apos;interrogation (secondes)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="121"/>
        <source>Tabbed dialog box</source>
        <translation>Boîte de dialogue à onglets</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="123"/>
        <source>Single dialog box</source>
        <translation>Boîte de dialogue seule</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="125"/>
        <source>Connect to projector when LINKUP received (v2 only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="127"/>
        <source>Enable listening for PJLink2 broadcast messages</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorWizard</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="216"/>
        <source>Duplicate IP Address</source>
        <translation>Adresse IP dupliquée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="224"/>
        <source>Invalid IP Address</source>
        <translation>Adresse IP invalide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="233"/>
        <source>Invalid Port Number</source>
        <translation>Numéro de port invalide</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProxyDialog</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="177"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProxyWidget</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="116"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="117"/>
        <source>No prox&amp;y</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="118"/>
        <source>&amp;Use system proxy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="119"/>
        <source>&amp;Manual proxy configuration</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="120"/>
        <source>e.g. proxy_server_address:port_no</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="121"/>
        <source>HTTP:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="123"/>
        <source>HTTPS:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="125"/>
        <source>Username:</source>
        <translation>Nom d&apos;utilisateur :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="126"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreenList</name>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="254"/>
        <source>Screen</source>
        <translation>Écran</translation>
    </message>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="257"/>
        <source>primary</source>
        <translation>primaire</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreensTab</name>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="44"/>
        <source>Screens</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="69"/>
        <source>Generic screen settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="70"/>
        <source>Display if a single screen</source>
        <translation>Afficher si il n&apos;y a qu&apos;un écran</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="297"/>
        <source>F&amp;ull screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="298"/>
        <source>Width:</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="299"/>
        <source>Use this screen as a display</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="302"/>
        <source>Left:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="303"/>
        <source>Custom &amp;geometry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="304"/>
        <source>Top:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="305"/>
        <source>Height:</source>
        <translation>Hauteur :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="306"/>
        <source>Identify Screens</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItem</name>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="289"/>
        <source>[slide {frame:d}]</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="590"/>
        <source>&lt;strong&gt;Start&lt;/strong&gt;: {start}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="594"/>
        <source>&lt;strong&gt;Length&lt;/strong&gt;: {length}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItemEditForm</name>
    <message>
        <location filename="../../openlp/core/ui/serviceitemeditdialog.py" line="70"/>
        <source>Reorder Service Item</source>
        <translation>Réordonne les éléments du service</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceManager</name>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="199"/>
        <source>Service Notes: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="246"/>
        <source>Notes: </source>
        <translation>Notes : </translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="254"/>
        <source>Playing time: </source>
        <translation>Durée du service : </translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="148"/>
        <source>Load an existing service.</source>
        <translation>Charge un service existant.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="152"/>
        <source>Save this service.</source>
        <translation>Enregistre ce service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="162"/>
        <source>Select a theme for the service.</source>
        <translation>Sélectionner un thème pour le service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move to &amp;top</source>
        <translation>Place en &amp;premier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move item to the top of the service.</source>
        <translation>Place l&apos;élément au début du service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move &amp;up</source>
        <translation>Déplace en &amp;haut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move item up one position in the service.</source>
        <translation>Déplace l&apos;élément d&apos;une position en haut.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move &amp;down</source>
        <translation>Déplace en &amp;bas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move item down one position in the service.</source>
        <translation>Déplace l&apos;élément d&apos;une position en bas.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move to &amp;bottom</source>
        <translation>Place en &amp;dernier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move item to the end of the service.</source>
        <translation>Place l&apos;élément a la fin du service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>&amp;Delete From Service</source>
        <translation>&amp;Retirer du service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>Delete the selected item from the service.</source>
        <translation>Retirer l&apos;élément sélectionné du service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>&amp;Expand all</source>
        <translation>&amp;Développer tout</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>Expand all the service items.</source>
        <translation>Développe tous les éléments du service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>&amp;Collapse all</source>
        <translation>&amp;Réduire tout</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>Collapse all the service items.</source>
        <translation>Réduit tous les éléments du service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Go Live</source>
        <translation>Lance le direct</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Send the selected item to Live.</source>
        <translation>Afficher l&apos;élément sélectionné en direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="241"/>
        <source>&amp;Add New Item</source>
        <translation>&amp;Ajoute un nouvel élément</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="243"/>
        <source>&amp;Add to Selected Item</source>
        <translation>&amp;Ajoute à l&apos;élément sélectionné</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="247"/>
        <source>&amp;Edit Item</source>
        <translation>&amp;Édite l&apos;élément</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="249"/>
        <source>&amp;Rename...</source>
        <translation>&amp;Renommer</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="252"/>
        <source>&amp;Reorder Item</source>
        <translation>&amp;Réordonne l&apos;élément</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="255"/>
        <source>&amp;Notes</source>
        <translation>&amp;Notes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="258"/>
        <source>&amp;Start Time</source>
        <translation>Temps de &amp;début</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="265"/>
        <source>Create New &amp;Custom Slide</source>
        <translation>&amp;Créer nouvelle diapositive personnalisée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="272"/>
        <source>&amp;Auto play slides</source>
        <translation>Jouer les diapositives &amp;automatiquement</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="276"/>
        <source>Auto play slides &amp;Loop</source>
        <translation>Jouer les diapositives automatiquement en &amp;boucle</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="281"/>
        <source>Auto play slides &amp;Once</source>
        <translation>Jouer les diapositives automatiquement une &amp;seule fois</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="812"/>
        <source>&amp;Delay between slides</source>
        <translation>Intervalle entre les diapositives</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="292"/>
        <source>Show &amp;Preview</source>
        <translation>Afficher en &amp;prévisualisation</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="297"/>
        <source>&amp;Change Item Theme</source>
        <translation>&amp;Change le thème de l&apos;élément</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="371"/>
        <source>Untitled Service</source>
        <translation>Service sans titre</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>OpenLP Service Files (*.osz *.oszl)</source>
        <translation>Fichier service OpenLP (*.osz *.oszl)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>Modified Service</source>
        <translation>Service modifié</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>The current service has been modified. Would you like to save this service?</source>
        <translation>Le service courant à été modifié. Voulez-vous l&apos;enregistrer ?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="572"/>
        <source>Service File(s) Missing</source>
        <translation>Fichier(s) service manquant</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="573"/>
        <source>The following file(s) in the service are missing: {name}

These files will be removed if you continue to save.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>Error Saving File</source>
        <translation>Erreur de sauvegarde du fichier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>There was an error saving your file.

{error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="653"/>
        <source>OpenLP Service Files - lite (*.oszl)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="654"/>
        <source>OpenLP Service Files (*.osz)</source>
        <translation>Fichier service OpenLP (*.osz)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="726"/>
        <source>The service file {file_path} could not be loaded because it is either corrupt, inaccessible, or not a valid OpenLP 2 or OpenLP 3 service file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="823"/>
        <source>&amp;Auto Start - active</source>
        <translation>Démarrage &amp;auto - actif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="827"/>
        <source>&amp;Auto Start - inactive</source>
        <translation>Démarrage &amp;auto - inactif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Input delay</source>
        <translation>Retard de l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Delay between slides in seconds.</source>
        <translation>Intervalle entre les diapositives en secondes.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1220"/>
        <source>Edit</source>
        <translation>Édite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1221"/>
        <source>Service copy only</source>
        <translation>Copie de service uniquement</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1224"/>
        <source>Slide theme</source>
        <translation>Thème de diapositive</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1228"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Missing Display Handler</source>
        <translation>Composant d&apos;affichage manquant</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1399"/>
        <source>Your item cannot be displayed as there is no handler to display it</source>
        <translation>Votre élément ne peut pas être affiché parce qu&apos;il n&apos;y a pas de composant pour l&apos;afficher</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Your item cannot be displayed as the plugin required to display it is missing or inactive</source>
        <translation>Votre élément ne peut pas être affiché parce que le module nécessaire est manquant ou désactivé</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Rename item title</source>
        <translation>Renommer titre article</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceNoteForm</name>
    <message>
        <location filename="../../openlp/core/ui/servicenoteform.py" line="72"/>
        <source>Service Item Notes</source>
        <translation>Notes sur l&apos;élément du service</translation>
    </message>
</context>
<context>
    <name>OpenLP.SettingsForm</name>
    <message>
        <location filename="../../openlp/core/ui/settingsdialog.py" line="62"/>
        <source>Configure OpenLP</source>
        <translation>Configuration d&apos;OpenLP</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListDialog</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="138"/>
        <source>Configure Shortcuts</source>
        <translation>Configurer les raccourcis</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="139"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>Sélectionnez une action puis cliquez sur un des boutons ci-dessous pour capturer un nouveau raccourci principal ou secondaire.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Shortcut</source>
        <translation>Raccourci</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Alternate</source>
        <translation>Alternatif</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="145"/>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="146"/>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="148"/>
        <source>Capture shortcut.</source>
        <translation>Capture un raccourci.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="151"/>
        <source>Restore the default shortcut of this action.</source>
        <translation>Restaure le raccourci par défaut de cette action.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Restore Default Shortcuts</source>
        <translation>Restaure les raccourcis par défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Do you want to restore all shortcuts to their defaults?</source>
        <translation>Voulez vous restaurer tous les raccourcis par leur valeur par défaut ?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="437"/>
        <source>The shortcut &quot;{key}&quot; is already assigned to another action,
please use a different shortcut.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="441"/>
        <source>Duplicate Shortcut</source>
        <translation>Raccourci dupliqué</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListForm</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an Action</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>Sélectionnez une action puis cliquez sur un des boutons ci-dessous pour capturer un nouveau raccourci principal ou secondaire.</translation>
    </message>
</context>
<context>
    <name>OpenLP.SlideController</name>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Previous Slide</source>
        <translation>Diapositive précédente</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Move to previous.</source>
        <translation>Déplace au précédant.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Next Slide</source>
        <translation>Diapositive suivante</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Move to next.</source>
        <translation>Déplace au suivant.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="266"/>
        <source>Hide</source>
        <translation>Cacher</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="268"/>
        <source>Move to preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="272"/>
        <source>Show Desktop</source>
        <translation>Afficher le bureau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="277"/>
        <source>Toggle Desktop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="282"/>
        <source>Toggle Blank to Theme</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="287"/>
        <source>Toggle Blank Screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="316"/>
        <source>Play Slides</source>
        <translation>Joue les diapositives</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="336"/>
        <source>Delay between slides in seconds.</source>
        <translation>Intervalle entre les diapositives en secondes.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="340"/>
        <source>Move to live.</source>
        <translation>Déplacer sur le direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="343"/>
        <source>Add to Service.</source>
        <translation>Ajoute au service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="347"/>
        <source>Edit and reload song preview.</source>
        <translation>Édite et recharge la prévisualisation du chant.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="351"/>
        <source>Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="358"/>
        <source>Start playing media.</source>
        <translation>Joue le média.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="362"/>
        <source>Pause playing media.</source>
        <translation>Mettre en pause la lecture.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="366"/>
        <source>Stop playing media.</source>
        <translation>Arrêter la lecture.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="370"/>
        <source>Loop playing media.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="377"/>
        <source>Video timer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="386"/>
        <source>Video position.</source>
        <translation>Position vidéo.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="397"/>
        <source>Audio Volume.</source>
        <translation>Volume sonore.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="414"/>
        <source>Go To</source>
        <translation>Aller à</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="442"/>
        <source>Go to &quot;Verse&quot;</source>
        <translation>Aller au &quot;Couplet&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="443"/>
        <source>Go to &quot;Chorus&quot;</source>
        <translation>Aller au &quot;Refrain&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="444"/>
        <source>Go to &quot;Bridge&quot;</source>
        <translation>Aller au &quot;Pont&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="446"/>
        <source>Go to &quot;Pre-Chorus&quot;</source>
        <translation>Aller au &quot;Pré-Refrain&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="447"/>
        <source>Go to &quot;Intro&quot;</source>
        <translation>Aller à &quot;Intro&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="448"/>
        <source>Go to &quot;Ending&quot;</source>
        <translation>Aller à &quot;Fin&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="449"/>
        <source>Go to &quot;Other&quot;</source>
        <translation>Aller à &quot;Autre&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="575"/>
        <source>Previous Service</source>
        <translation>Service précédent</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="580"/>
        <source>Next Service</source>
        <translation>Service suivant</translation>
    </message>
</context>
<context>
    <name>OpenLP.SourceSelectForm</name>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="150"/>
        <source>Ignoring current changes and return to OpenLP</source>
        <translation>Ignorer les changements actuels et retourner à OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="153"/>
        <source>Delete all user-defined text and revert to PJLink default text</source>
        <translation>Supprimer le texte personnalisé et recharger le texte par défaut de PJLink</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="156"/>
        <source>Discard changes and reset to previous user-defined text</source>
        <translation>Annuler les modifications et recharger le texte personnalisé précédent</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="159"/>
        <source>Save changes and return to OpenLP</source>
        <translation>Sauvegarder les changements et retourner à OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="397"/>
        <source>Edit Projector Source Text</source>
        <translation>Modifier le texte de la source du projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="399"/>
        <source>Select Projector Source</source>
        <translation>Choisir la source du projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="481"/>
        <source>Delete entries for this projector</source>
        <translation>Supprimer les entrées pour ce projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="482"/>
        <source>Are you sure you want to delete ALL user-defined source input text for this projector?</source>
        <translation>Êtes-vous sûr de bien vouloir supprimer tout le texte d&apos;entrée source de ce projecteur ?</translation>
    </message>
</context>
<context>
    <name>OpenLP.SpellTextEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="405"/>
        <source>Language:</source>
        <translation>Langue :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="416"/>
        <source>Spelling Suggestions</source>
        <translation>Suggestions orthographiques</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="424"/>
        <source>Formatting Tags</source>
        <translation>Tags de formatage</translation>
    </message>
</context>
<context>
    <name>OpenLP.StartTimeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="70"/>
        <source>Theme Layout</source>
        <translation>Disposition du thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="71"/>
        <source>The blue box shows the main area.</source>
        <translation>La boîte bleu indique la zone principale.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="72"/>
        <source>The red box shows the footer.</source>
        <translation>La boîte rouge indique la zone de pied de page.</translation>
    </message>
</context>
<context>
    <name>OpenLP.StartTime_form</name>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="116"/>
        <source>Item Start and Finish Time</source>
        <translation>Temps de début et de fin de l&apos;élément</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="123"/>
        <source>Hours:</source>
        <translation>Heures :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="124"/>
        <source>Minutes:</source>
        <translation>Minutes :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="125"/>
        <source>Seconds:</source>
        <translation>Secondes :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="126"/>
        <source>Start</source>
        <translation>Début</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="127"/>
        <source>Finish</source>
        <translation>Fini</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="128"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Time Validation Error</source>
        <translation>Erreur de validation du temps</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="72"/>
        <source>Finish time is set after the end of the media item</source>
        <translation>Le temps de fin est défini après la fin de l’élément média</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Start time is after the finish time of the media item</source>
        <translation>Le temps de début est après le temps de fin de l&apos;élément média</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="167"/>
        <source>(approximately %d lines per slide)</source>
        <translation>(environ %d lignes par diapo)</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeManager</name>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="68"/>
        <source>Create a new theme.</source>
        <translation>Crée un nouveau thème.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit Theme</source>
        <translation>Édite le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit a theme.</source>
        <translation>Édite un thème.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete Theme</source>
        <translation>Supprime le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete a theme.</source>
        <translation>Supprime un thème.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import Theme</source>
        <translation>Import le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import a theme.</source>
        <translation>Import un thème.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export Theme</source>
        <translation>Export le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export a theme.</source>
        <translation>Export un thème.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="108"/>
        <source>&amp;Edit Theme</source>
        <translation>&amp;Édite le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="111"/>
        <source>&amp;Copy Theme</source>
        <translation>&amp;Copie le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="114"/>
        <source>&amp;Rename Theme</source>
        <translation>&amp;Renomme le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="117"/>
        <source>&amp;Delete Theme</source>
        <translation>&amp;Supprime le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="121"/>
        <source>Set As &amp;Global Default</source>
        <translation>Définir comme défaut &amp;Global</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="125"/>
        <source>&amp;Export Theme</source>
        <translation>&amp;Exporte le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="260"/>
        <source>{text} (default)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>You must select a theme to rename.</source>
        <translation>Vous devez sélectionner un thème à renommer.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename Confirmation</source>
        <translation>Confirme le renommage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename {theme_name} theme?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="310"/>
        <source>Copy of {name}</source>
        <comment>Copy of &lt;theme name&gt;</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="340"/>
        <source>You must select a theme to edit.</source>
        <translation>Vous devez sélectionner un thème à éditer.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>You must select a theme to delete.</source>
        <translation>Vous devez sélectionner un thème à supprimer.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete Confirmation</source>
        <translation>Confirmation de suppression</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete {theme_name} theme?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="394"/>
        <source>You have not selected a theme.</source>
        <translation>Vous n&apos;avez pas sélectionner de thème.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="398"/>
        <source>Save Theme - ({name})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>OpenLP Themes (*.otz)</source>
        <translation>Thèmes OpenLP (*.otz)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Theme Exported</source>
        <translation>Thème exporté</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Your theme has been successfully exported.</source>
        <translation>Votre thème a été exporté avec succès.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>Theme Export Failed</source>
        <translation>L&apos;export du thème a échoué</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>The {theme_name} export failed because this error occurred: {err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>Select Theme Import File</source>
        <translation>Sélectionner le fichier thème à importer</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="505"/>
        <source>{name} (default)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme Already Exists</source>
        <translation>Le thème existe déjà</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme {name} already exists. Do you want to replace it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>Import Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>There was a problem importing {file_name}.

It is corrupt, inaccessible or not a valid theme.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>Validation Error</source>
        <translation>Erreur de validation</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>A theme with this name already exists.</source>
        <translation>Un autre thème porte déjà ce nom.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="773"/>
        <source>You are unable to delete the default theme.</source>
        <translation>Vous ne pouvez pas supprimer le thème par défaut.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="782"/>
        <source>{count} time(s) by {plugin}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Unable to delete theme</source>
        <translation>Impossible de supprimer le thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Theme is currently used 

{text}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ThemeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>Background Image Empty</source>
        <translation>L&apos;image de fond est vide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>You have not selected a background image. Please select one before continuing.</source>
        <translation>Vous n&apos;avez pas sélectionné une image de fond. Veuillez en sélectionner une avant de continuer.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="301"/>
        <source>Edit Theme - {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>Theme Name Missing</source>
        <translation>Nom du thème manquant</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>There is no name for this theme. Please enter one.</source>
        <translation>Ce thème n&apos;a pas de nom. Veuillez en saisir un.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Theme Name Invalid</source>
        <translation>Nom du thème invalide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Invalid theme name. Please enter one.</source>
        <translation>Nom du thème invalide. Veuillez en saisir un.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="128"/>
        <source>Select Image</source>
        <translation>Sélectionnez une image</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="153"/>
        <source>Select Video</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="411"/>
        <source>Theme Wizard</source>
        <translation>Assistant de thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="412"/>
        <source>Welcome to the Theme Wizard</source>
        <translation>Bienvenue dans l&apos;assistant de thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="414"/>
        <source>This wizard will help you to create and edit your themes. Click the next button below to start the process by setting up your background.</source>
        <translation>Cet assistant vous permet de créer et d&apos;éditer vos thèmes. Cliquer sur le bouton suivant pour démarrer le processus en choisissant votre fond.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="417"/>
        <source>Set Up Background</source>
        <translation>Choisir le font</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="418"/>
        <source>Set up your theme&apos;s background according to the parameters below.</source>
        <translation>Choisir le fond de votre thème à l&apos;aide des paramètres ci-dessous.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="420"/>
        <source>Background type:</source>
        <translation>Type de fond :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="421"/>
        <source>Solid color</source>
        <translation>Couleur unie</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="422"/>
        <source>Gradient</source>
        <translation>Dégradé</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="425"/>
        <source>Transparent</source>
        <translation>Transparent</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="427"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="468"/>
        <source>color:</source>
        <translation>couleur :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="430"/>
        <source>Starting color:</source>
        <translation>Couleur de début :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="431"/>
        <source>Ending color:</source>
        <translation>Couleur de fin :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="432"/>
        <source>Gradient:</source>
        <translation>Dégradé :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="433"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="435"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="437"/>
        <source>Circular</source>
        <translation>Circulaire</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="439"/>
        <source>Top Left - Bottom Right</source>
        <translation>Haut gauche - Bas droite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="441"/>
        <source>Bottom Left - Top Right</source>
        <translation>Bas gauche - Haut droite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="445"/>
        <source>Background color:</source>
        <translation>Couleur de fond :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="447"/>
        <source>Main Area Font Details</source>
        <translation>Détails de la police de la zone principale</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="448"/>
        <source>Define the font and display characteristics for the Display text</source>
        <translation>Définir la police et les caractéristique d&apos;affichage de ce texte</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="467"/>
        <source>Font:</source>
        <translation>Police :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="469"/>
        <source>Size:</source>
        <translation>Taille :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="454"/>
        <source>Line Spacing:</source>
        <translation>Espace entre les lignes :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="456"/>
        <source>&amp;Outline:</source>
        <translation>&amp;Contour :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="459"/>
        <source>&amp;Shadow:</source>
        <translation>&amp;Ombre :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="462"/>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="463"/>
        <source>Italic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="464"/>
        <source>Footer Area Font Details</source>
        <translation>Détails de la police de la zone du pied de page</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="465"/>
        <source>Define the font and display characteristics for the Footer text</source>
        <translation>Définir la police et les caractéristiques d&apos;affichage du texte de pied de page</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="471"/>
        <source>Text Formatting Details</source>
        <translation>Détails de formatage du texte</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="472"/>
        <source>Allows additional display formatting information to be defined</source>
        <translation>Permet de définir des paramètres d&apos;affichage supplémentaires</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="474"/>
        <source>Horizontal Align:</source>
        <translation>Alignement horizontal :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="475"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="476"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="477"/>
        <source>Center</source>
        <translation>Centré</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="478"/>
        <source>Justify</source>
        <translation>Justifier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="479"/>
        <source>Transitions:</source>
        <translation>Transitions :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="480"/>
        <source>Fade</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="481"/>
        <source>Slide</source>
        <translation>Diapositive</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="482"/>
        <source>Concave</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="483"/>
        <source>Convex</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="484"/>
        <source>Zoom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="485"/>
        <source>Speed:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="486"/>
        <source>Normal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="487"/>
        <source>Fast</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="488"/>
        <source>Slow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="489"/>
        <source>Output Area Locations</source>
        <translation>Emplacement de la zone d&apos;affichage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="490"/>
        <source>Allows you to change and move the Main and Footer areas.</source>
        <translation>Permet de déplacer les zones principale et de pied de page.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="492"/>
        <source>&amp;Main Area</source>
        <translation>Zone &amp;principale</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="493"/>
        <source>&amp;Use default location</source>
        <translation>&amp;Utilise l&apos;emplacement par défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="503"/>
        <source>X position:</source>
        <translation>Position x :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="510"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="505"/>
        <source>Y position:</source>
        <translation>Position y :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="507"/>
        <source>Width:</source>
        <translation>Largeur :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="509"/>
        <source>Height:</source>
        <translation>Hauteur :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="502"/>
        <source>&amp;Footer Area</source>
        <translation>Zone de &amp;pied de page</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="511"/>
        <source>Use default location</source>
        <translation>Utilise l&apos;emplacement par défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="513"/>
        <source>Layout Preview</source>
        <translation>Prévisualiser la mise en page</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="514"/>
        <source>Preview and Save</source>
        <translation>Prévisualiser et Sauvegarder</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="515"/>
        <source>Preview the theme and save it.</source>
        <translation>Prévisualiser le thème et le sauvegarder.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="516"/>
        <source>Theme name:</source>
        <translation>Nom du thème :</translation>
    </message>
</context>
<context>
    <name>OpenLP.Themes</name>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="74"/>
        <source>Recreating Theme Thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="75"/>
        <source>TextLabel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ThemesTab</name>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="44"/>
        <source>Themes</source>
        <translation>Thèmes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="116"/>
        <source>Global Theme</source>
        <translation>Thème global</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="117"/>
        <source>Universal Settings</source>
        <translation>Paramètres globaux</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="118"/>
        <source>&amp;Wrap footer text</source>
        <translation>&amp;Envelopper le texte de pied de page</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="119"/>
        <source>Theme Level</source>
        <translation>Politique d&apos;application du thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="120"/>
        <source>S&amp;ong Level</source>
        <translation>Politique d&apos;application du &amp;chant</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="121"/>
        <source>Use the theme from each song in the database. If a song doesn&apos;t have a theme associated with it, then use the service&apos;s theme. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation>Utilise le thème pour chaque chants de la base de données. Si un chant n&apos;a pas de thème associé, le thème du service est utilisé. Si le service n&apos;a pas de thème, le thème global est utilisé.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="125"/>
        <source>&amp;Service Level</source>
        <translation>Politique d&apos;application du &amp;service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="126"/>
        <source>Use the theme from the service, overriding any of the individual songs&apos; themes. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation>Utilise le thème du service, surcharge le thème de chaque chants. Si le service n&apos;a pas de thème, le thème global est utilisé.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="130"/>
        <source>&amp;Global Level</source>
        <translation>Niveau &amp;global</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="131"/>
        <source>Use the global theme, overriding any themes associated with either the service or the songs.</source>
        <translation>Utilise le thème global, surcharge tous les thèmes associés aux services ou aux chants.</translation>
    </message>
</context>
<context>
    <name>OpenLP.Ui</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="337"/>
        <source>About</source>
        <translation>A propos de</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="338"/>
        <source>&amp;Add</source>
        <translation>&amp;Ajoute</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="339"/>
        <source>Add group</source>
        <translation>Ajouter un groupe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="340"/>
        <source>Add group.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="341"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="342"/>
        <source>All Files</source>
        <translation>Tous les Fichiers</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="343"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="344"/>
        <source>Background Color</source>
        <translation>Couleur de fond</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="345"/>
        <source>Background color:</source>
        <translation>Couleur de fond :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="346"/>
        <source>Search is Empty or too Short</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="347"/>
        <source>&lt;strong&gt;The search you have entered is empty or shorter than 3 characters long.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please try again with a longer search.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="350"/>
        <source>No Bibles Available</source>
        <translation>Aucune Bible disponible</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="351"/>
        <source>&lt;strong&gt;There are no Bibles currently installed.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please use the Import Wizard to install one or more Bibles.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="353"/>
        <source>Bottom</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="354"/>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="355"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="356"/>
        <source>CCLI number:</source>
        <translation>Numéro CCLI :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="357"/>
        <source>CCLI song number:</source>
        <translation>Numéro de chant CCLI :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="358"/>
        <source>Create a new service.</source>
        <translation>Crée un nouveau service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="359"/>
        <source>Confirm Delete</source>
        <translation>Confirme la suppression</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="360"/>
        <source>Continuous</source>
        <translation>Continu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="361"/>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="362"/>
        <source>Default Color:</source>
        <translation>Couleur par défaut :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="363"/>
        <source>Service %Y-%m-%d %H-%M</source>
        <comment>This may not contain any of the following characters: /\?*|&lt;&gt;[]&quot;:+
See http://docs.python.org/library/datetime.html#strftime-strptime-behavior for more information.</comment>
        <translation>Service %Y-%m-%d %H-%M</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="367"/>
        <source>&amp;Delete</source>
        <translation>&amp;Supprime</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="368"/>
        <source>Display style:</source>
        <translation>Style d&apos;affichage :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="369"/>
        <source>Duplicate Error</source>
        <translation>Erreur de duplication</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="370"/>
        <source>&amp;Edit</source>
        <translation>&amp;Édite</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="371"/>
        <source>Empty Field</source>
        <translation>Champ vide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="372"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="373"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="374"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="375"/>
        <source>File appears to be corrupt.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="376"/>
        <source>pt</source>
        <comment>Abbreviated font point size unit</comment>
        <translation>pt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="377"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="378"/>
        <source>h</source>
        <comment>The abbreviated unit for hours</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="379"/>
        <source>Invalid Folder Selected</source>
        <comment>Singular</comment>
        <translation>Dossier sélectionné invalide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="380"/>
        <source>Invalid File Selected</source>
        <comment>Singular</comment>
        <translation>Fichier sélectionné invalide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="381"/>
        <source>Invalid Files Selected</source>
        <comment>Plural</comment>
        <translation>Fichiers sélectionnés invalides</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="382"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="383"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="384"/>
        <source>Layout style:</source>
        <translation>Type de disposition :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="385"/>
        <source>Live</source>
        <translation>Direct</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="386"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="387"/>
        <source>Live Background Error</source>
        <translation>Erreur de fond du direct</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="388"/>
        <source>Live Toolbar</source>
        <translation>Bar d&apos;outils direct</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="389"/>
        <source>Load</source>
        <translation>Charge</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="390"/>
        <source>Manufacturer</source>
        <comment>Singular</comment>
        <translation>Constructeur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="391"/>
        <source>Manufacturers</source>
        <comment>Plural</comment>
        <translation>Constructeurs</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="392"/>
        <source>Model</source>
        <comment>Singular</comment>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="393"/>
        <source>Models</source>
        <comment>Plural</comment>
        <translation>Modèles</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="394"/>
        <source>m</source>
        <comment>The abbreviated unit for minutes</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="395"/>
        <source>Middle</source>
        <translation>Milieu</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="396"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="397"/>
        <source>New Service</source>
        <translation>Nouveau service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="398"/>
        <source>New Theme</source>
        <translation>Nouveau thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="399"/>
        <source>Next Track</source>
        <translation>Piste suivante</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="400"/>
        <source>No Folder Selected</source>
        <comment>Singular</comment>
        <translation>Aucun dossier sélectionné</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="401"/>
        <source>No File Selected</source>
        <comment>Singular</comment>
        <translation>Pas de fichier sélectionné</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="402"/>
        <source>No Files Selected</source>
        <comment>Plural</comment>
        <translation>Aucun fichiers sélectionnés</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="403"/>
        <source>No Item Selected</source>
        <comment>Singular</comment>
        <translation>Aucun élément sélectionné</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="404"/>
        <source>No Items Selected</source>
        <comment>Plural</comment>
        <translation>Aucun éléments sélectionnés</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="405"/>
        <source>No Search Results</source>
        <translation>Aucun résultat de recherche</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="406"/>
        <source>OpenLP</source>
        <translation>OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="407"/>
        <source>OpenLP 2.0 and up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="408"/>
        <source>OpenLP is already running on this machine. 
Closing this instance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="409"/>
        <source>Open service.</source>
        <translation>Ouvrir le service.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="410"/>
        <source>Optional, this will be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="411"/>
        <source>Optional, this won&apos;t be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="412"/>
        <source>Play Slides in Loop</source>
        <translation>Afficher les diapositives en boucle</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="413"/>
        <source>Play Slides to End</source>
        <translation>Afficher les diapositives jusqu’à la fin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="414"/>
        <source>Preview</source>
        <translation>Prévisualisation</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="415"/>
        <source>Preview Toolbar</source>
        <translation>Prévisualiser barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="416"/>
        <source>Print Service</source>
        <translation>Imprimer le service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="417"/>
        <source>Projector</source>
        <comment>Singular</comment>
        <translation>Projecteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="418"/>
        <source>Projectors</source>
        <comment>Plural</comment>
        <translation>Projecteurs</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="419"/>
        <source>Replace Background</source>
        <translation>Remplace le fond</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="420"/>
        <source>Replace live background.</source>
        <translation>Remplace le fond du direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="421"/>
        <source>Replace live background is not available when the WebKit player is disabled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="423"/>
        <source>Reset Background</source>
        <translation>Réinitialiser le fond</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="424"/>
        <source>Reset live background.</source>
        <translation>Restaure le fond du direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="425"/>
        <source>Required, this will be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="426"/>
        <source>s</source>
        <comment>The abbreviated unit for seconds</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="428"/>
        <source>Save &amp;&amp; Preview</source>
        <translation>Enregistrer &amp;&amp; prévisualiser</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="429"/>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="430"/>
        <source>Search Themes...</source>
        <comment>Search bar place holder text </comment>
        <translation>Recherche dans les thèmes...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="431"/>
        <source>You must select an item to delete.</source>
        <translation>Vous devez sélectionner un élément à supprimer.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="432"/>
        <source>You must select an item to edit.</source>
        <translation>Vous devez sélectionner un élément à éditer.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="433"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="434"/>
        <source>Save Service</source>
        <translation>Enregistre le service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="435"/>
        <source>Service</source>
        <translation>Service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="436"/>
        <source>Please type more text to use &apos;Search As You Type&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="437"/>
        <source>Optional &amp;Split</source>
        <translation>Optionnel &amp;Partager</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="438"/>
        <source>Split a slide into two only if it does not fit on the screen as one slide.</source>
        <translation>Divisez la diapositive en deux seulement si elle ne loge pas sur l&apos;écran.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="64"/>
        <source>Starting import...</source>
        <translation>Démarrage de l&apos;import...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="441"/>
        <source>Stop Play Slides in Loop</source>
        <translation>Arrête la boucle de diapositive</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="442"/>
        <source>Stop Play Slides to End</source>
        <translation>Arrête la boucle de diapositive à la fin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="443"/>
        <source>Theme</source>
        <comment>Singular</comment>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="444"/>
        <source>Themes</source>
        <comment>Plural</comment>
        <translation>Thèmes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="445"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="446"/>
        <source>Top</source>
        <translation>Haut</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="447"/>
        <source>Unsupported File</source>
        <translation>Fichier non supporté</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="448"/>
        <source>Verse Per Slide</source>
        <translation>Un verset par diapositive</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="449"/>
        <source>Verse Per Line</source>
        <translation>Un verset par ligne</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="450"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="451"/>
        <source>View</source>
        <translation>Afficher</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="452"/>
        <source>View Mode</source>
        <translation>Mode d&apos;affichage</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="453"/>
        <source>Video</source>
        <translation>Vidéo</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="454"/>
        <source>Web Interface, Download and Install latest Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="455"/>
        <source>Book Chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="456"/>
        <source>Chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="457"/>
        <source>Verse</source>
        <translation>Couplet</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="459"/>
        <source>Psalm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="460"/>
        <source>Book names may be shortened from full names, for an example Ps 23 = Psalm 23</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="706"/>
        <source>Written by</source>
        <translation>Ecrit par</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="163"/>
        <source>Delete the selected item.</source>
        <translation>Supprime l&apos;élément sélectionné.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="166"/>
        <source>Move selection up one position.</source>
        <translation>Déplace la sélection d&apos;une position vers le haut.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="169"/>
        <source>Move selection down one position.</source>
        <translation>Déplace la sélection d&apos;une position vers le bas.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="310"/>
        <source>&amp;Vertical Align:</source>
        <translation>Alignement &amp;vertical :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="53"/>
        <source>Finished import.</source>
        <translation>Import terminé.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="54"/>
        <source>Format:</source>
        <translation>Format :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="56"/>
        <source>Importing</source>
        <translation>Import en cours</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="57"/>
        <source>Importing &quot;{source}&quot;...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="58"/>
        <source>Select Import Source</source>
        <translation>Sélectionnez la source à importer</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="59"/>
        <source>Select the import format and the location to import from.</source>
        <translation>Sélectionner le format d&apos;import et le chemin du fichier à importer.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="60"/>
        <source>Open {file_type} File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="61"/>
        <source>Open {folder_name} Folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="62"/>
        <source>%p%</source>
        <translation>%p%</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="63"/>
        <source>Ready.</source>
        <translation>Prêt.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="65"/>
        <source>You need to specify one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>Vous devez spécifier un fichier %s à importer.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="67"/>
        <source>You need to specify at least one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>Vous devez spécifier au moins un fichier de %s à importer.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="69"/>
        <source>You need to specify one %s folder to import from.</source>
        <comment>A song format e.g. PowerSong</comment>
        <translation>Vous devez spécifier un dossier %s à importer.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="372"/>
        <source>Welcome to the Bible Import Wizard</source>
        <translation>Bienvenue dans l&apos;assistant d&apos;import de Bible</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="137"/>
        <source>Welcome to the Duplicate Song Removal Wizard</source>
        <translation>Bienvenue dans l&apos;assistant de suppression des chants en double</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="144"/>
        <source>Welcome to the Song Export Wizard</source>
        <translation>Bienvenue dans l&apos;assistant d&apos;export de Chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="135"/>
        <source>Welcome to the Song Import Wizard</source>
        <translation>Bienvenue dans l&apos;assistant d&apos;import de Chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="33"/>
        <source>Author</source>
        <comment>Singular</comment>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="34"/>
        <source>Authors</source>
        <comment>Plural</comment>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="35"/>
        <source>Author Unknown</source>
        <translation>Auteur inconnu</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="37"/>
        <source>Songbook</source>
        <comment>Singular</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="38"/>
        <source>Songbooks</source>
        <comment>Plural</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="39"/>
        <source>Title and/or verses not found</source>
        <translation>Titre et/ou paragraphe non trouvé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="40"/>
        <source>Song Maintenance</source>
        <translation>Entretien des Chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="41"/>
        <source>Topic</source>
        <comment>Singular</comment>
        <translation>Sujet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="42"/>
        <source>Topics</source>
        <comment>Plural</comment>
        <translation>Sujets</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="43"/>
        <source>XML syntax error</source>
        <translation>Erreur de syntaxe XML</translation>
    </message>
</context>
<context>
    <name>OpenLP.core.lib</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="400"/>
        <source>{one} and {two}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="402"/>
        <source>{first} and {last}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenPL.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="499"/>
        <source>Other</source>
        <translation>Autre</translation>
    </message>
</context>
<context>
    <name>Openlp.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="119"/>
        <source>Source select dialog interface</source>
        <translation>Interface de sélection de la source</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin</name>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="168"/>
        <source>&lt;strong&gt;Presentation Plugin&lt;/strong&gt;&lt;br /&gt;The presentation plugin provides the ability to show presentations using a number of different programs. The choice of available presentation programs is available to the user in a drop down box.</source>
        <translation>&lt;strong&gt;Module de présentation&lt;/strong&gt;&lt;br /&gt;Le module de présentation permet d&apos;afficher des présentations issues d&apos;autres logiciels. La liste des logiciels disponibles se trouve dans les options d&apos;OpenLP rubrique Présentation.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="181"/>
        <source>Presentation</source>
        <comment>name singular</comment>
        <translation>Présentation</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="182"/>
        <source>Presentations</source>
        <comment>name plural</comment>
        <translation>Présentations</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="186"/>
        <source>Presentations</source>
        <comment>container title</comment>
        <translation>Présentations</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="190"/>
        <source>Load a new presentation.</source>
        <translation>Charge une nouvelle présentation.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="194"/>
        <source>Delete the selected presentation.</source>
        <translation>Supprime la présentation sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="195"/>
        <source>Preview the selected presentation.</source>
        <translation>Prévisualiser la présentation sélectionnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="196"/>
        <source>Send the selected presentation live.</source>
        <translation>Envoie la présentation sélectionnée au direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="197"/>
        <source>Add the selected presentation to the service.</source>
        <translation>Ajoute la présentation sélectionnée au service.</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="62"/>
        <source>Select Presentation(s)</source>
        <translation>Sélectionner un(des) Présentation(s)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="63"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="64"/>
        <source>Present using:</source>
        <translation>Actuellement utilisé :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="92"/>
        <source>Presentations ({text})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>File Exists</source>
        <translation>Ce fichier existe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>A presentation with that filename already exists.</source>
        <translation>Une présentation utilise déjà ce nom de fichier.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="207"/>
        <source>This type of presentation is not supported.</source>
        <translation>Ce type de présentation n&apos;est pas supporté.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>Missing Presentation</source>
        <translation>Présentation manquante</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>The presentation {name} no longer exists.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="369"/>
        <source>The presentation {name} is incomplete, please reload.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PresentationPlugin.PowerpointDocument</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/powerpointcontroller.py" line="536"/>
        <source>An error occurred in the PowerPoint integration and the presentation will be stopped. Restart the presentation if you wish to present it.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PresentationPlugin.PresentationTab</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="105"/>
        <source>Available Controllers</source>
        <translation>Logiciels de présentation disponibles</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="111"/>
        <source>PDF options</source>
        <translation>Options PDF</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="112"/>
        <source>PowerPoint options</source>
        <translation>Options PowerPoint</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="113"/>
        <source>Allow presentation application to be overridden</source>
        <translation>Autoriser l&apos;application de présentation à être surcharger</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="115"/>
        <source>Clicking on the current slide advances to the next effect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="118"/>
        <source>Let PowerPoint control the size and monitor of the presentations
(This may fix PowerPoint scaling issues in Windows 8 and 10)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="122"/>
        <source>Use given full path for mudraw or ghostscript binary:</source>
        <translation>Utilisez le chemin fourni pour mudraw ou l&apos;exécutable de ghostscript :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="124"/>
        <source>Select mudraw or ghostscript binary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="131"/>
        <source>{name} (unavailable)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="228"/>
        <source>The program is not ghostscript or mudraw which is required.</source>
        <translation>Le programme qui a été fourni n&apos;est pas mudraw ni ghostscript.</translation>
    </message>
</context>
<context>
    <name>RemotePlugin</name>
    <message>
        <location filename="../../openlp/core/api/http/server.py" line="159"/>
        <source>Importing Website</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RemotePlugin.Mobile</name>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="57"/>
        <source>Remote</source>
        <translation>Contrôle à distance</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="58"/>
        <source>Stage View</source>
        <translation>Prompteur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="59"/>
        <source>Live View</source>
        <translation>Vue du direct</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="60"/>
        <source>Chords View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="67"/>
        <source>Service Manager</source>
        <translation>Gestionnaire de services</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="68"/>
        <source>Slide Controller</source>
        <translation>Contrôleur de diapositive</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="69"/>
        <source>Alerts</source>
        <translation>Alertes</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="70"/>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="71"/>
        <source>Home</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="72"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="73"/>
        <source>Blank</source>
        <translation>Vide</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="74"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="75"/>
        <source>Desktop</source>
        <translation>Bureau</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="76"/>
        <source>Show</source>
        <translation>Affiche</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="77"/>
        <source>Prev</source>
        <translation>Préc</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="78"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="79"/>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="80"/>
        <source>Show Alert</source>
        <translation>Affiche une alerte</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="81"/>
        <source>Go Live</source>
        <translation>Lance le direct</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="82"/>
        <source>Add to Service</source>
        <translation>Ajouter au service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="83"/>
        <source>Add &amp;amp; Go to Service</source>
        <translation>Ajouter &amp;amp; Se rendre au Service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="84"/>
        <source>No Results</source>
        <translation>Pas de résultats</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="85"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="86"/>
        <source>Service</source>
        <translation>Service</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="87"/>
        <source>Slides</source>
        <translation>Diapos</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="88"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
</context>
<context>
    <name>RemotePlugin.RemoteTab</name>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="158"/>
        <source>Remote Interface</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="159"/>
        <source>Server Settings</source>
        <translation>Configuration du serveur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="160"/>
        <source>Serve on IP address:</source>
        <translation>Ecoute sur l&apos;adresse IP :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="161"/>
        <source>Port number:</source>
        <translation>Numéro de port :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="162"/>
        <source>Remote URL:</source>
        <translation>URL du contrôle à distance :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="163"/>
        <source>Stage view URL:</source>
        <translation>URL du Prompteur :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="164"/>
        <source>Live view URL:</source>
        <translation>URL du direct :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="165"/>
        <source>Chords view URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="166"/>
        <source>Display stage time in 12h format</source>
        <translation>Affiche l&apos;heure du prompteur au format 12h</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="167"/>
        <source>Show thumbnails of non-text slides in remote and stage view.</source>
        <translation>Afficher les miniatures des diapositives sans texte sur la vue distante et le prompteur.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="169"/>
        <source>Remote App</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="170"/>
        <source>Scan the QR code or click &lt;a href=&quot;{qr}&quot;&gt;download&lt;/a&gt; to download an app for your mobile device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="174"/>
        <source>User Authentication</source>
        <translation>Indentification utilisateur</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="177"/>
        <source>User id:</source>
        <translation>ID utilisateur :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="178"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="179"/>
        <source>Current Version number:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="180"/>
        <source>Latest Version number:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongPlugin.ReportSongList</name>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>Save File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>song_extract.csv</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>CSV format (*.csv)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report Creation</source>
        <translation>Création du rapport</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report 
{name} 
has been successfully created. </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>Song Extraction Failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>An error occurred while extracting: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongUsagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="90"/>
        <source>&amp;Song Usage Tracking</source>
        <translation>Suivre de l&apos;utilisation des &amp;chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>&amp;Delete Tracking Data</source>
        <translation>&amp;Supprime les données de suivi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>Delete song usage data up to a specified date.</source>
        <translation>Supprime les données de l&apos;utilisation des chants jusqu&apos;à une date donnée.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>&amp;Extract Tracking Data</source>
        <translation>&amp;Extraire les données de suivi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>Generate a report on song usage.</source>
        <translation>Génère un rapport de l&apos;utilisation des chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="104"/>
        <source>Toggle Tracking</source>
        <translation>Activer/Désactiver le suivi</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="118"/>
        <source>Toggle the tracking of song usage.</source>
        <translation>Activer/Désactiver le suivi de l&apos;utilisation des chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="154"/>
        <source>Song Usage</source>
        <translation>Suivi de l&apos;utilisation des chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="178"/>
        <source>Song usage tracking is active.</source>
        <translation>Le suivi de l&apos;utilisation des chants est actif.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="183"/>
        <source>Song usage tracking is inactive.</source>
        <translation>Le suivi de l&apos;utilisation des chants est inactif.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="193"/>
        <source>display</source>
        <translation>affiche</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="201"/>
        <source>printed</source>
        <translation>imprimé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="238"/>
        <source>&lt;strong&gt;SongUsage Plugin&lt;/strong&gt;&lt;br /&gt;This plugin tracks the usage of songs in services.</source>
        <translation>&lt;strong&gt;Module de suivi des chants&lt;/strong&gt;&lt;br /&gt;Ce module permet d&apos;effectuer des statistiques sur la projection des chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="249"/>
        <source>SongUsage</source>
        <comment>name singular</comment>
        <translation>Suivi de l&apos;utilisation des chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="250"/>
        <source>SongUsage</source>
        <comment>name plural</comment>
        <translation>Suivi de l&apos;utilisation des chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="254"/>
        <source>SongUsage</source>
        <comment>container title</comment>
        <translation>Suivi de l&apos;utilisation des chants</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDeleteForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="64"/>
        <source>Delete Song Usage Data</source>
        <translation>Supprime les données de suivi de l&apos;utilisation des chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="66"/>
        <source>Select the date up to which the song usage data should be deleted. 
All data recorded before this date will be permanently deleted.</source>
        <translation>Sélectionnez la date jusqu&apos;à laquelle les données d&apos;utilisation des chants devra être supprimé. 
Toutes les données enregistrées avant cette date seront définitivement supprimées.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Delete Selected Song Usage Events?</source>
        <translation>Supprime les événements sélectionné ?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Are you sure you want to delete selected Song Usage data?</source>
        <translation>Êtes vous sur de vouloir supprimer les données de suivi sélectionnées ?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>Deletion Successful</source>
        <translation>Suppression effectuée</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>All requested data has been deleted successfully.</source>
        <translation>Toutes les données demandées ont été supprimées avec succès. </translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDetailForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="85"/>
        <source>Song Usage Extraction</source>
        <translation>Extraction de l&apos;utilisation des chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="87"/>
        <source>Select Date Range</source>
        <translation>Sélectionner une période</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="88"/>
        <source>to</source>
        <translation>à</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="89"/>
        <source>Report Location</source>
        <translation>Emplacement du rapport</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>Output Path Not Selected</source>
        <translation>Répertoire de destination non sélectionné</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>You have not set a valid output location for your song usage report.
Please select an existing path on your computer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="91"/>
        <source>usage_detail_{old}_{new}.txt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report Creation</source>
        <translation>Création du rapport</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report
{name}
has been successfully created.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>Report Creation Failed</source>
        <translation>Création du rapport en échec</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>An error occurred while creating the report: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="301"/>
        <source>Arabic (CP-1256)</source>
        <translation>Arabe (CP-1256)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="302"/>
        <source>Baltic (CP-1257)</source>
        <translation>Baltique (CP-1257)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="303"/>
        <source>Central European (CP-1250)</source>
        <translation>Europe centrale (CP-1250)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="304"/>
        <source>Cyrillic (CP-1251)</source>
        <translation>Cyrillique (CP-1251)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="305"/>
        <source>Greek (CP-1253)</source>
        <translation>Grecque (CP-1253)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="306"/>
        <source>Hebrew (CP-1255)</source>
        <translation>Hébreux (CP-1255)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="307"/>
        <source>Japanese (CP-932)</source>
        <translation>Japonais (CP-932)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="308"/>
        <source>Korean (CP-949)</source>
        <translation>Coréen (CP-949)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="309"/>
        <source>Simplified Chinese (CP-936)</source>
        <translation>Chinois simplifié (CP-936)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="310"/>
        <source>Thai (CP-874)</source>
        <translation>Thaï (CP-874)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="311"/>
        <source>Traditional Chinese (CP-950)</source>
        <translation>Chinois Traditionnel (CP-950)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="312"/>
        <source>Turkish (CP-1254)</source>
        <translation>Turque (CP-1254)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="313"/>
        <source>Vietnam (CP-1258)</source>
        <translation>Vietnamiens (CP-1258)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="314"/>
        <source>Western European (CP-1252)</source>
        <translation>Europe de l&apos;ouest (CP-1252)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Character Encoding</source>
        <translation>Encodage des caractères</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="323"/>
        <source>The codepage setting is responsible
for the correct character representation.
Usually you are fine with the preselected choice.</source>
        <translation>Le paramétrage de la table des caractères
permet un affichage correct des caractères.
L&apos;option déjà sélectionnée est en général la bonne.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Please choose the character encoding.
The encoding is responsible for the correct character representation.</source>
        <translation>Veuillez choisir l&apos;encodage des caractères.
L&apos;encodage permet un affichage correct des caractères.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>&amp;Song</source>
        <translation>&amp;Chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="186"/>
        <source>Import songs using the import wizard.</source>
        <translation>Importer des chants en utilisant l&apos;assistant d&apos;import.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>CCLI SongSelect</source>
        <translation>CCLI SongSelect</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>Import songs from CCLI&apos;s SongSelect service.</source>
        <translation>Importer les chants à partir du service SongSelect CCLI.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>Exports songs using the export wizard.</source>
        <translation>Export des chants en utilisant l&apos;assistant d&apos;export.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="223"/>
        <source>Songs</source>
        <translation>Chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>&amp;Re-index Songs</source>
        <translation>&amp;Ré-indexation des Chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>Re-index the songs database to improve searching and ordering.</source>
        <translation>Ré-indexation de la base de données des chants pour accélérer la recherche et le tri.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find &amp;Duplicate Songs</source>
        <translation>Trouver chants &amp;dupliqués</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find and remove duplicate songs in the song database.</source>
        <translation>Trouve et supprime les doublons dans la base des chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Song List Report</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Produce a CSV file of all the songs in the database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="259"/>
        <source>Reindexing songs...</source>
        <translation>Ré-indexation des chants en cours...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="261"/>
        <source>Reindexing songs</source>
        <translation>Réindexation des chansons en cours</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="304"/>
        <source>&lt;strong&gt;Songs Plugin&lt;/strong&gt;&lt;br /&gt;The songs plugin provides the ability to display and manage songs.</source>
        <translation>&lt;strong&gt;Module Chants&lt;/strong&gt;&lt;br /&gt;Le module Chants permet d&apos;afficher et de gérer des chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="347"/>
        <source>Song</source>
        <comment>name singular</comment>
        <translation>Chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="348"/>
        <source>Songs</source>
        <comment>name plural</comment>
        <translation>Chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="352"/>
        <source>Songs</source>
        <comment>container title</comment>
        <translation>Chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="358"/>
        <source>Add a new song.</source>
        <translation>Ajoute un nouveau chant.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="359"/>
        <source>Edit the selected song.</source>
        <translation>Édite le chant sélectionné.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="360"/>
        <source>Delete the selected song.</source>
        <translation>Supprime le chant sélectionné.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="361"/>
        <source>Preview the selected song.</source>
        <translation>Prévisualiser le chant sélectionné.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="362"/>
        <source>Send the selected song live.</source>
        <translation>Envoie le chant sélectionné au direct.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="363"/>
        <source>Add the selected song to the service.</source>
        <translation>Ajoute le chant sélectionné au service.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="388"/>
        <source>Importing Songs</source>
        <translation>Import de chansons en cours</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="67"/>
        <source>Words</source>
        <comment>Author who wrote the lyrics of a song</comment>
        <translation>Paroles</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="68"/>
        <source>Music</source>
        <comment>Author who wrote the music of a song</comment>
        <translation>Musique</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="69"/>
        <source>Words and Music</source>
        <comment>Author who wrote both lyrics and music of a song</comment>
        <translation>Paroles et musique</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="71"/>
        <source>Translation</source>
        <comment>Author who translated the song</comment>
        <translation>Traduction</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="74"/>
        <source>Author Maintenance</source>
        <translation>Auteur de maintenance</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="75"/>
        <source>Display name:</source>
        <translation>Nom affiché :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="76"/>
        <source>First name:</source>
        <translation>Prénom :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="77"/>
        <source>Last name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="92"/>
        <source>You need to type in the first name of the author.</source>
        <translation>Vous devez entrer le prénom de l&apos;auteur.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="97"/>
        <source>You need to type in the last name of the author.</source>
        <translation>Vous devez entrer le nom de l&apos;auteur.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="102"/>
        <source>You have not set a display name for the author, combine the first and last names?</source>
        <translation>Nous n&apos;avez pas défini de nom à afficher pour l&apos;auteur, combiner le prénom et le nom ? </translation>
    </message>
</context>
<context>
    <name>SongsPlugin.CCLIFileImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/cclifile.py" line="84"/>
        <source>The file does not have a valid extension.</source>
        <translation>Le fichier a une extension non valide.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.DreamBeamImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/dreambeam.py" line="101"/>
        <source>Invalid DreamBeam song file_path. Missing DreamSong tag.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.EasyWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="316"/>
        <source>Administered by {admin}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="393"/>
        <source>&quot;{title}&quot; could not be imported. {entry}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="206"/>
        <source>This file does not exist.</source>
        <translation>Ce fichier n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="210"/>
        <source>Could not find the &quot;Songs.MB&quot; file. It must be in the same folder as the &quot;Songs.DB&quot; file.</source>
        <translation>Ne trouve pas le fichier &quot;Songs.MB&quot;. Il doit être dans le même dossier que le fichier &quot;Songs.DB&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="225"/>
        <source>This file is not a valid EasyWorship database.</source>
        <translation>Ce fichier n&apos;est pas une base EasyWorship valide.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="254"/>
        <source>Could not retrieve encoding.</source>
        <translation>Impossible de trouver l&apos;encodage.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="337"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="350"/>
        <source>This does not appear to be a valid Easy Worship 6 database directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="352"/>
        <source>This is not a valid Easy Worship 6 database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="426"/>
        <source>Unexpected data formatting.</source>
        <translation>Format de fichier inattendu.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="431"/>
        <source>No song text found.</source>
        <translation>Aucun chant n&apos;a été trouvé.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="471"/>
        <source>
[above are Song Tags with notes imported from EasyWorship]</source>
        <translation>
[ci-dessus les balises des chants avec des notes importées de EasyWorship]</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="134"/>
        <source>Meta Data</source>
        <translation>Méta données</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="155"/>
        <source>Custom Book Names</source>
        <translation>Noms de livres personnalisés</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditSongForm</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="427"/>
        <source>&amp;Save &amp;&amp; Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="301"/>
        <source>Song Editor</source>
        <translation>Éditeur de Chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="302"/>
        <source>&amp;Title:</source>
        <translation>&amp;Titre :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="303"/>
        <source>Alt&amp;ernate title:</source>
        <translation>Titre alt&amp;ernatif :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="304"/>
        <source>&amp;Lyrics:</source>
        <translation>&amp;Paroles :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="305"/>
        <source>&amp;Verse order:</source>
        <translation>Ordre des &amp;paragraphes :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="308"/>
        <source>Ed&amp;it All</source>
        <translation>Édite &amp;tous</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="310"/>
        <source>Title &amp;&amp; Lyrics</source>
        <translation>Titre &amp;&amp; paroles</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="313"/>
        <source>&amp;Add to Song</source>
        <translation>&amp;Ajoute au Chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="314"/>
        <source>&amp;Edit Author Type</source>
        <translation>&amp;Modifier le type d&apos;auteur</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="337"/>
        <source>&amp;Remove</source>
        <translation>&amp;Supprime</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="316"/>
        <source>&amp;Manage Authors, Topics, Songbooks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="318"/>
        <source>A&amp;dd to Song</source>
        <translation>A&amp;joute au Chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="319"/>
        <source>R&amp;emove</source>
        <translation>&amp;Supprime</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="321"/>
        <source>Add &amp;to Song</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="322"/>
        <source>Re&amp;move</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="323"/>
        <source>Authors, Topics &amp;&amp; Songbooks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="326"/>
        <source>New &amp;Theme</source>
        <translation>Nouveau &amp;thème</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="327"/>
        <source>Copyright Information</source>
        <translation>Information du copyright</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="330"/>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="331"/>
        <source>Theme, Copyright Info &amp;&amp; Comments</source>
        <translation>Thème, copyright &amp;&amp; commentaires</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="333"/>
        <source>Linked Audio</source>
        <translation>Fichier audio attaché</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="335"/>
        <source>Add &amp;File(s)</source>
        <translation>Ajoute un(des) &amp;fichier(s)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="336"/>
        <source>Add &amp;Media</source>
        <translation>Ajoute un &amp;média</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="338"/>
        <source>Remove &amp;All</source>
        <translation>Supprime &amp;tout</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="340"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; Not all of the verses are in use.</source>
        <translation>&lt;strong&gt;Attention :&lt;/strong&gt; Tous les versets ne sont pas utilisés.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="342"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; You have not entered a verse order.</source>
        <translation>&lt;strong&gt;Attention :&lt;/strong&gt; Vous n&apos;avez pas entré d&apos;ordre de verset.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="202"/>
        <source>There are no verses corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="207"/>
        <source>There is no verse corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="211"/>
        <source>Invalid Verse Order</source>
        <translation>Ordre des paragraphes invalides</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="225"/>
        <source>You need to type in a song title.</source>
        <translation>Vous devez entrer un titre pour ce chant.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="231"/>
        <source>You need to type in at least one verse.</source>
        <translation>Vous devez entrer au moins un paragraphe.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="237"/>
        <source>You need to have an author for this song.</source>
        <translation>Vous devez entrer un auteur pour ce chant.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="256"/>
        <source>There are misplaced formatting tags in the following verses:

{tag}

Please correct these tags before continuing.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="265"/>
        <source>You have {count} verses named {name} {number}. You can have at most 26 verses with the same name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>Add Author</source>
        <translation>Ajoute un auteur</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>This author does not exist, do you want to add them?</source>
        <translation>Cet auteur n&apos;existe pas, voulez-vous l&apos;ajouter ?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="603"/>
        <source>This author is already in the list.</source>
        <translation>Cet auteur ce trouve déjà dans la liste.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="610"/>
        <source>You have not selected a valid author. Either select an author from the list, or type in a new author and click the &quot;Add Author to Song&quot; button to add the new author.</source>
        <translation>Vous n&apos;avez pas sélectionné un auteur valide. Vous pouvez sélectionner un auteur dans la liste, ou entrer le nom d&apos;un nouvel auteur et cliquez sur &quot;Ajouter un auteur au Chant&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Edit Author Type</source>
        <translation>Modifier le type d&apos;auteur</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Choose type for this author</source>
        <translation>Choisir un type pour cet auteur</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>Add Topic</source>
        <translation>Ajoute un sujet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>This topic does not exist, do you want to add it?</source>
        <translation>Ce sujet n&apos;existe pas voulez-vous l&apos;ajouter ?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="679"/>
        <source>This topic is already in the list.</source>
        <translation>Ce sujet ce trouve déjà dans la liste.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="688"/>
        <source>You have not selected a valid topic. Either select a topic from the list, or type in a new topic and click the &quot;Add Topic to Song&quot; button to add the new topic.</source>
        <translation>Vous n&apos;avez pas sélectionné de sujet valide. Vous pouvez sélectionner un sujet dans la liste, ou entrer le nom d&apos;un nouveau sujet et cliquez sur &quot;Ajouter un sujet au Chant&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>Add Songbook</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>This Songbook does not exist, do you want to add it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="724"/>
        <source>This Songbook is already in the list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="732"/>
        <source>You have not selected a valid Songbook. Either select a Songbook from the list, or type in a new Songbook and click the &quot;Add to Song&quot; button to add the new Songbook.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="932"/>
        <source>Open File(s)</source>
        <translation>Ouvrir un(des) fichier(s)</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="91"/>
        <source>Edit Verse</source>
        <translation>Édite le paragraphe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="92"/>
        <source>&amp;Verse type:</source>
        <translation>&amp;Type de paragraphe :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="102"/>
        <source>&amp;Forced Split</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="103"/>
        <source>Split the verse when displayed regardless of the screen size.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="105"/>
        <source>&amp;Insert</source>
        <translation>&amp;Insère</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="106"/>
        <source>Split a slide into two by inserting a verse splitter.</source>
        <translation>Divise une diapositive en deux en insérant un séparateur de paragraphe.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="109"/>
        <source>Transpose:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="110"/>
        <source>Up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="111"/>
        <source>Down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="146"/>
        <source>Transposing failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="240"/>
        <source>Invalid Chord</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.ExportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="128"/>
        <source>Select Destination Folder</source>
        <translation>Sélectionner le répertoire de destination</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="143"/>
        <source>Song Export Wizard</source>
        <translation>Assistant d&apos;export de chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="146"/>
        <source>This wizard will help to export your songs to the open and free &lt;strong&gt;OpenLyrics &lt;/strong&gt; worship song format.</source>
        <translation>Cet assistant vous aide à exporter vos chants au format de chants de louange libre et gratuit &lt;strong&gt;OpenLyrics&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="149"/>
        <source>Select Songs</source>
        <translation>Sélectionner des chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="150"/>
        <source>Check the songs you want to export.</source>
        <translation>Coche les chants que vous voulez exporter.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="153"/>
        <source>Uncheck All</source>
        <translation>Décocher tous</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="154"/>
        <source>Check All</source>
        <translation>Cocher tous</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="155"/>
        <source>Select Directory</source>
        <translation>Sélectionner un répertoire</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="156"/>
        <source>Select the directory where you want the songs to be saved.</source>
        <translation>Sélectionner le répertoire où vous voulez enregistrer vos chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="158"/>
        <source>Directory:</source>
        <translation>Répertoire :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="159"/>
        <source>Exporting</source>
        <translation>Exportation</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="160"/>
        <source>Please wait while your songs are exported.</source>
        <translation>Merci d&apos;attendre que vos chants soient exportés.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="176"/>
        <source>You need to add at least one Song to export.</source>
        <translation>Vous devez exporter au moins un chant.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>No Save Location specified</source>
        <translation>Aucun emplacement de sauvegarde défini</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>You need to specify a directory.</source>
        <translation>Vous devez spécifier un répertoire.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="239"/>
        <source>Starting export...</source>
        <translation>Démarrer l&apos;export...</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.FoilPresenterSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/foilpresenter.py" line="387"/>
        <source>Invalid Foilpresenter song file. No verses found.</source>
        <translation>Fichier Foilpresenter invalide. Pas de couplet trouvé.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="339"/>
        <source>Enable search as you type</source>
        <translation>Activer la recherche à la frappe</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="134"/>
        <source>Song Import Wizard</source>
        <translation>Assistant d&apos;import de Chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="137"/>
        <source>This wizard will help you to import songs from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>Cet assistant vous permet d&apos;importer des chants de divers formats. Cliquez sur le bouton suivant ci-dessous pour démarrer le processus en sélectionnant le format à importer.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="152"/>
        <source>Add Files...</source>
        <translation>Ajoute des fichiers...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="154"/>
        <source>Remove File(s)</source>
        <translation>Supprime un(des) fichier(s)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="165"/>
        <source>Please wait while your songs are imported.</source>
        <translation>Veuillez patienter pendant l&apos;import de vos chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="169"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="170"/>
        <source>Save to File</source>
        <translation>Enregistre dans un fichier</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="336"/>
        <source>Your Song import failed. {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="197"/>
        <source>This importer has been disabled.</source>
        <translation>Cet importeur a été désactivé.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="209"/>
        <source>OpenLyrics Files</source>
        <translation>Fichiers OpenLyrics</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="210"/>
        <source>OpenLyrics or OpenLP 2 Exported Song</source>
        <translation>Chant exporté en OpenLyrics ou OpenLP 2.0</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="217"/>
        <source>OpenLP 2 Databases</source>
        <translation>Base de données OpenLP 2</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="220"/>
        <source>Generic Document/Presentation</source>
        <translation>Document/Présentation générique</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="223"/>
        <source>The generic document/presentation importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation>L&apos;import générique de document/présentation a été désactivé car OpenLP ne peut accéder à OpenOffice ou LibreOffice.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="226"/>
        <source>Select Document/Presentation Files</source>
        <translation>Sélectionner les fichiers Document/Présentation</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="232"/>
        <source>CCLI SongSelect Files</source>
        <translation>CCLI Song Sélectionner Fichiers</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="239"/>
        <source>ChordPro Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="246"/>
        <source>DreamBeam Song Files</source>
        <translation>Fichiers chants DreamBeam</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="253"/>
        <source>EasySlides XML File</source>
        <translation>Fichier XML EasySlides</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="260"/>
        <source>EasyWorship Song Database</source>
        <translation>Base de données de chants d&apos;EasyWorship</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="268"/>
        <source>EasyWorship 6 Song Data Directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="276"/>
        <source>EasyWorship Service File</source>
        <translation>Fichier de service EasyWorship</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="283"/>
        <source>Foilpresenter Song Files</source>
        <translation>Fichiers Chant Foilpresenter</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="291"/>
        <source>LiveWorship Database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="298"/>
        <source>LyriX Files</source>
        <translation>Fichiers LyriX</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="299"/>
        <source>LyriX (Exported TXT-files)</source>
        <translation>LyriX (Fichiers TXT exportés)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="306"/>
        <source>MediaShout Database</source>
        <translation>Base de données MediaShout</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="307"/>
        <source>The MediaShout importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation>L&apos;importeur MediaShout n&apos;est supporté que sous Windows. Il a été désactivé à cause d&apos;un module Python manquant. Si vous voulez utiliser cet importeur, vous devez installer le module &quot;pyodbc&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="323"/>
        <source>OPS Pro database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="324"/>
        <source>The OPS Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="333"/>
        <source>PowerPraise Song Files</source>
        <translation>Fichiers PowerPraise</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="340"/>
        <source>You need to specify a valid PowerSong 1.0 database folder.</source>
        <translation>Vous devez spécifier un dossier de données PowerSong 1.0 valide.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="347"/>
        <source>PresentationManager Song Files</source>
        <translation>Fichiers PresentationManager</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="354"/>
        <source>ProPresenter Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="361"/>
        <source>Singing The Faith Exported Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="363"/>
        <source>First use Singing The Faith Electonic edition to export the song(s) in Text format.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="371"/>
        <source>SongBeamer Files</source>
        <translation>Fichiers SongBeamer</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="379"/>
        <source>SongPro Text Files</source>
        <translation>Fichiers texte SongPro</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="380"/>
        <source>SongPro (Export File)</source>
        <translation>SongPro (Fichiers exportés)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="381"/>
        <source>In SongPro, export your songs using the File -&gt; Export menu</source>
        <translation>Dans SongPro, exportez vos chansons en utilisant le menu Fichier -&gt; Exporter</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="388"/>
        <source>SongShow Plus Song Files</source>
        <translation>Fichiers Chant SongShow Plus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="395"/>
        <source>Songs Of Fellowship Song Files</source>
        <translation>Fichiers Chant Songs Of Fellowship</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="397"/>
        <source>The Songs of Fellowship importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation>L&apos;import de chants Fellowship a été désactivé car OpenLP ne peut accéder à OpenOffice ou LibreOffice.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="405"/>
        <source>SundayPlus Song Files</source>
        <translation>Fichier chants SundayPlus</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="412"/>
        <source>VideoPsalm Files</source>
        <translation>Fichiers VideoPsalm</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="413"/>
        <source>VideoPsalm</source>
        <translation>VideoPsalm</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="414"/>
        <source>The VideoPsalm songbooks are normally located in {path}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="422"/>
        <source>Words Of Worship Song Files</source>
        <translation>Fichiers Chant Words Of Worship</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="430"/>
        <source>Worship Assistant Files</source>
        <translation>Fichiers Worship Assistant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="432"/>
        <source>Worship Assistant (CSV)</source>
        <translation>Worship Assistant (CSV)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="433"/>
        <source>In Worship Assistant, export your Database to a CSV file.</source>
        <translation>À partir de Worship Assistant, exportez votre base en fichier CSV.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="441"/>
        <source>WorshipCenter Pro Song Files</source>
        <translation>Fichier WorshipCenter Pro</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="443"/>
        <source>The WorshipCenter Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation>L&apos;import WorshipCenter Pro ne fonctionne que sous Windows. Il a été désactivé à cause d&apos;un module Python manquant. Si vous voulez importer, vous devez installer le module &quot;pyodbc&quot;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="453"/>
        <source>ZionWorx (CSV)</source>
        <translation>ZionWorx (CSV)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="454"/>
        <source>First convert your ZionWorx database to a CSV text file, as explained in the &lt;a href=&quot;http://manual.openlp.org/songs.html#importing-from-zionworx&quot;&gt;User Manual&lt;/a&gt;.</source>
        <translation>D&apos;abord, convertissez votre base de données ZionWorx à un fichier texte CSV, comme c&apos;est expliqué dans le &lt;a href=&quot;http://manual.openlp.org/songs.html#importing-from-zionworx&quot;&gt;manuel utilisateur&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.LiveWorshipImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="87"/>
        <source>Extracting data from database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="133"/>
        <source>Could not find Valentina DB ADK libraries </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="161"/>
        <source>Loading the extracting data</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.LyrixImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>File {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>Error: {error}</source>
        <translation>Erreur: {error}</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaFilesForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="65"/>
        <source>Select Media File(s)</source>
        <translation>Sélectionner un(des) fichier(s) média</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="66"/>
        <source>Select one or more audio files from the list below, and click OK to import them into this song.</source>
        <translation>Sélectionnez un ou plusieurs fichier depuis la liste ci-dessous, et cliquez sur le bouton OK pour les importer dans ce chant.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="721"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Titles</source>
        <translation>Titres</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Titles...</source>
        <translation>Recherche dans les titres...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="140"/>
        <source>Maintain the lists of authors, topics and books.</source>
        <translation>Maintenir la liste des auteurs, sujets et carnets de chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Entire Song</source>
        <translation>Chant entier</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Entire Song...</source>
        <translation>Recherche dans le chant entier...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Lyrics</source>
        <translation>Paroles</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Lyrics...</source>
        <translation>Recherche dans les paroles...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Authors...</source>
        <translation>Recherche dans les auteurs...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Topics...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Songbooks...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Copyright...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>CCLI number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search CCLI number...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="512"/>
        <source>Are you sure you want to delete the following songs?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="540"/>
        <source>copy</source>
        <comment>For song cloning</comment>
        <translation>copier</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="642"/>
        <source>Media</source>
        <translation>Médias</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="699"/>
        <source>CCLI License: </source>
        <translation>Licence CCLI :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="729"/>
        <source>Failed to render Song footer html.
See log for details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaShoutImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/mediashout.py" line="62"/>
        <source>Unable to open the MediaShout database.</source>
        <translation>Impossible d&apos;ouvrir la base de données MediaShout.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.OPSProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="65"/>
        <source>Unable to connect the OPS Pro database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="87"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLPSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openlp.py" line="109"/>
        <source>Not a valid OpenLP 2 song database.</source>
        <translation>Base de données de chant OpenLP 2 invalide.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLyricsExport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsexport.py" line="68"/>
        <source>Exporting &quot;{title}&quot;...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opensong.py" line="139"/>
        <source>Invalid OpenSong song file. Missing song tag.</source>
        <translation>Fichier OpenSong invalide. Balise song manquante.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.PowerSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No songs to import.</source>
        <translation>Aucun chant à importer.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No {text} files found.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="118"/>
        <source>Invalid {text} file. Unexpected byte value.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="136"/>
        <source>Invalid {text} file. Missing &quot;TITLE&quot; header.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="142"/>
        <source>Invalid {text} file. Missing &quot;COPYRIGHTLINE&quot; header.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="148"/>
        <source>Verses not found. Missing &quot;PART&quot; header.</source>
        <translation>Versets non trouvé. Entête &quot;PART&quot; manquante.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.PresentationManagerImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/presentationmanager.py" line="57"/>
        <source>File is not in XML-format, which is the only format supported.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SingingTheFaithImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="192"/>
        <source>Unknown hint {hint}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>File {file}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>Error: {error}</source>
        <translation>Erreur: {error}</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongBookForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="66"/>
        <source>Songbook Maintenance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="67"/>
        <source>&amp;Name:</source>
        <translation>&amp;Nom :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="68"/>
        <source>&amp;Publisher:</source>
        <translation>&amp;Éditeur :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookform.py" line="61"/>
        <source>You need to type in a name for the book.</source>
        <translation>Vous devez entrer un nom pour le carnet de chants.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongExportForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="253"/>
        <source>Finished export. To import these files use the &lt;strong&gt;OpenLyrics&lt;/strong&gt; importer.</source>
        <translation>Export terminé. Pour importer ces fichiers utilisez l’outil d&apos;import &lt;strong&gt;OpenLyrics&lt;/strong&gt;.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="257"/>
        <source>Your song export failed.</source>
        <translation>Votre export de chant a échoué.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="259"/>
        <source>Your song export failed because this error occurred: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="67"/>
        <source>Cannot access OpenOffice or LibreOffice</source>
        <translation>Impossible d’accéder à OpenOffice ou LibreOffice</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="82"/>
        <source>Unable to open file</source>
        <translation>Impossible d&apos;ouvrir le fichier</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="84"/>
        <source>File not found</source>
        <translation>Fichier non trouvé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="104"/>
        <source>copyright</source>
        <translation>copyright</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="118"/>
        <source>The following songs could not be imported:</source>
        <translation>Les chants suivants ne peuvent être importé :</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongMaintenanceForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="248"/>
        <source>Could not add your author.</source>
        <translation>Impossible d&apos;ajouter votre auteur.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="251"/>
        <source>This author already exists.</source>
        <translation>Cet auteur existe déjà.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="264"/>
        <source>Could not add your topic.</source>
        <translation>Impossible d&apos;ajouter votre sujet.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="267"/>
        <source>This topic already exists.</source>
        <translation>Ce sujet existe déjà.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="281"/>
        <source>Could not add your book.</source>
        <translation>Impossible d&apos;ajouter votre carnet de chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="284"/>
        <source>This book already exists.</source>
        <translation>Ce carnet de chants existe déjà.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="389"/>
        <source>Could not save your changes.</source>
        <translation>Impossible d&apos;enregistrer vos modifications.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="316"/>
        <source>The author {original} already exists. Would you like to make songs with author {new} use the existing author {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="329"/>
        <source>Could not save your modified author, because the author already exists.</source>
        <translation>Impossible d&apos;enregistrer vos modifications de l&apos;auteur, car l&apos;auteur existe déjà.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="352"/>
        <source>The topic {original} already exists. Would you like to make songs with topic {new} use the existing topic {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="362"/>
        <source>Could not save your modified topic, because it already exists.</source>
        <translation>Impossible d&apos;enregistrer vos modifications du sujet, car le sujet existe déjà.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="391"/>
        <source>The book {original} already exists. Would you like to make songs with book {new} use the existing book {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Delete Author</source>
        <translation>Supprime l&apos;auteur</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Are you sure you want to delete the selected author?</source>
        <translation>Êtes-vous sûr de bien vouloir supprimer l&apos;auteur sélectionné ?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>This author cannot be deleted, they are currently assigned to at least one song.</source>
        <translation>Cet auteur ne peut être supprimé, il est actuellement utilisé par au moins un chant.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Delete Topic</source>
        <translation>Supprime le sujet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Are you sure you want to delete the selected topic?</source>
        <translation>Êtes-vous sûr de bien vouloir supprimer le sujet sélectionné ?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>This topic cannot be deleted, it is currently assigned to at least one song.</source>
        <translation>Ce sujet ne peut être supprimé, il est actuellement utilisé par au moins un chant.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Delete Book</source>
        <translation>Supprime le carnet de chants</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Are you sure you want to delete the selected book?</source>
        <translation>Êtes-vous sûr de bien vouloir supprimer le carnet de chants sélectionné ?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>This book cannot be deleted, it is currently assigned to at least one song.</source>
        <translation>Ce carnet de chants ne peut être supprimé, il est actuellement utilisé par au moins un chant.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongSelectForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="233"/>
        <source>CCLI SongSelect Importer</source>
        <translation>Import CCLI SongSelect</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="234"/>
        <source>&lt;strong&gt;Note:&lt;/strong&gt; An Internet connection is required in order to import songs from CCLI SongSelect.</source>
        <translation>&lt;strong&gt;Remarque :&lt;/strong&gt; Une connexion internet est nécessaire pour importer des chants depuis CCLI SongSelect.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="238"/>
        <source>Username:</source>
        <translation>Nom d&apos;utilisateur :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="239"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="240"/>
        <source>Save username and password</source>
        <translation>Sauvegarder nom d&apos;utilisateur et mot de passe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="241"/>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="242"/>
        <source>Search Text:</source>
        <translation>Recherche dans le texte :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="243"/>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="244"/>
        <source>Stop</source>
        <translation>Arrêt</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="346"/>
        <source>Found {count:d} song(s)</source>
        <translation>{count:d} chant(s) trouvé(s)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="247"/>
        <source>Logout</source>
        <translation>Déconnexion</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="248"/>
        <source>View</source>
        <translation>Afficher</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="249"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="250"/>
        <source>Author(s):</source>
        <translation>Auteur(s):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="251"/>
        <source>Copyright:</source>
        <translation>Droits d&apos;auteur :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="252"/>
        <source>CCLI Number:</source>
        <translation>Numéro CCLI :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="253"/>
        <source>Lyrics:</source>
        <translation>Paroles :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="254"/>
        <source>Back</source>
        <translation>Arrière</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="255"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>More than 1000 results</source>
        <translation>Plus de 1000 résultats</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>Your search has returned more than 1000 results, it has been stopped. Please refine your search to fetch better results.</source>
        <translation>Le résultat de votre recherche contient plus de 1000 résultats, elle a été arrêtée.
Veuillez affiner votre recherche.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="139"/>
        <source>Logging out...</source>
        <translation>Déconnexion...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>Incomplete song</source>
        <translation>Chant incomplet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>This song is missing some information, like the lyrics, and cannot be imported.</source>
        <translation>Il manque des informations à ce chant, comme des paroles, il ne peut pas être importé.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>Save Username and Password</source>
        <translation>Sauvegarder nom d&apos;utilisateur et mot de passe</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>WARNING: Saving your username and password is INSECURE, your password is stored in PLAIN TEXT. Click Yes to save your password or No to cancel this.</source>
        <translation>ATTENTION: Enregistrer votre nom d&apos;utilisateur et votre mot de passe n&apos;est pas sécurisé, votre mot de passe sera conservé en clair. Cliquez sur Oui pour l&apos;enregistrer malgré tout ou Non pour annuler.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>Error Logging In</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>There was a problem logging in, perhaps your username or password is incorrect?</source>
        <translation>Il y a eu une erreur de connexion, peut-être un compte ou mot de passe incorrect ?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>Free user</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>You logged in with a free account, the search will be limited to songs in the public domain.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Song Imported</source>
        <translation>Chant importé</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Your song has been imported, would you like to import more songs?</source>
        <translation>Votre chant a été importé, voulez-vous importer plus de chants ?</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongsTab</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="116"/>
        <source>Song related settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="117"/>
        <source>Enable &quot;Go to verse&quot; button in Live panel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="119"/>
        <source>Update service from song edit</source>
        <translation>Mettre à jour le service après une modification de chant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="120"/>
        <source>Import missing songs from Service files</source>
        <translation>Importer les chants manquant des fichiers du service</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="122"/>
        <source>Add Songbooks as first slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="124"/>
        <source>If enabled all text between &quot;[&quot; and &quot;]&quot; will be regarded as chords.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="126"/>
        <source>Chords</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="127"/>
        <source>Display chords in the main view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="128"/>
        <source>Ignore chords when importing songs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="130"/>
        <source>Chord notation to use:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="131"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="132"/>
        <source>German</source>
        <translation>Allemand</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="133"/>
        <source>Neo-Latin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="135"/>
        <source>Footer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="140"/>
        <source>Song Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="141"/>
        <source>Alternate Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="142"/>
        <source>Written By</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="143"/>
        <source>Authors when type is not set</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="145"/>
        <source>Authors (Type &quot;Words&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="147"/>
        <source>Authors (Type &quot;Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="149"/>
        <source>Authors (Type &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="151"/>
        <source>Authors (Type &quot;Translation&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="152"/>
        <source>Authors (Type &quot;Words&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="154"/>
        <source>Authors (Type &quot;Music&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="156"/>
        <source>Copyright information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="157"/>
        <source>Songbook Entries</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="159"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="160"/>
        <source>Song CCLI Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="161"/>
        <source>Topics</source>
        <translation>Sujets</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Placeholder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="172"/>
        <source>can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="173"/>
        <source>list of entries, can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="177"/>
        <source>How to use Footers:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Footer Template</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Mako Syntax</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="181"/>
        <source>Reset Template</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.TopicsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="60"/>
        <source>Topic Maintenance</source>
        <translation>Maintenance des sujets</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="61"/>
        <source>Topic name:</source>
        <translation>Nom du sujet :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsform.py" line="58"/>
        <source>You need to type in a topic name.</source>
        <translation>Vous devez entrer un nom de sujet.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.VerseType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="149"/>
        <source>Verse</source>
        <translation>Couplet</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="150"/>
        <source>Chorus</source>
        <translation>Refrain</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="151"/>
        <source>Bridge</source>
        <translation>Pont</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="152"/>
        <source>Pre-Chorus</source>
        <translation>F-Pré-refrain</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="153"/>
        <source>Intro</source>
        <translation>Introduction</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="154"/>
        <source>Ending</source>
        <translation>Fin</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="155"/>
        <source>Other</source>
        <translation>Autre</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.VideoPsalmImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/videopsalm.py" line="134"/>
        <source>Error: {error}</source>
        <translation>Erreur: {error}</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.WordsofWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/wordsofworship.py" line="177"/>
        <source>Invalid Words of Worship song file. Missing {text!r} header.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipAssistantImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Error reading CSV file.</source>
        <translation>Impossible de lire le fichier CSV.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Line {number:d}: {error}</source>
        <translation>Ligne {number:d}: {error}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="182"/>
        <source>Record {count:d}</source>
        <translation>Entrée {count:d}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="122"/>
        <source>Decoding error: {error}</source>
        <translation>Erreur de décodage: {error}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="127"/>
        <source>File not valid WorshipAssistant CSV format.</source>
        <translation>Format de fichier CSV WorshipAssistant invalide.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipCenterProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipcenterpro.py" line="59"/>
        <source>Unable to connect the WorshipCenter Pro database.</source>
        <translation>Impossible de connecter la base WorshipCenter Pro.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.ZionWorxImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Error reading CSV file.</source>
        <translation>Impossible de lire le fichier CSV.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Line {number:d}: {error}</source>
        <translation>Ligne {number:d}: {error}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Record {index}</source>
        <translation>Entrée {index}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Decoding error: {error}</source>
        <translation>Erreur de décodage: {error}</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="106"/>
        <source>File not valid ZionWorx CSV format.</source>
        <translation>Format de fichier CSV ZionWorx invalide.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="120"/>
        <source>Record %d</source>
        <translation>Entrée %d</translation>
    </message>
</context>
<context>
    <name>Wizard</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="136"/>
        <source>Wizard</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="140"/>
        <source>This wizard will help you to remove duplicate songs from the song database. You will have a chance to review every potential duplicate song before it is deleted. So no songs will be deleted without your explicit approval.</source>
        <translation>Cet assistant va vous permettre de supprimer tous les chants en double. Vous pourrez les vérifier uns par uns avant qu&apos;ils ne soient supprimés.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="145"/>
        <source>Searching for duplicate songs.</source>
        <translation>Recherche des chants dupliqués.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="146"/>
        <source>Please wait while your songs database is analyzed.</source>
        <translation>Veuillez patienter durant l&apos;analyse de la base des chants.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="148"/>
        <source>Here you can decide which songs to remove and which ones to keep.</source>
        <translation>Vous pouvez choisir ici quels chants supprimer et lesquels garder.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="155"/>
        <source>Review duplicate songs ({current}/{total})</source>
        <translation>Réviser les chants en double ({current}/{total})</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>No duplicate songs have been found in the database.</source>
        <translation>Pas de chants en double trouvé dans la base de données. </translation>
    </message>
</context>
<context>
    <name>common.languages</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>(Afan) Oromo</source>
        <comment>Language code: om</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Abkhazian</source>
        <comment>Language code: ab</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afar</source>
        <comment>Language code: aa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afrikaans</source>
        <comment>Language code: af</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Albanian</source>
        <comment>Language code: sq</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amharic</source>
        <comment>Language code: am</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amuzgo</source>
        <comment>Language code: amu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ancient Greek</source>
        <comment>Language code: grc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Arabic</source>
        <comment>Language code: ar</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Armenian</source>
        <comment>Language code: hy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Assamese</source>
        <comment>Language code: as</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Aymara</source>
        <comment>Language code: ay</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Azerbaijani</source>
        <comment>Language code: az</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bashkir</source>
        <comment>Language code: ba</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Basque</source>
        <comment>Language code: eu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bengali</source>
        <comment>Language code: bn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bhutani</source>
        <comment>Language code: dz</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bihari</source>
        <comment>Language code: bh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bislama</source>
        <comment>Language code: bi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Breton</source>
        <comment>Language code: br</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bulgarian</source>
        <comment>Language code: bg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Burmese</source>
        <comment>Language code: my</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Byelorussian</source>
        <comment>Language code: be</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cakchiquel</source>
        <comment>Language code: cak</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cambodian</source>
        <comment>Language code: km</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Catalan</source>
        <comment>Language code: ca</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Chinese</source>
        <comment>Language code: zh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Comaltepec Chinantec</source>
        <comment>Language code: cco</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Corsican</source>
        <comment>Language code: co</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Croatian</source>
        <comment>Language code: hr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Czech</source>
        <comment>Language code: cs</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Danish</source>
        <comment>Language code: da</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Dutch</source>
        <comment>Language code: nl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>English</source>
        <comment>Language code: en</comment>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Esperanto</source>
        <comment>Language code: eo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Estonian</source>
        <comment>Language code: et</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Faeroese</source>
        <comment>Language code: fo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Fiji</source>
        <comment>Language code: fj</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Finnish</source>
        <comment>Language code: fi</comment>
        <translation>Finlandais</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>French</source>
        <comment>Language code: fr</comment>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Frisian</source>
        <comment>Language code: fy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Galician</source>
        <comment>Language code: gl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Georgian</source>
        <comment>Language code: ka</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>German</source>
        <comment>Language code: de</comment>
        <translation>Allemand</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greek</source>
        <comment>Language code: el</comment>
        <translation>Grec</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greenlandic</source>
        <comment>Language code: kl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Guarani</source>
        <comment>Language code: gn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Gujarati</source>
        <comment>Language code: gu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Haitian Creole</source>
        <comment>Language code: ht</comment>
        <translation>Créole</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hausa</source>
        <comment>Language code: ha</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hebrew (former iw)</source>
        <comment>Language code: he</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hiligaynon</source>
        <comment>Language code: hil</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hindi</source>
        <comment>Language code: hi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hungarian</source>
        <comment>Language code: hu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Icelandic</source>
        <comment>Language code: is</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Indonesian (former in)</source>
        <comment>Language code: id</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingua</source>
        <comment>Language code: ia</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingue</source>
        <comment>Language code: ie</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inuktitut (Eskimo)</source>
        <comment>Language code: iu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inupiak</source>
        <comment>Language code: ik</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Irish</source>
        <comment>Language code: ga</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Italian</source>
        <comment>Language code: it</comment>
        <translation>Italien</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Jakalteko</source>
        <comment>Language code: jac</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Japanese</source>
        <comment>Language code: ja</comment>
        <translation>Japonais</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Javanese</source>
        <comment>Language code: jw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>K&apos;iche&apos;</source>
        <comment>Language code: quc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kannada</source>
        <comment>Language code: kn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kashmiri</source>
        <comment>Language code: ks</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kazakh</source>
        <comment>Language code: kk</comment>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kekchí </source>
        <comment>Language code: kek</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kinyarwanda</source>
        <comment>Language code: rw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirghiz</source>
        <comment>Language code: ky</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirundi</source>
        <comment>Language code: rn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Korean</source>
        <comment>Language code: ko</comment>
        <translation>Koran</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kurdish</source>
        <comment>Language code: ku</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Laothian</source>
        <comment>Language code: lo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latin</source>
        <comment>Language code: la</comment>
        <translation>Latin</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latvian, Lettish</source>
        <comment>Language code: lv</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lingala</source>
        <comment>Language code: ln</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lithuanian</source>
        <comment>Language code: lt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Macedonian</source>
        <comment>Language code: mk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malagasy</source>
        <comment>Language code: mg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malay</source>
        <comment>Language code: ms</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malayalam</source>
        <comment>Language code: ml</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maltese</source>
        <comment>Language code: mt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mam</source>
        <comment>Language code: mam</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mri</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Marathi</source>
        <comment>Language code: mr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Moldavian</source>
        <comment>Language code: mo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mongolian</source>
        <comment>Language code: mn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nahuatl</source>
        <comment>Language code: nah</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nauru</source>
        <comment>Language code: na</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nepali</source>
        <comment>Language code: ne</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Norwegian</source>
        <comment>Language code: no</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Occitan</source>
        <comment>Language code: oc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Oriya</source>
        <comment>Language code: or</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Pashto, Pushto</source>
        <comment>Language code: ps</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Persian</source>
        <comment>Language code: fa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Plautdietsch</source>
        <comment>Language code: pdt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Polish</source>
        <comment>Language code: pl</comment>
        <translation>Polonais</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Portuguese</source>
        <comment>Language code: pt</comment>
        <translation>Portugais</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Punjabi</source>
        <comment>Language code: pa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Quechua</source>
        <comment>Language code: qu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Rhaeto-Romance</source>
        <comment>Language code: rm</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Romanian</source>
        <comment>Language code: ro</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Russian</source>
        <comment>Language code: ru</comment>
        <translation>Russe</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Samoan</source>
        <comment>Language code: sm</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sangro</source>
        <comment>Language code: sg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sanskrit</source>
        <comment>Language code: sa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Scots Gaelic</source>
        <comment>Language code: gd</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbian</source>
        <comment>Language code: sr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbo-Croatian</source>
        <comment>Language code: sh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sesotho</source>
        <comment>Language code: st</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Setswana</source>
        <comment>Language code: tn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Shona</source>
        <comment>Language code: sn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sindhi</source>
        <comment>Language code: sd</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Singhalese</source>
        <comment>Language code: si</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Siswati</source>
        <comment>Language code: ss</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovak</source>
        <comment>Language code: sk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovenian</source>
        <comment>Language code: sl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Somali</source>
        <comment>Language code: so</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Spanish</source>
        <comment>Language code: es</comment>
        <translation>Espagnol</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sudanese</source>
        <comment>Language code: su</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swahili</source>
        <comment>Language code: sw</comment>
        <translation>Swahili</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swedish</source>
        <comment>Language code: sv</comment>
        <translation>Suédois</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tagalog</source>
        <comment>Language code: tl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tajik</source>
        <comment>Language code: tg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tamil</source>
        <comment>Language code: ta</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tatar</source>
        <comment>Language code: tt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tegulu</source>
        <comment>Language code: te</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Thai</source>
        <comment>Language code: th</comment>
        <translation>Thailand&apos;s</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tibetan</source>
        <comment>Language code: bo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tigrinya</source>
        <comment>Language code: ti</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tonga</source>
        <comment>Language code: to</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tsonga</source>
        <comment>Language code: ts</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkish</source>
        <comment>Language code: tr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkmen</source>
        <comment>Language code: tk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Twi</source>
        <comment>Language code: tw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uigur</source>
        <comment>Language code: ug</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ukrainian</source>
        <comment>Language code: uk</comment>
        <translation>Ukrainien</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Urdu</source>
        <comment>Language code: ur</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uspanteco</source>
        <comment>Language code: usp</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uzbek</source>
        <comment>Language code: uz</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Vietnamese</source>
        <comment>Language code: vi</comment>
        <translation>Vietnamien</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Volapuk</source>
        <comment>Language code: vo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Welch</source>
        <comment>Language code: cy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Wolof</source>
        <comment>Language code: wo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Xhosa</source>
        <comment>Language code: xh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yiddish (former ji)</source>
        <comment>Language code: yi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yoruba</source>
        <comment>Language code: yo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zhuang</source>
        <comment>Language code: za</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zulu</source>
        <comment>Language code: zu</comment>
        <translation>Zulu</translation>
    </message>
</context>
</TS>
<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_TW" version="2.0">
<context>
    <name>AlertsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>&amp;Alert</source>
        <translation>警報(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>Show an alert message.</source>
        <translation>顯示警報訊息。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="212"/>
        <source>&lt;strong&gt;Alerts Plugin&lt;/strong&gt;&lt;br /&gt;The alert plugin controls the displaying of alerts on the display screen.</source>
        <translation>&lt;strong&gt;警報插件&lt;/strong&gt;&lt;br /&gt;警報插件控制在顯示器畫面顯示的警報字句。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="222"/>
        <source>Alert</source>
        <comment>name singular</comment>
        <translation>警報</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="223"/>
        <source>Alerts</source>
        <comment>name plural</comment>
        <translation>警報</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="227"/>
        <source>Alerts</source>
        <comment>container title</comment>
        <translation>警報</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertForm</name>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="93"/>
        <source>Alert Message</source>
        <translation>警報訊息</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="94"/>
        <source>Alert &amp;text:</source>
        <translation>警報文字(&amp;T)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="95"/>
        <source>&amp;Parameter:</source>
        <translation>參數(&amp;P)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="96"/>
        <source>&amp;New</source>
        <translation>新增(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="97"/>
        <source>&amp;Save</source>
        <translation>存檔(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="98"/>
        <source>Displ&amp;ay</source>
        <translation>顯示(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="99"/>
        <source>Display &amp;&amp; Cl&amp;ose</source>
        <translation>顯示並關閉(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>New Alert</source>
        <translation>新的警報</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>You haven't specified any text for your alert. 
Please type in some text before clicking New.</source>
        <translation>您尚未在警報文字欄指定任何文字。
請在新建前輸入一些文字。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>No Parameter Found</source>
        <translation>找不到參數</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>You have not entered a parameter to be replaced.
Do you want to continue anyway?</source>
        <translation>您尚未輸入參數替換,是否要繼續？</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>No Placeholder Found</source>
        <translation>找不到提示</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>The alert text does not contain '&lt;&gt;'.
Do you want to continue anyway?</source>
        <translation>警報訊息沒有包含&apos;&lt;&gt;&apos;，
是否繼續？</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsManager</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertsmanager.py" line="73"/>
        <source>Alert message created and displayed.</source>
        <translation>警報訊息已顯示。</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsTab</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="119"/>
        <source>Font Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="120"/>
        <source>Font name:</source>
        <translation>字型名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="121"/>
        <source>Font color:</source>
        <translation>字型顏色：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="123"/>
        <source>Font size:</source>
        <translation>字體大小：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="125"/>
        <source>Background Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="126"/>
        <source>Other Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="127"/>
        <source>Alert timeout:</source>
        <translation>警報中止：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="129"/>
        <source>Repeat (no. of times):</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="130"/>
        <source>Enable Scrolling</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin</name>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="125"/>
        <source>&amp;Bible</source>
        <translation>聖經(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="141"/>
        <source>&lt;strong&gt;Bible Plugin&lt;/strong&gt;&lt;br /&gt;The Bible plugin provides the ability to display Bible verses from different sources during the service.</source>
        <translation>&lt;strong&gt;聖經插件&lt;/strong&gt;&lt;br /&gt;使用聖經插件在聚會中顯示不同的聖經來源。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="174"/>
        <source>Bible</source>
        <comment>name singular</comment>
        <translation>聖經</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="175"/>
        <source>Bibles</source>
        <comment>name plural</comment>
        <translation>聖經</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="179"/>
        <source>Bibles</source>
        <comment>container title</comment>
        <translation>聖經</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="184"/>
        <source>Import a Bible.</source>
        <translation>匯入一本聖經。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="185"/>
        <source>Add a new Bible.</source>
        <translation>新增一本聖經。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="186"/>
        <source>Edit the selected Bible.</source>
        <translation>編輯所選擇的聖經。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="187"/>
        <source>Delete the selected Bible.</source>
        <translation>刪除所選擇的聖經。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="188"/>
        <source>Preview the selected Bible.</source>
        <translation>預覽所選擇的聖經。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="189"/>
        <source>Send the selected Bible live.</source>
        <translation>傳送所選擇的聖經到現場Live。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="190"/>
        <source>Add the selected Bible to the service.</source>
        <translation>新增所選擇的聖經到聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="76"/>
        <source>Genesis</source>
        <translation>創世紀</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="77"/>
        <source>Exodus</source>
        <translation>出埃及記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="78"/>
        <source>Leviticus</source>
        <translation>利未記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="79"/>
        <source>Numbers</source>
        <translation>民數記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="80"/>
        <source>Deuteronomy</source>
        <translation>申命記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="81"/>
        <source>Joshua</source>
        <translation>約書亞記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="82"/>
        <source>Judges</source>
        <translation>士師記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="83"/>
        <source>Ruth</source>
        <translation>路得記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="84"/>
        <source>1 Samuel</source>
        <translation>撒母爾記上</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="85"/>
        <source>2 Samuel</source>
        <translation>撒母爾記下</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="86"/>
        <source>1 Kings</source>
        <translation>列王記上</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="87"/>
        <source>2 Kings</source>
        <translation>列王記下</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="88"/>
        <source>1 Chronicles</source>
        <translation>歷代志上</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="89"/>
        <source>2 Chronicles</source>
        <translation>歷代志下</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="90"/>
        <source>Ezra</source>
        <translation>以斯拉記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="91"/>
        <source>Nehemiah</source>
        <translation>尼希米記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="92"/>
        <source>Esther</source>
        <translation>以斯帖記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="93"/>
        <source>Job</source>
        <translation>約伯記</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="94"/>
        <source>Psalms</source>
        <translation>詩篇</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="95"/>
        <source>Proverbs</source>
        <translation>箴言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="96"/>
        <source>Ecclesiastes</source>
        <translation>傳道書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="97"/>
        <source>Song of Solomon</source>
        <translation>雅歌</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="98"/>
        <source>Isaiah</source>
        <translation>以賽亞書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="99"/>
        <source>Jeremiah</source>
        <translation>以賽亞書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="100"/>
        <source>Lamentations</source>
        <translation>耶利米哀歌</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="101"/>
        <source>Ezekiel</source>
        <translation>以西結書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="102"/>
        <source>Daniel</source>
        <translation>但以理書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="103"/>
        <source>Hosea</source>
        <translation>何西阿書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="104"/>
        <source>Joel</source>
        <translation>約珥書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="105"/>
        <source>Amos</source>
        <translation>阿摩司書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="106"/>
        <source>Obadiah</source>
        <translation>俄巴底亞書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="107"/>
        <source>Jonah</source>
        <translation>約拿書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="108"/>
        <source>Micah</source>
        <translation>彌迦書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="109"/>
        <source>Nahum</source>
        <translation>那鴻書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="110"/>
        <source>Habakkuk</source>
        <translation>哈巴谷書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="111"/>
        <source>Zephaniah</source>
        <translation>西番雅書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="112"/>
        <source>Haggai</source>
        <translation>哈該書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="113"/>
        <source>Zechariah</source>
        <translation>撒迦利書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="114"/>
        <source>Malachi</source>
        <translation>瑪拉基書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="115"/>
        <source>Matthew</source>
        <translation>馬太福音</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="116"/>
        <source>Mark</source>
        <translation>馬可福音</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="117"/>
        <source>Luke</source>
        <translation>路加福音</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="118"/>
        <source>John</source>
        <translation>約翰福音</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="119"/>
        <source>Acts</source>
        <translation>使徒行傳</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="120"/>
        <source>Romans</source>
        <translation>羅馬書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="121"/>
        <source>1 Corinthians</source>
        <translation>哥林多前書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="122"/>
        <source>2 Corinthians</source>
        <translation>哥林多後書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="123"/>
        <source>Galatians</source>
        <translation>加拉太書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="124"/>
        <source>Ephesians</source>
        <translation>以弗所書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="125"/>
        <source>Philippians</source>
        <translation>腓立比書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="126"/>
        <source>Colossians</source>
        <translation>歌羅西書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="127"/>
        <source>1 Thessalonians</source>
        <translation>帖撒羅尼迦前書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="128"/>
        <source>2 Thessalonians</source>
        <translation>帖撒羅尼迦後書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="129"/>
        <source>1 Timothy</source>
        <translation>提摩太前書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="130"/>
        <source>2 Timothy</source>
        <translation>提摩太後書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="131"/>
        <source>Titus</source>
        <translation>提多書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="132"/>
        <source>Philemon</source>
        <translation>腓利門書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="133"/>
        <source>Hebrews</source>
        <translation>希伯來書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="134"/>
        <source>James</source>
        <translation>雅各書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="135"/>
        <source>1 Peter</source>
        <translation>彼得前書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="136"/>
        <source>2 Peter</source>
        <translation>彼得後書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="137"/>
        <source>1 John</source>
        <translation>約翰壹書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="138"/>
        <source>2 John</source>
        <translation>約翰貳書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="139"/>
        <source>3 John</source>
        <translation>約翰參書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="140"/>
        <source>Jude</source>
        <translation>猶大書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="141"/>
        <source>Revelation</source>
        <translation>啟示錄</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="142"/>
        <source>Judith</source>
        <translation>友弟德傳</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="143"/>
        <source>Wisdom</source>
        <translation>智慧篇</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="144"/>
        <source>Tobit</source>
        <translation>多俾亞傳</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="145"/>
        <source>Sirach</source>
        <translation>德訓篇</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="146"/>
        <source>Baruch</source>
        <translation>巴路克</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="147"/>
        <source>1 Maccabees</source>
        <translation>瑪喀比一書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="148"/>
        <source>2 Maccabees</source>
        <translation>瑪喀比二書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="149"/>
        <source>3 Maccabees</source>
        <translation>瑪喀比三書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="150"/>
        <source>4 Maccabees</source>
        <translation>瑪喀比四書</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="151"/>
        <source>Rest of Daniel</source>
        <translation>但以理補篇</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="152"/>
        <source>Rest of Esther</source>
        <translation>以斯帖補篇</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="153"/>
        <source>Prayer of Manasses</source>
        <translation>瑪拿西禱言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="154"/>
        <source>Letter of Jeremiah</source>
        <translation>耶利米書信</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="155"/>
        <source>Prayer of Azariah</source>
        <translation>亞薩利亞禱言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="156"/>
        <source>Susanna</source>
        <translation>蘇撒納</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="157"/>
        <source>Bel</source>
        <translation>貝爾</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="158"/>
        <source>1 Esdras</source>
        <translation>厄斯德拉上</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="159"/>
        <source>2 Esdras</source>
        <translation>厄斯德拉下</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>:</source>
        <comment>Verse identifier e.g. Genesis 1 : 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>v</source>
        <comment>Verse identifier e.g. Genesis 1 v 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>節</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>V</source>
        <comment>Verse identifier e.g. Genesis 1 V 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>節</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verse</source>
        <comment>Verse identifier e.g. Genesis 1 verse 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>節</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verses</source>
        <comment>Verse identifier e.g. Genesis 1 verses 1 - 2 = Genesis Chapter 1 Verses 1 to 2</comment>
        <translation>節</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>-</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>to</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>到</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>,</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2, 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>,</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>and</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2 and 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>及</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="185"/>
        <source>end</source>
        <comment>ending identifier e.g. Genesis 1 verse 1 - end = Genesis Chapter 1 Verses 1 To The Last Verse</comment>
        <translation>結束</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No Book Found</source>
        <translation>找不到書卷名</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No matching book could be found in this Bible. Check that you have spelled the name of the book correctly.</source>
        <translation>找不到符合書卷名。請檢查有無錯別字。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="64"/>
        <source>The proxy server {proxy} was found in the bible {name}.&lt;br&gt;Would you like to set it as the proxy for OpenLP?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="69"/>
        <source>both</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleEditForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="162"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>您需要指定您聖經的譯本名稱。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="168"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>您需要為您的聖經設定版權訊息。 在公開領域也必須標註。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>Bible Exists</source>
        <translation>聖經已存在</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>此聖經譯本已存在。請匯入一個不同的聖經譯本，或先刪除已存在的譯本。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="190"/>
        <source>You need to specify a book name for &quot;{text}&quot;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="197"/>
        <source>The book name &quot;{name}&quot; is not correct.
Numbers can only be used at the beginning and must
be followed by one or more non-numeric characters.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>Duplicate Book Name</source>
        <translation>重複的書卷名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>The Book Name &quot;{name}&quot; has been entered more than once.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="53"/>
        <source>The file &quot;{file}&quot; you supplied is compressed. You must decompress it before import.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="224"/>
        <source>unknown type of</source>
        <comment>This looks like an unknown type of XML bible.</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleManager</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Web Bible cannot be used in Text Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Text Search is not available with Web Bibles.
Please use the Scripture Reference Search instead.

This means that the currently selected Bible is a Web Bible.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>Scripture Reference Error</source>
        <translation>經節參照錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>&lt;strong&gt;The reference you typed is invalid!&lt;br&gt;&lt;br&gt;Please make sure that your reference follows one of these patterns:&lt;/strong&gt;&lt;br&gt;&lt;br&gt;%s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BiblesTab</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="178"/>
        <source>Verse Display</source>
        <translation>經節顯示</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="179"/>
        <source>Show verse numbers</source>
        <translation>顯示經節數字</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="180"/>
        <source>Only show new chapter numbers</source>
        <translation>只顯示新的第幾章</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="183"/>
        <source>Bible theme:</source>
        <translation>聖經佈景主題：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="187"/>
        <source>No Brackets</source>
        <translation>沒有括號</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="189"/>
        <source>( And )</source>
        <translation>( 和 )</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="191"/>
        <source>{ And }</source>
        <translation>{ 和 }</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="193"/>
        <source>[ And ]</source>
        <translation>[ 和 ]</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="195"/>
        <source>Note: Changes do not affect verses in the Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="197"/>
        <source>Display second Bible verses</source>
        <translation>顯示第二聖經經文</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="198"/>
        <source>Custom Scripture References</source>
        <translation>自定義經文表示</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="199"/>
        <source>Verse separator:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="200"/>
        <source>Range separator:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="201"/>
        <source>List separator:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="202"/>
        <source>End mark:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="203"/>
        <source>Multiple alternative verse separators may be defined.
They have to be separated by a vertical bar &quot;|&quot;.
Please clear this edit line to use the default value.</source>
        <translation>定義多個符號需使用 &quot;|&quot; 來分隔。
欲使用預設值請清除內容。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="210"/>
        <source>Default Bible Language</source>
        <translation>預設聖經語言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="211"/>
        <source>Book name language in search field,
search results and on display:</source>
        <translation>在搜尋範圍、搜尋結果及畫面輸出的書卷名稱語言：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="213"/>
        <source>Bible Language</source>
        <translation>聖經語言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="215"/>
        <source>Application Language</source>
        <translation>應用程式語言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="217"/>
        <source>English</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="219"/>
        <source>Quick Search Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="220"/>
        <source>Reset search type to &quot;Text or Scripture Reference&quot; on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="223"/>
        <source>Don&apos;t show error if nothing is found in &quot;Text or Scripture Reference&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="226"/>
        <source>Search automatically while typing (Text search must contain a
minimum of {count} characters and a space for performance reasons)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="84"/>
        <source>Select Book Name</source>
        <translation>選擇經卷名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="85"/>
        <source>The following book name cannot be matched up internally. Please select the corresponding name from the list.</source>
        <translation>下列書卷名稱無法與內部書卷對應。請從列表中選擇相對應的名稱。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="88"/>
        <source>Current name:</source>
        <translation>目前名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="89"/>
        <source>Corresponding name:</source>
        <translation>對應名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="90"/>
        <source>Show Books From</source>
        <translation>顯示書卷從</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="91"/>
        <source>Old Testament</source>
        <translation>舊約</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="92"/>
        <source>New Testament</source>
        <translation>新約</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="93"/>
        <source>Apocrypha</source>
        <translation>次經</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknameform.py" line="109"/>
        <source>You need to select a book.</source>
        <translation>你需要選擇一卷書。</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.CSVBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="123"/>
        <source>Importing books... {book}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="145"/>
        <source>Importing verses from {book}...</source>
        <comment>Importing verses from &lt;book name&gt;...</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="132"/>
        <source>Bible Editor</source>
        <translation>編輯聖經</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="136"/>
        <source>License Details</source>
        <translation>授權詳細資訊</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="137"/>
        <source>Version name:</source>
        <translation>版本名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="138"/>
        <source>Copyright:</source>
        <translation>版權：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="139"/>
        <source>Permissions:</source>
        <translation>權限：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="140"/>
        <source>Full license:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="141"/>
        <source>Default Bible Language</source>
        <translation>預設聖經語言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="142"/>
        <source>Book name language in search field, search results and on display:</source>
        <translation>在搜尋範圍、搜尋結果及畫面輸出的書卷名稱語言：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="145"/>
        <source>Global Settings</source>
        <translation>全域設定</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="146"/>
        <source>Bible Language</source>
        <translation>聖經語言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="149"/>
        <source>Application Language</source>
        <translation>應用程式語言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="151"/>
        <source>English</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="93"/>
        <source>This is a Web Download Bible.
It is not possible to customize the Book Names.</source>
        <translation>這是網路版聖經。無法自定書卷名稱。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="98"/>
        <source>To use the customized book names, &quot;Bible language&quot; must be selected on the Meta Data tab or, if &quot;Global settings&quot; is selected, on the Bible page in Configure OpenLP.</source>
        <translation>欲使用自定義書卷名稱，在&apos;後設資料(Meta Data)&apos;頁或OpenLP設定的&apos;聖經&apos;頁面上(若已選擇全域設定) 必須選擇&quot;聖經語言&quot;。</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.HTTPBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="607"/>
        <source>Registering Bible and loading books...</source>
        <translation>正在註冊聖經並載入書卷...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="623"/>
        <source>Registering Language...</source>
        <translation>正在註冊語言...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="630"/>
        <source>Importing {book}...</source>
        <comment>Importing &lt;book name&gt;...</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>Download Error</source>
        <translation>下載錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>There was a problem downloading your verse selection. Please check your Internet connection, and if this error continues to occur please consider reporting a bug.</source>
        <translation>下載選擇的經節時發生錯誤，請檢查您的網路連線。若狀況持續發生，請回報此錯誤。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>Parse Error</source>
        <translation>語法錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>There was a problem extracting your verse selection. If this error continues to occur please consider reporting a bug.</source>
        <translation>解碼選擇的經節時發生錯誤。若狀況持續發生，請回報此錯誤。</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="192"/>
        <source>CSV File</source>
        <translation>CSV檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="371"/>
        <source>Bible Import Wizard</source>
        <translation>聖經匯入精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="374"/>
        <source>This wizard will help you to import Bibles from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>這個精靈將幫助您匯入各種格式的聖經。點擊下一步按鈕開始選擇要匯入的格式。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="386"/>
        <source>Web Download</source>
        <translation>網路下載</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="396"/>
        <source>Bible file:</source>
        <translation>聖經檔案：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="391"/>
        <source>Books file:</source>
        <translation>書卷檔：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="392"/>
        <source>Verses file:</source>
        <translation>經文檔案：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="394"/>
        <source>Location:</source>
        <translation>位置：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="397"/>
        <source>Click to download bible list</source>
        <translation>點擊下載聖經清單</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="398"/>
        <source>Download bible list</source>
        <translation>下載聖經清單</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="399"/>
        <source>Crosswalk</source>
        <translation>Crosswalk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="401"/>
        <source>BibleGateway</source>
        <translation>BibleGateway</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="403"/>
        <source>Bibleserver</source>
        <translation>Bibleserver</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="405"/>
        <source>Bible:</source>
        <translation>聖經：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="409"/>
        <source>Bibles:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="407"/>
        <source>SWORD data folder:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="408"/>
        <source>SWORD zip-file:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="410"/>
        <source>Import from folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="412"/>
        <source>Import from Zip-file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="417"/>
        <source>To import SWORD bibles the pysword python module must be installed. Please read the manual for instructions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="420"/>
        <source>License Details</source>
        <translation>授權詳細資訊</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="422"/>
        <source>Set up the Bible&apos;s license details.</source>
        <translation>設定聖經的授權詳細資料。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="424"/>
        <source>Version name:</source>
        <translation>版本名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="425"/>
        <source>Copyright:</source>
        <translation>版權：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="426"/>
        <source>Permissions:</source>
        <translation>權限：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="427"/>
        <source>Full license:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="429"/>
        <source>Please wait while your Bible is imported.</source>
        <translation>請稍等，您的聖經正在匯入。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="461"/>
        <source>You need to specify a file with books of the Bible to use in the import.</source>
        <translation>您需要指定一個具有可用於匯入之聖經經卷的檔案。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="468"/>
        <source>You need to specify a file of Bible verses to import.</source>
        <translation>您需要指定要匯入的聖經經文檔案。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="526"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>您需要指定您聖經的譯本名稱。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="532"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>您需要為您的聖經設定版權訊息。 在公開領域也必須標註。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>Bible Exists</source>
        <translation>聖經已存在</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>此聖經譯本已存在。請匯入一個不同的聖經譯本，或先刪除已存在的譯本。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>Error during download</source>
        <translation>下載過程發生錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>An error occurred while downloading the list of bibles from %s.</source>
        <translation>從 %s 下載聖經清單時發生錯誤。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="691"/>
        <source>Registering Bible...</source>
        <translation>正在註冊聖經...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="757"/>
        <source>Registered Bible. Please note, that verses will be downloaded on demand and thus an internet connection is required.</source>
        <translation>註冊聖經。請注意，即將下載經文且需要網路連線。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="767"/>
        <source>Your Bible import failed.</source>
        <translation>無法匯入你的聖經。</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="66"/>
        <source>Select Language</source>
        <translation>選擇語言</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="68"/>
        <source>OpenLP is unable to determine the language of this translation of the Bible. Please select the language from the list below.</source>
        <translation>OpenLP無法確定這個聖經譯本的語言。請從下面的列表中選擇語言。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="72"/>
        <source>Language:</source>
        <translation>語言：</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languageform.py" line="62"/>
        <source>You need to choose a language.</source>
        <translation>你需要選擇一個語言。</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="161"/>
        <source>Find</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="164"/>
        <source>Find:</source>
        <translation>尋找：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="170"/>
        <source>Select</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="178"/>
        <source>Sort bible books alphabetically.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="180"/>
        <source>Book:</source>
        <translation>書卷：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="192"/>
        <source>From:</source>
        <translation>從：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="198"/>
        <source>To:</source>
        <translation>到：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="204"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="209"/>
        <source>Second:</source>
        <translation>第二本：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="273"/>
        <source>Chapter:</source>
        <translation>章：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="274"/>
        <source>Verse:</source>
        <translation>節：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="278"/>
        <source>Clear the results on the current tab.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="279"/>
        <source>Add the search results to the saved list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Scripture Reference</source>
        <translation>經文參照</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Scripture Reference...</source>
        <translation>搜尋經文參照...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text Search</source>
        <translation>文字搜尋</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Text...</source>
        <translation>搜尋文字...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="458"/>
        <source>Are you sure you want to completely delete &quot;{bible}&quot; Bible from OpenLP?

You will need to re-import this Bible to use it again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="506"/>
        <source>Saved ({result_count})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="509"/>
        <source>Results ({result_count})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="588"/>
        <source>OpenLP cannot combine single and dual Bible verse search results. Do you want to clear your saved results?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="618"/>
        <source>Bible not fully loaded.</source>
        <translation>聖經未完整載入。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>Verses not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>The second Bible &quot;{second_name}&quot; does not contain all the verses that are in the main Bible &quot;{name}&quot;.
Only verses found in both Bibles will be shown.

{count:d} verses have not been included in the results.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.OsisImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="171"/>
        <source>Removing unused tags (this may take a few minutes)...</source>
        <translation>刪除未使用的標籤（這可能需要幾分鐘）...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="200"/>
        <source>Importing {book} {chapter}...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.Sword</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="88"/>
        <source>Importing {name}...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.SwordImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="93"/>
        <source>An unexpected error happened while importing the SWORD bible, please report this to the OpenLP developers.
{error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.ZefaniaImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="90"/>
        <source>Incorrect Bible file type supplied. Zefania Bibles may be compressed. You must decompress them before import.</source>
        <translation>提供了不正確的聖經文件類型, OpenSong 聖經或許被壓縮過. 您必須在導入前解壓縮它們。</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.Zefnia</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="83"/>
        <source>Importing {book} {chapter}...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CustomPlugin</name>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="77"/>
        <source>&lt;strong&gt;Custom Slide Plugin &lt;/strong&gt;&lt;br /&gt;The custom slide plugin provides the ability to set up custom text slides that can be displayed on the screen the same way songs are. This plugin provides greater freedom over the songs plugin.</source>
        <translation>&lt;strong&gt;自定義幻燈片插件&lt;/strong&gt;&lt;br /&gt;自定義幻燈片插件提供設置可像歌曲一樣放置在螢幕上的自定義文字幻燈片的能力。這個插件在歌曲插件提供了更大的自由度。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="109"/>
        <source>Custom Slide</source>
        <comment>name singular</comment>
        <translation>自訂幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="110"/>
        <source>Custom Slides</source>
        <comment>name plural</comment>
        <translation>自訂幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="114"/>
        <source>Custom Slides</source>
        <comment>container title</comment>
        <translation>自訂幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="118"/>
        <source>Load a new custom slide.</source>
        <translation>載入新幻燈片.。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="119"/>
        <source>Import a custom slide.</source>
        <translation>匯入一張幻燈片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="120"/>
        <source>Add a new custom slide.</source>
        <translation>新增幻燈片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="121"/>
        <source>Edit the selected custom slide.</source>
        <translation>編輯幻燈片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="122"/>
        <source>Delete the selected custom slide.</source>
        <translation>刪除幻燈片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="123"/>
        <source>Preview the selected custom slide.</source>
        <translation>預覽幻燈片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="124"/>
        <source>Send the selected custom slide live.</source>
        <translation>傳送幻燈片至現場Live。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="125"/>
        <source>Add the selected custom slide to the service.</source>
        <translation>新增幻燈片至聚會。</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.CustomTab</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="56"/>
        <source>Custom Display</source>
        <translation>自定義顯示</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="57"/>
        <source>Display footer</source>
        <translation>顯示頁尾</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="58"/>
        <source>Import missing custom slides from service files</source>
        <translation>從聚會管理匯入遺失的自訂幻燈片檔案</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditCustomForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="104"/>
        <source>Edit Custom Slides</source>
        <translation>編輯幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="105"/>
        <source>&amp;Title:</source>
        <translation>標題(&amp;T)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="107"/>
        <source>Add a new slide at bottom.</source>
        <translation>在底部新增一張幻燈片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="109"/>
        <source>Edit the selected slide.</source>
        <translation>編輯幻燈片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="110"/>
        <source>Ed&amp;it All</source>
        <translation>編輯全部(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="111"/>
        <source>Edit all the slides at once.</source>
        <translation>同時編輯全部的幻燈片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="112"/>
        <source>The&amp;me:</source>
        <translation>佈景主題(&amp;M)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="113"/>
        <source>&amp;Credits:</source>
        <translation>參與名單(&amp;C):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="239"/>
        <source>You need to type in a title.</source>
        <translation>你需要輸入標題。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="243"/>
        <source>You need to add at least one slide.</source>
        <translation>你需要新增至少一頁幻燈片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="50"/>
        <source>Insert Slide</source>
        <translation>插入幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="51"/>
        <source>Split a slide into two by inserting a slide splitter.</source>
        <translation>插入一個幻燈片分格符號來將一張幻燈片分為兩張。</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="47"/>
        <source>Edit Slide</source>
        <translation>編輯幻燈片</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="200"/>
        <source>Are you sure you want to delete the &quot;{items:d}&quot; selected custom slide(s)?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="261"/>
        <source>copy</source>
        <comment>For item cloning</comment>
        <translation>複製</translation>
    </message>
</context>
<context>
    <name>ImagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="70"/>
        <source>&lt;strong&gt;Image Plugin&lt;/strong&gt;&lt;br /&gt;The image plugin provides displaying of images.&lt;br /&gt;One of the distinguishing features of this plugin is the ability to group a number of images together in the service manager, making the displaying of multiple images easier. This plugin can also make use of OpenLP&apos;s &quot;timed looping&quot; feature to create a slide show that runs automatically. In addition to this, images from the plugin can be used to override the current theme&apos;s background, which renders text-based items like songs with the selected image as a background instead of the background provided by the theme.</source>
        <translation>&lt;strong&gt;圖片插件&lt;/strong&gt;&lt;br /&gt;圖片插件提供圖片顯示。&lt;br /&gt;
這個插件其中一個顯著的特徵是它可以在聚會管理中將多張照片分組，更容易顯示多張照片。這個插件也可以使用OpenLP的計時循環效果來建立可自動撥放的幻燈片效果。除此之外，該插件中的圖片可以用來覆蓋當前主題的背景，使得文字項目如歌曲使用選擇的圖片作為背景，而不是由佈景主題所提供的背景。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="89"/>
        <source>Image</source>
        <comment>name singular</comment>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="90"/>
        <source>Images</source>
        <comment>name plural</comment>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="93"/>
        <source>Images</source>
        <comment>container title</comment>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="96"/>
        <source>Add new image(s).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="98"/>
        <source>Add a new image.</source>
        <translation>加入新圖片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="99"/>
        <source>Edit the selected image.</source>
        <translation>編輯圖片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="100"/>
        <source>Delete the selected image.</source>
        <translation>刪除圖片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="101"/>
        <source>Preview the selected image.</source>
        <translation>預覽圖片。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="102"/>
        <source>Send the selected image live.</source>
        <translation>傳送圖片至現場Live。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="103"/>
        <source>Add the selected image to the service.</source>
        <translation>加入選擇的圖片到聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="191"/>
        <source>Add new image(s)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ImagePlugin.AddGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="54"/>
        <source>Add group</source>
        <translation>新增群組</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="55"/>
        <source>Parent group:</source>
        <translation>主群組：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="56"/>
        <source>Group name:</source>
        <translation>群組名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="67"/>
        <source>You need to type in a group name.</source>
        <translation>您需要輸入一個群組名稱。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="655"/>
        <source>Could not add the new group.</source>
        <translation>無法新增新的群組。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="658"/>
        <source>This group already exists.</source>
        <translation>此群組已存在。</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ChooseGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="82"/>
        <source>Select Image Group</source>
        <translation>選擇影像群組</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="83"/>
        <source>Add images to group:</source>
        <translation>新增圖片到群組：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="84"/>
        <source>No group</source>
        <translation>無群組</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="85"/>
        <source>Existing group</source>
        <translation>現有群組</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="86"/>
        <source>New group</source>
        <translation>新群組</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="170"/>
        <source>Select Attachment</source>
        <translation>選擇附件</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="54"/>
        <source>-- Top-level group --</source>
        <translation>-- 最上層群組 --</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="81"/>
        <source>Select Image(s)</source>
        <translation>選擇圖片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="243"/>
        <source>You must select an image or group to delete.</source>
        <translation>您必須選擇一個圖案或群組刪除。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Remove group</source>
        <translation>移除群組</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Are you sure you want to remove &quot;{name}&quot; and everything in it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>Missing Image(s)</source>
        <translation>遺失圖片</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="596"/>
        <source>The following image(s) no longer exist: {names}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>The following image(s) no longer exist: {names}
Do you want to add the other images anyway?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="679"/>
        <source>You must select an image to replace the background with.</source>
        <translation>您必須選擇一個圖案來替換背景。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="693"/>
        <source>There was no display item to amend.</source>
        <translation>沒有顯示的項目可以修改。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="697"/>
        <source>There was a problem replacing your background, the image file &quot;{name}&quot; no longer exists.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ImagesPlugin.ImageTab</name>
    <message>
        <location filename="../../openlp/plugins/images/lib/imagetab.py" line="63"/>
        <source>Visible background for images with aspect ratio different to screen.</source>
        <translation>可見的背景圖片與螢幕長寬比例不同。</translation>
    </message>
</context>
<context>
    <name>MediaPlugin</name>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="81"/>
        <source>&lt;strong&gt;Media Plugin&lt;/strong&gt;&lt;br /&gt;The media plugin provides playback of audio and video.</source>
        <translation>&lt;strong&gt;媒體插件&lt;/strong&gt;&lt;br /&gt;媒體插件提供聲音與影像的播放。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="91"/>
        <source>Media</source>
        <comment>name singular</comment>
        <translation>媒體</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="92"/>
        <source>Media</source>
        <comment>name plural</comment>
        <translation>媒體</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="96"/>
        <source>Media</source>
        <comment>container title</comment>
        <translation>媒體</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="100"/>
        <source>Load new media.</source>
        <translation>載入新媒體。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="102"/>
        <source>Add new media.</source>
        <translation>加入新媒體。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="103"/>
        <source>Edit the selected media.</source>
        <translation>編輯媒體。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="104"/>
        <source>Delete the selected media.</source>
        <translation>刪除媒體。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="105"/>
        <source>Preview the selected media.</source>
        <translation>預覽媒體。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="106"/>
        <source>Send the selected media live.</source>
        <translation>傳送媒體至現場Live。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="107"/>
        <source>Add the selected media to the service.</source>
        <translation>加入到聚會。</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelector</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="184"/>
        <source>Select Media Clip</source>
        <translation>選擇媒體片段</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="185"/>
        <source>Source</source>
        <translation>來源</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="186"/>
        <source>Media path:</source>
        <translation>媒體路徑：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="187"/>
        <source>Select drive from list</source>
        <translation>從清單選擇驅動</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="189"/>
        <source>Load disc</source>
        <translation>載入光碟</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="190"/>
        <source>Track Details</source>
        <translation>軌道細節</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="191"/>
        <source>Title:</source>
        <translation>標題：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="192"/>
        <source>Audio track:</source>
        <translation>聲音軌：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="193"/>
        <source>Subtitle track:</source>
        <translation>副標題：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="201"/>
        <source>HH:mm:ss.z</source>
        <translation>HH:mm:ss.z</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="195"/>
        <source>Clip Range</source>
        <translation>片段範圍</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="196"/>
        <source>Start point:</source>
        <translation>起始點：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="198"/>
        <source>Set start point</source>
        <translation>設定起始點</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="199"/>
        <source>Jump to start point</source>
        <translation>跳至起始點</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="200"/>
        <source>End point:</source>
        <translation>結束點：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="202"/>
        <source>Set end point</source>
        <translation>設定結束點</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="203"/>
        <source>Jump to end point</source>
        <translation>跳至結束點</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelectorForm</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="195"/>
        <source>No path was given</source>
        <translation>沒有選取路徑</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="201"/>
        <source>Given path does not exists</source>
        <translation>選取的路徑不存在</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="218"/>
        <source>An error happened during initialization of VLC player</source>
        <translation>初始化 VLC 播放器發生錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="239"/>
        <source>VLC player failed playing the media</source>
        <translation>此媒體 VLC播放器無法播放</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>CD not loaded correctly</source>
        <translation>CD無法正確載入</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>The CD was not loaded correctly, please re-load and try again.</source>
        <translation>CD無法正確載入，請重新放入再重試。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>DVD not loaded correctly</source>
        <translation>DVD無法正確載入</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>The DVD was not loaded correctly, please re-load and try again.</source>
        <translation>DVD無法正確載入，請重新放入再重試。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Set name of mediaclip</source>
        <translation>設定媒體片段的名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Name of mediaclip:</source>
        <translation>片段名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="602"/>
        <source>Enter a valid name or cancel</source>
        <translation>輸入有效的名稱或取消</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>Invalid character</source>
        <translation>無效字元</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>The name of the mediaclip must not contain the character &quot;:&quot;</source>
        <translation>片段的名稱不得包含 &quot;:&quot; 字元</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="290"/>
        <source>Unsupported File</source>
        <translation>檔案不支援</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="93"/>
        <source>Select Media</source>
        <translation>選擇媒體</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="123"/>
        <source>Load CD/DVD</source>
        <translation>載入 CD/DVD</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>Missing Media File</source>
        <translation>遺失媒體檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="155"/>
        <source>The optical disc {name} is no longer available.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>The file {name} no longer exists.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="206"/>
        <source>Videos ({video});;Audio ({audio});;{files} (*)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="216"/>
        <source>You must select a media file to delete.</source>
        <translation>您必須選擇一個要刪除的媒體檔。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="233"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="237"/>
        <source>Show Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>Mediaclip already saved</source>
        <translation>媒體片段已存檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>This mediaclip has already been saved</source>
        <translation>該媒體片段已儲存</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="74"/>
        <source>Video:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="76"/>
        <source>Audio:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="95"/>
        <source>Live Media</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="96"/>
        <source>Stream Media Command</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="97"/>
        <source>VLC arguments</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="98"/>
        <source>Start Live items automatically</source>
        <translation>自動啟動Live項目</translation>
    </message>
</context>
<context>
    <name>OpenLP</name>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>Data Directory Error</source>
        <translation>資料目錄錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>OpenLP data folder was not found in:

{path}

The location of the data folder was previously changed from the OpenLP's default location. If the data was stored on removable device, that device needs to be made available.

You may reset the data location back to the default location, or you can try to make the current location available.

Do you want to reset to the default data location? If not, OpenLP will be closed so you can try to fix the the problem.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="238"/>
        <source>Backup</source>
        <translation>備份</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="221"/>
        <source>OpenLP has been upgraded, do you want to create
a backup of the old data folder?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="232"/>
        <source>Backup of the data folder failed!</source>
        <translation>資料夾備份失敗！</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="235"/>
        <source>A backup of the data folder has been created at:

{text}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings Upgrade</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="426"/>
        <source>Your settings are about to be upgraded. A backup will be created at {back_up_path}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings back up failed.

Continuining to upgrade.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/__init__.py" line="415"/>
        <source>Image Files</source>
        <translation>圖像檔</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Open</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="79"/>
        <source>Video Files</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.AboutForm</name>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="103"/>
        <source>&lt;p&gt;OpenLP {{version}}{{revision}} - Open Source Lyrics Projection&lt;br&gt;Copyright {crs} 2004-{yr} OpenLP Developers&lt;/p&gt;&lt;p&gt;Find out more about OpenLP: &lt;a href=&quot;https://openlp.org/&quot;&gt;https://openlp.org/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with this program.  If not, see &lt;a href=&quot;https://www.gnu.org/licenses/&quot;&gt;https://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="118"/>
        <source>OpenLP is written and maintained by volunteers all over the world in their spare time. If you would like to see this project succeed, please consider contributing to it by clicking the &quot;contribute&quot; button below.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="122"/>
        <source>OpenLP would not be possible without the following software libraries:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="155"/>
        <source>&lt;h3&gt;Final credit:&lt;/h3&gt;&lt;blockquote&gt;&lt;p&gt;For God so loved the world that He gave His one and only Son, so that whoever believes in Him will not perish but inherit eternal life.&lt;/p&gt;&lt;p&gt;John 3:16&lt;/p&gt;&lt;/blockquote&gt;&lt;p&gt;And last but not least, final credit goes to God our Father, for sending His Son to die on the cross, setting us free from sin. We bring this software to you for free because He has set us free.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="164"/>
        <source>Credits</source>
        <translation>參與者</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="744"/>
        <source>License</source>
        <translation>授權</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="746"/>
        <source>Contribute</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutform.py" line="57"/>
        <source> build {version}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.AdvancedTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="59"/>
        <source>Advanced</source>
        <translation>進階</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="252"/>
        <source>UI Settings</source>
        <translation>介面設定</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="253"/>
        <source>Data Location</source>
        <translation>資料位置</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="254"/>
        <source>Number of recent service files to display:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="255"/>
        <source>Open the last used Library tab on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="257"/>
        <source>Double-click to send items straight to Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="259"/>
        <source>Preview items when clicked in Library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="261"/>
        <source>Preview items when clicked in Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="263"/>
        <source>Expand new service items on creation</source>
        <translation>新增項目到聚會時展開</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="265"/>
        <source>Max height for non-text slides
in slide controller:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="267"/>
        <source>Disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="268"/>
        <source>Automatic</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="269"/>
        <source>When changing slides:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="271"/>
        <source>Do not auto-scroll</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="272"/>
        <source>Auto-scroll the previous slide into view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="274"/>
        <source>Auto-scroll the previous slide to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="276"/>
        <source>Auto-scroll the previous slide to middle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="278"/>
        <source>Auto-scroll the current slide into view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="280"/>
        <source>Auto-scroll the current slide to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="282"/>
        <source>Auto-scroll the current slide to middle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="284"/>
        <source>Auto-scroll the current slide to bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="286"/>
        <source>Auto-scroll the next slide into view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="288"/>
        <source>Auto-scroll the next slide to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="290"/>
        <source>Auto-scroll the next slide to middle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="292"/>
        <source>Auto-scroll the next slide to bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="294"/>
        <source>Enable application exit confirmation</source>
        <translation>當應用程式退出時確認</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="299"/>
        <source>Use dark style (needs restart)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="300"/>
        <source>Default Service Name</source>
        <translation>預設聚會名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="301"/>
        <source>Enable default service name</source>
        <translation>使用預設聚會名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="302"/>
        <source>Date and Time:</source>
        <translation>時間及日期：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="303"/>
        <source>Monday</source>
        <translation>星期一</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="304"/>
        <source>Tuesday</source>
        <translation>星期二</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="305"/>
        <source>Wednesday</source>
        <translation>星期三</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="306"/>
        <source>Thursday</source>
        <translation>星期四</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="307"/>
        <source>Friday</source>
        <translation>星期五</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="308"/>
        <source>Saturday</source>
        <translation>星期六</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="309"/>
        <source>Sunday</source>
        <translation>星期日</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="310"/>
        <source>Now</source>
        <translation>現在</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="311"/>
        <source>Time when usual service starts.</source>
        <translation>平時聚會開始時間。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="312"/>
        <source>Name:</source>
        <translation>名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="313"/>
        <source>Consult the OpenLP manual for usage.</source>
        <translation>參閱OpenLP使用手冊。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="314"/>
        <source>Revert to the default service name &quot;{name}&quot;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="317"/>
        <source>Example:</source>
        <translation>範例：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="318"/>
        <source>Hide mouse cursor when over display window</source>
        <translation>移動到顯示畫面上時隱藏滑鼠遊標</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="319"/>
        <source>Path:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="320"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="321"/>
        <source>Cancel OpenLP data directory location change.</source>
        <translation>取消 OpenLP 資料目錄位置變更。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="323"/>
        <source>Copy data to new location.</source>
        <translation>複製資料到新位置。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="324"/>
        <source>Copy the OpenLP data files to the new location.</source>
        <translation>複製 OpenLP 資料檔案到新位置。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="326"/>
        <source>&lt;strong&gt;WARNING:&lt;/strong&gt; New data directory location contains OpenLP data files.  These files WILL be replaced during a copy.</source>
        <translation>&lt;strong&gt;警告:&lt;/strong&gt; 新資料目錄位置包含OpenLP 資料檔案.  這些檔案將會在複製時被替換。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="329"/>
        <source>Display Workarounds</source>
        <translation>顯示異常解決方法</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="330"/>
        <source>Ignore Aspect Ratio</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="331"/>
        <source>Bypass X11 Window Manager</source>
        <translation>繞過 X11視窗管理器</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="332"/>
        <source>Use alternating row colours in lists</source>
        <translation>在列表使用交替式色彩</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="490"/>
        <source>Syntax error.</source>
        <translation>語法錯誤。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Confirm Data Directory Change</source>
        <translation>確認更改資料目錄</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Are you sure you want to change the location of the OpenLP data directory to:

{path}

The data directory will be changed when OpenLP is closed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="559"/>
        <source>Overwrite Existing Data</source>
        <translation>複寫已存在的資料</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>Restart Required</source>
        <translation>需要重新啟動</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>This change will only take effect once OpenLP has been restarted.</source>
        <translation>此變更只會在 OpenLP 重新啟動後生效。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="186"/>
        <source>Select Logo File</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ColorButton</name>
    <message>
        <location filename="../../openlp/core/widgets/buttons.py" line="44"/>
        <source>Click to select a color.</source>
        <translation>點擊以選擇顏色。</translation>
    </message>
</context>
<context>
    <name>OpenLP.DB</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="542"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="543"/>
        <source>Video</source>
        <translation>影像</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="544"/>
        <source>Digital</source>
        <translation>數位</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="545"/>
        <source>Storage</source>
        <translation>儲存</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="546"/>
        <source>Network</source>
        <translation>網路</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="547"/>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="551"/>
        <source>1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="552"/>
        <source>2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="553"/>
        <source>3</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="554"/>
        <source>4</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="555"/>
        <source>5</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="556"/>
        <source>6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="557"/>
        <source>7</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="558"/>
        <source>8</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="559"/>
        <source>9</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="560"/>
        <source>A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="561"/>
        <source>B</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="562"/>
        <source>C</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="563"/>
        <source>D</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="564"/>
        <source>E</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="565"/>
        <source>F</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="566"/>
        <source>G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="567"/>
        <source>H</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="568"/>
        <source>I</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="569"/>
        <source>J</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="570"/>
        <source>K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="571"/>
        <source>L</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="572"/>
        <source>M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="573"/>
        <source>N</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="574"/>
        <source>O</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="575"/>
        <source>P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="576"/>
        <source>Q</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="577"/>
        <source>R</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="578"/>
        <source>S</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="579"/>
        <source>T</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="580"/>
        <source>U</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="581"/>
        <source>V</source>
        <translation>節</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="582"/>
        <source>W</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="583"/>
        <source>X</source>
        <translation>X座標</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="584"/>
        <source>Y</source>
        <translation>Y座標</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="585"/>
        <source>Z</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.DisplayWindow</name>
    <message>
        <location filename="../../openlp/core/display/window.py" line="121"/>
        <source>Display Window</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="94"/>
        <source>Error Occurred</source>
        <translation>發生錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="96"/>
        <source>&lt;strong&gt;Please describe what you were trying to do.&lt;/strong&gt; &amp;nbsp;If possible, write in English.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="99"/>
        <source>&lt;strong&gt;Oops, OpenLP hit a problem and couldn&apos;t recover!&lt;br&gt;&lt;br&gt;You can help &lt;/strong&gt; the OpenLP developers to &lt;strong&gt;fix this&lt;/strong&gt; by&lt;br&gt; sending them a &lt;strong&gt;bug report to {email}&lt;/strong&gt;{newlines}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="105"/>
        <source>{first_part}&lt;strong&gt;No email app? &lt;/strong&gt; You can &lt;strong&gt;save&lt;/strong&gt; this information to a &lt;strong&gt;file&lt;/strong&gt; and&lt;br&gt;send it from your &lt;strong&gt;mail on browser&lt;/strong&gt; via an &lt;strong&gt;attachment.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;&lt;strong&gt;Thank you&lt;/strong&gt; for being part of making OpenLP better!&lt;br&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="112"/>
        <source>Send E-Mail</source>
        <translation>寄送E-Mail</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="113"/>
        <source>Save to File</source>
        <translation>儲存為檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="114"/>
        <source>Attach File</source>
        <translation>附加檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>Failed to Save Report</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>The following error occured when saving the report.

{exception}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="153"/>
        <source>&lt;strong&gt;Thank you for your description!&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="157"/>
        <source>&lt;strong&gt;Tell us what you were doing when this happened.&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="161"/>
        <source>&lt;strong&gt;Please enter a more detailed description of the situation&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionForm</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="77"/>
        <source>Platform: {platform}
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Save Crash Report</source>
        <translation>儲存崩潰報告</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Text files (*.txt *.log *.text)</source>
        <translation>文字檔 (*.txt *.log *.text)</translation>
    </message>
</context>
<context>
    <name>OpenLP.FileRenameForm</name>
    <message>
        <location filename="../../openlp/core/ui/filerenamedialog.py" line="60"/>
        <source>New File Name:</source>
        <translation>新檔案名稱:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="55"/>
        <source>File Copy</source>
        <translation>複製檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="57"/>
        <source>File Rename</source>
        <translation>重新命名檔案</translation>
    </message>
</context>
<context>
    <name>OpenLP.FirstTimeLanguageForm</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="68"/>
        <source>Select Translation</source>
        <translation>選擇翻譯</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="69"/>
        <source>Choose the translation you&apos;d like to use in OpenLP.</source>
        <translation>選擇您希望使用的OpenLP翻譯版本。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="71"/>
        <source>Translation:</source>
        <translation>翻譯：</translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>OpenLP.FirstTimeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="520"/>
        <source>Network Error</source>
        <translation>網路錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="177"/>
        <source>There was a network error attempting to connect to retrieve initial configuration information</source>
        <translation>嘗試連接獲取初始配置資訊發生一個網路錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="184"/>
        <source>Downloading {name}...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>Invalid index file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>OpenLP was unable to read the resource index file. Please try again later.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>Download Error</source>
        <translation>下載錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="417"/>
        <source>There was a connection problem during download, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation>下載過程中發生連線問題，所以接下來的下載將被跳過。請稍後再重新執行首次配置精靈。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="429"/>
        <source>Setting Up And Downloading</source>
        <translation>設定並下載中</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="430"/>
        <source>Please wait while OpenLP is set up and your data is downloaded.</source>
        <translation>請稍等，OpenLP在設定並下載您的資料。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="434"/>
        <source>Setting Up</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="448"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="451"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="455"/>
        <source>Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="457"/>
        <source>Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>There was a connection problem while downloading, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation>下載過程中發生連線問題，所以接下來的下載將被跳過。請稍後再重新執行首次配置精靈。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="521"/>
        <source>Unable to download some files</source>
        <translation>無法下載一些檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="262"/>
        <source>First Time Wizard</source>
        <translation>首次配置精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="263"/>
        <source>Welcome to the First Time Wizard</source>
        <translation>歡迎使用首次配置精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="265"/>
        <source>This wizard will help you to configure OpenLP for initial use. Click the &apos;{next_button}&apos; button below to start.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="269"/>
        <source>Internet Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="271"/>
        <source>Downloading Resource Index</source>
        <translation>下載資源索引</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="272"/>
        <source>Please wait while the resource index is downloaded.</source>
        <translation>請稍候，資源索引正在下載。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="274"/>
        <source>Please wait while OpenLP downloads the resource index file...</source>
        <translation>請稍候，OpenLP下載資源索引文件...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="276"/>
        <source>Select parts of the program you wish to use</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="277"/>
        <source>You can also change these settings after the Wizard.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="279"/>
        <source>Displays</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="280"/>
        <source>Choose the main display screen for OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="282"/>
        <source>Songs</source>
        <translation>歌曲</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="283"/>
        <source>Custom Slides – Easier to manage than songs and they have their own list of slides</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="286"/>
        <source>Bibles – Import and show Bibles</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="287"/>
        <source>Images – Show images or replace background with them</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="289"/>
        <source>Presentations – Show .ppt, .odp and .pdf files</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="291"/>
        <source>Media – Playback of Audio and Video files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="292"/>
        <source>Song Usage Monitor</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="293"/>
        <source>Alerts – Display informative messages while showing other slides</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="295"/>
        <source>Resource Data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="296"/>
        <source>Can OpenLP download some resource data?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="297"/>
        <source>OpenLP has collected some resources that we have permission to distribute.

If you would like to download some of these resources click the &apos;{next_button}&apos; button, otherwise click the &apos;{finish_button}&apos; button.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="303"/>
        <source>No Internet Connection</source>
        <translation>沒有連線到網際網路</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="304"/>
        <source>Cannot connect to the internet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="305"/>
        <source>OpenLP could not connect to the internet to get information about the sample data available.

Please check your internet connection. If your church uses a proxy server click the 'Internet Settings' button below and enter the server details there.

Click the '{back_button}' button to try again.

If you click the &apos;{finish_button}&apos; button you can download the data at a later time by selecting &apos;Re-run First Time Wizard&apos; from the &apos;Tools&apos; menu in OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="314"/>
        <source>Sample Songs</source>
        <translation>歌曲範本</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="315"/>
        <source>Select and download public domain songs.</source>
        <translation>選擇並下載公共領域的歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="316"/>
        <source>Sample Bibles</source>
        <translation>聖經範本</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="317"/>
        <source>Select and download free Bibles.</source>
        <translation>選擇並下載免費聖經。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="319"/>
        <source>Sample Themes</source>
        <translation>佈景主題範本</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="320"/>
        <source>Select and download sample themes.</source>
        <translation>選擇並下載佈景主題範例。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="321"/>
        <source>Default theme:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="322"/>
        <source>Select all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="323"/>
        <source>Deselect all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="324"/>
        <source>Downloading and Configuring</source>
        <translation>下載並設定</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="325"/>
        <source>Please wait while resources are downloaded and OpenLP is configured.</source>
        <translation>請稍候，正在下載資源和配置OpenLP。</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagDialog</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="111"/>
        <source>Configure Formatting Tags</source>
        <translation>設定格式化標籤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="114"/>
        <source>Default Formatting</source>
        <translation>預設格式</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="125"/>
        <source>Description</source>
        <translation>說明</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="126"/>
        <source>Tag</source>
        <translation>標籤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="127"/>
        <source>Start HTML</source>
        <translation>開始HTML</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="128"/>
        <source>End HTML</source>
        <translation>結束HTML</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="124"/>
        <source>Custom Formatting</source>
        <translation>字定格式</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagForm</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="79"/>
        <source>Tag {tag} already defined.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="81"/>
        <source>Description {tag} already defined.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="148"/>
        <source>Start tag {tag} is not valid HTML</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="168"/>
        <source>End tag {end} does not match end tag for start tag {start}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="92"/>
        <source>New Tag {row:d}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="96"/>
        <source>&lt;HTML here&gt;</source>
        <translation>&lt;HTML here&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="199"/>
        <source>Validation Error</source>
        <translation>驗證錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="179"/>
        <source>Description is missing</source>
        <translation>描述遺失</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="182"/>
        <source>Tag is missing</source>
        <translation>標籤遺失</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTags</name>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="63"/>
        <source>Red</source>
        <translation>紅色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="69"/>
        <source>Black</source>
        <translation>黑色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="75"/>
        <source>Blue</source>
        <translation>藍色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="81"/>
        <source>Yellow</source>
        <translation>黃色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="87"/>
        <source>Green</source>
        <translation>綠色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="93"/>
        <source>Pink</source>
        <translation>粉紅色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="99"/>
        <source>Orange</source>
        <translation>橙色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="105"/>
        <source>Purple</source>
        <translation>紫色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="111"/>
        <source>White</source>
        <translation>白色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="117"/>
        <source>Superscript</source>
        <translation>上標</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="122"/>
        <source>Subscript</source>
        <translation>下標</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="127"/>
        <source>Paragraph</source>
        <translation>段落</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="132"/>
        <source>Bold</source>
        <translation>粗體</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="137"/>
        <source>Italics</source>
        <translation>斜體</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="141"/>
        <source>Underline</source>
        <translation>底線</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="147"/>
        <source>Break</source>
        <translation>中斷</translation>
    </message>
</context>
<context>
    <name>OpenLP.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="296"/>
        <source>Experimental features (use at your own risk)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="334"/>
        <source>Service Item Slide Limits</source>
        <translation>聚會項目幻燈片限制</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="335"/>
        <source>Behavior of next/previous on the last/first slide:</source>
        <translation>下一個/前一個 在 最後一個/第一個幻燈片的行為：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="336"/>
        <source>&amp;Remain on Slide</source>
        <translation>保持在幻燈片(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="337"/>
        <source>&amp;Wrap around</source>
        <translation>環繞(&amp;W)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="338"/>
        <source>&amp;Move to next/previous service item</source>
        <translation>移到 下一個/前一個 聚會項目(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="160"/>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="161"/>
        <source>Application Startup</source>
        <translation>程式啟動時</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="162"/>
        <source>Show blank screen warning</source>
        <translation>顯示空白畫面警告</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="163"/>
        <source>Automatically open the previous service file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="164"/>
        <source>Show the splash screen</source>
        <translation>顯示啟動畫面</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="165"/>
        <source>Logo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="167"/>
        <source>Logo file:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="168"/>
        <source>Don&apos;t show logo on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="169"/>
        <source>Check for updates to OpenLP</source>
        <translation>檢查 OpenLP 更新</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="170"/>
        <source>Application Settings</source>
        <translation>應用程式設定</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="171"/>
        <source>Prompt to save before starting a new service</source>
        <translation>開始新聚會前提示儲存</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="173"/>
        <source>Unblank display when changing slide in Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="175"/>
        <source>Unblank display when sending items to Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="177"/>
        <source>Automatically preview the next item in service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="179"/>
        <source>Timed slide interval:</source>
        <translation>投影片間隔時間：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="180"/>
        <source> sec</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="181"/>
        <source>CCLI Details</source>
        <translation>CCLI詳細資訊</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="183"/>
        <source>SongSelect username:</source>
        <translation>SongSelect 帳號：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="184"/>
        <source>SongSelect password:</source>
        <translation>SongSelect 密碼：</translation>
    </message>
</context>
<context>
    <name>OpenLP.LanguageManager</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Language</source>
        <translation>語言</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Please restart OpenLP to use your new language setting.</source>
        <translation>使用新語言設定請重新啟動OpenLP。</translation>
    </message>
</context>
<context>
    <name>OpenLP.MainWindow</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="261"/>
        <source>English</source>
        <comment>Please add the name of your language here</comment>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="304"/>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="361"/>
        <source>&amp;File</source>
        <translation>檔案(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="362"/>
        <source>&amp;Import</source>
        <translation>匯入(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="363"/>
        <source>&amp;Export</source>
        <translation>匯出(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="364"/>
        <source>&amp;Recent Services</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="365"/>
        <source>&amp;View</source>
        <translation>檢視(&amp;V)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="366"/>
        <source>&amp;Layout Presets</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="367"/>
        <source>&amp;Tools</source>
        <translation>工具(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="368"/>
        <source>&amp;Settings</source>
        <translation>設定(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="395"/>
        <source>&amp;Language</source>
        <translation>語言(&amp;L)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="370"/>
        <source>&amp;Help</source>
        <translation>幫助(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="371"/>
        <source>Library</source>
        <translation>工具庫</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="372"/>
        <source>Service</source>
        <translation>聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="373"/>
        <source>Themes</source>
        <translation>佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="374"/>
        <source>Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="375"/>
        <source>&amp;New Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="378"/>
        <source>&amp;Open Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="380"/>
        <source>Open an existing service.</source>
        <translation>開啟現有的聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="381"/>
        <source>&amp;Save Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="383"/>
        <source>Save the current service to disk.</source>
        <translation>儲存目前的聚會到磁碟。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="384"/>
        <source>Save Service &amp;As...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="385"/>
        <source>Save Service As</source>
        <translation>儲存聚會到</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="386"/>
        <source>Save the current service under a new name.</source>
        <translation>儲存目前的聚會到新的名稱。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="389"/>
        <source>Print the current service.</source>
        <translation>列印目前聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="390"/>
        <source>E&amp;xit</source>
        <translation>離開(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="391"/>
        <source>Close OpenLP - Shut down the program.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="394"/>
        <source>&amp;Theme</source>
        <translation>佈景主題(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="396"/>
        <source>Configure &amp;Shortcuts...</source>
        <translation>設置快捷鍵(&amp;S)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="397"/>
        <source>Configure &amp;Formatting Tags...</source>
        <translation>設置格式標籤(&amp;F)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="398"/>
        <source>&amp;Configure OpenLP...</source>
        <translation>設置 OpenLP(&amp;C)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="399"/>
        <source>Export settings to a *.config file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="405"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="402"/>
        <source>Import settings from a *.config file previously exported from this or another machine.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="406"/>
        <source>&amp;Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="407"/>
        <source>Hide or show Projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="408"/>
        <source>Toggle visibility of the Projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="410"/>
        <source>L&amp;ibrary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="411"/>
        <source>Hide or show the Library.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="412"/>
        <source>Toggle the visibility of the Library.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="414"/>
        <source>&amp;Themes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="415"/>
        <source>Hide or show themes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="416"/>
        <source>Toggle visibility of the Themes.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="418"/>
        <source>&amp;Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="419"/>
        <source>Hide or show Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="420"/>
        <source>Toggle visibility of the Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="422"/>
        <source>&amp;Preview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="423"/>
        <source>Hide or show Preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="424"/>
        <source>Toggle visibility of the Preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="426"/>
        <source>Li&amp;ve</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="427"/>
        <source>Hide or show Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="428"/>
        <source>L&amp;ock visibility of the panels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="429"/>
        <source>Lock visibility of the panels.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="430"/>
        <source>Toggle visibility of the Live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="431"/>
        <source>&amp;Manage Plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="432"/>
        <source>You can enable and disable plugins from here.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="434"/>
        <source>&amp;About</source>
        <translation>關於(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="435"/>
        <source>More information about OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="436"/>
        <source>&amp;User Manual</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="438"/>
        <source>Jump to the search box of the current active plugin.</source>
        <translation>跳至當前使用中插件的搜尋欄。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="440"/>
        <source>&amp;Web Site</source>
        <translation>網站(&amp;W)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="443"/>
        <source>Set the interface language to {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="445"/>
        <source>&amp;Autodetect</source>
        <translation>自動檢測(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="446"/>
        <source>Use the system language, if available.</source>
        <translation>如果可用，使用系統語言。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="447"/>
        <source>Add &amp;Tool...</source>
        <translation>新增工具(&amp;T)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="448"/>
        <source>Add an application to the list of tools.</source>
        <translation>新增應用程序到工具列表。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="450"/>
        <source>Open &amp;Data Folder...</source>
        <translation>開啟檔案資料夾(&amp;D)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="451"/>
        <source>Open the folder where songs, bibles and other data resides.</source>
        <translation>開啟歌曲、聖經及其他文件所在資料夾。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="453"/>
        <source>Re-run First Time Wizard</source>
        <translation>重新執行首次配置精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="454"/>
        <source>Re-run the First Time Wizard, importing songs, Bibles and themes.</source>
        <translation>重新執行首次配置精靈，匯入歌曲、聖經及主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="456"/>
        <source>Update Theme Images</source>
        <translation>更新佈景主題圖片</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="457"/>
        <source>Update the preview images for all themes.</source>
        <translation>為所有佈景主題更新預覽圖片。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="459"/>
        <source>&amp;Show all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="460"/>
        <source>Reset the interface back to the default layout and show all the panels.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="462"/>
        <source>&amp;Setup</source>
        <translation>設置(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="463"/>
        <source>Use layout that focuses on setting up the Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="465"/>
        <source>&amp;Live</source>
        <translation>現場Live(&amp;L)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="466"/>
        <source>Use layout that focuses on Live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="548"/>
        <source>Waiting for some things to finish...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="550"/>
        <source>Please Wait</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="631"/>
        <source>Version {new} of OpenLP is now available for download (you are currently running version {current}). 

You can download the latest version from https://openlp.org/.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="634"/>
        <source>OpenLP Version Updated</source>
        <translation>OpenLP 版本已更新</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Re-run First Time Wizard?</source>
        <translation>重新執行首次配置精靈？</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Are you sure you want to re-run the First Time Wizard?

Re-running this wizard may make changes to your current OpenLP configuration and possibly add songs to your existing songs list and change your default theme.</source>
        <translation>您確定要重新執行首次配置精靈嗎？

重新執行可能會改變您當前的OpenLP配置並可能在以儲存的歌曲列表中添加歌曲及改變默認的佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>OpenLP Main Display Blanked</source>
        <translation>OpenLP 主顯示已清空</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>The Main Display has been blanked out</source>
        <translation>主顯示已被清空</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Import settings?</source>
        <translation>匯入設定？</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Are you sure you want to import settings?

 Importing settings will make permanent changes to your current OpenLP configuration.

 Importing incorrect settings may cause erratic behaviour or OpenLP to terminate abnormally.</source>
        <translation>您確定要匯入設定嗎?

匯入設定將會永久性的改變目前 OpenLP 配置。

匯入不正確的設定可能導致飛預期的行為或 OpenLP 不正常終止。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>Import settings</source>
        <translation>匯入設定</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>OpenLP Settings (*.conf)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="889"/>
        <source>The file you have selected does not appear to be a valid OpenLP settings file.

Processing has terminated and no changes have been made.</source>
        <translation>您選擇的檔案似乎不是有效的 OpenLP 設定檔。

程序已終止並且未作任何變更。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>OpenLP will now close.  Imported settings will be applied the next time you start OpenLP.</source>
        <translation>OpenLP 即將關閉。匯入的設定將會在下一次啟動 OpenLP 時套用。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>Export Settings File</source>
        <translation>匯出設定檔</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>Export setting error</source>
        <translation>匯出設定錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>An error occurred while exporting the settings: {err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Exit OpenLP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Are you sure you want to exit OpenLP?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1042"/>
        <source>&amp;Exit OpenLP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1108"/>
        <source>Default Theme: {theme}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear List</source>
        <comment>Clear List of recent files</comment>
        <translation>清除列表</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear the list of recent files.</source>
        <translation>清空最近使用的文件列表。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1345"/>
        <source>Copying OpenLP data to new data directory location - {path} - Please wait for copy to finish</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1353"/>
        <source>OpenLP Data directory copy failed

{err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1355"/>
        <source>New Data Directory Error</source>
        <translation>新的資料目錄錯誤</translation>
    </message>
</context>
<context>
    <name>OpenLP.Manager</name>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>Database Error</source>
        <translation>資料庫錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="157"/>
        <source>OpenLP cannot load your database.

Database: {db}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>The database being loaded was created in a more recent version of OpenLP. The database is version {db_ver}, while OpenLP expects version {db_up}. The database will not be loaded.

Database: {db_name}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.MediaController</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="100"/>
        <source>The media integration library is missing (python - vlc is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="102"/>
        <source>The media integration library is missing (python - pymediainfo is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="122"/>
        <source>No Displays have been configured, so Live Media has been disabled</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.MediaManagerItem</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="382"/>
        <source>No Items Selected</source>
        <translation>未選擇項目</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="161"/>
        <source>&amp;Add to selected Service Item</source>
        <translation>新增選擇的項目到聚會(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File Type</source>
        <translation>無效的檔案類型</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File {file_path}.
File extension not supported</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="391"/>
        <source>Duplicate files were found on import and were ignored.</source>
        <translation>匯入時發現重複檔案並忽略。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="496"/>
        <source>You must select one or more items to preview.</source>
        <translation>您必須選擇一或多個項目預覽。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="514"/>
        <source>You must select one or more items to send live.</source>
        <translation>您必須選擇一或多個項目送到現場Live。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="563"/>
        <source>You must select one or more items to add.</source>
        <translation>您必須選擇一或多個項目新增。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="607"/>
        <source>You must select one or more items.</source>
        <translation>您必須選擇一或多個項目。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="614"/>
        <source>You must select an existing service item to add to.</source>
        <translation>您必須選擇一個現有的聚會新增。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>Invalid Service Item</source>
        <translation>無效的聚會項目</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>You must select a {title} service item.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="119"/>
        <source>&amp;Clone</source>
        <translation>複製(&amp;C)</translation>
    </message>
</context>
<context>
    <name>OpenLP.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="51"/>
        <source>Media</source>
        <translation>媒體</translation>
    </message>
</context>
<context>
    <name>OpenLP.OpenLyricsImportError</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="707"/>
        <source>&lt;lyrics&gt; tag is missing.</source>
        <translation>&lt;lyrics&gt; 標籤遺失。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="712"/>
        <source>&lt;verse&gt; tag is missing.</source>
        <translation>&lt;verse&gt; 標籤遺失。</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="494"/>
        <source>Fan</source>
        <translation>風扇</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="495"/>
        <source>Lamp</source>
        <translation>燈泡</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="496"/>
        <source>Temperature</source>
        <translation>溫度</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="497"/>
        <source>Cover</source>
        <translation>蓋子</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="498"/>
        <source>Filter</source>
        <translation>濾波器</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="430"/>
        <source>No message</source>
        <translation>沒有訊息</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="759"/>
        <source>Error while sending data to projector</source>
        <translation>傳送資料到投影機時發生錯誤</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLinkConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="166"/>
        <source>Acknowledge a PJLink SRCH command - returns MAC address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="171"/>
        <source>Blank/unblank video and/or mute audio.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="176"/>
        <source>Query projector PJLink class support.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="181"/>
        <source>Query error status from projector. Returns fan/lamp/temp/cover/filter/other error status.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="187"/>
        <source>Query number of hours on filter.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="192"/>
        <source>Freeze or unfreeze current image being projected.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="197"/>
        <source>Query projector manufacturer name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="202"/>
        <source>Query projector product name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="207"/>
        <source>Query projector for other information set by manufacturer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="212"/>
        <source>Query specified input source name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="217"/>
        <source>Switch to specified video source.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="222"/>
        <source>Query available input sources.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="227"/>
        <source>Query current input resolution.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="232"/>
        <source>Query lamp time and on/off status. Multiple lamps supported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="237"/>
        <source>UDP Status - Projector is now available on network. Includes MAC address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="242"/>
        <source>Adjust microphone volume by 1 step.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="247"/>
        <source>Query customer-set projector name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="252"/>
        <source>Initial connection with authentication/no authentication request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="257"/>
        <source>Turn lamp on or off/standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="262"/>
        <source>Query replacement air filter model number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="267"/>
        <source>Query replacement lamp model number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="272"/>
        <source>Query recommended resolution.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="277"/>
        <source>Query projector serial number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="282"/>
        <source>UDP broadcast search request for available projectors. Reply is ACKN.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="287"/>
        <source>Query projector software version number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="292"/>
        <source>Adjust speaker volume by 1 step.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PathEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="299"/>
        <source>Browse for directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="300"/>
        <source>Revert to default directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="302"/>
        <source>Browse for file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="303"/>
        <source>Revert to default file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="317"/>
        <source>Select Directory</source>
        <translation>選擇目錄</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="321"/>
        <source>Select File</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PluginForm</name>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="74"/>
        <source>Manage Plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="75"/>
        <source>Plugin Details</source>
        <translation>插件詳細資訊</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="77"/>
        <source>Status:</source>
        <translation>狀態：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="78"/>
        <source>Active</source>
        <translation>已啟用</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="149"/>
        <source>{name} (Disabled)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="145"/>
        <source>{name} (Active)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="147"/>
        <source>{name} (Inactive)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PluginManager</name>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="173"/>
        <source>Unable to initialise the following plugins:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="179"/>
        <source>See the log file for more details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceDialog</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Page</source>
        <translation>適合頁面</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Width</source>
        <translation>適合寬度</translation>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceForm</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="61"/>
        <source>Print</source>
        <translation>列印</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="69"/>
        <source>Copy as Text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="71"/>
        <source>Copy as HTML</source>
        <translation>複製為 HTML</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="137"/>
        <source>Zoom Out</source>
        <translation>縮小</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="138"/>
        <source>Zoom Original</source>
        <translation>原始大小</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="139"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="140"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="141"/>
        <source>Title:</source>
        <translation>標題：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="142"/>
        <source>Service Note Text:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="143"/>
        <source>Other Options</source>
        <translation>其他選項</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="144"/>
        <source>Include slide text if available</source>
        <translation>包含幻燈片文字(如果可用)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="145"/>
        <source>Add page break before each text item</source>
        <translation>每個文字項目前插入分頁符號</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="146"/>
        <source>Include service item notes</source>
        <translation>包含聚會項目的筆記</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="147"/>
        <source>Include play length of media items</source>
        <translation>包含媒體播放長度</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="148"/>
        <source>Show chords</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="149"/>
        <source>Service Sheet</source>
        <translation>聚會手冊</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="401"/>
        <source>The address specified with socket.bind() is already in use and was set to be exclusive</source>
        <translation>socket.bind() 指定的位址已在使用中，被設置為獨占模式</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="404"/>
        <source>PJLink returned &quot;ERRA: Authentication Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="405"/>
        <source>The connection was refused by the peer (or timed out)</source>
        <translation>連線拒絕(或逾時)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="407"/>
        <source>Projector cover open detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="408"/>
        <source>PJLink class not supported</source>
        <translation>PJLink類別不支援</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="409"/>
        <source>The datagram was larger than the operating system&apos;s limit</source>
        <translation>數據包比作業系統限制大</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="411"/>
        <source>Error condition detected</source>
        <translation>錯誤狀態檢測</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="412"/>
        <source>Projector fan error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="413"/>
        <source>Projector check filter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="414"/>
        <source>General projector error</source>
        <translation>一般投影機錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="415"/>
        <source>The host address was not found</source>
        <translation>找不到主機位址</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="416"/>
        <source>PJLink invalid packet received</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="417"/>
        <source>Projector lamp error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="418"/>
        <source>An error occurred with the network (Possibly someone pulled the plug?)</source>
        <translation>網路發生錯誤(可能有人拔掉插頭？)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="420"/>
        <source>PJlink authentication Mismatch Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="421"/>
        <source>Projector not connected error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="422"/>
        <source>PJLink returned &quot;ERR2: Invalid Parameter&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="423"/>
        <source>PJLink Invalid prefix character</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="424"/>
        <source>PJLink returned &quot;ERR4: Projector/Display Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="425"/>
        <source>The socket is using a proxy, and the proxy requires authentication</source>
        <translation>連接使用代理伺服器Proxy，且代理伺服器需要認證</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="428"/>
        <source>The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established)</source>
        <translation>到代理伺服器的連接意外地被關閉(連接到最後對等建立之前)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="431"/>
        <source>Could not contact the proxy server because the connection to that server was denied</source>
        <translation>無法連接代理伺服器，因為連接該伺服器被拒絕</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="434"/>
        <source>The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase.</source>
        <translation>到代理伺服器的連接逾時或代理伺服器停在回應認證階段。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="437"/>
        <source>The proxy address set with setProxy() was not found</source>
        <translation>設置setProxy()找不到代理伺服器位址</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="439"/>
        <source>The connection negotiation with the proxy server failed because the response from the proxy server could not be understood</source>
        <translation>與代理伺服器的連接協商失敗，因為從代理伺服器的回應無法了解</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="442"/>
        <source>The remote host closed the connection</source>
        <translation>遠端主機關閉連線</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="444"/>
        <source>The SSL/TLS handshake failed</source>
        <translation>SSL / TLS 信號交換錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="446"/>
        <source>The address specified to socket.bind() does not belong to the host</source>
        <translation>指定的socket.bind() 位址不屬於主機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="449"/>
        <source>The socket operation failed because the application lacked the required privileges</source>
        <translation>連接操作失敗，因缺乏必要的權限</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="452"/>
        <source>The local system ran out of resources (e.g., too many sockets)</source>
        <translation>本地端沒有資源了(例如：連接太多)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="454"/>
        <source>The socket operation timed out</source>
        <translation>連接操作逾時</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="456"/>
        <source>Projector high temperature detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="457"/>
        <source>PJLink returned &quot;ERR3: Busy&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="458"/>
        <source>PJLink returned &quot;ERR1: Undefined Command&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="459"/>
        <source>The last operation attempted has not finished yet (still in progress in the background)</source>
        <translation>最後一個嘗試的操作尚未完成。(仍在背景程序)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="462"/>
        <source>Unknown condiction detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="463"/>
        <source>An unidentified socket error occurred</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="464"/>
        <source>The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support)</source>
        <translation>不支援本地作業系統要求的連接(EX：缺少支援IPv6)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="467"/>
        <source>Warning condition detected</source>
        <translation>預警狀態檢測</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="468"/>
        <source>Connection initializing with pin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="469"/>
        <source>Socket is bount to an address or port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="470"/>
        <source>Connection initializing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="471"/>
        <source>Socket is about to close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="472"/>
        <source>Connected</source>
        <translation>已連線</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="473"/>
        <source>Connecting</source>
        <translation>連線中</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="474"/>
        <source>Cooldown in progress</source>
        <translation>正在進行冷卻</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="475"/>
        <source>Command returned with OK</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="476"/>
        <source>Performing a host name lookup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="477"/>
        <source>Projector Information available</source>
        <translation>投影機提供的訊息</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="478"/>
        <source>Initialize in progress</source>
        <translation>進行初始化中</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="479"/>
        <source>Socket it listening (internal use only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="480"/>
        <source>No network activity at this time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="481"/>
        <source>Received data</source>
        <translation>接收到的數據</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="482"/>
        <source>Sending data</source>
        <translation>發送數據</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="483"/>
        <source>Not Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="484"/>
        <source>Off</source>
        <translation>關</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="485"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="486"/>
        <source>Power is on</source>
        <translation>電源在開啟</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="487"/>
        <source>Power in standby</source>
        <translation>電源在待機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="488"/>
        <source>Getting status</source>
        <translation>取得狀態</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="489"/>
        <source>Warmup in progress</source>
        <translation>正在進行熱機</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEdit</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>Name Not Set</source>
        <translation>未設定名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>You must enter a name for this entry.&lt;br /&gt;Please enter a new name for this entry.</source>
        <translation>你必須為此項目輸入一個名稱。&lt;br /&gt;請為這個項目輸入新的名稱。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="196"/>
        <source>Duplicate Name</source>
        <translation>重複的名稱</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEditForm</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="115"/>
        <source>Add New Projector</source>
        <translation>新增投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="118"/>
        <source>Edit Projector</source>
        <translation>編輯投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="120"/>
        <source>IP Address</source>
        <translation>IP 位址</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="123"/>
        <source>Port Number</source>
        <translation>連接埠</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="125"/>
        <source>PIN</source>
        <translation>PIN</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="127"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="129"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="131"/>
        <source>Notes</source>
        <translation>筆記</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>Database Error</source>
        <translation>資料庫錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>There was an error saving projector information. See the log for the error</source>
        <translation>儲存投影機資訊發生錯誤。請查看錯誤記錄</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorManager</name>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add Projector</source>
        <translation>新增投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add a new projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit Projector</source>
        <translation>編輯投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete Projector</source>
        <translation>刪除投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Select Input Source</source>
        <translation>選擇輸入來源</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Choose input source on selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View Projector</source>
        <translation>查看投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View selected projector information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="100"/>
        <source>Connect to selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors</source>
        <translation>連接選擇的投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projectors</source>
        <translation>離線選擇的投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projector</source>
        <translation>離線選擇的投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projector</source>
        <translation>打開投影機電源</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="129"/>
        <source>Power on selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Standby selected projector</source>
        <translation>選擇的投影機待機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="143"/>
        <source>Put selected projector in standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Put selected projectors in standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="156"/>
        <source>Blank selected projector screen</source>
        <translation>選擇的投影機畫面空白</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projector screen</source>
        <translation>選擇的投影機畫面顯示</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="170"/>
        <source>Show selected projector screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projectors screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="199"/>
        <source>&amp;View Projector Information</source>
        <translation>檢視投影機資訊(&amp;V)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="204"/>
        <source>&amp;Edit Projector</source>
        <translation>編輯投影機(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="210"/>
        <source>&amp;Connect Projector</source>
        <translation>投影機連線(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="215"/>
        <source>D&amp;isconnect Projector</source>
        <translation>投影機斷線(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="221"/>
        <source>Power &amp;On Projector</source>
        <translation>投影機電源打開(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="226"/>
        <source>Power O&amp;ff Projector</source>
        <translation>投影機電源關閉(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="232"/>
        <source>Select &amp;Input</source>
        <translation>選擇輸入(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="237"/>
        <source>Edit Input Source</source>
        <translation>修改輸入來源</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="242"/>
        <source>&amp;Blank Projector Screen</source>
        <translation>投影機畫面空白(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="247"/>
        <source>&amp;Show Projector Screen</source>
        <translation>投影機畫面顯示(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="253"/>
        <source>&amp;Delete Projector</source>
        <translation>刪除投影機(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="503"/>
        <source>Are you sure you want to delete this projector?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="648"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="650"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="652"/>
        <source>Port</source>
        <translation>連接埠</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="654"/>
        <source>Notes</source>
        <translation>筆記</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="658"/>
        <source>Projector information not available at this time.</source>
        <translation>此時沒有可使用的投影機資訊。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="660"/>
        <source>Projector Name</source>
        <translation>投影機名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="663"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="665"/>
        <source>Model</source>
        <translation>型號</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="667"/>
        <source>PJLink Class</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="670"/>
        <source>Software Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="673"/>
        <source>Serial Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="676"/>
        <source>Lamp Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="679"/>
        <source>Filter Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="682"/>
        <source>Other info</source>
        <translation>其他資訊</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="685"/>
        <source>Power status</source>
        <translation>電源狀態</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Shutter is</source>
        <translation>閘板是</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Closed</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="691"/>
        <source>Current source input is</source>
        <translation>當前輸入來源是</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="698"/>
        <source>Unavailable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="700"/>
        <source>ON</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="702"/>
        <source>OFF</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="703"/>
        <source>Lamp</source>
        <translation>燈泡</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="708"/>
        <source>Hours</source>
        <translation>時</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="713"/>
        <source>No current errors or warnings</source>
        <translation>當前沒有錯誤或警告</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="715"/>
        <source>Current errors/warnings</source>
        <translation>當前錯誤 / 警告</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="718"/>
        <source>Projector Information</source>
        <translation>投影機資訊</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="941"/>
        <source>Authentication Error</source>
        <translation>驗證錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="956"/>
        <source>No Authentication Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="998"/>
        <source>Not Implemented Yet</source>
        <translation>尚未實行</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="52"/>
        <source>Projector</source>
        <translation>投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="111"/>
        <source>Communication Options</source>
        <translation>連接選項</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="113"/>
        <source>Connect to projectors on startup</source>
        <translation>啟動時連接到投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="115"/>
        <source>Socket timeout (seconds)</source>
        <translation>連接逾時 (秒)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="117"/>
        <source>Poll time (seconds)</source>
        <translation>獲取時間 (秒)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="121"/>
        <source>Tabbed dialog box</source>
        <translation>標籤式對話框</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="123"/>
        <source>Single dialog box</source>
        <translation>單一對話框</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="125"/>
        <source>Connect to projector when LINKUP received (v2 only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="127"/>
        <source>Enable listening for PJLink2 broadcast messages</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorWizard</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="216"/>
        <source>Duplicate IP Address</source>
        <translation>重複的IP位址</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="224"/>
        <source>Invalid IP Address</source>
        <translation>無效的IP位址</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="233"/>
        <source>Invalid Port Number</source>
        <translation>無效的連接埠</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProxyDialog</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="177"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProxyWidget</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="116"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="117"/>
        <source>No prox&amp;y</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="118"/>
        <source>&amp;Use system proxy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="119"/>
        <source>&amp;Manual proxy configuration</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="120"/>
        <source>e.g. proxy_server_address:port_no</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="121"/>
        <source>HTTP:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="123"/>
        <source>HTTPS:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="125"/>
        <source>Username:</source>
        <translation>帳號：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="126"/>
        <source>Password:</source>
        <translation>使用者密碼：</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreenList</name>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="254"/>
        <source>Screen</source>
        <translation>螢幕</translation>
    </message>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="257"/>
        <source>primary</source>
        <translation>主要的</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreensTab</name>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="44"/>
        <source>Screens</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="69"/>
        <source>Generic screen settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="70"/>
        <source>Display if a single screen</source>
        <translation>如果只有單螢幕也顯示</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="297"/>
        <source>F&amp;ull screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="298"/>
        <source>Width:</source>
        <translation>寬度：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="299"/>
        <source>Use this screen as a display</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="302"/>
        <source>Left:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="303"/>
        <source>Custom &amp;geometry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="304"/>
        <source>Top:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="305"/>
        <source>Height:</source>
        <translation>高度：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="306"/>
        <source>Identify Screens</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItem</name>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="289"/>
        <source>[slide {frame:d}]</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="590"/>
        <source>&lt;strong&gt;Start&lt;/strong&gt;: {start}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="594"/>
        <source>&lt;strong&gt;Length&lt;/strong&gt;: {length}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItemEditForm</name>
    <message>
        <location filename="../../openlp/core/ui/serviceitemeditdialog.py" line="70"/>
        <source>Reorder Service Item</source>
        <translation>重新排列聚會物件</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceManager</name>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="199"/>
        <source>Service Notes: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="246"/>
        <source>Notes: </source>
        <translation>筆記：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="254"/>
        <source>Playing time: </source>
        <translation>播放時間：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="148"/>
        <source>Load an existing service.</source>
        <translation>載入現有聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="152"/>
        <source>Save this service.</source>
        <translation>儲存聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="162"/>
        <source>Select a theme for the service.</source>
        <translation>選擇佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move to &amp;top</source>
        <translation>置頂(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move item to the top of the service.</source>
        <translation>移到最上方。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move &amp;up</source>
        <translation>上移(&amp;U)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move item up one position in the service.</source>
        <translation>向上移動一個位置。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move &amp;down</source>
        <translation>下移(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move item down one position in the service.</source>
        <translation>向下移動一個位置。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move to &amp;bottom</source>
        <translation>置底(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move item to the end of the service.</source>
        <translation>移到最下方。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>&amp;Delete From Service</source>
        <translation>從聚會中刪除(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>Delete the selected item from the service.</source>
        <translation>從聚會刪除所選擇的項目。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>&amp;Expand all</source>
        <translation>全部展開(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>Expand all the service items.</source>
        <translation>展開所有聚會項目。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>&amp;Collapse all</source>
        <translation>全部收合(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>Collapse all the service items.</source>
        <translation>收合所有聚會項目。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Go Live</source>
        <translation>現場Live</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Send the selected item to Live.</source>
        <translation>傳送選擇的項目至現場Live。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="241"/>
        <source>&amp;Add New Item</source>
        <translation>添加新項目(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="243"/>
        <source>&amp;Add to Selected Item</source>
        <translation>新增到選擇的項目(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="247"/>
        <source>&amp;Edit Item</source>
        <translation>編輯(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="249"/>
        <source>&amp;Rename...</source>
        <translation>重新命名(&amp;R)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="252"/>
        <source>&amp;Reorder Item</source>
        <translation>重新排列項目(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="255"/>
        <source>&amp;Notes</source>
        <translation>筆記(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="258"/>
        <source>&amp;Start Time</source>
        <translation>開始時間(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="265"/>
        <source>Create New &amp;Custom Slide</source>
        <translation>建立新自訂幻燈片(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="272"/>
        <source>&amp;Auto play slides</source>
        <translation>幻燈片自動撥放(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="276"/>
        <source>Auto play slides &amp;Loop</source>
        <translation>自動循環播放幻燈片(&amp;L)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="281"/>
        <source>Auto play slides &amp;Once</source>
        <translation>自動一次撥放幻燈片(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="812"/>
        <source>&amp;Delay between slides</source>
        <translation>幻燈片間延遲(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="292"/>
        <source>Show &amp;Preview</source>
        <translation>預覽(&amp;P)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="297"/>
        <source>&amp;Change Item Theme</source>
        <translation>變更佈景主題(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="371"/>
        <source>Untitled Service</source>
        <translation>無標題聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>Open File</source>
        <translation>開啟檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>OpenLP Service Files (*.osz *.oszl)</source>
        <translation>OpenLP 聚會檔 (*.osz *.oszl)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>Modified Service</source>
        <translation>聚會已修改</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>The current service has been modified. Would you like to save this service?</source>
        <translation>目前的聚會已修改。您想要儲存此聚會嗎？</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="572"/>
        <source>Service File(s) Missing</source>
        <translation>聚會檔案遺失</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="573"/>
        <source>The following file(s) in the service are missing: {name}

These files will be removed if you continue to save.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>Error Saving File</source>
        <translation>儲存檔案錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>There was an error saving your file.

{error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="653"/>
        <source>OpenLP Service Files - lite (*.oszl)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="654"/>
        <source>OpenLP Service Files (*.osz)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="726"/>
        <source>The service file {file_path} could not be loaded because it is either corrupt, inaccessible, or not a valid OpenLP 2 or OpenLP 3 service file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="823"/>
        <source>&amp;Auto Start - active</source>
        <translation>自動開始(&amp;A) - 已啟用</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="827"/>
        <source>&amp;Auto Start - inactive</source>
        <translation>自動開始(&amp;A) - 未啟用</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Input delay</source>
        <translation>輸入延遲</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Delay between slides in seconds.</source>
        <translation>幻燈片之間延遲。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1220"/>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1221"/>
        <source>Service copy only</source>
        <translation>複製聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1224"/>
        <source>Slide theme</source>
        <translation>幻燈片佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1228"/>
        <source>Notes</source>
        <translation>筆記</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Missing Display Handler</source>
        <translation>遺失顯示處理器</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1399"/>
        <source>Your item cannot be displayed as there is no handler to display it</source>
        <translation>您的項目無法顯示，因為沒有處理器來顯示它</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Your item cannot be displayed as the plugin required to display it is missing or inactive</source>
        <translation>您的項目無法顯示，所需的插件遺失或無效</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Rename item title</source>
        <translation>重新命名標題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Title:</source>
        <translation>標題：</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceNoteForm</name>
    <message>
        <location filename="../../openlp/core/ui/servicenoteform.py" line="72"/>
        <source>Service Item Notes</source>
        <translation>聚會項目筆記</translation>
    </message>
</context>
<context>
    <name>OpenLP.SettingsForm</name>
    <message>
        <location filename="../../openlp/core/ui/settingsdialog.py" line="62"/>
        <source>Configure OpenLP</source>
        <translation>OpenLP 設定</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListDialog</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="138"/>
        <source>Configure Shortcuts</source>
        <translation>設定快捷鍵</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="139"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>選擇一個動作並點擊一個按鈕來開始記錄新的主要或替代的快捷鍵。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Action</source>
        <translation>啟用</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Shortcut</source>
        <translation>快捷建</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Alternate</source>
        <translation>替代</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="145"/>
        <source>Default</source>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="146"/>
        <source>Custom</source>
        <translation>自訂</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="148"/>
        <source>Capture shortcut.</source>
        <translation>記錄快捷鍵。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="151"/>
        <source>Restore the default shortcut of this action.</source>
        <translation>將此動作恢復預設快捷鍵。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Restore Default Shortcuts</source>
        <translation>回復預設捷徑</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Do you want to restore all shortcuts to their defaults?</source>
        <translation>你確定要回復全部捷徑為預設值？</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="437"/>
        <source>The shortcut &quot;{key}&quot; is already assigned to another action,
please use a different shortcut.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="441"/>
        <source>Duplicate Shortcut</source>
        <translation>複製捷徑</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListForm</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an Action</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>選擇一個動作並點擊一個按鈕來開始記錄新的主要或替代的快捷鍵。</translation>
    </message>
</context>
<context>
    <name>OpenLP.SlideController</name>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Previous Slide</source>
        <translation>上一張幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Move to previous.</source>
        <translation>移至前一項。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Next Slide</source>
        <translation>下一張幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Move to next.</source>
        <translation>移至下一項。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="266"/>
        <source>Hide</source>
        <translation>隱藏</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="268"/>
        <source>Move to preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="272"/>
        <source>Show Desktop</source>
        <translation>顯示桌面</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="277"/>
        <source>Toggle Desktop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="282"/>
        <source>Toggle Blank to Theme</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="287"/>
        <source>Toggle Blank Screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="316"/>
        <source>Play Slides</source>
        <translation>播放幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="336"/>
        <source>Delay between slides in seconds.</source>
        <translation>幻燈片之間延遲。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="340"/>
        <source>Move to live.</source>
        <translation>移動至現場Live。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="343"/>
        <source>Add to Service.</source>
        <translation>新增至聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="347"/>
        <source>Edit and reload song preview.</source>
        <translation>編輯並重新載入歌曲預覽。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="351"/>
        <source>Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="358"/>
        <source>Start playing media.</source>
        <translation>開始播放媒體。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="362"/>
        <source>Pause playing media.</source>
        <translation>暫停播放媒體。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="366"/>
        <source>Stop playing media.</source>
        <translation>停止播放媒體。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="370"/>
        <source>Loop playing media.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="377"/>
        <source>Video timer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="386"/>
        <source>Video position.</source>
        <translation>影片位置。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="397"/>
        <source>Audio Volume.</source>
        <translation>聲音音量。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="414"/>
        <source>Go To</source>
        <translation>到</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="442"/>
        <source>Go to &quot;Verse&quot;</source>
        <translation>切換到&quot;主歌&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="443"/>
        <source>Go to &quot;Chorus&quot;</source>
        <translation>切換到&quot;副歌&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="444"/>
        <source>Go to &quot;Bridge&quot;</source>
        <translation>切換到&quot;橋段&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="446"/>
        <source>Go to &quot;Pre-Chorus&quot;</source>
        <translation>切換到&quot;導歌&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="447"/>
        <source>Go to &quot;Intro&quot;</source>
        <translation>切換到&quot;前奏&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="448"/>
        <source>Go to &quot;Ending&quot;</source>
        <translation>切換到&quot;結尾&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="449"/>
        <source>Go to &quot;Other&quot;</source>
        <translation>切換到&quot;其他&quot;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="575"/>
        <source>Previous Service</source>
        <translation>上一個聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="580"/>
        <source>Next Service</source>
        <translation>下一個聚會</translation>
    </message>
</context>
<context>
    <name>OpenLP.SourceSelectForm</name>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="150"/>
        <source>Ignoring current changes and return to OpenLP</source>
        <translation>忽略當前的變更並回到 OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="153"/>
        <source>Delete all user-defined text and revert to PJLink default text</source>
        <translation>刪除所有使用者自定文字並恢復 PJLink 預設文字</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="156"/>
        <source>Discard changes and reset to previous user-defined text</source>
        <translation>放棄變更並恢復到之前使用者定義的文字</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="159"/>
        <source>Save changes and return to OpenLP</source>
        <translation>儲存變更並回到OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="397"/>
        <source>Edit Projector Source Text</source>
        <translation>編輯投影機來源文字</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="399"/>
        <source>Select Projector Source</source>
        <translation>選擇投影機來源</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="481"/>
        <source>Delete entries for this projector</source>
        <translation>從列表中刪除這台投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="482"/>
        <source>Are you sure you want to delete ALL user-defined source input text for this projector?</source>
        <translation>您確定要刪除這個投影機所有使用者自定義的輸入來源文字？</translation>
    </message>
</context>
<context>
    <name>OpenLP.SpellTextEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="405"/>
        <source>Language:</source>
        <translation>語言：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="416"/>
        <source>Spelling Suggestions</source>
        <translation>拼寫建議</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="424"/>
        <source>Formatting Tags</source>
        <translation>格式標籤</translation>
    </message>
</context>
<context>
    <name>OpenLP.StartTimeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="70"/>
        <source>Theme Layout</source>
        <translation>佈景主題版面</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="71"/>
        <source>The blue box shows the main area.</source>
        <translation>藍色框是顯示的主要區域。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="72"/>
        <source>The red box shows the footer.</source>
        <translation>紅色框顯示頁腳。</translation>
    </message>
</context>
<context>
    <name>OpenLP.StartTime_form</name>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="116"/>
        <source>Item Start and Finish Time</source>
        <translation>項目啟動和結束時間</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="123"/>
        <source>Hours:</source>
        <translation>時：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="124"/>
        <source>Minutes:</source>
        <translation>分：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="125"/>
        <source>Seconds:</source>
        <translation>第二本：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="126"/>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="127"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="128"/>
        <source>Length</source>
        <translation>長度</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Time Validation Error</source>
        <translation>時間驗證錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="72"/>
        <source>Finish time is set after the end of the media item</source>
        <translation>完成時間是媒體項目結束後</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Start time is after the finish time of the media item</source>
        <translation>開始時間是在媒體項目結束時間之後</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="167"/>
        <source>(approximately %d lines per slide)</source>
        <translation>(每張幻燈片大約 %d 行)</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeManager</name>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="68"/>
        <source>Create a new theme.</source>
        <translation>新建佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit Theme</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit a theme.</source>
        <translation>編輯佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete Theme</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete a theme.</source>
        <translation>刪除佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import Theme</source>
        <translation>匯入佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import a theme.</source>
        <translation>匯入佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export Theme</source>
        <translation>匯出佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export a theme.</source>
        <translation>匯出佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="108"/>
        <source>&amp;Edit Theme</source>
        <translation>編輯佈景主題(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="111"/>
        <source>&amp;Copy Theme</source>
        <translation>複製佈景主題(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="114"/>
        <source>&amp;Rename Theme</source>
        <translation>重命名佈景主題(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="117"/>
        <source>&amp;Delete Theme</source>
        <translation>刪除佈景主題(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="121"/>
        <source>Set As &amp;Global Default</source>
        <translation>設定為全域預設(&amp;G)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="125"/>
        <source>&amp;Export Theme</source>
        <translation>匯出佈景主題(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="260"/>
        <source>{text} (default)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>You must select a theme to rename.</source>
        <translation>您必須選擇一個要重命名的佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename Confirmation</source>
        <translation>確認重新命名</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename {theme_name} theme?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="310"/>
        <source>Copy of {name}</source>
        <comment>Copy of &lt;theme name&gt;</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="340"/>
        <source>You must select a theme to edit.</source>
        <translation>您必須選擇一個要編輯的佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>You must select a theme to delete.</source>
        <translation>您必須選擇一個要刪除的佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete Confirmation</source>
        <translation>確認刪除</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete {theme_name} theme?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="394"/>
        <source>You have not selected a theme.</source>
        <translation>您沒有選擇佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="398"/>
        <source>Save Theme - ({name})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>OpenLP Themes (*.otz)</source>
        <translation>OpenLP 佈景主題 (*.otz)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Theme Exported</source>
        <translation>佈景主題已匯出</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Your theme has been successfully exported.</source>
        <translation>您的佈景主題已成功匯出。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>Theme Export Failed</source>
        <translation>佈景主題匯出失敗</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>The {theme_name} export failed because this error occurred: {err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>Select Theme Import File</source>
        <translation>選擇匯入的佈景主題檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="505"/>
        <source>{name} (default)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme Already Exists</source>
        <translation>佈景主題已存在</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme {name} already exists. Do you want to replace it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>Import Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>There was a problem importing {file_name}.

It is corrupt, inaccessible or not a valid theme.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>Validation Error</source>
        <translation>驗證錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>A theme with this name already exists.</source>
        <translation>已存在以這個檔案命名的佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="773"/>
        <source>You are unable to delete the default theme.</source>
        <translation>您無法刪除預設的佈景主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="782"/>
        <source>{count} time(s) by {plugin}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Unable to delete theme</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Theme is currently used 

{text}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ThemeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>Background Image Empty</source>
        <translation>背景圖片空白</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>You have not selected a background image. Please select one before continuing.</source>
        <translation>您尚未選擇背景圖片。請繼續前先選擇一個圖片。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="301"/>
        <source>Edit Theme - {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>Theme Name Missing</source>
        <translation>遺失佈景主題名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>There is no name for this theme. Please enter one.</source>
        <translation>這個佈景主題沒有名稱。請輸入一個名稱。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Theme Name Invalid</source>
        <translation>無效的佈景名稱</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Invalid theme name. Please enter one.</source>
        <translation>無效的佈景名稱。請輸入一個名稱。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="128"/>
        <source>Select Image</source>
        <translation>選擇圖像</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="153"/>
        <source>Select Video</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="411"/>
        <source>Theme Wizard</source>
        <translation>佈景主題精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="412"/>
        <source>Welcome to the Theme Wizard</source>
        <translation>歡迎來到佈景主題精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="414"/>
        <source>This wizard will help you to create and edit your themes. Click the next button below to start the process by setting up your background.</source>
        <translation>此精靈將幫助您創建和編輯您的佈景主題。點擊下一步按鈕進入程序設置你的背景。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="417"/>
        <source>Set Up Background</source>
        <translation>設定背景</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="418"/>
        <source>Set up your theme&apos;s background according to the parameters below.</source>
        <translation>根據下面的參數設定您的佈景主題背景。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="420"/>
        <source>Background type:</source>
        <translation>背景種類：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="421"/>
        <source>Solid color</source>
        <translation>純色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="422"/>
        <source>Gradient</source>
        <translation>漸層</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="425"/>
        <source>Transparent</source>
        <translation>透明</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="427"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="468"/>
        <source>color:</source>
        <translation>顏色：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="430"/>
        <source>Starting color:</source>
        <translation>啟始顏色：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="431"/>
        <source>Ending color:</source>
        <translation>結束顏色：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="432"/>
        <source>Gradient:</source>
        <translation>漸層：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="433"/>
        <source>Horizontal</source>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="435"/>
        <source>Vertical</source>
        <translation>垂直</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="437"/>
        <source>Circular</source>
        <translation>圓形</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="439"/>
        <source>Top Left - Bottom Right</source>
        <translation>左上 - 右下</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="441"/>
        <source>Bottom Left - Top Right</source>
        <translation>左下 - 右上</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="445"/>
        <source>Background color:</source>
        <translation>背景顏色：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="447"/>
        <source>Main Area Font Details</source>
        <translation>主要範圍字型細節</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="448"/>
        <source>Define the font and display characteristics for the Display text</source>
        <translation>定義了顯示文字的字體和顯示屬性</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="467"/>
        <source>Font:</source>
        <translation>字型：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="469"/>
        <source>Size:</source>
        <translation>大小：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="454"/>
        <source>Line Spacing:</source>
        <translation>行距：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="456"/>
        <source>&amp;Outline:</source>
        <translation>概述(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="459"/>
        <source>&amp;Shadow:</source>
        <translation>陰影(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="462"/>
        <source>Bold</source>
        <translation>粗體</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="463"/>
        <source>Italic</source>
        <translation>斜體</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="464"/>
        <source>Footer Area Font Details</source>
        <translation>頁尾區域字型細節</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="465"/>
        <source>Define the font and display characteristics for the Footer text</source>
        <translation>定義頁尾文字的字體和顯示屬性</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="471"/>
        <source>Text Formatting Details</source>
        <translation>文字格式細節</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="472"/>
        <source>Allows additional display formatting information to be defined</source>
        <translation>允許定義附加的顯示格式訊息</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="474"/>
        <source>Horizontal Align:</source>
        <translation>水平對齊：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="475"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="476"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="477"/>
        <source>Center</source>
        <translation>置中</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="478"/>
        <source>Justify</source>
        <translation>對齊</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="479"/>
        <source>Transitions:</source>
        <translation>轉換：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="480"/>
        <source>Fade</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="481"/>
        <source>Slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="482"/>
        <source>Concave</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="483"/>
        <source>Convex</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="484"/>
        <source>Zoom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="485"/>
        <source>Speed:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="486"/>
        <source>Normal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="487"/>
        <source>Fast</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="488"/>
        <source>Slow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="489"/>
        <source>Output Area Locations</source>
        <translation>輸出範圍位置</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="490"/>
        <source>Allows you to change and move the Main and Footer areas.</source>
        <translation>允許您更改和移動主要和頁尾範圍。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="492"/>
        <source>&amp;Main Area</source>
        <translation>主要範圍(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="493"/>
        <source>&amp;Use default location</source>
        <translation>使用預設位置(&amp;U)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="503"/>
        <source>X position:</source>
        <translation>X位置：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="510"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="505"/>
        <source>Y position:</source>
        <translation>Y位置：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="507"/>
        <source>Width:</source>
        <translation>寬度：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="509"/>
        <source>Height:</source>
        <translation>高度：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="502"/>
        <source>&amp;Footer Area</source>
        <translation>頁尾範圍(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="511"/>
        <source>Use default location</source>
        <translation>使用預設位置</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="513"/>
        <source>Layout Preview</source>
        <translation>面板預覽</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="514"/>
        <source>Preview and Save</source>
        <translation>預覽並儲存</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="515"/>
        <source>Preview the theme and save it.</source>
        <translation>預覽佈景主題並儲存它。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="516"/>
        <source>Theme name:</source>
        <translation>佈景主題名稱：</translation>
    </message>
</context>
<context>
    <name>OpenLP.Themes</name>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="74"/>
        <source>Recreating Theme Thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="75"/>
        <source>TextLabel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ThemesTab</name>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="44"/>
        <source>Themes</source>
        <translation>佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="116"/>
        <source>Global Theme</source>
        <translation>全域佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="117"/>
        <source>Universal Settings</source>
        <translation>通用設定</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="118"/>
        <source>&amp;Wrap footer text</source>
        <translation>頁尾文字換行(&amp;W)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="119"/>
        <source>Theme Level</source>
        <translation>佈景等級</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="120"/>
        <source>S&amp;ong Level</source>
        <translation>歌曲等級(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="121"/>
        <source>Use the theme from each song in the database. If a song doesn&apos;t have a theme associated with it, then use the service&apos;s theme. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation>每首歌使用資料庫裡關聯的佈景。 如果沒有關聯的佈景，則使用聚會的佈景，如果聚會沒有佈景，則使用全域佈景。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="125"/>
        <source>&amp;Service Level</source>
        <translation>聚會等級(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="126"/>
        <source>Use the theme from the service, overriding any of the individual songs&apos; themes. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation>使用聚會的佈景覆蓋所有歌曲的佈景，如果聚會沒有佈景則使用全域佈景。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="130"/>
        <source>&amp;Global Level</source>
        <translation>全域等級(&amp;G)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="131"/>
        <source>Use the global theme, overriding any themes associated with either the service or the songs.</source>
        <translation>使用全域佈景主題，覆蓋所有聚會或歌曲關聯的佈景主題。</translation>
    </message>
</context>
<context>
    <name>OpenLP.Ui</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="337"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="338"/>
        <source>&amp;Add</source>
        <translation>新增(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="339"/>
        <source>Add group</source>
        <translation>新增群組</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="340"/>
        <source>Add group.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="341"/>
        <source>Advanced</source>
        <translation>進階</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="342"/>
        <source>All Files</source>
        <translation>所有檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="343"/>
        <source>Automatic</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="344"/>
        <source>Background Color</source>
        <translation>背景顏色</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="345"/>
        <source>Background color:</source>
        <translation>背景顏色：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="346"/>
        <source>Search is Empty or too Short</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="347"/>
        <source>&lt;strong&gt;The search you have entered is empty or shorter than 3 characters long.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please try again with a longer search.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="350"/>
        <source>No Bibles Available</source>
        <translation>沒有可用的聖經</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="351"/>
        <source>&lt;strong&gt;There are no Bibles currently installed.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please use the Import Wizard to install one or more Bibles.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="353"/>
        <source>Bottom</source>
        <translation>底部</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="354"/>
        <source>Browse...</source>
        <translation>瀏覽...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="355"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="356"/>
        <source>CCLI number:</source>
        <translation>CCLI編號:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="357"/>
        <source>CCLI song number:</source>
        <translation>CCLI歌曲編號:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="358"/>
        <source>Create a new service.</source>
        <translation>新增一個聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="359"/>
        <source>Confirm Delete</source>
        <translation>確認刪除</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="360"/>
        <source>Continuous</source>
        <translation>連續</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="361"/>
        <source>Default</source>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="362"/>
        <source>Default Color:</source>
        <translation>預設顏色：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="363"/>
        <source>Service %Y-%m-%d %H-%M</source>
        <comment>This may not contain any of the following characters: /\?*|&lt;&gt;[]&quot;:+
See http://docs.python.org/library/datetime.html#strftime-strptime-behavior for more information.</comment>
        <translation>聚會 %Y-%m-%d %H-%M</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="367"/>
        <source>&amp;Delete</source>
        <translation>刪除(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="368"/>
        <source>Display style:</source>
        <translation>顯示方式：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="369"/>
        <source>Duplicate Error</source>
        <translation>重複的錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="370"/>
        <source>&amp;Edit</source>
        <translation>編輯(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="371"/>
        <source>Empty Field</source>
        <translation>空白區域</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="372"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="373"/>
        <source>Export</source>
        <translation>匯出</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="374"/>
        <source>File</source>
        <translation>檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="375"/>
        <source>File appears to be corrupt.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="376"/>
        <source>pt</source>
        <comment>Abbreviated font point size unit</comment>
        <translation>pt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="377"/>
        <source>Help</source>
        <translation>幫助</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="378"/>
        <source>h</source>
        <comment>The abbreviated unit for hours</comment>
        <translation>時</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="379"/>
        <source>Invalid Folder Selected</source>
        <comment>Singular</comment>
        <translation>選擇了無效的資料夾</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="380"/>
        <source>Invalid File Selected</source>
        <comment>Singular</comment>
        <translation>選擇了無效的檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="381"/>
        <source>Invalid Files Selected</source>
        <comment>Plural</comment>
        <translation>選擇了無效的檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="382"/>
        <source>Image</source>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="383"/>
        <source>Import</source>
        <translation>匯入</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="384"/>
        <source>Layout style:</source>
        <translation>布局樣式：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="385"/>
        <source>Live</source>
        <translation>現場Live</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="386"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="387"/>
        <source>Live Background Error</source>
        <translation>現場Live背景錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="388"/>
        <source>Live Toolbar</source>
        <translation>現場Live工具列</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="389"/>
        <source>Load</source>
        <translation>載入</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="390"/>
        <source>Manufacturer</source>
        <comment>Singular</comment>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="391"/>
        <source>Manufacturers</source>
        <comment>Plural</comment>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="392"/>
        <source>Model</source>
        <comment>Singular</comment>
        <translation>型號</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="393"/>
        <source>Models</source>
        <comment>Plural</comment>
        <translation>型號</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="394"/>
        <source>m</source>
        <comment>The abbreviated unit for minutes</comment>
        <translation>分</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="395"/>
        <source>Middle</source>
        <translation>中間</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="396"/>
        <source>New</source>
        <translation>新</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="397"/>
        <source>New Service</source>
        <translation>新聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="398"/>
        <source>New Theme</source>
        <translation>新佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="399"/>
        <source>Next Track</source>
        <translation>下一個音軌</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="400"/>
        <source>No Folder Selected</source>
        <comment>Singular</comment>
        <translation>未選擇資料夾</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="401"/>
        <source>No File Selected</source>
        <comment>Singular</comment>
        <translation>檔案未選擇</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="402"/>
        <source>No Files Selected</source>
        <comment>Plural</comment>
        <translation>檔案未選擇</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="403"/>
        <source>No Item Selected</source>
        <comment>Singular</comment>
        <translation>項目未選擇</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="404"/>
        <source>No Items Selected</source>
        <comment>Plural</comment>
        <translation>未選擇項目</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="405"/>
        <source>No Search Results</source>
        <translation>沒有搜尋結果</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="406"/>
        <source>OpenLP</source>
        <translation>OpenLP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="407"/>
        <source>OpenLP 2.0 and up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="408"/>
        <source>OpenLP is already running on this machine. 
Closing this instance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="409"/>
        <source>Open service.</source>
        <translation>開啟聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="410"/>
        <source>Optional, this will be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="411"/>
        <source>Optional, this won&apos;t be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="412"/>
        <source>Play Slides in Loop</source>
        <translation>循環播放幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="413"/>
        <source>Play Slides to End</source>
        <translation>播放幻燈片至結尾</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="414"/>
        <source>Preview</source>
        <translation>預覽</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="415"/>
        <source>Preview Toolbar</source>
        <translation>預覽工具列</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="416"/>
        <source>Print Service</source>
        <translation>列印聚會管理</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="417"/>
        <source>Projector</source>
        <comment>Singular</comment>
        <translation>投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="418"/>
        <source>Projectors</source>
        <comment>Plural</comment>
        <translation>投影機</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="419"/>
        <source>Replace Background</source>
        <translation>替換背景</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="420"/>
        <source>Replace live background.</source>
        <translation>替換現場Live背景。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="421"/>
        <source>Replace live background is not available when the WebKit player is disabled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="423"/>
        <source>Reset Background</source>
        <translation>重設背景</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="424"/>
        <source>Reset live background.</source>
        <translation>重設現場Live背景。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="425"/>
        <source>Required, this will be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="426"/>
        <source>s</source>
        <comment>The abbreviated unit for seconds</comment>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="428"/>
        <source>Save &amp;&amp; Preview</source>
        <translation>儲存 &amp;&amp; 預覽</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="429"/>
        <source>Search</source>
        <translation>搜尋</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="430"/>
        <source>Search Themes...</source>
        <comment>Search bar place holder text </comment>
        <translation>搜尋佈景主題...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="431"/>
        <source>You must select an item to delete.</source>
        <translation>您必須選擇要刪除的項目。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="432"/>
        <source>You must select an item to edit.</source>
        <translation>您必須選擇要修改的項目。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="433"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="434"/>
        <source>Save Service</source>
        <translation>儲存聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="435"/>
        <source>Service</source>
        <translation>聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="436"/>
        <source>Please type more text to use &apos;Search As You Type&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="437"/>
        <source>Optional &amp;Split</source>
        <translation>選擇分隔(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="438"/>
        <source>Split a slide into two only if it does not fit on the screen as one slide.</source>
        <translation>如果一張幻燈片無法放置在一個螢幕上，則將它分為兩頁。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="64"/>
        <source>Starting import...</source>
        <translation>開始匯入...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="441"/>
        <source>Stop Play Slides in Loop</source>
        <translation>停止循環播放幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="442"/>
        <source>Stop Play Slides to End</source>
        <translation>停止順序播放幻燈片到結尾</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="443"/>
        <source>Theme</source>
        <comment>Singular</comment>
        <translation>佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="444"/>
        <source>Themes</source>
        <comment>Plural</comment>
        <translation>佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="445"/>
        <source>Tools</source>
        <translation>工具</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="446"/>
        <source>Top</source>
        <translation>上方</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="447"/>
        <source>Unsupported File</source>
        <translation>檔案不支援</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="448"/>
        <source>Verse Per Slide</source>
        <translation>每張投影片</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="449"/>
        <source>Verse Per Line</source>
        <translation>每一行</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="450"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="451"/>
        <source>View</source>
        <translation>檢視</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="452"/>
        <source>View Mode</source>
        <translation>檢視模式</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="453"/>
        <source>Video</source>
        <translation>影像</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="454"/>
        <source>Web Interface, Download and Install latest Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="455"/>
        <source>Book Chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="456"/>
        <source>Chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="457"/>
        <source>Verse</source>
        <translation>V主歌</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="459"/>
        <source>Psalm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="460"/>
        <source>Book names may be shortened from full names, for an example Ps 23 = Psalm 23</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="706"/>
        <source>Written by</source>
        <translation>撰寫</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="163"/>
        <source>Delete the selected item.</source>
        <translation>刪除選擇的項目。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="166"/>
        <source>Move selection up one position.</source>
        <translation>向上移動一個位置。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="169"/>
        <source>Move selection down one position.</source>
        <translation>向下移動一個位置。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="310"/>
        <source>&amp;Vertical Align:</source>
        <translation>垂直對齊(&amp;V)：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="53"/>
        <source>Finished import.</source>
        <translation>匯入完成。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="54"/>
        <source>Format:</source>
        <translation>格式：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="56"/>
        <source>Importing</source>
        <translation>匯入中</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="57"/>
        <source>Importing &quot;{source}&quot;...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="58"/>
        <source>Select Import Source</source>
        <translation>選擇匯入來源</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="59"/>
        <source>Select the import format and the location to import from.</source>
        <translation>選擇匯入格式與匯入的位置。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="60"/>
        <source>Open {file_type} File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="61"/>
        <source>Open {folder_name} Folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="62"/>
        <source>%p%</source>
        <translation>%p%</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="63"/>
        <source>Ready.</source>
        <translation>準備完成。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="65"/>
        <source>You need to specify one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>您需要指定至少一個要匯入的%s 檔案。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="67"/>
        <source>You need to specify at least one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>您需要指定至少一個要匯入的 %s 檔。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="69"/>
        <source>You need to specify one %s folder to import from.</source>
        <comment>A song format e.g. PowerSong</comment>
        <translation>您需要指定至少一個要匯入的%s 資料夾。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="372"/>
        <source>Welcome to the Bible Import Wizard</source>
        <translation>歡迎使用聖經匯入精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="137"/>
        <source>Welcome to the Duplicate Song Removal Wizard</source>
        <translation>歡迎來到重複歌曲清除精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="144"/>
        <source>Welcome to the Song Export Wizard</source>
        <translation>歡迎使用歌曲匯出精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="135"/>
        <source>Welcome to the Song Import Wizard</source>
        <translation>歡迎使用歌曲匯入精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="33"/>
        <source>Author</source>
        <comment>Singular</comment>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="34"/>
        <source>Authors</source>
        <comment>Plural</comment>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="35"/>
        <source>Author Unknown</source>
        <translation>未知作者</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="37"/>
        <source>Songbook</source>
        <comment>Singular</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="38"/>
        <source>Songbooks</source>
        <comment>Plural</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="39"/>
        <source>Title and/or verses not found</source>
        <translation>找不到標題 和/或 段落</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="40"/>
        <source>Song Maintenance</source>
        <translation>歌曲維護</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="41"/>
        <source>Topic</source>
        <comment>Singular</comment>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="42"/>
        <source>Topics</source>
        <comment>Plural</comment>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="43"/>
        <source>XML syntax error</source>
        <translation>XML 語法錯誤</translation>
    </message>
</context>
<context>
    <name>OpenLP.core.lib</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="400"/>
        <source>{one} and {two}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="402"/>
        <source>{first} and {last}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenPL.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="499"/>
        <source>Other</source>
        <translation>O其他</translation>
    </message>
</context>
<context>
    <name>Openlp.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="119"/>
        <source>Source select dialog interface</source>
        <translation>選擇來源對話框界面</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin</name>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="168"/>
        <source>&lt;strong&gt;Presentation Plugin&lt;/strong&gt;&lt;br /&gt;The presentation plugin provides the ability to show presentations using a number of different programs. The choice of available presentation programs is available to the user in a drop down box.</source>
        <translation>&lt;strong&gt;簡報插件&lt;/strong&gt;&lt;br /&gt;簡報插件提供使用許多不同的程式來展示。用戶可在下拉式選單中選擇可使用的呈現程式。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="181"/>
        <source>Presentation</source>
        <comment>name singular</comment>
        <translation>簡報</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="182"/>
        <source>Presentations</source>
        <comment>name plural</comment>
        <translation>簡報</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="186"/>
        <source>Presentations</source>
        <comment>container title</comment>
        <translation>簡報</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="190"/>
        <source>Load a new presentation.</source>
        <translation>載入新的簡報。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="194"/>
        <source>Delete the selected presentation.</source>
        <translation>刪除所選的簡報。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="195"/>
        <source>Preview the selected presentation.</source>
        <translation>預覽所選擇的簡報。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="196"/>
        <source>Send the selected presentation live.</source>
        <translation>傳送選擇的簡報至現場Live.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="197"/>
        <source>Add the selected presentation to the service.</source>
        <translation>新增所選擇的簡報至聚會管理。</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="62"/>
        <source>Select Presentation(s)</source>
        <translation>選擇簡報</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="63"/>
        <source>Automatic</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="64"/>
        <source>Present using:</source>
        <translation>目前使用：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="92"/>
        <source>Presentations ({text})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>File Exists</source>
        <translation>檔案已存在</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>A presentation with that filename already exists.</source>
        <translation>已存在以這個檔案名稱的簡報。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="207"/>
        <source>This type of presentation is not supported.</source>
        <translation>不支援此格式的簡報。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>Missing Presentation</source>
        <translation>簡報遺失</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>The presentation {name} no longer exists.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="369"/>
        <source>The presentation {name} is incomplete, please reload.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PresentationPlugin.PowerpointDocument</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/powerpointcontroller.py" line="536"/>
        <source>An error occurred in the PowerPoint integration and the presentation will be stopped. Restart the presentation if you wish to present it.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PresentationPlugin.PresentationTab</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="105"/>
        <source>Available Controllers</source>
        <translation>可用控制器</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="111"/>
        <source>PDF options</source>
        <translation>PDF選項</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="112"/>
        <source>PowerPoint options</source>
        <translation>PowerPoint 選項</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="113"/>
        <source>Allow presentation application to be overridden</source>
        <translation>允許簡報應用程式被覆蓋</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="115"/>
        <source>Clicking on the current slide advances to the next effect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="118"/>
        <source>Let PowerPoint control the size and monitor of the presentations
(This may fix PowerPoint scaling issues in Windows 8 and 10)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="122"/>
        <source>Use given full path for mudraw or ghostscript binary:</source>
        <translation>指定完整的mudraw或Ghostscript二進制文件路徑：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="124"/>
        <source>Select mudraw or ghostscript binary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="131"/>
        <source>{name} (unavailable)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="228"/>
        <source>The program is not ghostscript or mudraw which is required.</source>
        <translation>此程式不是Ghostscript或mudraw 所必需的。</translation>
    </message>
</context>
<context>
    <name>RemotePlugin</name>
    <message>
        <location filename="../../openlp/core/api/http/server.py" line="159"/>
        <source>Importing Website</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RemotePlugin.Mobile</name>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="57"/>
        <source>Remote</source>
        <translation>遠端</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="58"/>
        <source>Stage View</source>
        <translation>舞台查看</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="59"/>
        <source>Live View</source>
        <translation>Live 顯示</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="60"/>
        <source>Chords View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="67"/>
        <source>Service Manager</source>
        <translation>聚會管理</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="68"/>
        <source>Slide Controller</source>
        <translation>幻燈片控制</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="69"/>
        <source>Alerts</source>
        <translation>警報</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="70"/>
        <source>Search</source>
        <translation>搜尋</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="71"/>
        <source>Home</source>
        <translation>首頁</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="72"/>
        <source>Refresh</source>
        <translation>重新整理</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="73"/>
        <source>Blank</source>
        <translation>顯示單色畫面</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="74"/>
        <source>Theme</source>
        <translation>佈景主題</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="75"/>
        <source>Desktop</source>
        <translation>桌面</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="76"/>
        <source>Show</source>
        <translation>顯示</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="77"/>
        <source>Prev</source>
        <translation>預覽</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="78"/>
        <source>Next</source>
        <translation>下一個</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="79"/>
        <source>Text</source>
        <translation>文字</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="80"/>
        <source>Show Alert</source>
        <translation>顯示警報</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="81"/>
        <source>Go Live</source>
        <translation>現場Live</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="82"/>
        <source>Add to Service</source>
        <translation>新增至聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="83"/>
        <source>Add &amp;amp; Go to Service</source>
        <translation>新增 &amp;amp; 到聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="84"/>
        <source>No Results</source>
        <translation>沒有結果</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="85"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="86"/>
        <source>Service</source>
        <translation>聚會</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="87"/>
        <source>Slides</source>
        <translation>幻燈片</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="88"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
</context>
<context>
    <name>RemotePlugin.RemoteTab</name>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="158"/>
        <source>Remote Interface</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="159"/>
        <source>Server Settings</source>
        <translation>伺服器設定</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="160"/>
        <source>Serve on IP address:</source>
        <translation>伺服器IP位址：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="161"/>
        <source>Port number:</source>
        <translation>連接埠：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="162"/>
        <source>Remote URL:</source>
        <translation>遠端URL：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="163"/>
        <source>Stage view URL:</source>
        <translation>舞台檢視 URL：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="164"/>
        <source>Live view URL:</source>
        <translation>Live 檢視 URL：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="165"/>
        <source>Chords view URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="166"/>
        <source>Display stage time in 12h format</source>
        <translation>以12小時制顯示現階段時間</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="167"/>
        <source>Show thumbnails of non-text slides in remote and stage view.</source>
        <translation>在遠端及舞台監看顯示縮圖。</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="169"/>
        <source>Remote App</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="170"/>
        <source>Scan the QR code or click &lt;a href=&quot;{qr}&quot;&gt;download&lt;/a&gt; to download an app for your mobile device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="174"/>
        <source>User Authentication</source>
        <translation>用戶認證</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="177"/>
        <source>User id:</source>
        <translation>使用者ID：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="178"/>
        <source>Password:</source>
        <translation>使用者密碼：</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="179"/>
        <source>Current Version number:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="180"/>
        <source>Latest Version number:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongPlugin.ReportSongList</name>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>Save File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>song_extract.csv</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>CSV format (*.csv)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report Creation</source>
        <translation>建立報告</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report 
{name} 
has been successfully created. </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>Song Extraction Failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>An error occurred while extracting: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongUsagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="90"/>
        <source>&amp;Song Usage Tracking</source>
        <translation>歌曲使用追蹤(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>&amp;Delete Tracking Data</source>
        <translation>刪除追蹤資料(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>Delete song usage data up to a specified date.</source>
        <translation>刪除指定日期的歌曲使用記錄。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>&amp;Extract Tracking Data</source>
        <translation>匯出追蹤資料(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>Generate a report on song usage.</source>
        <translation>產生歌曲使用記錄報告。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="104"/>
        <source>Toggle Tracking</source>
        <translation>切換追蹤</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="118"/>
        <source>Toggle the tracking of song usage.</source>
        <translation>切換追蹤歌曲使用記錄。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="154"/>
        <source>Song Usage</source>
        <translation>歌曲使用記錄</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="178"/>
        <source>Song usage tracking is active.</source>
        <translation>追蹤歌曲使用記錄。(已啟用)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="183"/>
        <source>Song usage tracking is inactive.</source>
        <translation>追蹤歌曲使用記錄。(未啟用)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="193"/>
        <source>display</source>
        <translation>顯示</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="201"/>
        <source>printed</source>
        <translation>列印</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="238"/>
        <source>&lt;strong&gt;SongUsage Plugin&lt;/strong&gt;&lt;br /&gt;This plugin tracks the usage of songs in services.</source>
        <translation>&lt;strong&gt;歌曲使用記錄插件&lt;/strong&gt;&lt;br /&gt;此插件追蹤在聚會裡歌曲的使用記錄。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="249"/>
        <source>SongUsage</source>
        <comment>name singular</comment>
        <translation>歌曲使用記錄</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="250"/>
        <source>SongUsage</source>
        <comment>name plural</comment>
        <translation>歌曲使用記錄</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="254"/>
        <source>SongUsage</source>
        <comment>container title</comment>
        <translation>歌曲使用記錄</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDeleteForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="64"/>
        <source>Delete Song Usage Data</source>
        <translation>刪除歌曲使用記錄</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="66"/>
        <source>Select the date up to which the song usage data should be deleted. 
All data recorded before this date will be permanently deleted.</source>
        <translation>選擇刪除在此日期之前的歌曲使用資料。在該日期之前的記錄下所有資料將會永久刪除。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Delete Selected Song Usage Events?</source>
        <translation>刪除選擇的歌曲使用事件記錄？</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Are you sure you want to delete selected Song Usage data?</source>
        <translation>您確定想要刪除選中的歌曲使用記錄？</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>Deletion Successful</source>
        <translation>刪除成功</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>All requested data has been deleted successfully.</source>
        <translation>所有要求的資料已成功刪除。</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDetailForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="85"/>
        <source>Song Usage Extraction</source>
        <translation>收集歌曲使用記錄</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="87"/>
        <source>Select Date Range</source>
        <translation>選擇時間範圍</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="88"/>
        <source>to</source>
        <translation>到</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="89"/>
        <source>Report Location</source>
        <translation>報告位置</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>Output Path Not Selected</source>
        <translation>未選擇輸出路徑</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>You have not set a valid output location for your song usage report.
Please select an existing path on your computer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="91"/>
        <source>usage_detail_{old}_{new}.txt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report Creation</source>
        <translation>建立報告</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report
{name}
has been successfully created.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>Report Creation Failed</source>
        <translation>報告建立錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>An error occurred while creating the report: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="301"/>
        <source>Arabic (CP-1256)</source>
        <translation>阿拉伯語 (CP-1256)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="302"/>
        <source>Baltic (CP-1257)</source>
        <translation>波羅的 (CP-1257)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="303"/>
        <source>Central European (CP-1250)</source>
        <translation>中歐 (CP-1250)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="304"/>
        <source>Cyrillic (CP-1251)</source>
        <translation>西里爾語 (CP-1251)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="305"/>
        <source>Greek (CP-1253)</source>
        <translation>希臘語 (CP-1253)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="306"/>
        <source>Hebrew (CP-1255)</source>
        <translation>希伯來語 (CP-1255)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="307"/>
        <source>Japanese (CP-932)</source>
        <translation>日語 (CP-932)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="308"/>
        <source>Korean (CP-949)</source>
        <translation>韓文 (CP-949)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="309"/>
        <source>Simplified Chinese (CP-936)</source>
        <translation>簡體中文 (CP-936)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="310"/>
        <source>Thai (CP-874)</source>
        <translation>泰語 (CP-874)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="311"/>
        <source>Traditional Chinese (CP-950)</source>
        <translation>繁體中文 (CP-950)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="312"/>
        <source>Turkish (CP-1254)</source>
        <translation>土耳其語 (CP-1254)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="313"/>
        <source>Vietnam (CP-1258)</source>
        <translation>越語 (CP-1258)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="314"/>
        <source>Western European (CP-1252)</source>
        <translation>西歐 (CP-1252)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Character Encoding</source>
        <translation>字元編碼</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="323"/>
        <source>The codepage setting is responsible
for the correct character representation.
Usually you are fine with the preselected choice.</source>
        <translation>編碼頁面設置正確的編碼來顯示字元。
通常您最好選擇預設的編碼。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Please choose the character encoding.
The encoding is responsible for the correct character representation.</source>
        <translation>請選擇字元編碼。
編碼負責顯示正確的字元。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>&amp;Song</source>
        <translation>歌曲(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="186"/>
        <source>Import songs using the import wizard.</source>
        <translation>使用匯入精靈來匯入歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>CCLI SongSelect</source>
        <translation>CCLI SongSelect</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>Import songs from CCLI&apos;s SongSelect service.</source>
        <translation>從 CCLI SongSelect 服務中匯入歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>Exports songs using the export wizard.</source>
        <translation>使用匯出精靈來匯出歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="223"/>
        <source>Songs</source>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>&amp;Re-index Songs</source>
        <translation>重建歌曲索引(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>Re-index the songs database to improve searching and ordering.</source>
        <translation>重建歌曲索引以提高搜尋及排序速度。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find &amp;Duplicate Songs</source>
        <translation>尋找重複的歌曲(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find and remove duplicate songs in the song database.</source>
        <translation>在歌曲資料庫中尋找並刪除重複的歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Song List Report</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Produce a CSV file of all the songs in the database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="259"/>
        <source>Reindexing songs...</source>
        <translation>重建歌曲索引中...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="261"/>
        <source>Reindexing songs</source>
        <translation>重建歌曲索引中</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="304"/>
        <source>&lt;strong&gt;Songs Plugin&lt;/strong&gt;&lt;br /&gt;The songs plugin provides the ability to display and manage songs.</source>
        <translation>&lt;strong&gt;歌曲插件&lt;/strong&gt;&lt;br /&gt;歌曲插件提供顯示及管理歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="347"/>
        <source>Song</source>
        <comment>name singular</comment>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="348"/>
        <source>Songs</source>
        <comment>name plural</comment>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="352"/>
        <source>Songs</source>
        <comment>container title</comment>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="358"/>
        <source>Add a new song.</source>
        <translation>新增一首歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="359"/>
        <source>Edit the selected song.</source>
        <translation>編輯所選擇的歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="360"/>
        <source>Delete the selected song.</source>
        <translation>刪除所選擇的歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="361"/>
        <source>Preview the selected song.</source>
        <translation>預覽所選擇的歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="362"/>
        <source>Send the selected song live.</source>
        <translation>傳送選擇的歌曲至現場Live。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="363"/>
        <source>Add the selected song to the service.</source>
        <translation>新增所選擇的歌曲到聚會。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="388"/>
        <source>Importing Songs</source>
        <translation>匯入歌曲中</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="67"/>
        <source>Words</source>
        <comment>Author who wrote the lyrics of a song</comment>
        <translation>作詞</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="68"/>
        <source>Music</source>
        <comment>Author who wrote the music of a song</comment>
        <translation>編曲</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="69"/>
        <source>Words and Music</source>
        <comment>Author who wrote both lyrics and music of a song</comment>
        <translation>詞曲</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="71"/>
        <source>Translation</source>
        <comment>Author who translated the song</comment>
        <translation>譯者</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="74"/>
        <source>Author Maintenance</source>
        <translation>作者維護</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="75"/>
        <source>Display name:</source>
        <translation>顯示名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="76"/>
        <source>First name:</source>
        <translation>姓氏：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="77"/>
        <source>Last name:</source>
        <translation>名字：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="92"/>
        <source>You need to type in the first name of the author.</source>
        <translation>您必須輸入作者姓氏。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="97"/>
        <source>You need to type in the last name of the author.</source>
        <translation>您必須輸入作者名字。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="102"/>
        <source>You have not set a display name for the author, combine the first and last names?</source>
        <translation>您尚未設定作者的顯示名稱，結合姓氏與名字？</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.CCLIFileImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/cclifile.py" line="84"/>
        <source>The file does not have a valid extension.</source>
        <translation>該檔案不具有效的副檔名。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.DreamBeamImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/dreambeam.py" line="101"/>
        <source>Invalid DreamBeam song file_path. Missing DreamSong tag.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.EasyWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="316"/>
        <source>Administered by {admin}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="393"/>
        <source>&quot;{title}&quot; could not be imported. {entry}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="206"/>
        <source>This file does not exist.</source>
        <translation>此檔案不存在。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="210"/>
        <source>Could not find the &quot;Songs.MB&quot; file. It must be in the same folder as the &quot;Songs.DB&quot; file.</source>
        <translation>找不到 &quot;Songs.DB&quot;檔。&quot;Songs.DB&quot;檔 必須放在相同的資料夾下。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="225"/>
        <source>This file is not a valid EasyWorship database.</source>
        <translation>此檔案不是有效的 EasyWorship 資料庫。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="254"/>
        <source>Could not retrieve encoding.</source>
        <translation>無法獲得編碼。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="337"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="350"/>
        <source>This does not appear to be a valid Easy Worship 6 database directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="352"/>
        <source>This is not a valid Easy Worship 6 database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="426"/>
        <source>Unexpected data formatting.</source>
        <translation>意外的檔案格式。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="431"/>
        <source>No song text found.</source>
        <translation>找不到歌曲文字。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="471"/>
        <source>
[above are Song Tags with notes imported from EasyWorship]</source>
        <translation>
[以上歌曲標籤匯入來自                          EasyWorship]</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="134"/>
        <source>Meta Data</source>
        <translation>後設資料(Meta Data)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="155"/>
        <source>Custom Book Names</source>
        <translation>自訂書卷名稱</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditSongForm</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="427"/>
        <source>&amp;Save &amp;&amp; Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="301"/>
        <source>Song Editor</source>
        <translation>歌曲編輯器</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="302"/>
        <source>&amp;Title:</source>
        <translation>標題(&amp;T)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="303"/>
        <source>Alt&amp;ernate title:</source>
        <translation>副標題(&amp;E):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="304"/>
        <source>&amp;Lyrics:</source>
        <translation>歌詞(&amp;L)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="305"/>
        <source>&amp;Verse order:</source>
        <translation>段落順序(&amp;V)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="308"/>
        <source>Ed&amp;it All</source>
        <translation>編輯全部(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="310"/>
        <source>Title &amp;&amp; Lyrics</source>
        <translation>標題 &amp;&amp; 歌詞</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="313"/>
        <source>&amp;Add to Song</source>
        <translation>新增至歌曲(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="314"/>
        <source>&amp;Edit Author Type</source>
        <translation>編輯作者類型(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="337"/>
        <source>&amp;Remove</source>
        <translation>移除(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="316"/>
        <source>&amp;Manage Authors, Topics, Songbooks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="318"/>
        <source>A&amp;dd to Song</source>
        <translation>新增至歌曲(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="319"/>
        <source>R&amp;emove</source>
        <translation>移除(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="321"/>
        <source>Add &amp;to Song</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="322"/>
        <source>Re&amp;move</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="323"/>
        <source>Authors, Topics &amp;&amp; Songbooks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="326"/>
        <source>New &amp;Theme</source>
        <translation>新增佈景主題(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="327"/>
        <source>Copyright Information</source>
        <translation>版權資訊</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="330"/>
        <source>Comments</source>
        <translation>評論</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="331"/>
        <source>Theme, Copyright Info &amp;&amp; Comments</source>
        <translation>佈景主題、版權資訊 &amp;&amp; 評論</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="333"/>
        <source>Linked Audio</source>
        <translation>連結聲音</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="335"/>
        <source>Add &amp;File(s)</source>
        <translation>加入檔案(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="336"/>
        <source>Add &amp;Media</source>
        <translation>加入媒體(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="338"/>
        <source>Remove &amp;All</source>
        <translation>移除全部(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="340"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; Not all of the verses are in use.</source>
        <translation>&lt;strong&gt;警告：&lt;/strong&gt; 沒有使用所有的歌詞段落。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="342"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; You have not entered a verse order.</source>
        <translation>&lt;strong&gt;警告：&lt;/strong&gt; 您還沒有輸入歌詞段落順序。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="202"/>
        <source>There are no verses corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="207"/>
        <source>There is no verse corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="211"/>
        <source>Invalid Verse Order</source>
        <translation>無效的段落順序</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="225"/>
        <source>You need to type in a song title.</source>
        <translation>您需要輸入歌曲標題。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="231"/>
        <source>You need to type in at least one verse.</source>
        <translation>您需要輸入至少一段歌詞。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="237"/>
        <source>You need to have an author for this song.</source>
        <translation>您需要給這首歌一位作者。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="256"/>
        <source>There are misplaced formatting tags in the following verses:

{tag}

Please correct these tags before continuing.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="265"/>
        <source>You have {count} verses named {name} {number}. You can have at most 26 verses with the same name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>Add Author</source>
        <translation>新增作者</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>This author does not exist, do you want to add them?</source>
        <translation>此作者不存在，您是否要新增？</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="603"/>
        <source>This author is already in the list.</source>
        <translation>此作者已存在於列表。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="610"/>
        <source>You have not selected a valid author. Either select an author from the list, or type in a new author and click the &quot;Add Author to Song&quot; button to add the new author.</source>
        <translation>您沒有選擇有效的作者。從列表中選擇一位作者，或輸入一位新作者名稱並點選&quot;將作者加入歌曲&quot;按鈕。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Edit Author Type</source>
        <translation>編輯作者類型</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Choose type for this author</source>
        <translation>為作者選擇一個類型</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>Add Topic</source>
        <translation>新增主題</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>This topic does not exist, do you want to add it?</source>
        <translation>此主題不存在，您是否要新增？</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="679"/>
        <source>This topic is already in the list.</source>
        <translation>此主題已存在於列表。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="688"/>
        <source>You have not selected a valid topic. Either select a topic from the list, or type in a new topic and click the &quot;Add Topic to Song&quot; button to add the new topic.</source>
        <translation>您沒有選擇有效的主題。從列表中選擇一個主題，或輸入一個新主題名稱並點選&quot;將主題加入歌曲&quot;按鈕。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>Add Songbook</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>This Songbook does not exist, do you want to add it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="724"/>
        <source>This Songbook is already in the list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="732"/>
        <source>You have not selected a valid Songbook. Either select a Songbook from the list, or type in a new Songbook and click the &quot;Add to Song&quot; button to add the new Songbook.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="932"/>
        <source>Open File(s)</source>
        <translation>開啟檔案</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="91"/>
        <source>Edit Verse</source>
        <translation>編輯段落</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="92"/>
        <source>&amp;Verse type:</source>
        <translation>段落類型(&amp;V)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="102"/>
        <source>&amp;Forced Split</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="103"/>
        <source>Split the verse when displayed regardless of the screen size.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="105"/>
        <source>&amp;Insert</source>
        <translation>插入(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="106"/>
        <source>Split a slide into two by inserting a verse splitter.</source>
        <translation>加入一個分頁符號將幻燈片分為兩頁。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="109"/>
        <source>Transpose:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="110"/>
        <source>Up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="111"/>
        <source>Down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="146"/>
        <source>Transposing failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="240"/>
        <source>Invalid Chord</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.ExportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="128"/>
        <source>Select Destination Folder</source>
        <translation>選擇目標資料夾</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="143"/>
        <source>Song Export Wizard</source>
        <translation>歌曲匯出精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="146"/>
        <source>This wizard will help to export your songs to the open and free &lt;strong&gt;OpenLyrics &lt;/strong&gt; worship song format.</source>
        <translation>這個精靈將幫助匯出您的歌曲為開放及免費的 &lt;strong&gt;OpenLyrics&lt;/strong&gt; 敬拜歌曲格式。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="149"/>
        <source>Select Songs</source>
        <translation>選擇歌曲</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="150"/>
        <source>Check the songs you want to export.</source>
        <translation>選擇您想匯出的歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="153"/>
        <source>Uncheck All</source>
        <translation>取消所有</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="154"/>
        <source>Check All</source>
        <translation>全選</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="155"/>
        <source>Select Directory</source>
        <translation>選擇目錄</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="156"/>
        <source>Select the directory where you want the songs to be saved.</source>
        <translation>選擇您希望儲存歌曲的目錄。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="158"/>
        <source>Directory:</source>
        <translation>目錄：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="159"/>
        <source>Exporting</source>
        <translation>匯出中</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="160"/>
        <source>Please wait while your songs are exported.</source>
        <translation>請稍等，您的歌曲正在匯出。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="176"/>
        <source>You need to add at least one Song to export.</source>
        <translation>您需要新增至少一首歌曲匯出。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>No Save Location specified</source>
        <translation>未指定儲存位置</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>You need to specify a directory.</source>
        <translation>您需要指定一個目錄。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="239"/>
        <source>Starting export...</source>
        <translation>開始匯出...</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.FoilPresenterSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/foilpresenter.py" line="387"/>
        <source>Invalid Foilpresenter song file. No verses found.</source>
        <translation>無效的Foilpresenter 歌曲檔。找不到段落。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="339"/>
        <source>Enable search as you type</source>
        <translation>啟用即時搜尋</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="134"/>
        <source>Song Import Wizard</source>
        <translation>歌曲匯入精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="137"/>
        <source>This wizard will help you to import songs from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>此精靈將幫助您從各種格式匯入歌曲。點擊 下一步 按鈕進入程序選擇要匯入的格式。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="152"/>
        <source>Add Files...</source>
        <translation>加入檔案...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="154"/>
        <source>Remove File(s)</source>
        <translation>移除檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="165"/>
        <source>Please wait while your songs are imported.</source>
        <translation>請稍等，您的歌曲正在匯入。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="169"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="170"/>
        <source>Save to File</source>
        <translation>儲存為檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="336"/>
        <source>Your Song import failed. {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="197"/>
        <source>This importer has been disabled.</source>
        <translation>匯入器已被禁用。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="209"/>
        <source>OpenLyrics Files</source>
        <translation>OpenLyrics 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="210"/>
        <source>OpenLyrics or OpenLP 2 Exported Song</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="217"/>
        <source>OpenLP 2 Databases</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="220"/>
        <source>Generic Document/Presentation</source>
        <translation>通用文件/簡報</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="223"/>
        <source>The generic document/presentation importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation>由於OpenLP 無法存取OpenOffice 或 LibreOffice，通用文件/簡報 匯入器已被禁止使用。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="226"/>
        <source>Select Document/Presentation Files</source>
        <translation>選擇 文件/簡報 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="232"/>
        <source>CCLI SongSelect Files</source>
        <translation>CCLI SongSelect 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="239"/>
        <source>ChordPro Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="246"/>
        <source>DreamBeam Song Files</source>
        <translation>DreamBeam Song 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="253"/>
        <source>EasySlides XML File</source>
        <translation>EasySlides XML 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="260"/>
        <source>EasyWorship Song Database</source>
        <translation>EasyWorship Song 資料庫</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="268"/>
        <source>EasyWorship 6 Song Data Directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="276"/>
        <source>EasyWorship Service File</source>
        <translation>EasyWorship Service 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="283"/>
        <source>Foilpresenter Song Files</source>
        <translation>Foilpresenter Song 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="291"/>
        <source>LiveWorship Database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="298"/>
        <source>LyriX Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="299"/>
        <source>LyriX (Exported TXT-files)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="306"/>
        <source>MediaShout Database</source>
        <translation>MediaShout 資料庫</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="307"/>
        <source>The MediaShout importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation>MediaShout 匯入器僅支援Windows。由於缺少 Pyrhon 模組已被禁用。如果您想要使用此匯入器，必須先安裝 &quot;pyodbc&quot; 模組。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="323"/>
        <source>OPS Pro database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="324"/>
        <source>The OPS Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="333"/>
        <source>PowerPraise Song Files</source>
        <translation>PowerPraise Song 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="340"/>
        <source>You need to specify a valid PowerSong 1.0 database folder.</source>
        <translation>您需要指定一個有效的 PowerSong 1.0資料庫 文件夾。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="347"/>
        <source>PresentationManager Song Files</source>
        <translation>PresentationManager Song 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="354"/>
        <source>ProPresenter Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="361"/>
        <source>Singing The Faith Exported Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="363"/>
        <source>First use Singing The Faith Electonic edition to export the song(s) in Text format.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="371"/>
        <source>SongBeamer Files</source>
        <translation>SongBeamer 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="379"/>
        <source>SongPro Text Files</source>
        <translation>SongPro 文字檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="380"/>
        <source>SongPro (Export File)</source>
        <translation>SongPro (匯入檔)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="381"/>
        <source>In SongPro, export your songs using the File -&gt; Export menu</source>
        <translation>在 SongPro 中，使用 檔案 -&gt; 匯出 選單來匯出您的歌曲</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="388"/>
        <source>SongShow Plus Song Files</source>
        <translation>SongShow Plus 歌曲檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="395"/>
        <source>Songs Of Fellowship Song Files</source>
        <translation>Songs Of Fellowship 歌曲檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="397"/>
        <source>The Songs of Fellowship importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation>由於 OpenLP 無法存取 OpenOffice 或 LibreOffice ，Songs of Fellowship 匯入器已被禁用。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="405"/>
        <source>SundayPlus Song Files</source>
        <translation>SundayPlus 歌曲檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="412"/>
        <source>VideoPsalm Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="413"/>
        <source>VideoPsalm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="414"/>
        <source>The VideoPsalm songbooks are normally located in {path}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="422"/>
        <source>Words Of Worship Song Files</source>
        <translation>Words Of Worship 歌曲檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="430"/>
        <source>Worship Assistant Files</source>
        <translation>Worship Assistant 檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="432"/>
        <source>Worship Assistant (CSV)</source>
        <translation>Worship Assistant (CSV)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="433"/>
        <source>In Worship Assistant, export your Database to a CSV file.</source>
        <translation>在 Worship Assistant 中，匯出資料庫到CSV檔。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="441"/>
        <source>WorshipCenter Pro Song Files</source>
        <translation>WorshipCenter Pro 歌曲檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="443"/>
        <source>The WorshipCenter Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation>WorshipCenter Pro  匯入器僅支援Windows。由於缺少 Pyrhon 模組已被禁用。如果您想要使用此匯入器，必須先安裝 &quot;pyodbc&quot; 模組。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="453"/>
        <source>ZionWorx (CSV)</source>
        <translation>ZionWorx (CSV)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="454"/>
        <source>First convert your ZionWorx database to a CSV text file, as explained in the &lt;a href=&quot;http://manual.openlp.org/songs.html#importing-from-zionworx&quot;&gt;User Manual&lt;/a&gt;.</source>
        <translation>先將 Zion Worx 資料庫轉換成 CSV 文字檔，說明請看 &lt;a href=&quot;http://manual.openlp.org/songs.html#importing-from-zionworx&quot;&gt;使用手冊&lt;/a&gt;。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.LiveWorshipImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="87"/>
        <source>Extracting data from database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="133"/>
        <source>Could not find Valentina DB ADK libraries </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="161"/>
        <source>Loading the extracting data</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.LyrixImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>File {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>Error: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaFilesForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="65"/>
        <source>Select Media File(s)</source>
        <translation>選擇媒體檔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="66"/>
        <source>Select one or more audio files from the list below, and click OK to import them into this song.</source>
        <translation>在列表中選擇一或多個聲音檔，並點選 OK 將它們匯入到這首歌曲。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="721"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Titles</source>
        <translation>標題</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Titles...</source>
        <translation>搜尋標題...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="140"/>
        <source>Maintain the lists of authors, topics and books.</source>
        <translation>作者、主題及歌本列表維護。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Entire Song</source>
        <translation>整首歌曲</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Entire Song...</source>
        <translation>搜尋整首歌曲...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Lyrics</source>
        <translation>歌詞</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Lyrics...</source>
        <translation>搜尋歌詞...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Authors...</source>
        <translation>搜尋作者...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Topics...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Songbooks...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Copyright</source>
        <translation>版權</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Copyright...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>CCLI number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search CCLI number...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="512"/>
        <source>Are you sure you want to delete the following songs?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="540"/>
        <source>copy</source>
        <comment>For song cloning</comment>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="642"/>
        <source>Media</source>
        <translation>媒體</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="699"/>
        <source>CCLI License: </source>
        <translation>CCLI授權：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="729"/>
        <source>Failed to render Song footer html.
See log for details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaShoutImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/mediashout.py" line="62"/>
        <source>Unable to open the MediaShout database.</source>
        <translation>無法開啟 MediaShout 資料庫。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.OPSProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="65"/>
        <source>Unable to connect the OPS Pro database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="87"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLPSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openlp.py" line="109"/>
        <source>Not a valid OpenLP 2 song database.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLyricsExport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsexport.py" line="68"/>
        <source>Exporting &quot;{title}&quot;...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opensong.py" line="139"/>
        <source>Invalid OpenSong song file. Missing song tag.</source>
        <translation>無效的 OpenSong 檔案。缺少歌曲標籤。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.PowerSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No songs to import.</source>
        <translation>沒有歌曲匯入。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No {text} files found.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="118"/>
        <source>Invalid {text} file. Unexpected byte value.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="136"/>
        <source>Invalid {text} file. Missing &quot;TITLE&quot; header.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="142"/>
        <source>Invalid {text} file. Missing &quot;COPYRIGHTLINE&quot; header.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="148"/>
        <source>Verses not found. Missing &quot;PART&quot; header.</source>
        <translation>找不到詩歌。遺失 &quot;PART&quot; 標頭。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.PresentationManagerImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/presentationmanager.py" line="57"/>
        <source>File is not in XML-format, which is the only format supported.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SingingTheFaithImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="192"/>
        <source>Unknown hint {hint}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>File {file}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>Error: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongBookForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="66"/>
        <source>Songbook Maintenance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="67"/>
        <source>&amp;Name:</source>
        <translation>名稱(&amp;N)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="68"/>
        <source>&amp;Publisher:</source>
        <translation>出版者(&amp;P)：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookform.py" line="61"/>
        <source>You need to type in a name for the book.</source>
        <translation>您需要輸入歌本名稱。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongExportForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="253"/>
        <source>Finished export. To import these files use the &lt;strong&gt;OpenLyrics&lt;/strong&gt; importer.</source>
        <translation>匯出完成。使用&lt;strong&gt;OpenLyrics&lt;/strong&gt;匯入器匯入這些檔案。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="257"/>
        <source>Your song export failed.</source>
        <translation>您的歌曲匯出失敗。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="259"/>
        <source>Your song export failed because this error occurred: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="67"/>
        <source>Cannot access OpenOffice or LibreOffice</source>
        <translation>無法存取 OpenOffice 或 LibreOffice</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="82"/>
        <source>Unable to open file</source>
        <translation>無法開啟文件</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="84"/>
        <source>File not found</source>
        <translation>找不到檔案</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="104"/>
        <source>copyright</source>
        <translation>版權</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="118"/>
        <source>The following songs could not be imported:</source>
        <translation>無法匯入以下歌曲：</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongMaintenanceForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="248"/>
        <source>Could not add your author.</source>
        <translation>無法新增您的作者。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="251"/>
        <source>This author already exists.</source>
        <translation>此作者已存在。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="264"/>
        <source>Could not add your topic.</source>
        <translation>無法新增您的主題。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="267"/>
        <source>This topic already exists.</source>
        <translation>此主題已存在。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="281"/>
        <source>Could not add your book.</source>
        <translation>無法新增您的歌本。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="284"/>
        <source>This book already exists.</source>
        <translation>此歌本已存在。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="389"/>
        <source>Could not save your changes.</source>
        <translation>無法儲存變更。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="316"/>
        <source>The author {original} already exists. Would you like to make songs with author {new} use the existing author {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="329"/>
        <source>Could not save your modified author, because the author already exists.</source>
        <translation>無法儲存變更的作者，因為該作者已存在。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="352"/>
        <source>The topic {original} already exists. Would you like to make songs with topic {new} use the existing topic {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="362"/>
        <source>Could not save your modified topic, because it already exists.</source>
        <translation>無法儲存變更的主題，因為該主題已存在。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="391"/>
        <source>The book {original} already exists. Would you like to make songs with book {new} use the existing book {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Delete Author</source>
        <translation>刪除作者</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Are you sure you want to delete the selected author?</source>
        <translation>您確定想要刪除選中的作者嗎?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>This author cannot be deleted, they are currently assigned to at least one song.</source>
        <translation>此作者無法刪除，它目前至少被指派給一首歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Delete Topic</source>
        <translation>刪除主題</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Are you sure you want to delete the selected topic?</source>
        <translation>您確定想要刪除選中的主題嗎?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>This topic cannot be deleted, it is currently assigned to at least one song.</source>
        <translation>此主題無法刪除，它目前至少被指派給一首歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Delete Book</source>
        <translation>刪除歌本</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Are you sure you want to delete the selected book?</source>
        <translation>您確定想要刪除選中的歌本嗎?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>This book cannot be deleted, it is currently assigned to at least one song.</source>
        <translation>此歌本無法刪除，它目前至少被指派給一首歌曲。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongSelectForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="233"/>
        <source>CCLI SongSelect Importer</source>
        <translation>CCLI SongSelect 匯入器</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="234"/>
        <source>&lt;strong&gt;Note:&lt;/strong&gt; An Internet connection is required in order to import songs from CCLI SongSelect.</source>
        <translation>&lt;strong&gt;注意：&lt;/strong&gt; 網路連線是為了從 CCLI SongSelect.匯入歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="238"/>
        <source>Username:</source>
        <translation>帳號：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="239"/>
        <source>Password:</source>
        <translation>使用者密碼：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="240"/>
        <source>Save username and password</source>
        <translation>儲存帳號及密碼</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="241"/>
        <source>Login</source>
        <translation>登入</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="242"/>
        <source>Search Text:</source>
        <translation>搜尋文字：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="243"/>
        <source>Search</source>
        <translation>搜尋</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="244"/>
        <source>Stop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="346"/>
        <source>Found {count:d} song(s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="247"/>
        <source>Logout</source>
        <translation>登出</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="248"/>
        <source>View</source>
        <translation>檢視</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="249"/>
        <source>Title:</source>
        <translation>標題：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="250"/>
        <source>Author(s):</source>
        <translation>作者：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="251"/>
        <source>Copyright:</source>
        <translation>版權：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="252"/>
        <source>CCLI Number:</source>
        <translation>CCLI編號：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="253"/>
        <source>Lyrics:</source>
        <translation>歌詞：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="254"/>
        <source>Back</source>
        <translation>退後</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="255"/>
        <source>Import</source>
        <translation>匯入</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>More than 1000 results</source>
        <translation>超過 1000 條結果</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>Your search has returned more than 1000 results, it has been stopped. Please refine your search to fetch better results.</source>
        <translation>您的搜尋結果超過 1000 項，即將中止。請縮小您的搜尋範圍以獲得更精確的結果。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="139"/>
        <source>Logging out...</source>
        <translation>登出中...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>Incomplete song</source>
        <translation>歌曲不完整</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>This song is missing some information, like the lyrics, and cannot be imported.</source>
        <translation>這首歌遺失某些資訊以至於無法匯入，例如歌詞。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>Save Username and Password</source>
        <translation>儲存帳號及密碼</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>WARNING: Saving your username and password is INSECURE, your password is stored in PLAIN TEXT. Click Yes to save your password or No to cancel this.</source>
        <translation>警告： 儲存帳號及密碼是不安全的，您的密碼將儲存成簡單的文字(未加密)。 選擇 Yes 儲存密碼或 No 取消。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>Error Logging In</source>
        <translation>登入錯誤</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>There was a problem logging in, perhaps your username or password is incorrect?</source>
        <translation>登入錯誤，請確認您的帳號或密碼是否正確？</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>Free user</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>You logged in with a free account, the search will be limited to songs in the public domain.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Song Imported</source>
        <translation>歌曲匯入</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Your song has been imported, would you like to import more songs?</source>
        <translation>您的歌曲已匯入，您想匯入更多歌曲嗎？</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.SongsTab</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="116"/>
        <source>Song related settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="117"/>
        <source>Enable &quot;Go to verse&quot; button in Live panel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="119"/>
        <source>Update service from song edit</source>
        <translation>編輯歌詞更新聚會管理</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="120"/>
        <source>Import missing songs from Service files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="122"/>
        <source>Add Songbooks as first slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="124"/>
        <source>If enabled all text between &quot;[&quot; and &quot;]&quot; will be regarded as chords.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="126"/>
        <source>Chords</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="127"/>
        <source>Display chords in the main view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="128"/>
        <source>Ignore chords when importing songs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="130"/>
        <source>Chord notation to use:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="131"/>
        <source>English</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="132"/>
        <source>German</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="133"/>
        <source>Neo-Latin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="135"/>
        <source>Footer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="140"/>
        <source>Song Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="141"/>
        <source>Alternate Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="142"/>
        <source>Written By</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="143"/>
        <source>Authors when type is not set</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="145"/>
        <source>Authors (Type &quot;Words&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="147"/>
        <source>Authors (Type &quot;Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="149"/>
        <source>Authors (Type &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="151"/>
        <source>Authors (Type &quot;Translation&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="152"/>
        <source>Authors (Type &quot;Words&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="154"/>
        <source>Authors (Type &quot;Music&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="156"/>
        <source>Copyright information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="157"/>
        <source>Songbook Entries</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="159"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="160"/>
        <source>Song CCLI Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="161"/>
        <source>Topics</source>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Placeholder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Description</source>
        <translation>說明</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="172"/>
        <source>can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="173"/>
        <source>list of entries, can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="177"/>
        <source>How to use Footers:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Footer Template</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Mako Syntax</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="181"/>
        <source>Reset Template</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.TopicsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="60"/>
        <source>Topic Maintenance</source>
        <translation>主題管理</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="61"/>
        <source>Topic name:</source>
        <translation>主題名稱：</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsform.py" line="58"/>
        <source>You need to type in a topic name.</source>
        <translation>您需要輸入至少一個主題名稱。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.VerseType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="149"/>
        <source>Verse</source>
        <translation>V主歌</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="150"/>
        <source>Chorus</source>
        <translation>C副歌</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="151"/>
        <source>Bridge</source>
        <translation>B橋段</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="152"/>
        <source>Pre-Chorus</source>
        <translation>P導歌</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="153"/>
        <source>Intro</source>
        <translation>I前奏</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="154"/>
        <source>Ending</source>
        <translation>E結尾</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="155"/>
        <source>Other</source>
        <translation>O其他</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.VideoPsalmImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/videopsalm.py" line="134"/>
        <source>Error: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.WordsofWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/wordsofworship.py" line="177"/>
        <source>Invalid Words of Worship song file. Missing {text!r} header.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipAssistantImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Error reading CSV file.</source>
        <translation>讀取CSV文件錯誤。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Line {number:d}: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="182"/>
        <source>Record {count:d}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="122"/>
        <source>Decoding error: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="127"/>
        <source>File not valid WorshipAssistant CSV format.</source>
        <translation>檔案非有效的WorshipAssistant CSV格式。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipCenterProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipcenterpro.py" line="59"/>
        <source>Unable to connect the WorshipCenter Pro database.</source>
        <translation>無法連線到 WorshipCenter Pro 資料庫。</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.ZionWorxImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Error reading CSV file.</source>
        <translation>讀取CSV文件錯誤。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Line {number:d}: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Record {index}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Decoding error: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="106"/>
        <source>File not valid ZionWorx CSV format.</source>
        <translation>檔案非有效的ZionWorx CSV格式。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="120"/>
        <source>Record %d</source>
        <translation>錄製：%d</translation>
    </message>
</context>
<context>
    <name>Wizard</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="136"/>
        <source>Wizard</source>
        <translation>精靈</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="140"/>
        <source>This wizard will help you to remove duplicate songs from the song database. You will have a chance to review every potential duplicate song before it is deleted. So no songs will be deleted without your explicit approval.</source>
        <translation>此精靈將幫助您從歌曲資料庫中移除重複的歌曲。您將在重複的歌曲移除前重新檢視內容，所以不會有歌曲未經您的同意而刪除。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="145"/>
        <source>Searching for duplicate songs.</source>
        <translation>搜尋重覆歌曲中。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="146"/>
        <source>Please wait while your songs database is analyzed.</source>
        <translation>請稍等，您的歌曲資料庫正在分析。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="148"/>
        <source>Here you can decide which songs to remove and which ones to keep.</source>
        <translation>在這裡，你可以決定刪除並保留哪些的歌曲。</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="155"/>
        <source>Review duplicate songs ({current}/{total})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>Information</source>
        <translation>資訊</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>No duplicate songs have been found in the database.</source>
        <translation>資料庫中找不到重複的歌曲。</translation>
    </message>
</context>
<context>
    <name>common.languages</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>(Afan) Oromo</source>
        <comment>Language code: om</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Abkhazian</source>
        <comment>Language code: ab</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afar</source>
        <comment>Language code: aa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afrikaans</source>
        <comment>Language code: af</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Albanian</source>
        <comment>Language code: sq</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amharic</source>
        <comment>Language code: am</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amuzgo</source>
        <comment>Language code: amu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ancient Greek</source>
        <comment>Language code: grc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Arabic</source>
        <comment>Language code: ar</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Armenian</source>
        <comment>Language code: hy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Assamese</source>
        <comment>Language code: as</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Aymara</source>
        <comment>Language code: ay</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Azerbaijani</source>
        <comment>Language code: az</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bashkir</source>
        <comment>Language code: ba</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Basque</source>
        <comment>Language code: eu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bengali</source>
        <comment>Language code: bn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bhutani</source>
        <comment>Language code: dz</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bihari</source>
        <comment>Language code: bh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bislama</source>
        <comment>Language code: bi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Breton</source>
        <comment>Language code: br</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bulgarian</source>
        <comment>Language code: bg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Burmese</source>
        <comment>Language code: my</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Byelorussian</source>
        <comment>Language code: be</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cakchiquel</source>
        <comment>Language code: cak</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cambodian</source>
        <comment>Language code: km</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Catalan</source>
        <comment>Language code: ca</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Chinese</source>
        <comment>Language code: zh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Comaltepec Chinantec</source>
        <comment>Language code: cco</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Corsican</source>
        <comment>Language code: co</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Croatian</source>
        <comment>Language code: hr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Czech</source>
        <comment>Language code: cs</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Danish</source>
        <comment>Language code: da</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Dutch</source>
        <comment>Language code: nl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>English</source>
        <comment>Language code: en</comment>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Esperanto</source>
        <comment>Language code: eo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Estonian</source>
        <comment>Language code: et</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Faeroese</source>
        <comment>Language code: fo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Fiji</source>
        <comment>Language code: fj</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Finnish</source>
        <comment>Language code: fi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>French</source>
        <comment>Language code: fr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Frisian</source>
        <comment>Language code: fy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Galician</source>
        <comment>Language code: gl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Georgian</source>
        <comment>Language code: ka</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>German</source>
        <comment>Language code: de</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greek</source>
        <comment>Language code: el</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greenlandic</source>
        <comment>Language code: kl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Guarani</source>
        <comment>Language code: gn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Gujarati</source>
        <comment>Language code: gu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Haitian Creole</source>
        <comment>Language code: ht</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hausa</source>
        <comment>Language code: ha</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hebrew (former iw)</source>
        <comment>Language code: he</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hiligaynon</source>
        <comment>Language code: hil</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hindi</source>
        <comment>Language code: hi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hungarian</source>
        <comment>Language code: hu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Icelandic</source>
        <comment>Language code: is</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Indonesian (former in)</source>
        <comment>Language code: id</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingua</source>
        <comment>Language code: ia</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingue</source>
        <comment>Language code: ie</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inuktitut (Eskimo)</source>
        <comment>Language code: iu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inupiak</source>
        <comment>Language code: ik</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Irish</source>
        <comment>Language code: ga</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Italian</source>
        <comment>Language code: it</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Jakalteko</source>
        <comment>Language code: jac</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Japanese</source>
        <comment>Language code: ja</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Javanese</source>
        <comment>Language code: jw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>K&apos;iche&apos;</source>
        <comment>Language code: quc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kannada</source>
        <comment>Language code: kn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kashmiri</source>
        <comment>Language code: ks</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kazakh</source>
        <comment>Language code: kk</comment>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kekchí </source>
        <comment>Language code: kek</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kinyarwanda</source>
        <comment>Language code: rw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirghiz</source>
        <comment>Language code: ky</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirundi</source>
        <comment>Language code: rn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Korean</source>
        <comment>Language code: ko</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kurdish</source>
        <comment>Language code: ku</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Laothian</source>
        <comment>Language code: lo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latin</source>
        <comment>Language code: la</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latvian, Lettish</source>
        <comment>Language code: lv</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lingala</source>
        <comment>Language code: ln</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lithuanian</source>
        <comment>Language code: lt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Macedonian</source>
        <comment>Language code: mk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malagasy</source>
        <comment>Language code: mg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malay</source>
        <comment>Language code: ms</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malayalam</source>
        <comment>Language code: ml</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maltese</source>
        <comment>Language code: mt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mam</source>
        <comment>Language code: mam</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mri</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Marathi</source>
        <comment>Language code: mr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Moldavian</source>
        <comment>Language code: mo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mongolian</source>
        <comment>Language code: mn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nahuatl</source>
        <comment>Language code: nah</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nauru</source>
        <comment>Language code: na</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nepali</source>
        <comment>Language code: ne</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Norwegian</source>
        <comment>Language code: no</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Occitan</source>
        <comment>Language code: oc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Oriya</source>
        <comment>Language code: or</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Pashto, Pushto</source>
        <comment>Language code: ps</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Persian</source>
        <comment>Language code: fa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Plautdietsch</source>
        <comment>Language code: pdt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Polish</source>
        <comment>Language code: pl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Portuguese</source>
        <comment>Language code: pt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Punjabi</source>
        <comment>Language code: pa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Quechua</source>
        <comment>Language code: qu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Rhaeto-Romance</source>
        <comment>Language code: rm</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Romanian</source>
        <comment>Language code: ro</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Russian</source>
        <comment>Language code: ru</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Samoan</source>
        <comment>Language code: sm</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sangro</source>
        <comment>Language code: sg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sanskrit</source>
        <comment>Language code: sa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Scots Gaelic</source>
        <comment>Language code: gd</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbian</source>
        <comment>Language code: sr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbo-Croatian</source>
        <comment>Language code: sh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sesotho</source>
        <comment>Language code: st</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Setswana</source>
        <comment>Language code: tn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Shona</source>
        <comment>Language code: sn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sindhi</source>
        <comment>Language code: sd</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Singhalese</source>
        <comment>Language code: si</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Siswati</source>
        <comment>Language code: ss</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovak</source>
        <comment>Language code: sk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovenian</source>
        <comment>Language code: sl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Somali</source>
        <comment>Language code: so</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Spanish</source>
        <comment>Language code: es</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sudanese</source>
        <comment>Language code: su</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swahili</source>
        <comment>Language code: sw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swedish</source>
        <comment>Language code: sv</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tagalog</source>
        <comment>Language code: tl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tajik</source>
        <comment>Language code: tg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tamil</source>
        <comment>Language code: ta</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tatar</source>
        <comment>Language code: tt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tegulu</source>
        <comment>Language code: te</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Thai</source>
        <comment>Language code: th</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tibetan</source>
        <comment>Language code: bo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tigrinya</source>
        <comment>Language code: ti</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tonga</source>
        <comment>Language code: to</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tsonga</source>
        <comment>Language code: ts</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkish</source>
        <comment>Language code: tr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkmen</source>
        <comment>Language code: tk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Twi</source>
        <comment>Language code: tw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uigur</source>
        <comment>Language code: ug</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ukrainian</source>
        <comment>Language code: uk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Urdu</source>
        <comment>Language code: ur</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uspanteco</source>
        <comment>Language code: usp</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uzbek</source>
        <comment>Language code: uz</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Vietnamese</source>
        <comment>Language code: vi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Volapuk</source>
        <comment>Language code: vo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Welch</source>
        <comment>Language code: cy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Wolof</source>
        <comment>Language code: wo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Xhosa</source>
        <comment>Language code: xh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yiddish (former ji)</source>
        <comment>Language code: yi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yoruba</source>
        <comment>Language code: yo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zhuang</source>
        <comment>Language code: za</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zulu</source>
        <comment>Language code: zu</comment>
        <translation type="unfinished"/>
    </message>
</context>
</TS>
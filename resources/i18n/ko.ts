<?xml version="1.0" ?><!DOCTYPE TS><TS language="ko" version="2.0">
<context>
    <name>AlertsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>&amp;Alert</source>
        <translation>알림</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="163"/>
        <source>Show an alert message.</source>
        <translation>알림 메세지를 보여줍니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="212"/>
        <source>&lt;strong&gt;Alerts Plugin&lt;/strong&gt;&lt;br /&gt;The alert plugin controls the displaying of alerts on the display screen.</source>
        <translation>&lt;strong&gt;알림 플러그인&lt;/strong&gt;&lt;br /&gt;알림 플러그인은 화면상의 알림 표시를 설정합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="222"/>
        <source>Alert</source>
        <comment>name singular</comment>
        <translation>알림</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="223"/>
        <source>Alerts</source>
        <comment>name plural</comment>
        <translation>알림</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/alertsplugin.py" line="227"/>
        <source>Alerts</source>
        <comment>container title</comment>
        <translation>알림</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertForm</name>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="93"/>
        <source>Alert Message</source>
        <translation>알림 메세지</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="94"/>
        <source>Alert &amp;text:</source>
        <translation>알림 문구(&amp;T):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="95"/>
        <source>&amp;Parameter:</source>
        <translation>매개변수(&amp;P):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="96"/>
        <source>&amp;New</source>
        <translation>새로 만들기(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="97"/>
        <source>&amp;Save</source>
        <translation>저장(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="98"/>
        <source>Displ&amp;ay</source>
        <translation>화면 표시(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertdialog.py" line="99"/>
        <source>Display &amp;&amp; Cl&amp;ose</source>
        <translation>화면 표시 후 닫기(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>New Alert</source>
        <translation>새 알림</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="108"/>
        <source>You haven't specified any text for your alert. 
Please type in some text before clicking New.</source>
        <translation>알림 표시 문구를 지정하지 않았습니다. 
새로 만들기를 누르기 전에 문구를 입력하세요.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>No Parameter Found</source>
        <translation>매개변수가 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="179"/>
        <source>You have not entered a parameter to be replaced.
Do you want to continue anyway?</source>
        <translation>변경할 매개변수를 입력하지 않았습니다.
계속 진행할까요?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>No Placeholder Found</source>
        <translation>플레이스 홀더가 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/forms/alertform.py" line="190"/>
        <source>The alert text does not contain '&lt;&gt;'.
Do you want to continue anyway?</source>
        <translation>알림문에  &apos;&lt;&gt;&apos;가 없습니다.
계속 진행할까요?</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsManager</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertsmanager.py" line="73"/>
        <source>Alert message created and displayed.</source>
        <translation>알림 메세지를 만들고 표시했습니다.</translation>
    </message>
</context>
<context>
    <name>AlertsPlugin.AlertsTab</name>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="119"/>
        <source>Font Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="120"/>
        <source>Font name:</source>
        <translation>글꼴:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="121"/>
        <source>Font color:</source>
        <translation>글꼴색:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="123"/>
        <source>Font size:</source>
        <translation>글꼴 크기:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="125"/>
        <source>Background Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="126"/>
        <source>Other Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="127"/>
        <source>Alert timeout:</source>
        <translation>경고 지속시간:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="129"/>
        <source>Repeat (no. of times):</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/alerts/lib/alertstab.py" line="130"/>
        <source>Enable Scrolling</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin</name>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="125"/>
        <source>&amp;Bible</source>
        <translation>성경(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="141"/>
        <source>&lt;strong&gt;Bible Plugin&lt;/strong&gt;&lt;br /&gt;The Bible plugin provides the ability to display Bible verses from different sources during the service.</source>
        <translation>&lt;strong&gt;성경 플러그인&lt;/ STRONG&gt; &lt;br /&gt; 성경플러그인은 예배 중에 서로 다른 원본에서 성경 구절을 불러와 표시 하는 기능을 제공합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="174"/>
        <source>Bible</source>
        <comment>name singular</comment>
        <translation>성경</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="175"/>
        <source>Bibles</source>
        <comment>name plural</comment>
        <translation>성경</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="179"/>
        <source>Bibles</source>
        <comment>container title</comment>
        <translation>성경</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="184"/>
        <source>Import a Bible.</source>
        <translation>성경 가져오기.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="185"/>
        <source>Add a new Bible.</source>
        <translation>새성경 추가하기.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="186"/>
        <source>Edit the selected Bible.</source>
        <translation>선택한 성경 수정하기.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="187"/>
        <source>Delete the selected Bible.</source>
        <translation>선택한 성경 삭제하기.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="188"/>
        <source>Preview the selected Bible.</source>
        <translation>선택한 성경 미리보기.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="189"/>
        <source>Send the selected Bible live.</source>
        <translation>선택한 성경 실황로 보내기.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/bibleplugin.py" line="190"/>
        <source>Add the selected Bible to the service.</source>
        <translation>선택한 성경 예배 항목으로 보내기.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="76"/>
        <source>Genesis</source>
        <translation>창세기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="77"/>
        <source>Exodus</source>
        <translation>출애굽기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="78"/>
        <source>Leviticus</source>
        <translation>레위기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="79"/>
        <source>Numbers</source>
        <translation>민수기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="80"/>
        <source>Deuteronomy</source>
        <translation>신명기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="81"/>
        <source>Joshua</source>
        <translation>여호수아</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="82"/>
        <source>Judges</source>
        <translation>사사기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="83"/>
        <source>Ruth</source>
        <translation>룻기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="84"/>
        <source>1 Samuel</source>
        <translation>사무엘상</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="85"/>
        <source>2 Samuel</source>
        <translation>사무엘하</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="86"/>
        <source>1 Kings</source>
        <translation>열왕기상</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="87"/>
        <source>2 Kings</source>
        <translation>열왕기하</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="88"/>
        <source>1 Chronicles</source>
        <translation>역대기상</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="89"/>
        <source>2 Chronicles</source>
        <translation>역대기하</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="90"/>
        <source>Ezra</source>
        <translation>에스라</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="91"/>
        <source>Nehemiah</source>
        <translation>느헤미야</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="92"/>
        <source>Esther</source>
        <translation>에스더</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="93"/>
        <source>Job</source>
        <translation>욥기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="94"/>
        <source>Psalms</source>
        <translation>시편</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="95"/>
        <source>Proverbs</source>
        <translation>잠언</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="96"/>
        <source>Ecclesiastes</source>
        <translation>전도서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="97"/>
        <source>Song of Solomon</source>
        <translation>아가</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="98"/>
        <source>Isaiah</source>
        <translation>이사야</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="99"/>
        <source>Jeremiah</source>
        <translation>예레미야</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="100"/>
        <source>Lamentations</source>
        <translation>예레미야애가</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="101"/>
        <source>Ezekiel</source>
        <translation>에스겔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="102"/>
        <source>Daniel</source>
        <translation>다니엘</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="103"/>
        <source>Hosea</source>
        <translation>호세아</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="104"/>
        <source>Joel</source>
        <translation>요엘</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="105"/>
        <source>Amos</source>
        <translation>아모스</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="106"/>
        <source>Obadiah</source>
        <translation>오바댜</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="107"/>
        <source>Jonah</source>
        <translation>요나</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="108"/>
        <source>Micah</source>
        <translation>미가</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="109"/>
        <source>Nahum</source>
        <translation>나훔</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="110"/>
        <source>Habakkuk</source>
        <translation>하박국</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="111"/>
        <source>Zephaniah</source>
        <translation>스바냐</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="112"/>
        <source>Haggai</source>
        <translation>학개</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="113"/>
        <source>Zechariah</source>
        <translation>스가랴</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="114"/>
        <source>Malachi</source>
        <translation>말라기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="115"/>
        <source>Matthew</source>
        <translation>마태복음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="116"/>
        <source>Mark</source>
        <translation>마가복음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="117"/>
        <source>Luke</source>
        <translation>누가복음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="118"/>
        <source>John</source>
        <translation>요한복음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="119"/>
        <source>Acts</source>
        <translation>사도행전</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="120"/>
        <source>Romans</source>
        <translation>로마서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="121"/>
        <source>1 Corinthians</source>
        <translation>고린도전서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="122"/>
        <source>2 Corinthians</source>
        <translation>고린도후서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="123"/>
        <source>Galatians</source>
        <translation>갈라디아서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="124"/>
        <source>Ephesians</source>
        <translation>에베소서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="125"/>
        <source>Philippians</source>
        <translation>빌립보서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="126"/>
        <source>Colossians</source>
        <translation>골로새서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="127"/>
        <source>1 Thessalonians</source>
        <translation>데살로니가전서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="128"/>
        <source>2 Thessalonians</source>
        <translation>데살로니가후서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="129"/>
        <source>1 Timothy</source>
        <translation>디모데전서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="130"/>
        <source>2 Timothy</source>
        <translation>디모데후서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="131"/>
        <source>Titus</source>
        <translation>디도서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="132"/>
        <source>Philemon</source>
        <translation>빌레몬서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="133"/>
        <source>Hebrews</source>
        <translation>히브리서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="134"/>
        <source>James</source>
        <translation>야고보서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="135"/>
        <source>1 Peter</source>
        <translation>베드로전서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="136"/>
        <source>2 Peter</source>
        <translation>베드로후서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="137"/>
        <source>1 John</source>
        <translation>요한1서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="138"/>
        <source>2 John</source>
        <translation>요한2서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="139"/>
        <source>3 John</source>
        <translation>요한3서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="140"/>
        <source>Jude</source>
        <translation>유다서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="141"/>
        <source>Revelation</source>
        <translation>요한계시록</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="142"/>
        <source>Judith</source>
        <translation>유딧기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="143"/>
        <source>Wisdom</source>
        <translation>지혜서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="144"/>
        <source>Tobit</source>
        <translation>토빗기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="145"/>
        <source>Sirach</source>
        <translation>집회서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="146"/>
        <source>Baruch</source>
        <translation>바룩서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="147"/>
        <source>1 Maccabees</source>
        <translation>마카베오기 상권</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="148"/>
        <source>2 Maccabees</source>
        <translation>마카베오기 하권 </translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="149"/>
        <source>3 Maccabees</source>
        <translation>마카베오기 제 3서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="150"/>
        <source>4 Maccabees</source>
        <translation>마카베오기 제 4서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="151"/>
        <source>Rest of Daniel</source>
        <translation>다니엘 속편</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="152"/>
        <source>Rest of Esther</source>
        <translation>에스더 속편</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="153"/>
        <source>Prayer of Manasses</source>
        <translation>므나쎄의 기도</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="154"/>
        <source>Letter of Jeremiah</source>
        <translation>예레미야서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="155"/>
        <source>Prayer of Azariah</source>
        <translation>아자리아의 기도와 세 청년의 찬송</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="156"/>
        <source>Susanna</source>
        <translation>수산나</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="157"/>
        <source>Bel</source>
        <translation>벨</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="158"/>
        <source>1 Esdras</source>
        <translation>에스드라 1서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="159"/>
        <source>2 Esdras</source>
        <translation>에스드라 2서</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>:</source>
        <comment>Verse identifier e.g. Genesis 1 : 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>v</source>
        <comment>Verse identifier e.g. Genesis 1 v 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>절</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>V</source>
        <comment>Verse identifier e.g. Genesis 1 V 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>절</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verse</source>
        <comment>Verse identifier e.g. Genesis 1 verse 1 = Genesis Chapter 1 Verse 1</comment>
        <translation>절</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="168"/>
        <source>verses</source>
        <comment>Verse identifier e.g. Genesis 1 verses 1 - 2 = Genesis Chapter 1 Verses 1 to 2</comment>
        <translation>절</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>-</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="175"/>
        <source>to</source>
        <comment>range identifier e.g. Genesis 1 verse 1 - 2 = Genesis Chapter 1 Verses 1 To 2</comment>
        <translation>~</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>,</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2, 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>,</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="180"/>
        <source>and</source>
        <comment>connecting identifier e.g. Genesis 1 verse 1 - 2 and 4 - 5 = Genesis Chapter 1 Verses 1 To 2 And Verses 4 To 5</comment>
        <translation>와</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/__init__.py" line="185"/>
        <source>end</source>
        <comment>ending identifier e.g. Genesis 1 verse 1 - end = Genesis Chapter 1 Verses 1 To The Last Verse</comment>
        <translation>마침</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No Book Found</source>
        <translation>성서 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="673"/>
        <source>No matching book could be found in this Bible. Check that you have spelled the name of the book correctly.</source>
        <translation>일치하는 이름을 가진 성서를 성경에서 찾을 수 없습니다. 이름을 올바르게 입력했는지 확인하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="64"/>
        <source>The proxy server {proxy} was found in the bible {name}.&lt;br&gt;Would you like to set it as the proxy for OpenLP?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/upgrade.py" line="69"/>
        <source>both</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleEditForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="162"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>성경의 버전 이름을 지정해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="168"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>성경에 대한 저작권을 설정해야합니다. 공개 도메인에있는 성경은 공개 도메인으로 표시해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>Bible Exists</source>
        <translation>이미있는 성경 입니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="176"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>이 성경은 이미 존재합니다. 다른 성경을 가져 오거나 먼저 기존의 성경을 삭제 해주세요.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="190"/>
        <source>You need to specify a book name for &quot;{text}&quot;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="197"/>
        <source>The book name &quot;{name}&quot; is not correct.
Numbers can only be used at the beginning and must
be followed by one or more non-numeric characters.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>Duplicate Book Name</source>
        <translation>서명 복사</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="210"/>
        <source>The Book Name &quot;{name}&quot; has been entered more than once.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="53"/>
        <source>The file &quot;{file}&quot; you supplied is compressed. You must decompress it before import.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="224"/>
        <source>unknown type of</source>
        <comment>This looks like an unknown type of XML bible.</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BibleManager</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Web Bible cannot be used in Text Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/manager.py" line="329"/>
        <source>Text Search is not available with Web Bibles.
Please use the Scripture Reference Search instead.

This means that the currently selected Bible is a Web Bible.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>Scripture Reference Error</source>
        <translation>성경 참조 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="792"/>
        <source>&lt;strong&gt;The reference you typed is invalid!&lt;br&gt;&lt;br&gt;Please make sure that your reference follows one of these patterns:&lt;/strong&gt;&lt;br&gt;&lt;br&gt;%s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BiblesTab</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="178"/>
        <source>Verse Display</source>
        <translation>구절 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="179"/>
        <source>Show verse numbers</source>
        <translation>구절 번호 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="180"/>
        <source>Only show new chapter numbers</source>
        <translation>새로운 장 번호만 보이기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="183"/>
        <source>Bible theme:</source>
        <translation>성경 테마:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="187"/>
        <source>No Brackets</source>
        <translation>괄호 숨기기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="189"/>
        <source>( And )</source>
        <translation>( 와 )</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="191"/>
        <source>{ And }</source>
        <translation>{ 와 }</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="193"/>
        <source>[ And ]</source>
        <translation>[ 와 ]</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="195"/>
        <source>Note: Changes do not affect verses in the Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="197"/>
        <source>Display second Bible verses</source>
        <translation>두 번째 성경 구절을 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="198"/>
        <source>Custom Scripture References</source>
        <translation>사용자 정의 성경 참조</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="199"/>
        <source>Verse separator:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="200"/>
        <source>Range separator:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="201"/>
        <source>List separator:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="202"/>
        <source>End mark:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="203"/>
        <source>Multiple alternative verse separators may be defined.
They have to be separated by a vertical bar &quot;|&quot;.
Please clear this edit line to use the default value.</source>
        <translation>여러 대응 절 구분자를 지정할 수 있습니다.
수직 막대 &quot;|&quot;를 사용하여 구분할 수 있습니다.
기본값을 사용하려면 이 줄은 비워두십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="210"/>
        <source>Default Bible Language</source>
        <translation>기본 성경 언어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="211"/>
        <source>Book name language in search field,
search results and on display:</source>
        <translation>검색 입력창, 검색 결과, 표시 내용에서
사용할 책 이름 언어 이름입니다:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="213"/>
        <source>Bible Language</source>
        <translation>성경 언어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="215"/>
        <source>Application Language</source>
        <translation>프로그램 언어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="217"/>
        <source>English</source>
        <translation>Korean</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="219"/>
        <source>Quick Search Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="220"/>
        <source>Reset search type to &quot;Text or Scripture Reference&quot; on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="223"/>
        <source>Don&apos;t show error if nothing is found in &quot;Text or Scripture Reference&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/biblestab.py" line="226"/>
        <source>Search automatically while typing (Text search must contain a
minimum of {count} characters and a space for performance reasons)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="84"/>
        <source>Select Book Name</source>
        <translation>성경을 선택하세요</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="85"/>
        <source>The following book name cannot be matched up internally. Please select the corresponding name from the list.</source>
        <translation>다음 서명은 내부적으로 일치할 수 없습니다. 목록에서 관련 명칭을 선택하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="88"/>
        <source>Current name:</source>
        <translation>현재 이름 :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="89"/>
        <source>Corresponding name:</source>
        <translation>해당 이름 :</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="90"/>
        <source>Show Books From</source>
        <translation>다음 범위부터 성서 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="91"/>
        <source>Old Testament</source>
        <translation>구약</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="92"/>
        <source>New Testament</source>
        <translation>신약</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknamedialog.py" line="93"/>
        <source>Apocrypha</source>
        <translation>외경</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.BookNameForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/booknameform.py" line="109"/>
        <source>You need to select a book.</source>
        <translation>선경책을 선택해야합니다.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.CSVBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="123"/>
        <source>Importing books... {book}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/csvbible.py" line="145"/>
        <source>Importing verses from {book}...</source>
        <comment>Importing verses from &lt;book name&gt;...</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="132"/>
        <source>Bible Editor</source>
        <translation>성경 편집</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="136"/>
        <source>License Details</source>
        <translation>라이선스 정보</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="137"/>
        <source>Version name:</source>
        <translation>버전 이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="138"/>
        <source>Copyright:</source>
        <translation>저작권:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="139"/>
        <source>Permissions:</source>
        <translation>권한:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="140"/>
        <source>Full license:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="141"/>
        <source>Default Bible Language</source>
        <translation>기본 성경 언어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="142"/>
        <source>Book name language in search field, search results and on display:</source>
        <translation>검색 창의 서명 언어, 검색 결과, 화면에 나타낼 항목:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="145"/>
        <source>Global Settings</source>
        <translation>전역 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="146"/>
        <source>Bible Language</source>
        <translation>성경 언어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="149"/>
        <source>Application Language</source>
        <translation>프로그램 언어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="151"/>
        <source>English</source>
        <translation>Korean</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="93"/>
        <source>This is a Web Download Bible.
It is not possible to customize the Book Names.</source>
        <translation>웹 다운로드 성경입니다.
서명을 사용자가 멋대로 입력할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibleform.py" line="98"/>
        <source>To use the customized book names, &quot;Bible language&quot; must be selected on the Meta Data tab or, if &quot;Global settings&quot; is selected, on the Bible page in Configure OpenLP.</source>
        <translation>서명을 임의대로 입력하여 사용하려면, 미디어 데이터 탭에서 &quot;성경 언어&quot;를 선택해야하며, &quot;전역 설정&quot;을 선택했다면 OpenLP 설정의 성경 페이지에서 선택해야 합니다.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.HTTPBible</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="607"/>
        <source>Registering Bible and loading books...</source>
        <translation>성경을 등록하고 성서 불러오는 중...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="623"/>
        <source>Registering Language...</source>
        <translation>언어 등록 중...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="630"/>
        <source>Importing {book}...</source>
        <comment>Importing &lt;book name&gt;...</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>Download Error</source>
        <translation>다운로드 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="773"/>
        <source>There was a problem downloading your verse selection. Please check your Internet connection, and if this error continues to occur please consider reporting a bug.</source>
        <translation>선택한 구절 다운로드에 문제가 발생했습니다. 인터넷 연결을 확인하십시오. 이 오류가 계속 일어나면 버그를 보고하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>Parse Error</source>
        <translation>해석 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/http.py" line="779"/>
        <source>There was a problem extracting your verse selection. If this error continues to occur please consider reporting a bug.</source>
        <translation>선택한 구절 선별에 문제가 발생했습니다. 이 오류가 계속 일어나면 버그를 보고하십시오.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="192"/>
        <source>CSV File</source>
        <translation>CSV파일</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="371"/>
        <source>Bible Import Wizard</source>
        <translation>성경 가져오기 마법사</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="374"/>
        <source>This wizard will help you to import Bibles from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>이 마법사는 여러가지 형식의 성경을 가져오도록 안내해드립니다. 다음 버튼을 눌러서 가져올 성경의 형식을 선택하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="386"/>
        <source>Web Download</source>
        <translation>웹 다운로드</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="396"/>
        <source>Bible file:</source>
        <translation>성경파일:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="391"/>
        <source>Books file:</source>
        <translation>책 파일:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="392"/>
        <source>Verses file:</source>
        <translation>구절 파일:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="394"/>
        <source>Location:</source>
        <translation>위치:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="397"/>
        <source>Click to download bible list</source>
        <translation>성경 목록을 다운로드 하려면 클릭하십시오</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="398"/>
        <source>Download bible list</source>
        <translation>성경 다운로드 목록</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="399"/>
        <source>Crosswalk</source>
        <translation>Crosswalk</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="401"/>
        <source>BibleGateway</source>
        <translation>BibleGateway</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="403"/>
        <source>Bibleserver</source>
        <translation>성경서버</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="405"/>
        <source>Bible:</source>
        <translation>성경:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="409"/>
        <source>Bibles:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="407"/>
        <source>SWORD data folder:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="408"/>
        <source>SWORD zip-file:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="410"/>
        <source>Import from folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="412"/>
        <source>Import from Zip-file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="417"/>
        <source>To import SWORD bibles the pysword python module must be installed. Please read the manual for instructions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="420"/>
        <source>License Details</source>
        <translation>라이선스 정보</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="422"/>
        <source>Set up the Bible&apos;s license details.</source>
        <translation>성경의 라이센스 세부 사항을 설정합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="424"/>
        <source>Version name:</source>
        <translation>버전 이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="425"/>
        <source>Copyright:</source>
        <translation>저작권:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="426"/>
        <source>Permissions:</source>
        <translation>권한:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="427"/>
        <source>Full license:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="429"/>
        <source>Please wait while your Bible is imported.</source>
        <translation>성경 가져오기를 진행하는 동안 기다려주세요.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="461"/>
        <source>You need to specify a file with books of the Bible to use in the import.</source>
        <translation>불러오기에 쓸 성경 파일을 지정해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="468"/>
        <source>You need to specify a file of Bible verses to import.</source>
        <translation>불러올 수있는 성경 구절의 파일을 지정해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="526"/>
        <source>You need to specify a version name for your Bible.</source>
        <translation>성경의 버전 이름을 지정해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="532"/>
        <source>You need to set a copyright for your Bible. Bibles in the Public Domain need to be marked as such.</source>
        <translation>성경에 대한 저작권을 설정해야합니다. 공개 도메인에있는 성경은 공개 도메인으로 표시해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>Bible Exists</source>
        <translation>이미있는 성경 입니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="547"/>
        <source>This Bible already exists. Please import a different Bible or first delete the existing one.</source>
        <translation>이 성경은 이미 존재합니다. 다른 성경을 가져 오거나 먼저 기존의 성경을 삭제 해주세요.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>Error during download</source>
        <translation>다운로드 중 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="587"/>
        <source>An error occurred while downloading the list of bibles from %s.</source>
        <translation>%s에서 성경 목록을 다운로드하는 중 오류가 발생했습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="691"/>
        <source>Registering Bible...</source>
        <translation>성경 등록 중...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="757"/>
        <source>Registered Bible. Please note, that verses will be downloaded on demand and thus an internet connection is required.</source>
        <translation>성경을 등록했습니다. 각 구절은 요청할 때 다운로드하므로 인터넷 연결이 필요함을 참고하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="767"/>
        <source>Your Bible import failed.</source>
        <translation>성경 가져오기 실패.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageDialog</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="66"/>
        <source>Select Language</source>
        <translation>언어 선택하세요</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="68"/>
        <source>OpenLP is unable to determine the language of this translation of the Bible. Please select the language from the list below.</source>
        <translation>OpenLP에서 성경 번역 언어를 확인할 수 없습니다. 아래 항목 중 언어를 선택하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languagedialog.py" line="72"/>
        <source>Language:</source>
        <translation>언어:</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.LanguageForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/languageform.py" line="62"/>
        <source>You need to choose a language.</source>
        <translation>언어를 선택 하십시오.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="161"/>
        <source>Find</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="164"/>
        <source>Find:</source>
        <translation>찾기:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="170"/>
        <source>Select</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="178"/>
        <source>Sort bible books alphabetically.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="180"/>
        <source>Book:</source>
        <translation>책: </translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="192"/>
        <source>From:</source>
        <translation>부터:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="198"/>
        <source>To:</source>
        <translation>까지:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="204"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="209"/>
        <source>Second:</source>
        <translation>두번째:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="273"/>
        <source>Chapter:</source>
        <translation>장:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="274"/>
        <source>Verse:</source>
        <translation>절:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="278"/>
        <source>Clear the results on the current tab.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="279"/>
        <source>Add the search results to the saved list.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text or Reference...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Scripture Reference</source>
        <translation>성경 참조</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Scripture Reference...</source>
        <translation>성경 참조 검색...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Text Search</source>
        <translation>텍스트 검색</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="319"/>
        <source>Search Text...</source>
        <translation>문구 검색...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="458"/>
        <source>Are you sure you want to completely delete &quot;{bible}&quot; Bible from OpenLP?

You will need to re-import this Bible to use it again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="506"/>
        <source>Saved ({result_count})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="509"/>
        <source>Results ({result_count})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="588"/>
        <source>OpenLP cannot combine single and dual Bible verse search results. Do you want to clear your saved results?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="618"/>
        <source>Bible not fully loaded.</source>
        <translation>성경을 완전히 불러오지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>Verses not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/mediaitem.py" line="766"/>
        <source>The second Bible &quot;{second_name}&quot; does not contain all the verses that are in the main Bible &quot;{name}&quot;.
Only verses found in both Bibles will be shown.

{count:d} verses have not been included in the results.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.OsisImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="171"/>
        <source>Removing unused tags (this may take a few minutes)...</source>
        <translation>사용하지 않는 태그 제거 중(몇 분 걸립니다)...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/bibleimport.py" line="200"/>
        <source>Importing {book} {chapter}...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.Sword</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="88"/>
        <source>Importing {name}...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.SwordImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/sword.py" line="93"/>
        <source>An unexpected error happened while importing the SWORD bible, please report this to the OpenLP developers.
{error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BiblesPlugin.ZefaniaImport</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="90"/>
        <source>Incorrect Bible file type supplied. Zefania Bibles may be compressed. You must decompress them before import.</source>
        <translation>성경 파일 형식이 들어있는 내용과 다릅니다. Zefania 성경 형식은 압축 상태인 것 같습니다. 가져오기 전 압축을 풀어야합니다.</translation>
    </message>
</context>
<context>
    <name>BiblesPlugin.Zefnia</name>
    <message>
        <location filename="../../openlp/plugins/bibles/lib/importers/zefania.py" line="83"/>
        <source>Importing {book} {chapter}...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CustomPlugin</name>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="77"/>
        <source>&lt;strong&gt;Custom Slide Plugin &lt;/strong&gt;&lt;br /&gt;The custom slide plugin provides the ability to set up custom text slides that can be displayed on the screen the same way songs are. This plugin provides greater freedom over the songs plugin.</source>
        <translation>&lt;strong&gt;사용자 정의 슬라이드 플러그인 &lt;/strong&gt;&lt;br /&gt;사용자 정의 슬라이드 플러그인에서는 화면에 표시할 수 있는 가사 문구 슬라이드 설정 기능을 제공합니다. 이 플러그인은 가사 플러그인 이상의 자유도를 제공합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="109"/>
        <source>Custom Slide</source>
        <comment>name singular</comment>
        <translation>사용자 지정 슬라이드</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="110"/>
        <source>Custom Slides</source>
        <comment>name plural</comment>
        <translation>사용자 지정 슬라이드</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="114"/>
        <source>Custom Slides</source>
        <comment>container title</comment>
        <translation>사용자 지정 슬라이드</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="118"/>
        <source>Load a new custom slide.</source>
        <translation>새 사용자 지정 슬라이드를 불러옵니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="119"/>
        <source>Import a custom slide.</source>
        <translation>사용자 지정 슬라이드를 가져옵니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="120"/>
        <source>Add a new custom slide.</source>
        <translation>새 사용자 지정 슬라이드를 추가합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="121"/>
        <source>Edit the selected custom slide.</source>
        <translation>선택한 사용자 지정 슬라이드를 편집합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="122"/>
        <source>Delete the selected custom slide.</source>
        <translation>사용자 지정 슬라이드를 삭제합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="123"/>
        <source>Preview the selected custom slide.</source>
        <translation>선택한 사용자 지정 슬라이드를 미리 봅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="124"/>
        <source>Send the selected custom slide live.</source>
        <translation>선택한 사용자 지정 슬라이드 실시간으로 보냅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/customplugin.py" line="125"/>
        <source>Add the selected custom slide to the service.</source>
        <translation>서비스에 선택한 사용자 정의 슬라이드를 추가합니다.</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.CustomTab</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="56"/>
        <source>Custom Display</source>
        <translation>사용자 지정 디스플레이</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="57"/>
        <source>Display footer</source>
        <translation>디스플레이 꼬리말</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/customtab.py" line="58"/>
        <source>Import missing custom slides from service files</source>
        <translation>예배 내용 파일에서 빠진 사용자 정의 슬라이드를 가져옵니다</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditCustomForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="104"/>
        <source>Edit Custom Slides</source>
        <translation>사용자 지정 슬라이드 수정</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="105"/>
        <source>&amp;Title:</source>
        <translation>제목(&amp;T):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="107"/>
        <source>Add a new slide at bottom.</source>
        <translation>맨 아래에 새 슬라이드를 추가.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="109"/>
        <source>Edit the selected slide.</source>
        <translation>선택한 슬라이드를 수정합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="110"/>
        <source>Ed&amp;it All</source>
        <translation>모두 편집(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="111"/>
        <source>Edit all the slides at once.</source>
        <translation>한 번에 모든 슬라이드를 편집</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="112"/>
        <source>The&amp;me:</source>
        <translation>테마(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomdialog.py" line="113"/>
        <source>&amp;Credits:</source>
        <translation>만든 사람(&amp;C):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="239"/>
        <source>You need to type in a title.</source>
        <translation>제목을 입력해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomform.py" line="243"/>
        <source>You need to add at least one slide.</source>
        <translation>최소한 하나의 슬라이드를 추가해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="50"/>
        <source>Insert Slide</source>
        <translation>슬라이드 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="51"/>
        <source>Split a slide into two by inserting a slide splitter.</source>
        <translation>슬라이드 분할선을 삽입하여 두 슬라이드로 나눕니다.</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/custom/forms/editcustomslidedialog.py" line="47"/>
        <source>Edit Slide</source>
        <translation>스라이드 수정</translation>
    </message>
</context>
<context>
    <name>CustomPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="200"/>
        <source>Are you sure you want to delete the &quot;{items:d}&quot; selected custom slide(s)?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/custom/lib/mediaitem.py" line="261"/>
        <source>copy</source>
        <comment>For item cloning</comment>
        <translation>복사하기</translation>
    </message>
</context>
<context>
    <name>ImagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="70"/>
        <source>&lt;strong&gt;Image Plugin&lt;/strong&gt;&lt;br /&gt;The image plugin provides displaying of images.&lt;br /&gt;One of the distinguishing features of this plugin is the ability to group a number of images together in the service manager, making the displaying of multiple images easier. This plugin can also make use of OpenLP&apos;s &quot;timed looping&quot; feature to create a slide show that runs automatically. In addition to this, images from the plugin can be used to override the current theme&apos;s background, which renders text-based items like songs with the selected image as a background instead of the background provided by the theme.</source>
        <translation>&lt;strong&gt;그림 플러그인&lt;/ STRONG&gt;&lt;br /&gt;그림 플러그인은 그림 표시 기능을 제공합니다.&lt;br /&gt;이 플러그인의 차별적 특징 중 하나는 서비스 관리자에서 여러 그림을 하나로 쉽게 묶어 화면에 표시합니다. 또한 자동으로 실행하는 슬라이드 쇼를 할 수 있도록 &quot;일정 시간 반복&quot; 기능을 사용할 수 있습니다. 플러그인의 그림은 현재 테마의 바탕 화면으로 바꿀 수 있으며, 이 바탕 화면은 테마에서 제공하는 배경 대신 선택한 이미지와 함께 화면에 붙어있는 텍스트 기반 노래 항목을 표시합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="89"/>
        <source>Image</source>
        <comment>name singular</comment>
        <translation>그림</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="90"/>
        <source>Images</source>
        <comment>name plural</comment>
        <translation>그림</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="93"/>
        <source>Images</source>
        <comment>container title</comment>
        <translation>그림</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="96"/>
        <source>Add new image(s).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="98"/>
        <source>Add a new image.</source>
        <translation>새 그림을 추가합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="99"/>
        <source>Edit the selected image.</source>
        <translation>선택한 그림을 편집합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="100"/>
        <source>Delete the selected image.</source>
        <translation>선택한 그림을 삭제합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="101"/>
        <source>Preview the selected image.</source>
        <translation>선택한 그림을 미리 봅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="102"/>
        <source>Send the selected image live.</source>
        <translation>선택한 그림을 실황으로 보냅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/imageplugin.py" line="103"/>
        <source>Add the selected image to the service.</source>
        <translation>선택한 그림을 예배 항목에 추가합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="191"/>
        <source>Add new image(s)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ImagePlugin.AddGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="54"/>
        <source>Add group</source>
        <translation>모음 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="55"/>
        <source>Parent group:</source>
        <translation>상위 모음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupdialog.py" line="56"/>
        <source>Group name:</source>
        <translation>모음 이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="67"/>
        <source>You need to type in a group name.</source>
        <translation>모음 이름을 입력해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="655"/>
        <source>Could not add the new group.</source>
        <translation>새 모음을 추가할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="658"/>
        <source>This group already exists.</source>
        <translation>이 모음이 이미 있습니다.</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ChooseGroupForm</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="82"/>
        <source>Select Image Group</source>
        <translation>그림 모음 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="83"/>
        <source>Add images to group:</source>
        <translation>그림을 모음에 추가:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="84"/>
        <source>No group</source>
        <translation>모음 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="85"/>
        <source>Existing group</source>
        <translation>기존의 모음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/forms/choosegroupdialog.py" line="86"/>
        <source>New group</source>
        <translation>새 모음</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="170"/>
        <source>Select Attachment</source>
        <translation>첨부 선택</translation>
    </message>
</context>
<context>
    <name>ImagePlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/images/forms/addgroupform.py" line="54"/>
        <source>-- Top-level group --</source>
        <translation>-- 상위 모음 --</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="81"/>
        <source>Select Image(s)</source>
        <translation>그림 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="243"/>
        <source>You must select an image or group to delete.</source>
        <translation>삭제할 그림 또는 모음을 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Remove group</source>
        <translation>모음 제거</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="260"/>
        <source>Are you sure you want to remove &quot;{name}&quot; and everything in it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>Missing Image(s)</source>
        <translation>그림 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="596"/>
        <source>The following image(s) no longer exist: {names}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="602"/>
        <source>The following image(s) no longer exist: {names}
Do you want to add the other images anyway?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="679"/>
        <source>You must select an image to replace the background with.</source>
        <translation>바탕 화면으로 바꿀 그림을 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="693"/>
        <source>There was no display item to amend.</source>
        <translation>새로 고칠 표시 항목이 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="697"/>
        <source>There was a problem replacing your background, the image file &quot;{name}&quot; no longer exists.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ImagesPlugin.ImageTab</name>
    <message>
        <location filename="../../openlp/plugins/images/lib/imagetab.py" line="63"/>
        <source>Visible background for images with aspect ratio different to screen.</source>
        <translation>화면과 다른 비율의 그림을 바탕 화면에 표시</translation>
    </message>
</context>
<context>
    <name>MediaPlugin</name>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="81"/>
        <source>&lt;strong&gt;Media Plugin&lt;/strong&gt;&lt;br /&gt;The media plugin provides playback of audio and video.</source>
        <translation>&lt;strong&gt;미디어 플러그인&lt;/strong&gt;&lt;br /&gt;음악과 동영상을 재생하는 미디어 플러그인입니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="91"/>
        <source>Media</source>
        <comment>name singular</comment>
        <translation>미디어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="92"/>
        <source>Media</source>
        <comment>name plural</comment>
        <translation>미디어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="96"/>
        <source>Media</source>
        <comment>container title</comment>
        <translation>미디어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="100"/>
        <source>Load new media.</source>
        <translation>새 미디어를 불러옵니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="102"/>
        <source>Add new media.</source>
        <translation>새 미디어를 추가합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="103"/>
        <source>Edit the selected media.</source>
        <translation>선택한 미디어를 수정합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="104"/>
        <source>Delete the selected media.</source>
        <translation>선택한 미디어를 삭제합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="105"/>
        <source>Preview the selected media.</source>
        <translation>선택한 미디어를 미리 봅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="106"/>
        <source>Send the selected media live.</source>
        <translation>선택한 미디어를 실황으로 보냅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/mediaplugin.py" line="107"/>
        <source>Add the selected media to the service.</source>
        <translation>선택한 미디어를 서비스에 추가합니다.</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelector</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="184"/>
        <source>Select Media Clip</source>
        <translation>미디어 클립 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="185"/>
        <source>Source</source>
        <translation>원본</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="186"/>
        <source>Media path:</source>
        <translation>미디어 경로:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="187"/>
        <source>Select drive from list</source>
        <translation>목록에서 드라이브 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="189"/>
        <source>Load disc</source>
        <translation>디스크 불러오기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="190"/>
        <source>Track Details</source>
        <translation>트랙 세부정보</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="191"/>
        <source>Title:</source>
        <translation>제목:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="192"/>
        <source>Audio track:</source>
        <translation>오디오 트랙:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="193"/>
        <source>Subtitle track:</source>
        <translation>부제 트랙:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="201"/>
        <source>HH:mm:ss.z</source>
        <translation>HH:mm:ss.z</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="195"/>
        <source>Clip Range</source>
        <translation>클립 범위</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="196"/>
        <source>Start point:</source>
        <translation>시작점:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="198"/>
        <source>Set start point</source>
        <translation>시작점 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="199"/>
        <source>Jump to start point</source>
        <translation>시작점으로 건너뛰기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="200"/>
        <source>End point:</source>
        <translation>끝점:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="202"/>
        <source>Set end point</source>
        <translation>끝점 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectordialog.py" line="203"/>
        <source>Jump to end point</source>
        <translation>끝점으로 건너뛰기</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaClipSelectorForm</name>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="195"/>
        <source>No path was given</source>
        <translation>경로를 지정하지 않음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="201"/>
        <source>Given path does not exists</source>
        <translation>주어진 경로가 없습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="218"/>
        <source>An error happened during initialization of VLC player</source>
        <translation>VLC 재생기 초기화 중 오류가 발생했습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="239"/>
        <source>VLC player failed playing the media</source>
        <translation>VLC 재생기가 미디어 재생에 실패했습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>CD not loaded correctly</source>
        <translation>CD를 올바르게 불러오지 않음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="568"/>
        <source>The CD was not loaded correctly, please re-load and try again.</source>
        <translation>CD를 올바르게 불러오지 않았습니다. 다시 불러오신후 시도하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>DVD not loaded correctly</source>
        <translation>DVD를 올바르게 불러오지 않음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="581"/>
        <source>The DVD was not loaded correctly, please re-load and try again.</source>
        <translation>DVD를 올바르게 불러오지 않았습니다. 다시 불러오신후 시도하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Set name of mediaclip</source>
        <translation>미디어 클립 이름 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="592"/>
        <source>Name of mediaclip:</source>
        <translation>미디어 클립 이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="602"/>
        <source>Enter a valid name or cancel</source>
        <translation>올바른 이름을 입력하거나 취소하십시오</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>Invalid character</source>
        <translation>잘못된 문자</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/forms/mediaclipselectorform.py" line="608"/>
        <source>The name of the mediaclip must not contain the character &quot;:&quot;</source>
        <translation>미디어 클립 이름에 &quot;:&quot; 문자가 들어있으면 안됩니다</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="290"/>
        <source>Unsupported File</source>
        <translation>지원하지 않는 파일</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="93"/>
        <source>Select Media</source>
        <translation>미디어 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="123"/>
        <source>Load CD/DVD</source>
        <translation>CD/DVD 불러오기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>Missing Media File</source>
        <translation>미디어 파일 사라짐</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="155"/>
        <source>The optical disc {name} is no longer available.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="172"/>
        <source>The file {name} no longer exists.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="206"/>
        <source>Videos ({video});;Audio ({audio});;{files} (*)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="216"/>
        <source>You must select a media file to delete.</source>
        <translation>삭제할 미디어 파일을 선택해야 합니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="233"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="237"/>
        <source>Show Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>Mediaclip already saved</source>
        <translation>미디어 클립을 이미 저장함</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/media/lib/mediaitem.py" line="327"/>
        <source>This mediaclip has already been saved</source>
        <translation>미디어 클립을 이미 저장했습니다</translation>
    </message>
</context>
<context>
    <name>MediaPlugin.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="74"/>
        <source>Video:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="76"/>
        <source>Audio:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="95"/>
        <source>Live Media</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="96"/>
        <source>Stream Media Command</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="97"/>
        <source>VLC arguments</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="98"/>
        <source>Start Live items automatically</source>
        <translation>라이브 항목을 자동으로 시작</translation>
    </message>
</context>
<context>
    <name>OpenLP</name>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>Data Directory Error</source>
        <translation>데이터 디렉터리 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="162"/>
        <source>OpenLP data folder was not found in:

{path}

The location of the data folder was previously changed from the OpenLP's default location. If the data was stored on removable device, that device needs to be made available.

You may reset the data location back to the default location, or you can try to make the current location available.

Do you want to reset to the default data location? If not, OpenLP will be closed so you can try to fix the the problem.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="238"/>
        <source>Backup</source>
        <translation>백업</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="221"/>
        <source>OpenLP has been upgraded, do you want to create
a backup of the old data folder?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="232"/>
        <source>Backup of the data folder failed!</source>
        <translation>데이터 폴더 백업에 실패했습니다!</translation>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="235"/>
        <source>A backup of the data folder has been created at:

{text}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings Upgrade</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="426"/>
        <source>Your settings are about to be upgraded. A backup will be created at {back_up_path}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/app.py" line="433"/>
        <source>Settings back up failed.

Continuining to upgrade.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/__init__.py" line="415"/>
        <source>Image Files</source>
        <translation>이미지 파일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Open</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="79"/>
        <source>Video Files</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.AboutForm</name>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="103"/>
        <source>&lt;p&gt;OpenLP {{version}}{{revision}} - Open Source Lyrics Projection&lt;br&gt;Copyright {crs} 2004-{yr} OpenLP Developers&lt;/p&gt;&lt;p&gt;Find out more about OpenLP: &lt;a href=&quot;https://openlp.org/&quot;&gt;https://openlp.org/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;You should have received a copy of the GNU General Public License along with this program.  If not, see &lt;a href=&quot;https://www.gnu.org/licenses/&quot;&gt;https://www.gnu.org/licenses/&lt;/a&gt;.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="118"/>
        <source>OpenLP is written and maintained by volunteers all over the world in their spare time. If you would like to see this project succeed, please consider contributing to it by clicking the &quot;contribute&quot; button below.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="122"/>
        <source>OpenLP would not be possible without the following software libraries:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="155"/>
        <source>&lt;h3&gt;Final credit:&lt;/h3&gt;&lt;blockquote&gt;&lt;p&gt;For God so loved the world that He gave His one and only Son, so that whoever believes in Him will not perish but inherit eternal life.&lt;/p&gt;&lt;p&gt;John 3:16&lt;/p&gt;&lt;/blockquote&gt;&lt;p&gt;And last but not least, final credit goes to God our Father, for sending His Son to die on the cross, setting us free from sin. We bring this software to you for free because He has set us free.&lt;/p&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="164"/>
        <source>Credits</source>
        <translation>만든 사람</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="744"/>
        <source>License</source>
        <translation>라이선스</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutdialog.py" line="746"/>
        <source>Contribute</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/aboutform.py" line="57"/>
        <source> build {version}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.AdvancedTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="59"/>
        <source>Advanced</source>
        <translation>고급 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="252"/>
        <source>UI Settings</source>
        <translation>UI 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="253"/>
        <source>Data Location</source>
        <translation>데이터 위치</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="254"/>
        <source>Number of recent service files to display:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="255"/>
        <source>Open the last used Library tab on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="257"/>
        <source>Double-click to send items straight to Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="259"/>
        <source>Preview items when clicked in Library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="261"/>
        <source>Preview items when clicked in Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="263"/>
        <source>Expand new service items on creation</source>
        <translation>새 예배 내용 항목을 만들 때 확장</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="265"/>
        <source>Max height for non-text slides
in slide controller:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="267"/>
        <source>Disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="268"/>
        <source>Automatic</source>
        <translation>자동</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="269"/>
        <source>When changing slides:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="271"/>
        <source>Do not auto-scroll</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="272"/>
        <source>Auto-scroll the previous slide into view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="274"/>
        <source>Auto-scroll the previous slide to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="276"/>
        <source>Auto-scroll the previous slide to middle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="278"/>
        <source>Auto-scroll the current slide into view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="280"/>
        <source>Auto-scroll the current slide to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="282"/>
        <source>Auto-scroll the current slide to middle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="284"/>
        <source>Auto-scroll the current slide to bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="286"/>
        <source>Auto-scroll the next slide into view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="288"/>
        <source>Auto-scroll the next slide to top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="290"/>
        <source>Auto-scroll the next slide to middle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="292"/>
        <source>Auto-scroll the next slide to bottom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="294"/>
        <source>Enable application exit confirmation</source>
        <translation>프로그램 나가기 확인 활성화</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="299"/>
        <source>Use dark style (needs restart)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="300"/>
        <source>Default Service Name</source>
        <translation>기본 예배 내용 이름</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="301"/>
        <source>Enable default service name</source>
        <translation>기본 예배 내용 이름 활성화</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="302"/>
        <source>Date and Time:</source>
        <translation>날짜와 시간:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="303"/>
        <source>Monday</source>
        <translation>월요일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="304"/>
        <source>Tuesday</source>
        <translation>화요일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="305"/>
        <source>Wednesday</source>
        <translation>수요일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="306"/>
        <source>Thursday</source>
        <translation>목요일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="307"/>
        <source>Friday</source>
        <translation>금요일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="308"/>
        <source>Saturday</source>
        <translation>토요일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="309"/>
        <source>Sunday</source>
        <translation>일요일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="310"/>
        <source>Now</source>
        <translation>지금</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="311"/>
        <source>Time when usual service starts.</source>
        <translation>일반적으로 예배를 시작하는 시간입니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="312"/>
        <source>Name:</source>
        <translation>명칭:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="313"/>
        <source>Consult the OpenLP manual for usage.</source>
        <translation>사용 방법은 OpenLP 설명서를 참고하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="314"/>
        <source>Revert to the default service name &quot;{name}&quot;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="317"/>
        <source>Example:</source>
        <translation>예:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="318"/>
        <source>Hide mouse cursor when over display window</source>
        <translation>표시 창 위로 커서가 지나갈 때 마우스 커서 숨김</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="319"/>
        <source>Path:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="320"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="321"/>
        <source>Cancel OpenLP data directory location change.</source>
        <translation>OpenLP 데이터 디렉토리 변경을 취소합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="323"/>
        <source>Copy data to new location.</source>
        <translation>데이터를 새로운 위치로 복사합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="324"/>
        <source>Copy the OpenLP data files to the new location.</source>
        <translation>OpenLP 데이터를 새로운 위치로 복사합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="326"/>
        <source>&lt;strong&gt;WARNING:&lt;/strong&gt; New data directory location contains OpenLP data files.  These files WILL be replaced during a copy.</source>
        <translation>&lt;strong&gt;경고:&lt;/strong&gt; 새로운 위치에 OpenLP 데이터 파일이 있습니다. 복사 하는 동안 해당 파일은 바뀝니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="329"/>
        <source>Display Workarounds</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="330"/>
        <source>Ignore Aspect Ratio</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="331"/>
        <source>Bypass X11 Window Manager</source>
        <translation>X11 창 관리자 건너뛰기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="332"/>
        <source>Use alternating row colours in lists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="490"/>
        <source>Syntax error.</source>
        <translation>문법 오류.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Confirm Data Directory Change</source>
        <translation>데이터 디렉터리 바꿈 확인</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="524"/>
        <source>Are you sure you want to change the location of the OpenLP data directory to:

{path}

The data directory will be changed when OpenLP is closed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="559"/>
        <source>Overwrite Existing Data</source>
        <translation>기존 데이터를 덮어씁니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>Restart Required</source>
        <translation>다시 시작 필요</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="599"/>
        <source>This change will only take effect once OpenLP has been restarted.</source>
        <translation>여기서 바꾼 설정은 OpenLP를 다시 시작했을 때 적용됩니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="186"/>
        <source>Select Logo File</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ColorButton</name>
    <message>
        <location filename="../../openlp/core/widgets/buttons.py" line="44"/>
        <source>Click to select a color.</source>
        <translation>색상을 선택하려면 클릭하세요.</translation>
    </message>
</context>
<context>
    <name>OpenLP.DB</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="542"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="543"/>
        <source>Video</source>
        <translation>동영상</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="544"/>
        <source>Digital</source>
        <translation>디지털</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="545"/>
        <source>Storage</source>
        <translation>저장소</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="546"/>
        <source>Network</source>
        <translation>네트워크</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="547"/>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="551"/>
        <source>1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="552"/>
        <source>2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="553"/>
        <source>3</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="554"/>
        <source>4</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="555"/>
        <source>5</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="556"/>
        <source>6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="557"/>
        <source>7</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="558"/>
        <source>8</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="559"/>
        <source>9</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="560"/>
        <source>A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="561"/>
        <source>B</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="562"/>
        <source>C</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="563"/>
        <source>D</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="564"/>
        <source>E</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="565"/>
        <source>F</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="566"/>
        <source>G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="567"/>
        <source>H</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="568"/>
        <source>I</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="569"/>
        <source>J</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="570"/>
        <source>K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="571"/>
        <source>L</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="572"/>
        <source>M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="573"/>
        <source>N</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="574"/>
        <source>O</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="575"/>
        <source>P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="576"/>
        <source>Q</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="577"/>
        <source>R</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="578"/>
        <source>S</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="579"/>
        <source>T</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="580"/>
        <source>U</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="581"/>
        <source>V</source>
        <translation>절</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="582"/>
        <source>W</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="583"/>
        <source>X</source>
        <translation>가로</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="584"/>
        <source>Y</source>
        <translation>세로</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="585"/>
        <source>Z</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.DisplayWindow</name>
    <message>
        <location filename="../../openlp/core/display/window.py" line="121"/>
        <source>Display Window</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionDialog</name>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="94"/>
        <source>Error Occurred</source>
        <translation>오류 발생</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="96"/>
        <source>&lt;strong&gt;Please describe what you were trying to do.&lt;/strong&gt; &amp;nbsp;If possible, write in English.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="99"/>
        <source>&lt;strong&gt;Oops, OpenLP hit a problem and couldn&apos;t recover!&lt;br&gt;&lt;br&gt;You can help &lt;/strong&gt; the OpenLP developers to &lt;strong&gt;fix this&lt;/strong&gt; by&lt;br&gt; sending them a &lt;strong&gt;bug report to {email}&lt;/strong&gt;{newlines}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="105"/>
        <source>{first_part}&lt;strong&gt;No email app? &lt;/strong&gt; You can &lt;strong&gt;save&lt;/strong&gt; this information to a &lt;strong&gt;file&lt;/strong&gt; and&lt;br&gt;send it from your &lt;strong&gt;mail on browser&lt;/strong&gt; via an &lt;strong&gt;attachment.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;&lt;strong&gt;Thank you&lt;/strong&gt; for being part of making OpenLP better!&lt;br&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="112"/>
        <source>Send E-Mail</source>
        <translation>이메일 보내기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="113"/>
        <source>Save to File</source>
        <translation>파일로 저장하기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptiondialog.py" line="114"/>
        <source>Attach File</source>
        <translation>파일 첨부</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>Failed to Save Report</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="115"/>
        <source>The following error occured when saving the report.

{exception}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="153"/>
        <source>&lt;strong&gt;Thank you for your description!&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="157"/>
        <source>&lt;strong&gt;Tell us what you were doing when this happened.&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="161"/>
        <source>&lt;strong&gt;Please enter a more detailed description of the situation&lt;/strong&gt;</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ExceptionForm</name>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="77"/>
        <source>Platform: {platform}
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Save Crash Report</source>
        <translation>오류 보고서 저장</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/exceptionform.py" line="98"/>
        <source>Text files (*.txt *.log *.text)</source>
        <translation>텍스트 파일 (*.txt *.log *.text)</translation>
    </message>
</context>
<context>
    <name>OpenLP.FileRenameForm</name>
    <message>
        <location filename="../../openlp/core/ui/filerenamedialog.py" line="60"/>
        <source>New File Name:</source>
        <translation>새 파일 이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="55"/>
        <source>File Copy</source>
        <translation>파일 복사</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/filerenameform.py" line="57"/>
        <source>File Rename</source>
        <translation>파일 이름 바꾸기</translation>
    </message>
</context>
<context>
    <name>OpenLP.FirstTimeLanguageForm</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="68"/>
        <source>Select Translation</source>
        <translation>번역 언어 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="69"/>
        <source>Choose the translation you&apos;d like to use in OpenLP.</source>
        <translation>OpenLP에서 사용할 번역 언어를 선택하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimelanguagedialog.py" line="71"/>
        <source>Translation:</source>
        <translation>번역</translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>OpenLP.FirstTimeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="520"/>
        <source>Network Error</source>
        <translation>네트워크 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="177"/>
        <source>There was a network error attempting to connect to retrieve initial configuration information</source>
        <translation>초기 설정 정보를 가져오려 연결하려던 중 네트워크 오류가 발생했습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="184"/>
        <source>Downloading {name}...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>Invalid index file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="217"/>
        <source>OpenLP was unable to read the resource index file. Please try again later.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>Download Error</source>
        <translation>다운로드 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="417"/>
        <source>There was a connection problem during download, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="429"/>
        <source>Setting Up And Downloading</source>
        <translation>설정 및 다운로드 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="430"/>
        <source>Please wait while OpenLP is set up and your data is downloaded.</source>
        <translation>OpenLP를 설정하고 데이터를 다운로드하는 동안 기다리십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="434"/>
        <source>Setting Up</source>
        <translation>설정하는 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="448"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="451"/>
        <source>Download complete. Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="455"/>
        <source>Click the &apos;{finish_button}&apos; button to return to OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="457"/>
        <source>Click the &apos;{finish_button}&apos; button to start OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="470"/>
        <source>There was a connection problem while downloading, so further downloads will be skipped. Try to re-run the First Time Wizard later.</source>
        <translation>다운로드 중 연결에 문제가 있어 건너뛰었습니다. 나중에 첫 단계 마법사를 다시 실행하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimeform.py" line="521"/>
        <source>Unable to download some files</source>
        <translation>일부 파일을 다운로드할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="262"/>
        <source>First Time Wizard</source>
        <translation>초기 마법사</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="263"/>
        <source>Welcome to the First Time Wizard</source>
        <translation>첫 단계 마법사입니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="265"/>
        <source>This wizard will help you to configure OpenLP for initial use. Click the &apos;{next_button}&apos; button below to start.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="269"/>
        <source>Internet Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="271"/>
        <source>Downloading Resource Index</source>
        <translation>자원 색인 다운로드 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="272"/>
        <source>Please wait while the resource index is downloaded.</source>
        <translation>자원 색인을 다운로드하는 동안 기다리십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="274"/>
        <source>Please wait while OpenLP downloads the resource index file...</source>
        <translation>OpenLP 에서 자원 색인 파일을 다운로드하는 동안 기다리십시오...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="276"/>
        <source>Select parts of the program you wish to use</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="277"/>
        <source>You can also change these settings after the Wizard.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="279"/>
        <source>Displays</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="280"/>
        <source>Choose the main display screen for OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="282"/>
        <source>Songs</source>
        <translation>곡</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="283"/>
        <source>Custom Slides – Easier to manage than songs and they have their own list of slides</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="286"/>
        <source>Bibles – Import and show Bibles</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="287"/>
        <source>Images – Show images or replace background with them</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="289"/>
        <source>Presentations – Show .ppt, .odp and .pdf files</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="291"/>
        <source>Media – Playback of Audio and Video files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="292"/>
        <source>Song Usage Monitor</source>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="293"/>
        <source>Alerts – Display informative messages while showing other slides</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="295"/>
        <source>Resource Data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="296"/>
        <source>Can OpenLP download some resource data?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="297"/>
        <source>OpenLP has collected some resources that we have permission to distribute.

If you would like to download some of these resources click the &apos;{next_button}&apos; button, otherwise click the &apos;{finish_button}&apos; button.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="303"/>
        <source>No Internet Connection</source>
        <translation>인터넷 연결 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="304"/>
        <source>Cannot connect to the internet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="305"/>
        <source>OpenLP could not connect to the internet to get information about the sample data available.

Please check your internet connection. If your church uses a proxy server click the 'Internet Settings' button below and enter the server details there.

Click the '{back_button}' button to try again.

If you click the &apos;{finish_button}&apos; button you can download the data at a later time by selecting &apos;Re-run First Time Wizard&apos; from the &apos;Tools&apos; menu in OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="314"/>
        <source>Sample Songs</source>
        <translation>샘플 음악</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="315"/>
        <source>Select and download public domain songs.</source>
        <translation>퍼블릭 도메인 곡을 선택하고 다운로드합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="316"/>
        <source>Sample Bibles</source>
        <translation>샘플 성경</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="317"/>
        <source>Select and download free Bibles.</source>
        <translation>무료 성경을 다운받기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="319"/>
        <source>Sample Themes</source>
        <translation>샘플 테마</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="320"/>
        <source>Select and download sample themes.</source>
        <translation>예제 테마를 선택하고 다운로드합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="321"/>
        <source>Default theme:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="322"/>
        <source>Select all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="323"/>
        <source>Deselect all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="324"/>
        <source>Downloading and Configuring</source>
        <translation>다운로드 및 설정 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/firsttimewizard.py" line="325"/>
        <source>Please wait while resources are downloaded and OpenLP is configured.</source>
        <translation>자원을 다운로드하고 OpenLP를 설정하는 동안 기다리십시오.</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagDialog</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="111"/>
        <source>Configure Formatting Tags</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="114"/>
        <source>Default Formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="125"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="126"/>
        <source>Tag</source>
        <translation>태그</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="127"/>
        <source>Start HTML</source>
        <translation>HTML 시작</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="128"/>
        <source>End HTML</source>
        <translation>HTML 끝</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagdialog.py" line="124"/>
        <source>Custom Formatting</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTagForm</name>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="79"/>
        <source>Tag {tag} already defined.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="81"/>
        <source>Description {tag} already defined.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="148"/>
        <source>Start tag {tag} is not valid HTML</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagcontroller.py" line="168"/>
        <source>End tag {end} does not match end tag for start tag {start}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="92"/>
        <source>New Tag {row:d}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="96"/>
        <source>&lt;HTML here&gt;</source>
        <translation>&lt;HTML을 여기에&gt;</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="199"/>
        <source>Validation Error</source>
        <translation>검증 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="179"/>
        <source>Description is missing</source>
        <translation>설명이 빠졌습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/formattingtagform.py" line="182"/>
        <source>Tag is missing</source>
        <translation>태그가 빠졌습니다</translation>
    </message>
</context>
<context>
    <name>OpenLP.FormattingTags</name>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="63"/>
        <source>Red</source>
        <translation>빨강</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="69"/>
        <source>Black</source>
        <translation>검정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="75"/>
        <source>Blue</source>
        <translation>파랑</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="81"/>
        <source>Yellow</source>
        <translation>노랑</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="87"/>
        <source>Green</source>
        <translation>초록</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="93"/>
        <source>Pink</source>
        <translation>분홍</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="99"/>
        <source>Orange</source>
        <translation>주황</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="105"/>
        <source>Purple</source>
        <translation>보라</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="111"/>
        <source>White</source>
        <translation>화이트</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="117"/>
        <source>Superscript</source>
        <translation>위 첨자</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="122"/>
        <source>Subscript</source>
        <translation>아래 첨자</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="127"/>
        <source>Paragraph</source>
        <translation>문단</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="132"/>
        <source>Bold</source>
        <translation>굵게</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="137"/>
        <source>Italics</source>
        <translation>기울임 꼴</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="141"/>
        <source>Underline</source>
        <translation>밑줄</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/formattingtags.py" line="147"/>
        <source>Break</source>
        <translation>줄바꿈</translation>
    </message>
</context>
<context>
    <name>OpenLP.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="296"/>
        <source>Experimental features (use at your own risk)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="334"/>
        <source>Service Item Slide Limits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="335"/>
        <source>Behavior of next/previous on the last/first slide:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="336"/>
        <source>&amp;Remain on Slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="337"/>
        <source>&amp;Wrap around</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="338"/>
        <source>&amp;Move to next/previous service item</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="160"/>
        <source>General</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="161"/>
        <source>Application Startup</source>
        <translation>프로그램 시작</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="162"/>
        <source>Show blank screen warning</source>
        <translation>빈 화면 경고 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="163"/>
        <source>Automatically open the previous service file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="164"/>
        <source>Show the splash screen</source>
        <translation>시작 화면 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="165"/>
        <source>Logo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="167"/>
        <source>Logo file:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="168"/>
        <source>Don&apos;t show logo on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="169"/>
        <source>Check for updates to OpenLP</source>
        <translation>OpenLP 최신 버전 확인</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="170"/>
        <source>Application Settings</source>
        <translation>프로그램 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="171"/>
        <source>Prompt to save before starting a new service</source>
        <translation>새 예배 과정 시작 전 저장 여부 묻기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="173"/>
        <source>Unblank display when changing slide in Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="175"/>
        <source>Unblank display when sending items to Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="177"/>
        <source>Automatically preview the next item in service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="179"/>
        <source>Timed slide interval:</source>
        <translation>슬라이드 표시 주기:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="180"/>
        <source> sec</source>
        <translation>초</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="181"/>
        <source>CCLI Details</source>
        <translation>CCLI 세부 정보</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="183"/>
        <source>SongSelect username:</source>
        <translation>SongSelect 사용자 이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/generaltab.py" line="184"/>
        <source>SongSelect password:</source>
        <translation>SongSelect 암호:</translation>
    </message>
</context>
<context>
    <name>OpenLP.LanguageManager</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Language</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="299"/>
        <source>Please restart OpenLP to use your new language setting.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.MainWindow</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="261"/>
        <source>English</source>
        <comment>Please add the name of your language here</comment>
        <translation>Korean</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="304"/>
        <source>General</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="361"/>
        <source>&amp;File</source>
        <translation>파일(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="362"/>
        <source>&amp;Import</source>
        <translation>가져오기(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="363"/>
        <source>&amp;Export</source>
        <translation>내보내기(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="364"/>
        <source>&amp;Recent Services</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="365"/>
        <source>&amp;View</source>
        <translation>보기(&amp;V)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="366"/>
        <source>&amp;Layout Presets</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="367"/>
        <source>&amp;Tools</source>
        <translation>도구(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="368"/>
        <source>&amp;Settings</source>
        <translation>설정(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="395"/>
        <source>&amp;Language</source>
        <translation>언어(&amp;L)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="370"/>
        <source>&amp;Help</source>
        <translation>도움말(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="371"/>
        <source>Library</source>
        <translation>라이브러리</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="372"/>
        <source>Service</source>
        <translation>예배</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="373"/>
        <source>Themes</source>
        <translation>테마</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="374"/>
        <source>Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="375"/>
        <source>&amp;New Service</source>
        <translation>새 예배 프로그램(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="378"/>
        <source>&amp;Open Service</source>
        <translation>예배 프로그램 열기(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="380"/>
        <source>Open an existing service.</source>
        <translation>기존 예배 내용을 엽니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="381"/>
        <source>&amp;Save Service</source>
        <translation>예배 프로그램 저장(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="383"/>
        <source>Save the current service to disk.</source>
        <translation>현재 예배 내용을 디스크에 저장합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="384"/>
        <source>Save Service &amp;As...</source>
        <translation>다른 이름으로 예배 프로그램 저장(&amp;A)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="385"/>
        <source>Save Service As</source>
        <translation>새로운 이름으로 예배 내용 저장</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="386"/>
        <source>Save the current service under a new name.</source>
        <translation>현재 예배 내용을 새 이름으로 저장합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="389"/>
        <source>Print the current service.</source>
        <translation>현재 서비스를 인쇄합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="390"/>
        <source>E&amp;xit</source>
        <translation>나가기(&amp;X)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="391"/>
        <source>Close OpenLP - Shut down the program.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="394"/>
        <source>&amp;Theme</source>
        <translation>테마(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="396"/>
        <source>Configure &amp;Shortcuts...</source>
        <translation>바로 가기 설정(&amp;S)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="397"/>
        <source>Configure &amp;Formatting Tags...</source>
        <translation>포매팅 태그 설정(&amp;F)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="398"/>
        <source>&amp;Configure OpenLP...</source>
        <translation>OpenLP 설정(&amp;C)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="399"/>
        <source>Export settings to a *.config file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="405"/>
        <source>Settings</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="402"/>
        <source>Import settings from a *.config file previously exported from this or another machine.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="406"/>
        <source>&amp;Projector Controller</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="407"/>
        <source>Hide or show Projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="408"/>
        <source>Toggle visibility of the Projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="410"/>
        <source>L&amp;ibrary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="411"/>
        <source>Hide or show the Library.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="412"/>
        <source>Toggle the visibility of the Library.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="414"/>
        <source>&amp;Themes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="415"/>
        <source>Hide or show themes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="416"/>
        <source>Toggle visibility of the Themes.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="418"/>
        <source>&amp;Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="419"/>
        <source>Hide or show Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="420"/>
        <source>Toggle visibility of the Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="422"/>
        <source>&amp;Preview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="423"/>
        <source>Hide or show Preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="424"/>
        <source>Toggle visibility of the Preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="426"/>
        <source>Li&amp;ve</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="427"/>
        <source>Hide or show Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="428"/>
        <source>L&amp;ock visibility of the panels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="429"/>
        <source>Lock visibility of the panels.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="430"/>
        <source>Toggle visibility of the Live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="431"/>
        <source>&amp;Manage Plugins</source>
        <translation>플러그인 관리(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="432"/>
        <source>You can enable and disable plugins from here.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="434"/>
        <source>&amp;About</source>
        <translation>정보(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="435"/>
        <source>More information about OpenLP.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="436"/>
        <source>&amp;User Manual</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="438"/>
        <source>Jump to the search box of the current active plugin.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="440"/>
        <source>&amp;Web Site</source>
        <translation>웹 사이트(&amp;W)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="443"/>
        <source>Set the interface language to {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="445"/>
        <source>&amp;Autodetect</source>
        <translation>자동 감지(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="446"/>
        <source>Use the system language, if available.</source>
        <translation>가능하다면 시스템 언어를 사용합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="447"/>
        <source>Add &amp;Tool...</source>
        <translation>도구 추가(&amp;T)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="448"/>
        <source>Add an application to the list of tools.</source>
        <translation>프로그램에 도구 목록을 추가합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="450"/>
        <source>Open &amp;Data Folder...</source>
        <translation>데이터 폴더 열기(&amp;D)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="451"/>
        <source>Open the folder where songs, bibles and other data resides.</source>
        <translation>노래, 성경, 기타 데이터가 있는 폴더를 엽니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="453"/>
        <source>Re-run First Time Wizard</source>
        <translation>초기 마법사 다시 실행</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="454"/>
        <source>Re-run the First Time Wizard, importing songs, Bibles and themes.</source>
        <translation>노래, 성경, 테마를 가져오는 초기 마법사를 다시 실행합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="456"/>
        <source>Update Theme Images</source>
        <translation>테마 그림 업데이트</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="457"/>
        <source>Update the preview images for all themes.</source>
        <translation>모든 테마의 미리보기 그림을 업데이트합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="459"/>
        <source>&amp;Show all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="460"/>
        <source>Reset the interface back to the default layout and show all the panels.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="462"/>
        <source>&amp;Setup</source>
        <translation>설정(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="463"/>
        <source>Use layout that focuses on setting up the Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="465"/>
        <source>&amp;Live</source>
        <translation>실황(&amp;L)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="466"/>
        <source>Use layout that focuses on Live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="548"/>
        <source>Waiting for some things to finish...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="550"/>
        <source>Please Wait</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="631"/>
        <source>Version {new} of OpenLP is now available for download (you are currently running version {current}). 

You can download the latest version from https://openlp.org/.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="634"/>
        <source>OpenLP Version Updated</source>
        <translation>OpenLP 버전 업데이트함</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Re-run First Time Wizard?</source>
        <translation>초기 마법사를 다시 실행하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="686"/>
        <source>Are you sure you want to re-run the First Time Wizard?

Re-running this wizard may make changes to your current OpenLP configuration and possibly add songs to your existing songs list and change your default theme.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>OpenLP Main Display Blanked</source>
        <translation>OpenLP 주 화면 비움</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="733"/>
        <source>The Main Display has been blanked out</source>
        <translation>주 화면을 비워놓았습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Import settings?</source>
        <translation>설정을 가져올까요?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="838"/>
        <source>Are you sure you want to import settings?

 Importing settings will make permanent changes to your current OpenLP configuration.

 Importing incorrect settings may cause erratic behaviour or OpenLP to terminate abnormally.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>Import settings</source>
        <translation>설정 가져오기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>OpenLP Settings (*.conf)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="889"/>
        <source>The file you have selected does not appear to be a valid OpenLP settings file.

Processing has terminated and no changes have been made.</source>
        <translation>선택하신 파일은 적절한 OpenLP 설정 파일이 아닌 것 같습니다.

작업은 중단되었으며 아무 것도 변경되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="931"/>
        <source>OpenLP will now close.  Imported settings will be applied the next time you start OpenLP.</source>
        <translation>OpenLP를 닫습니다. 가져온 설정은 다음에 OpenLP를 시작할 때 반영합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="943"/>
        <source>Export Settings File</source>
        <translation>내보내기 설정 파일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>Export setting error</source>
        <translation>내보내기 설정 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="956"/>
        <source>An error occurred while exporting the settings: {err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Exit OpenLP</source>
        <translation>OpenLP 나가기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1035"/>
        <source>Are you sure you want to exit OpenLP?</source>
        <translation>정말로 OpenLP를 나가시겠습니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1042"/>
        <source>&amp;Exit OpenLP</source>
        <translation>OpenLP 나가기(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1108"/>
        <source>Default Theme: {theme}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear List</source>
        <comment>Clear List of recent files</comment>
        <translation>목록 지우기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1264"/>
        <source>Clear the list of recent files.</source>
        <translation>최근 파일 목록을 지웁니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1345"/>
        <source>Copying OpenLP data to new data directory location - {path} - Please wait for copy to finish</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1353"/>
        <source>OpenLP Data directory copy failed

{err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/mainwindow.py" line="1355"/>
        <source>New Data Directory Error</source>
        <translation>새 데이터 디렉터리 오류</translation>
    </message>
</context>
<context>
    <name>OpenLP.Manager</name>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>Database Error</source>
        <translation>데이터베이스 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="157"/>
        <source>OpenLP cannot load your database.

Database: {db}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/db.py" line="370"/>
        <source>The database being loaded was created in a more recent version of OpenLP. The database is version {db_ver}, while OpenLP expects version {db_up}. The database will not be loaded.

Database: {db_name}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.MediaController</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="100"/>
        <source>The media integration library is missing (python - vlc is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="102"/>
        <source>The media integration library is missing (python - pymediainfo is not installed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/media/mediacontroller.py" line="122"/>
        <source>No Displays have been configured, so Live Media has been disabled</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.MediaManagerItem</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="382"/>
        <source>No Items Selected</source>
        <translation>선택한 항목 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/images/lib/mediaitem.py" line="161"/>
        <source>&amp;Add to selected Service Item</source>
        <translation>선택한 서비스 항목 추가(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File Type</source>
        <translation>잘못된 파일 형식</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="344"/>
        <source>Invalid File {file_path}.
File extension not supported</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="391"/>
        <source>Duplicate files were found on import and were ignored.</source>
        <translation>가져오기 과정에 중복 파일을 찾았으며 건너뛰었습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="496"/>
        <source>You must select one or more items to preview.</source>
        <translation>미리 볼 하나 이상의 항목을 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="514"/>
        <source>You must select one or more items to send live.</source>
        <translation>실황으로 보낼 하나 이상의 항목을 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="563"/>
        <source>You must select one or more items to add.</source>
        <translation>하나 이상의 추가 아이템을 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="607"/>
        <source>You must select one or more items.</source>
        <translation>하나 이상의 항목을 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="614"/>
        <source>You must select an existing service item to add to.</source>
        <translation>추가할 기존 서비스 항목을 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>Invalid Service Item</source>
        <translation>잘못된 서비스 항목</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/mediamanageritem.py" line="622"/>
        <source>You must select a {title} service item.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="119"/>
        <source>&amp;Clone</source>
        <translation>복제(&amp;C)</translation>
    </message>
</context>
<context>
    <name>OpenLP.MediaTab</name>
    <message>
        <location filename="../../openlp/core/ui/media/mediatab.py" line="51"/>
        <source>Media</source>
        <translation>미디어</translation>
    </message>
</context>
<context>
    <name>OpenLP.OpenLyricsImportError</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="707"/>
        <source>&lt;lyrics&gt; tag is missing.</source>
        <translation>&lt;lyrics&gt; 태그가 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsxml.py" line="712"/>
        <source>&lt;verse&gt; tag is missing.</source>
        <translation>&lt;verse&gt; 태그가 없습니다.</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="494"/>
        <source>Fan</source>
        <translation>냉각팬</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="495"/>
        <source>Lamp</source>
        <translation>램프</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="496"/>
        <source>Temperature</source>
        <translation>온도</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="497"/>
        <source>Cover</source>
        <translation>덮개</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="498"/>
        <source>Filter</source>
        <translation>필터</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="430"/>
        <source>No message</source>
        <translation>메시지 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/pjlink.py" line="759"/>
        <source>Error while sending data to projector</source>
        <translation>프로젝터에 데이터 보내는 중 오류</translation>
    </message>
</context>
<context>
    <name>OpenLP.PJLinkConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="166"/>
        <source>Acknowledge a PJLink SRCH command - returns MAC address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="171"/>
        <source>Blank/unblank video and/or mute audio.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="176"/>
        <source>Query projector PJLink class support.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="181"/>
        <source>Query error status from projector. Returns fan/lamp/temp/cover/filter/other error status.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="187"/>
        <source>Query number of hours on filter.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="192"/>
        <source>Freeze or unfreeze current image being projected.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="197"/>
        <source>Query projector manufacturer name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="202"/>
        <source>Query projector product name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="207"/>
        <source>Query projector for other information set by manufacturer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="212"/>
        <source>Query specified input source name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="217"/>
        <source>Switch to specified video source.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="222"/>
        <source>Query available input sources.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="227"/>
        <source>Query current input resolution.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="232"/>
        <source>Query lamp time and on/off status. Multiple lamps supported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="237"/>
        <source>UDP Status - Projector is now available on network. Includes MAC address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="242"/>
        <source>Adjust microphone volume by 1 step.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="247"/>
        <source>Query customer-set projector name.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="252"/>
        <source>Initial connection with authentication/no authentication request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="257"/>
        <source>Turn lamp on or off/standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="262"/>
        <source>Query replacement air filter model number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="267"/>
        <source>Query replacement lamp model number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="272"/>
        <source>Query recommended resolution.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="277"/>
        <source>Query projector serial number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="282"/>
        <source>UDP broadcast search request for available projectors. Reply is ACKN.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="287"/>
        <source>Query projector software version number.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="292"/>
        <source>Adjust speaker volume by 1 step.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PathEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="299"/>
        <source>Browse for directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="300"/>
        <source>Revert to default directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="302"/>
        <source>Browse for file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="303"/>
        <source>Revert to default file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="317"/>
        <source>Select Directory</source>
        <translation>디렉터리 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="321"/>
        <source>Select File</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PluginForm</name>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="74"/>
        <source>Manage Plugins</source>
        <translation>플러그인 관리</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="75"/>
        <source>Plugin Details</source>
        <translation>플러그인 세부 정보</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="77"/>
        <source>Status:</source>
        <translation>상태:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/plugindialog.py" line="78"/>
        <source>Active</source>
        <translation>활성</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="149"/>
        <source>{name} (Disabled)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="145"/>
        <source>{name} (Active)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/pluginform.py" line="147"/>
        <source>{name} (Inactive)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PluginManager</name>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="173"/>
        <source>Unable to initialise the following plugins:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/pluginmanager.py" line="179"/>
        <source>See the log file for more details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceDialog</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Page</source>
        <translation>페이지에 맞춤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="151"/>
        <source>Fit Width</source>
        <translation>폭 맞춤</translation>
    </message>
</context>
<context>
    <name>OpenLP.PrintServiceForm</name>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="61"/>
        <source>Print</source>
        <translation>인쇄</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="69"/>
        <source>Copy as Text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="71"/>
        <source>Copy as HTML</source>
        <translation>HTML로 복사</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="137"/>
        <source>Zoom Out</source>
        <translation>축소</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="138"/>
        <source>Zoom Original</source>
        <translation>원래 크기로</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="139"/>
        <source>Zoom In</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="140"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="141"/>
        <source>Title:</source>
        <translation>제목:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="142"/>
        <source>Service Note Text:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="143"/>
        <source>Other Options</source>
        <translation>기타 옵션</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="144"/>
        <source>Include slide text if available</source>
        <translation>가능할 경우 슬라이드 문구 포함</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="145"/>
        <source>Add page break before each text item</source>
        <translation>각각의 문구 항목 앞에 페이지 건너뛰기 문자 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="146"/>
        <source>Include service item notes</source>
        <translation>서비스 항목 참고 포함</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="147"/>
        <source>Include play length of media items</source>
        <translation>미디어 항목의 재생 시간 길이 포함</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="148"/>
        <source>Show chords</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printservicedialog.py" line="149"/>
        <source>Service Sheet</source>
        <translation>서비스 시트</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorConstants</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="401"/>
        <source>The address specified with socket.bind() is already in use and was set to be exclusive</source>
        <translation>socket.bind()로 지정한 주소가 이미 사용중이며 배타적 주소로 설정했습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="404"/>
        <source>PJLink returned &quot;ERRA: Authentication Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="405"/>
        <source>The connection was refused by the peer (or timed out)</source>
        <translation>피어가 연결을 거절함(또는 시간 초과)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="407"/>
        <source>Projector cover open detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="408"/>
        <source>PJLink class not supported</source>
        <translation>PJLink 클래스를 지원하지 않음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="409"/>
        <source>The datagram was larger than the operating system&apos;s limit</source>
        <translation>데이터그램 크기가 운영체제에서 허용하는 크기를 넘었습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="411"/>
        <source>Error condition detected</source>
        <translation>오류 상태 감지</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="412"/>
        <source>Projector fan error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="413"/>
        <source>Projector check filter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="414"/>
        <source>General projector error</source>
        <translation>일반 프로젝터 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="415"/>
        <source>The host address was not found</source>
        <translation>호스트 주소를 찾을 수 없습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="416"/>
        <source>PJLink invalid packet received</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="417"/>
        <source>Projector lamp error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="418"/>
        <source>An error occurred with the network (Possibly someone pulled the plug?)</source>
        <translation>네트워크에 오류가 발생했습니다(누군가가 플러그를 뽑았을지도?)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="420"/>
        <source>PJlink authentication Mismatch Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="421"/>
        <source>Projector not connected error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="422"/>
        <source>PJLink returned &quot;ERR2: Invalid Parameter&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="423"/>
        <source>PJLink Invalid prefix character</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="424"/>
        <source>PJLink returned &quot;ERR4: Projector/Display Error&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="425"/>
        <source>The socket is using a proxy, and the proxy requires authentication</source>
        <translation>소켓은 프록시를 거치며 프록시에서 인증이 필요합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="428"/>
        <source>The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established)</source>
        <translation>서버 연결이 예상치 않게 끊겼습니다(마지막 피어로의 연결이 성립하기 전)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="431"/>
        <source>Could not contact the proxy server because the connection to that server was denied</source>
        <translation>서버 연결이 거부되어 프록시 서버에 연결할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="434"/>
        <source>The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase.</source>
        <translation>프록시 서버 연결 시간을 초과했거나 인증 과정에서 프록시 서버 응답이 멈추었습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="437"/>
        <source>The proxy address set with setProxy() was not found</source>
        <translation>setProxy()로 설정한 프록시 주소가 없습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="439"/>
        <source>The connection negotiation with the proxy server failed because the response from the proxy server could not be understood</source>
        <translation>프록시 서버에서 온 응답을 해석할 수 없어 프록시 서버 연결 과정 처리에 실패했습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="442"/>
        <source>The remote host closed the connection</source>
        <translation>원격 호스트의 연결이 끊어졌습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="444"/>
        <source>The SSL/TLS handshake failed</source>
        <translation>SSL/TLS 처리 과정 실패</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="446"/>
        <source>The address specified to socket.bind() does not belong to the host</source>
        <translation>socket.bind()로 지정한 주소는 호스트 주소가 아닙니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="449"/>
        <source>The socket operation failed because the application lacked the required privileges</source>
        <translation>프로그램에서 필요한 권한이 빠져 소켓 처리에 실패했습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="452"/>
        <source>The local system ran out of resources (e.g., too many sockets)</source>
        <translation>로컬 시스템의 자원이 부족합니다(예: 소켓 수 초과)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="454"/>
        <source>The socket operation timed out</source>
        <translation>소켓 처리 시간이 지났습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="456"/>
        <source>Projector high temperature detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="457"/>
        <source>PJLink returned &quot;ERR3: Busy&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="458"/>
        <source>PJLink returned &quot;ERR1: Undefined Command&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="459"/>
        <source>The last operation attempted has not finished yet (still in progress in the background)</source>
        <translation>마지막 처리 시도가 아직 끝나지 않았습니다(여전히 백그라운드에서 처리 중)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="462"/>
        <source>Unknown condiction detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="463"/>
        <source>An unidentified socket error occurred</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="464"/>
        <source>The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support)</source>
        <translation>요청한 소켓 처리를 로컬 운영체제에서 지원하지 않습니다(예: IPv6 지원 기능 빠짐)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="467"/>
        <source>Warning condition detected</source>
        <translation>경고 상태 감지</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="468"/>
        <source>Connection initializing with pin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="469"/>
        <source>Socket is bount to an address or port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="470"/>
        <source>Connection initializing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="471"/>
        <source>Socket is about to close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="472"/>
        <source>Connected</source>
        <translation>연결함</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="473"/>
        <source>Connecting</source>
        <translation>연결 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="474"/>
        <source>Cooldown in progress</source>
        <translation>냉각 진행 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="475"/>
        <source>Command returned with OK</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="476"/>
        <source>Performing a host name lookup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="477"/>
        <source>Projector Information available</source>
        <translation>존재하는 프로젝트 정보</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="478"/>
        <source>Initialize in progress</source>
        <translation>초기화 진행 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="479"/>
        <source>Socket it listening (internal use only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="480"/>
        <source>No network activity at this time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="481"/>
        <source>Received data</source>
        <translation>데이터 수신함</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="482"/>
        <source>Sending data</source>
        <translation>데이터 보내는 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="483"/>
        <source>Not Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="484"/>
        <source>Off</source>
        <translation>끔</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="485"/>
        <source>OK</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="486"/>
        <source>Power is on</source>
        <translation>전원 켜짐</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="487"/>
        <source>Power in standby</source>
        <translation>대기모드 전원</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="488"/>
        <source>Getting status</source>
        <translation>상태 가져오는 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="489"/>
        <source>Warmup in progress</source>
        <translation>예열 진행 중</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEdit</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>Name Not Set</source>
        <translation>이름 설정하지 않음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="186"/>
        <source>You must enter a name for this entry.&lt;br /&gt;Please enter a new name for this entry.</source>
        <translation>이 항목의 이름을 선택해야합니다.&lt;br /&gt;이 항목의 새 이름을 입력하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="196"/>
        <source>Duplicate Name</source>
        <translation>중복 이름</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorEditForm</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="115"/>
        <source>Add New Projector</source>
        <translation>새 프로젝터 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="118"/>
        <source>Edit Projector</source>
        <translation>프로젝터 편집</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="120"/>
        <source>IP Address</source>
        <translation>IP 주소</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="123"/>
        <source>Port Number</source>
        <translation>포트 번호</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="125"/>
        <source>PIN</source>
        <translation>PIN</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="127"/>
        <source>Name</source>
        <translation>명칭</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="129"/>
        <source>Location</source>
        <translation>위치</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="131"/>
        <source>Notes</source>
        <translation>참고</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>Database Error</source>
        <translation>데이터베이스 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="255"/>
        <source>There was an error saving projector information. See the log for the error</source>
        <translation>프로젝터 정보 저장에 오류가 있습니다. 오류 로그를 살펴보십시오</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorManager</name>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add Projector</source>
        <translation>프로젝터 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="70"/>
        <source>Add a new projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit Projector</source>
        <translation>프로젝터 편집</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="76"/>
        <source>Edit selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete Projector</source>
        <translation>프로젝터 삭제</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="81"/>
        <source>Delete selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Select Input Source</source>
        <translation>입력 소스 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="87"/>
        <source>Choose input source on selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View Projector</source>
        <translation>프로젝터 보기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="93"/>
        <source>View selected projector information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="100"/>
        <source>Connect to selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors</source>
        <translation>선택한 프로젝터에 연결합니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="107"/>
        <source>Connect to selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projectors</source>
        <translation>선택한 프로젝터 연결 끊기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="114"/>
        <source>Disconnect from selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projector</source>
        <translation>선택한 프로젝터 연결을 끊습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="121"/>
        <source>Disconnect from selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projector</source>
        <translation>선택한 프로젝터 전원 켜기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="129"/>
        <source>Power on selected projector.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="136"/>
        <source>Power on selected projectors.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Standby selected projector</source>
        <translation>선택한 프로젝터 대기모드 진입</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="143"/>
        <source>Put selected projector in standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="149"/>
        <source>Put selected projectors in standby.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="156"/>
        <source>Blank selected projector screen</source>
        <translation>선택한 프로젝터 화면 비우기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="163"/>
        <source>Blank selected projectors screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projector screen</source>
        <translation>선택한 프로젝트 화면 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="170"/>
        <source>Show selected projector screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="177"/>
        <source>Show selected projectors screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="199"/>
        <source>&amp;View Projector Information</source>
        <translation>프로젝터 정보 보기(&amp;V)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="204"/>
        <source>&amp;Edit Projector</source>
        <translation>프로젝터 편집(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="210"/>
        <source>&amp;Connect Projector</source>
        <translation>프로젝터 연결하기(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="215"/>
        <source>D&amp;isconnect Projector</source>
        <translation>프로젝터 연결 끊기(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="221"/>
        <source>Power &amp;On Projector</source>
        <translation>프로젝터 전원 켜기(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="226"/>
        <source>Power O&amp;ff Projector</source>
        <translation>프로젝터 전원 끄기(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="232"/>
        <source>Select &amp;Input</source>
        <translation>입력 선택(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="237"/>
        <source>Edit Input Source</source>
        <translation>입력 소스 편집</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="242"/>
        <source>&amp;Blank Projector Screen</source>
        <translation>프로젝터 화면 비우기(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="247"/>
        <source>&amp;Show Projector Screen</source>
        <translation>프로젝터 화면 표시(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="253"/>
        <source>&amp;Delete Projector</source>
        <translation>프로젝터 제거(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="503"/>
        <source>Are you sure you want to delete this projector?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="648"/>
        <source>Name</source>
        <translation>명칭</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="650"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="652"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="654"/>
        <source>Notes</source>
        <translation>참고</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="658"/>
        <source>Projector information not available at this time.</source>
        <translation>현재 프로젝터 정보를 찾을 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="660"/>
        <source>Projector Name</source>
        <translation>프로젝터 이름</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="663"/>
        <source>Manufacturer</source>
        <translation>제조사</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="665"/>
        <source>Model</source>
        <translation>모델</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="667"/>
        <source>PJLink Class</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="670"/>
        <source>Software Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="673"/>
        <source>Serial Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="676"/>
        <source>Lamp Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="679"/>
        <source>Filter Model Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="682"/>
        <source>Other info</source>
        <translation>기타 정보</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="685"/>
        <source>Power status</source>
        <translation>전원 상태</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Shutter is</source>
        <translation>덮개:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="687"/>
        <source>Closed</source>
        <translation>닫힘</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="691"/>
        <source>Current source input is</source>
        <translation>현재 입력 소스</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="698"/>
        <source>Unavailable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="700"/>
        <source>ON</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="702"/>
        <source>OFF</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="703"/>
        <source>Lamp</source>
        <translation>램프</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="708"/>
        <source>Hours</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="713"/>
        <source>No current errors or warnings</source>
        <translation>현재 오류 또는 경고 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="715"/>
        <source>Current errors/warnings</source>
        <translation>현재 오류/경고</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="718"/>
        <source>Projector Information</source>
        <translation>프로젝터 정보</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="941"/>
        <source>Authentication Error</source>
        <translation>인증 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="956"/>
        <source>No Authentication Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/manager.py" line="998"/>
        <source>Not Implemented Yet</source>
        <translation>아직 구현하지 않음</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="52"/>
        <source>Projector</source>
        <translation>프로젝터</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="111"/>
        <source>Communication Options</source>
        <translation>통신 옵션</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="113"/>
        <source>Connect to projectors on startup</source>
        <translation>시작시 프로젝터 연결</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="115"/>
        <source>Socket timeout (seconds)</source>
        <translation>소켓 제한시간(초)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="117"/>
        <source>Poll time (seconds)</source>
        <translation>폴링 시간(초)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="121"/>
        <source>Tabbed dialog box</source>
        <translation>탭 대화상자</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="123"/>
        <source>Single dialog box</source>
        <translation>단일 대화상자</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="125"/>
        <source>Connect to projector when LINKUP received (v2 only)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="127"/>
        <source>Enable listening for PJLink2 broadcast messages</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProjectorWizard</name>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="216"/>
        <source>Duplicate IP Address</source>
        <translation>중복된 IP 주소</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="224"/>
        <source>Invalid IP Address</source>
        <translation>잘못된 IP 주소</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/editform.py" line="233"/>
        <source>Invalid Port Number</source>
        <translation>잘못된 포트 번호</translation>
    </message>
</context>
<context>
    <name>OpenLP.ProxyDialog</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="177"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ProxyWidget</name>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="116"/>
        <source>Proxy Server Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="117"/>
        <source>No prox&amp;y</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="118"/>
        <source>&amp;Use system proxy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="119"/>
        <source>&amp;Manual proxy configuration</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="120"/>
        <source>e.g. proxy_server_address:port_no</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="121"/>
        <source>HTTP:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="123"/>
        <source>HTTPS:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="125"/>
        <source>Username:</source>
        <translation>사용자 이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="126"/>
        <source>Password:</source>
        <translation>암호:</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreenList</name>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="254"/>
        <source>Screen</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../../openlp/core/display/screens.py" line="257"/>
        <source>primary</source>
        <translation>첫번째</translation>
    </message>
</context>
<context>
    <name>OpenLP.ScreensTab</name>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="44"/>
        <source>Screens</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="69"/>
        <source>Generic screen settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/screenstab.py" line="70"/>
        <source>Display if a single screen</source>
        <translation>단일 화면 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="297"/>
        <source>F&amp;ull screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="298"/>
        <source>Width:</source>
        <translation>너비:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="299"/>
        <source>Use this screen as a display</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="302"/>
        <source>Left:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="303"/>
        <source>Custom &amp;geometry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="304"/>
        <source>Top:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="305"/>
        <source>Height:</source>
        <translation>높이:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/widgets.py" line="306"/>
        <source>Identify Screens</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItem</name>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="289"/>
        <source>[slide {frame:d}]</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="590"/>
        <source>&lt;strong&gt;Start&lt;/strong&gt;: {start}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/serviceitem.py" line="594"/>
        <source>&lt;strong&gt;Length&lt;/strong&gt;: {length}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ServiceItemEditForm</name>
    <message>
        <location filename="../../openlp/core/ui/serviceitemeditdialog.py" line="70"/>
        <source>Reorder Service Item</source>
        <translation>예배 항목 재정렬</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceManager</name>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="199"/>
        <source>Service Notes: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="246"/>
        <source>Notes: </source>
        <translation>노트:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/printserviceform.py" line="254"/>
        <source>Playing time: </source>
        <translation>재생 시간:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="148"/>
        <source>Load an existing service.</source>
        <translation>기존 예배 내용을 불러옵니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="152"/>
        <source>Save this service.</source>
        <translation>이 예배를 저장합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="162"/>
        <source>Select a theme for the service.</source>
        <translation>예배 내용에 대한 테마를 선택하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move to &amp;top</source>
        <translation>상위로 이동(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="185"/>
        <source>Move item to the top of the service.</source>
        <translation>서비스 상위로 항목을 이동합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move &amp;up</source>
        <translation>위로 이동(&amp;U)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="190"/>
        <source>Move item up one position in the service.</source>
        <translation>서비스의 위 부분으로 항목을 이동합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move &amp;down</source>
        <translation>아래로 이동(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="195"/>
        <source>Move item down one position in the service.</source>
        <translation>서비스의 아래 부분으로 항목을 이동합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move to &amp;bottom</source>
        <translation>하단으로 이동(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="200"/>
        <source>Move item to the end of the service.</source>
        <translation>서비스의 끝 부분으로 항목을 이동합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>&amp;Delete From Service</source>
        <translation>&amp;예배에서 삭제</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="206"/>
        <source>Delete the selected item from the service.</source>
        <translation>선택한 항목을 예배에서 제거합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>&amp;Expand all</source>
        <translation>&amp;모두 펼치기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="212"/>
        <source>Expand all the service items.</source>
        <translation>모든 예배 항목을 펼칩니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>&amp;Collapse all</source>
        <translation>&amp;모두 접기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="217"/>
        <source>Collapse all the service items.</source>
        <translation>모든 예배 항목을 접습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Go Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="223"/>
        <source>Send the selected item to Live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="241"/>
        <source>&amp;Add New Item</source>
        <translation>&amp;새로운 항목 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="243"/>
        <source>&amp;Add to Selected Item</source>
        <translation>선택한 항목에 추가(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="247"/>
        <source>&amp;Edit Item</source>
        <translation>&amp;항목 수정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="249"/>
        <source>&amp;Rename...</source>
        <translation>이름 바꾸기(&amp;R)...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="252"/>
        <source>&amp;Reorder Item</source>
        <translation>항목 재정렬(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="255"/>
        <source>&amp;Notes</source>
        <translation>참고(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="258"/>
        <source>&amp;Start Time</source>
        <translation>시작 시간(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="265"/>
        <source>Create New &amp;Custom Slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="272"/>
        <source>&amp;Auto play slides</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="276"/>
        <source>Auto play slides &amp;Loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="281"/>
        <source>Auto play slides &amp;Once</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="812"/>
        <source>&amp;Delay between slides</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="292"/>
        <source>Show &amp;Preview</source>
        <translation>미리 보기 표시(&amp;P)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="297"/>
        <source>&amp;Change Item Theme</source>
        <translation>항목 테마 바꾸기(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="371"/>
        <source>Untitled Service</source>
        <translation>제목 없는 예배</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>Open File</source>
        <translation>파일 열기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="462"/>
        <source>OpenLP Service Files (*.osz *.oszl)</source>
        <translation>OpenLP 예배 파일 (*.osz *.oszl)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>Modified Service</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="476"/>
        <source>The current service has been modified. Would you like to save this service?</source>
        <translation>현재 예배가 수정되었습니다. 이 예배를 저장할 까요?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="572"/>
        <source>Service File(s) Missing</source>
        <translation>예배 파일 빠짐</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="573"/>
        <source>The following file(s) in the service are missing: {name}

These files will be removed if you continue to save.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>Error Saving File</source>
        <translation>파일 저장 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="616"/>
        <source>There was an error saving your file.

{error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="653"/>
        <source>OpenLP Service Files - lite (*.oszl)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="654"/>
        <source>OpenLP Service Files (*.osz)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="726"/>
        <source>The service file {file_path} could not be loaded because it is either corrupt, inaccessible, or not a valid OpenLP 2 or OpenLP 3 service file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="823"/>
        <source>&amp;Auto Start - active</source>
        <translation>자동 시작 활성(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="827"/>
        <source>&amp;Auto Start - inactive</source>
        <translation>자동 시작 비활성(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Input delay</source>
        <translation>입력 지연</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="906"/>
        <source>Delay between slides in seconds.</source>
        <translation>초단위 슬라이드 지연 시간입니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1220"/>
        <source>Edit</source>
        <translation>수정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1221"/>
        <source>Service copy only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1224"/>
        <source>Slide theme</source>
        <translation>스라이드 테마</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1228"/>
        <source>Notes</source>
        <translation>참고</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Missing Display Handler</source>
        <translation>디스플레이 핸들러 빠짐</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1399"/>
        <source>Your item cannot be displayed as there is no handler to display it</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1465"/>
        <source>Your item cannot be displayed as the plugin required to display it is missing or inactive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Rename item title</source>
        <translation>항목 제목 이름 바꾸기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/servicemanager.py" line="1490"/>
        <source>Title:</source>
        <translation>제목:</translation>
    </message>
</context>
<context>
    <name>OpenLP.ServiceNoteForm</name>
    <message>
        <location filename="../../openlp/core/ui/servicenoteform.py" line="72"/>
        <source>Service Item Notes</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.SettingsForm</name>
    <message>
        <location filename="../../openlp/core/ui/settingsdialog.py" line="62"/>
        <source>Configure OpenLP</source>
        <translation>OpenLP 설정</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListDialog</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="138"/>
        <source>Configure Shortcuts</source>
        <translation>단축키 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="139"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>동작을 선택한 후 처음 또는 대체 쇼트컷 녹화를 각각 시작하려면 하단 단추를 누르십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Action</source>
        <translation>동작</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Shortcut</source>
        <translation>단축키</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="142"/>
        <source>Alternate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="145"/>
        <source>Default</source>
        <translation>기본</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="146"/>
        <source>Custom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="148"/>
        <source>Capture shortcut.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistdialog.py" line="151"/>
        <source>Restore the default shortcut of this action.</source>
        <translation>이 동작에 대한 기본 단축키를 복구합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Restore Default Shortcuts</source>
        <translation>기본 단축키 복구</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="284"/>
        <source>Do you want to restore all shortcuts to their defaults?</source>
        <translation>모든 단축키를 기본 값으로 복구하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="437"/>
        <source>The shortcut &quot;{key}&quot; is already assigned to another action,
please use a different shortcut.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="441"/>
        <source>Duplicate Shortcut</source>
        <translation>단축키 중복</translation>
    </message>
</context>
<context>
    <name>OpenLP.ShortcutListForm</name>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an Action</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/shortcutlistform.py" line="327"/>
        <source>Select an action and click one of the buttons below to start capturing a new primary or alternate shortcut, respectively.</source>
        <translation>동작을 선택한 후 처음 또는 대체 쇼트컷 녹화를 각각 시작하려면 하단 단추를 누르십시오.</translation>
    </message>
</context>
<context>
    <name>OpenLP.SlideController</name>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Previous Slide</source>
        <translation>앞 슬라이드</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="243"/>
        <source>Move to previous.</source>
        <translation>이전으로 이동합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Next Slide</source>
        <translation>다음 스라이드</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="250"/>
        <source>Move to next.</source>
        <translation>다음으로 이동합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="266"/>
        <source>Hide</source>
        <translation>숨김</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="268"/>
        <source>Move to preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="272"/>
        <source>Show Desktop</source>
        <translation>바탕화면 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="277"/>
        <source>Toggle Desktop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="282"/>
        <source>Toggle Blank to Theme</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="287"/>
        <source>Toggle Blank Screen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="316"/>
        <source>Play Slides</source>
        <translation>슬라이드 재생</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="336"/>
        <source>Delay between slides in seconds.</source>
        <translation>초단위 슬라이드 지연 시간입니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="340"/>
        <source>Move to live.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="343"/>
        <source>Add to Service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="347"/>
        <source>Edit and reload song preview.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="351"/>
        <source>Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="358"/>
        <source>Start playing media.</source>
        <translation>미디어 재생 시작.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="362"/>
        <source>Pause playing media.</source>
        <translation>미디어 재생 일시 중지.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="366"/>
        <source>Stop playing media.</source>
        <translation>미디어 재생 중단.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="370"/>
        <source>Loop playing media.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="377"/>
        <source>Video timer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="386"/>
        <source>Video position.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="397"/>
        <source>Audio Volume.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="414"/>
        <source>Go To</source>
        <translation>다음으로 이동</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="442"/>
        <source>Go to &quot;Verse&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="443"/>
        <source>Go to &quot;Chorus&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="444"/>
        <source>Go to &quot;Bridge&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="446"/>
        <source>Go to &quot;Pre-Chorus&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="447"/>
        <source>Go to &quot;Intro&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="448"/>
        <source>Go to &quot;Ending&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="449"/>
        <source>Go to &quot;Other&quot;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="575"/>
        <source>Previous Service</source>
        <translation>이전 예배</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/slidecontroller.py" line="580"/>
        <source>Next Service</source>
        <translation>다음 예배</translation>
    </message>
</context>
<context>
    <name>OpenLP.SourceSelectForm</name>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="150"/>
        <source>Ignoring current changes and return to OpenLP</source>
        <translation>현재 바뀐 내용을 무시하고 OpenLP로 복귀</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="153"/>
        <source>Delete all user-defined text and revert to PJLink default text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="156"/>
        <source>Discard changes and reset to previous user-defined text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="159"/>
        <source>Save changes and return to OpenLP</source>
        <translation>바꾸니 내용을 저장하고 OpenLP로 복귀</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="397"/>
        <source>Edit Projector Source Text</source>
        <translation>프로젝터 텍스트 편집</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="399"/>
        <source>Select Projector Source</source>
        <translation>프로젝터 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="481"/>
        <source>Delete entries for this projector</source>
        <translation>이 프로젝터의 항목 삭제</translation>
    </message>
    <message>
        <location filename="../../openlp/core/projectors/sourceselectform.py" line="482"/>
        <source>Are you sure you want to delete ALL user-defined source input text for this projector?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.SpellTextEdit</name>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="405"/>
        <source>Language:</source>
        <translation>언어:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="416"/>
        <source>Spelling Suggestions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/edits.py" line="424"/>
        <source>Formatting Tags</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.StartTimeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="70"/>
        <source>Theme Layout</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="71"/>
        <source>The blue box shows the main area.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themelayoutdialog.py" line="72"/>
        <source>The red box shows the footer.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.StartTime_form</name>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="116"/>
        <source>Item Start and Finish Time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="123"/>
        <source>Hours:</source>
        <translation>시간:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="124"/>
        <source>Minutes:</source>
        <translation>분:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="125"/>
        <source>Seconds:</source>
        <translation>초:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="126"/>
        <source>Start</source>
        <translation>시작</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="127"/>
        <source>Finish</source>
        <translation>마침</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimedialog.py" line="128"/>
        <source>Length</source>
        <translation>길이</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Time Validation Error</source>
        <translation>시간 검증 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="72"/>
        <source>Finish time is set after the end of the media item</source>
        <translation>마침 시간을 미디어 항목 끝 다음에 설정했습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/starttimeform.py" line="77"/>
        <source>Start time is after the finish time of the media item</source>
        <translation>시작 시간이 미디어 항목 마침 시간의 다음입니다</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeForm</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="167"/>
        <source>(approximately %d lines per slide)</source>
        <translation>(슬라이드당 평균 %d 줄)</translation>
    </message>
</context>
<context>
    <name>OpenLP.ThemeManager</name>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="68"/>
        <source>Create a new theme.</source>
        <translation>새 테마를 만듭니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit Theme</source>
        <translation>테마 수정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="72"/>
        <source>Edit a theme.</source>
        <translation>테마를 수정합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete Theme</source>
        <translation>테마 삭제</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="77"/>
        <source>Delete a theme.</source>
        <translation>테마를 삭제합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import Theme</source>
        <translation>테마 가저오기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="85"/>
        <source>Import a theme.</source>
        <translation>테마를 가저옵니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export Theme</source>
        <translation>테마 내보내기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="90"/>
        <source>Export a theme.</source>
        <translation>테마를 내보냅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="108"/>
        <source>&amp;Edit Theme</source>
        <translation>테마 수정(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="111"/>
        <source>&amp;Copy Theme</source>
        <translation>테마 복사(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="114"/>
        <source>&amp;Rename Theme</source>
        <translation> 테마 이름 바꾸기(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="117"/>
        <source>&amp;Delete Theme</source>
        <translation>테마 삭제(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="121"/>
        <source>Set As &amp;Global Default</source>
        <translation>전역 기본값으로 설정(&amp;G)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="125"/>
        <source>&amp;Export Theme</source>
        <translation>테마 내보내기(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="260"/>
        <source>{text} (default)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>You must select a theme to rename.</source>
        <translation>이름을 바꿀 테마를 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename Confirmation</source>
        <translation>이름 바꾸기 확인</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="283"/>
        <source>Rename {theme_name} theme?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="310"/>
        <source>Copy of {name}</source>
        <comment>Copy of &lt;theme name&gt;</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="340"/>
        <source>You must select a theme to edit.</source>
        <translation>편집할 테마를 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>You must select a theme to delete.</source>
        <translation>삭제할 테마를 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete Confirmation</source>
        <translation>삭제 확인</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="357"/>
        <source>Delete {theme_name} theme?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="394"/>
        <source>You have not selected a theme.</source>
        <translation>테마를 선택하지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="398"/>
        <source>Save Theme - ({name})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>OpenLP Themes (*.otz)</source>
        <translation>OpenLP 테마 (*.otz)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Theme Exported</source>
        <translation>테마 내보냄</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="408"/>
        <source>Your theme has been successfully exported.</source>
        <translation>테마를 성공적으로 내보냈습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>Theme Export Failed</source>
        <translation>테마 내보내기 실패</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="431"/>
        <source>The {theme_name} export failed because this error occurred: {err}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="447"/>
        <source>Select Theme Import File</source>
        <translation>테마를 가져올 파일 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="505"/>
        <source>{name} (default)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme Already Exists</source>
        <translation>이미 있는 테마</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="555"/>
        <source>Theme {name} already exists. Do you want to replace it?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>Import Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="617"/>
        <source>There was a problem importing {file_name}.

It is corrupt, inaccessible or not a valid theme.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>Validation Error</source>
        <translation>검증 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="642"/>
        <source>A theme with this name already exists.</source>
        <translation>이 이름을 가진 테마가 이미 있습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="773"/>
        <source>You are unable to delete the default theme.</source>
        <translation>기본 테마를 삭제할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="782"/>
        <source>{count} time(s) by {plugin}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Unable to delete theme</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/thememanager.py" line="789"/>
        <source>Theme is currently used 

{text}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ThemeWizard</name>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>Background Image Empty</source>
        <translation>배경 그림 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="193"/>
        <source>You have not selected a background image. Please select one before continuing.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="301"/>
        <source>Edit Theme - {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>Theme Name Missing</source>
        <translation>테마 이름 빠짐</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="570"/>
        <source>There is no name for this theme. Please enter one.</source>
        <translation>이 테마에 이름이 없습니다. 이름을 입력하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Theme Name Invalid</source>
        <translation>테마 이름이 잘못됨</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeform.py" line="575"/>
        <source>Invalid theme name. Please enter one.</source>
        <translation>잘못된 테마 이름입니다. 테마 이름을 입력하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="128"/>
        <source>Select Image</source>
        <translation>이미지를 선택하세요</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="153"/>
        <source>Select Video</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="411"/>
        <source>Theme Wizard</source>
        <translation>테마 마법사</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="412"/>
        <source>Welcome to the Theme Wizard</source>
        <translation>테마 마법사를 사용하시는 여러분 반갑습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="414"/>
        <source>This wizard will help you to create and edit your themes. Click the next button below to start the process by setting up your background.</source>
        <translation>이 마법사는 테마를 만들고 편집하도록 도와줍니다. 배경을 설정하여 과정을 시작하려면 다음 단추를 누르십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="417"/>
        <source>Set Up Background</source>
        <translation>배경 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="418"/>
        <source>Set up your theme&apos;s background according to the parameters below.</source>
        <translation>하단의 매개 변수 값에 따라 테마의 배경을 설정합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="420"/>
        <source>Background type:</source>
        <translation>배경 형식:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="421"/>
        <source>Solid color</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="422"/>
        <source>Gradient</source>
        <translation>그레디언트</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="425"/>
        <source>Transparent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="427"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="468"/>
        <source>color:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="430"/>
        <source>Starting color:</source>
        <translation>시작 색상:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="431"/>
        <source>Ending color:</source>
        <translation>마침 색상:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="432"/>
        <source>Gradient:</source>
        <translation>그레디언트:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="433"/>
        <source>Horizontal</source>
        <translation>수평</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="435"/>
        <source>Vertical</source>
        <translation>수직</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="437"/>
        <source>Circular</source>
        <translation>원형</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="439"/>
        <source>Top Left - Bottom Right</source>
        <translation>좌측 상단 - 우측 하단</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="441"/>
        <source>Bottom Left - Top Right</source>
        <translation>좌측 하단 - 우측 상단</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="445"/>
        <source>Background color:</source>
        <translation>배경색:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="447"/>
        <source>Main Area Font Details</source>
        <translation>주 영역 글꼴 세부 모양새</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="448"/>
        <source>Define the font and display characteristics for the Display text</source>
        <translation>표시 문자열의 글꼴 및 표시 모양새를 정의합니다</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="467"/>
        <source>Font:</source>
        <translation>글꼴:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="469"/>
        <source>Size:</source>
        <translation>크기:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="454"/>
        <source>Line Spacing:</source>
        <translation>줄 간격:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="456"/>
        <source>&amp;Outline:</source>
        <translation>외곽선(&amp;O):</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="459"/>
        <source>&amp;Shadow:</source>
        <translation>그림자(&amp;S):</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="462"/>
        <source>Bold</source>
        <translation>굵게</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="463"/>
        <source>Italic</source>
        <translation>기울임 꼴</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="464"/>
        <source>Footer Area Font Details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="465"/>
        <source>Define the font and display characteristics for the Footer text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="471"/>
        <source>Text Formatting Details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="472"/>
        <source>Allows additional display formatting information to be defined</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="474"/>
        <source>Horizontal Align:</source>
        <translation>수평 정렬:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="475"/>
        <source>Left</source>
        <translation>좌측</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="476"/>
        <source>Right</source>
        <translation>우측</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="477"/>
        <source>Center</source>
        <translation>가운데</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="478"/>
        <source>Justify</source>
        <translation>균등 배치</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="479"/>
        <source>Transitions:</source>
        <translation>변환:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="480"/>
        <source>Fade</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="481"/>
        <source>Slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="482"/>
        <source>Concave</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="483"/>
        <source>Convex</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="484"/>
        <source>Zoom</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="485"/>
        <source>Speed:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="486"/>
        <source>Normal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="487"/>
        <source>Fast</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="488"/>
        <source>Slow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="489"/>
        <source>Output Area Locations</source>
        <translation>출력 영역 위치</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="490"/>
        <source>Allows you to change and move the Main and Footer areas.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="492"/>
        <source>&amp;Main Area</source>
        <translation>주 영역(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="493"/>
        <source>&amp;Use default location</source>
        <translation>기본 위치 사용(&amp;U)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="503"/>
        <source>X position:</source>
        <translation>X 위치:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="510"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="505"/>
        <source>Y position:</source>
        <translation>Y 위치:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="507"/>
        <source>Width:</source>
        <translation>너비:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="509"/>
        <source>Height:</source>
        <translation>높이:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="502"/>
        <source>&amp;Footer Area</source>
        <translation>각주 영역(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="511"/>
        <source>Use default location</source>
        <translation>기본 위치 사용</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="513"/>
        <source>Layout Preview</source>
        <translation>배치 미리보기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="514"/>
        <source>Preview and Save</source>
        <translation>미리보고 저장</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="515"/>
        <source>Preview the theme and save it.</source>
        <translation>테마를 미리 보고 저장합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themewizard.py" line="516"/>
        <source>Theme name:</source>
        <translation>테마 이름:</translation>
    </message>
</context>
<context>
    <name>OpenLP.Themes</name>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="74"/>
        <source>Recreating Theme Thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themeprogressdialog.py" line="75"/>
        <source>TextLabel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.ThemesTab</name>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="44"/>
        <source>Themes</source>
        <translation>테마</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="116"/>
        <source>Global Theme</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="117"/>
        <source>Universal Settings</source>
        <translation>공통 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="118"/>
        <source>&amp;Wrap footer text</source>
        <translation>각주 자동 줄 바꿈(&amp;W)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="119"/>
        <source>Theme Level</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="120"/>
        <source>S&amp;ong Level</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="121"/>
        <source>Use the theme from each song in the database. If a song doesn&apos;t have a theme associated with it, then use the service&apos;s theme. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="125"/>
        <source>&amp;Service Level</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="126"/>
        <source>Use the theme from the service, overriding any of the individual songs&apos; themes. If the service doesn&apos;t have a theme, then use the global theme.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="130"/>
        <source>&amp;Global Level</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/ui/themestab.py" line="131"/>
        <source>Use the global theme, overriding any themes associated with either the service or the songs.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenLP.Ui</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="337"/>
        <source>About</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="338"/>
        <source>&amp;Add</source>
        <translation>추가(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="339"/>
        <source>Add group</source>
        <translation>모음 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="340"/>
        <source>Add group.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="341"/>
        <source>Advanced</source>
        <translation>고급 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="342"/>
        <source>All Files</source>
        <translation>모든 파일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="343"/>
        <source>Automatic</source>
        <translation>자동</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="344"/>
        <source>Background Color</source>
        <translation>배경색</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="345"/>
        <source>Background color:</source>
        <translation>배경색:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="346"/>
        <source>Search is Empty or too Short</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="347"/>
        <source>&lt;strong&gt;The search you have entered is empty or shorter than 3 characters long.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please try again with a longer search.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="350"/>
        <source>No Bibles Available</source>
        <translation>성경 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="351"/>
        <source>&lt;strong&gt;There are no Bibles currently installed.&lt;/strong&gt;&lt;br&gt;&lt;br&gt;Please use the Import Wizard to install one or more Bibles.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="353"/>
        <source>Bottom</source>
        <translation>아래</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="354"/>
        <source>Browse...</source>
        <translation>찾아보기...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="355"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="356"/>
        <source>CCLI number:</source>
        <translation>CCLI 번호:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="357"/>
        <source>CCLI song number:</source>
        <translation>CCLI 곡 번호:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="358"/>
        <source>Create a new service.</source>
        <translation>새 예배 자료를 만듭니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="359"/>
        <source>Confirm Delete</source>
        <translation>삭제 확인</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="360"/>
        <source>Continuous</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="361"/>
        <source>Default</source>
        <translation>기본</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="362"/>
        <source>Default Color:</source>
        <translation>기본 색상:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="363"/>
        <source>Service %Y-%m-%d %H-%M</source>
        <comment>This may not contain any of the following characters: /\?*|&lt;&gt;[]&quot;:+
See http://docs.python.org/library/datetime.html#strftime-strptime-behavior for more information.</comment>
        <translation>예베 %Y-%m-%d %H-%M</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="367"/>
        <source>&amp;Delete</source>
        <translation>삭제(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="368"/>
        <source>Display style:</source>
        <translation>표시 방식:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="369"/>
        <source>Duplicate Error</source>
        <translation>복제 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="370"/>
        <source>&amp;Edit</source>
        <translation>수정(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="371"/>
        <source>Empty Field</source>
        <translation>빈 내용</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="372"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="373"/>
        <source>Export</source>
        <translation>내보내기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="374"/>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="375"/>
        <source>File appears to be corrupt.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="376"/>
        <source>pt</source>
        <comment>Abbreviated font point size unit</comment>
        <translation>pt</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="377"/>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="378"/>
        <source>h</source>
        <comment>The abbreviated unit for hours</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="379"/>
        <source>Invalid Folder Selected</source>
        <comment>Singular</comment>
        <translation>잘못된 폴더 선택함</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="380"/>
        <source>Invalid File Selected</source>
        <comment>Singular</comment>
        <translation>잘못된 파일 선택함</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="381"/>
        <source>Invalid Files Selected</source>
        <comment>Plural</comment>
        <translation>잘못된 파일 선택함</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="382"/>
        <source>Image</source>
        <translation>그림</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="383"/>
        <source>Import</source>
        <translation>가져오기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="384"/>
        <source>Layout style:</source>
        <translation>배치 방식:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="385"/>
        <source>Live</source>
        <translation>라이브</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="386"/>
        <source>Live Stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="387"/>
        <source>Live Background Error</source>
        <translation>라이브 배경 오류</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="388"/>
        <source>Live Toolbar</source>
        <translation>라이브 도구 모음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="389"/>
        <source>Load</source>
        <translation>불러오기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="390"/>
        <source>Manufacturer</source>
        <comment>Singular</comment>
        <translation>제조사</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="391"/>
        <source>Manufacturers</source>
        <comment>Plural</comment>
        <translation>제조사</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="392"/>
        <source>Model</source>
        <comment>Singular</comment>
        <translation>모델</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="393"/>
        <source>Models</source>
        <comment>Plural</comment>
        <translation>모델</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="394"/>
        <source>m</source>
        <comment>The abbreviated unit for minutes</comment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="395"/>
        <source>Middle</source>
        <translation>가운데</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="396"/>
        <source>New</source>
        <translation>새로 만들기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="397"/>
        <source>New Service</source>
        <translation>새 예배</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="398"/>
        <source>New Theme</source>
        <translation>새 테마</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="399"/>
        <source>Next Track</source>
        <translation>다음 트랙</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="400"/>
        <source>No Folder Selected</source>
        <comment>Singular</comment>
        <translation>선택한 폴더 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="401"/>
        <source>No File Selected</source>
        <comment>Singular</comment>
        <translation>선택한 파일 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="402"/>
        <source>No Files Selected</source>
        <comment>Plural</comment>
        <translation>선택한 파일 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="403"/>
        <source>No Item Selected</source>
        <comment>Singular</comment>
        <translation>선택한 항목 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="404"/>
        <source>No Items Selected</source>
        <comment>Plural</comment>
        <translation>선택한 항목 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="405"/>
        <source>No Search Results</source>
        <translation>검색 결과 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="406"/>
        <source>OpenLP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="407"/>
        <source>OpenLP 2.0 and up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="408"/>
        <source>OpenLP is already running on this machine. 
Closing this instance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="409"/>
        <source>Open service.</source>
        <translation>예배를 엽니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="410"/>
        <source>Optional, this will be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="411"/>
        <source>Optional, this won&apos;t be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="412"/>
        <source>Play Slides in Loop</source>
        <translation>슬라이드 반복 재생</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="413"/>
        <source>Play Slides to End</source>
        <translation>슬라이드를 끝까지 재생</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="414"/>
        <source>Preview</source>
        <translation>미리보기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="415"/>
        <source>Preview Toolbar</source>
        <translation>미리보기 도구 모음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="416"/>
        <source>Print Service</source>
        <translation>인쇄 서비스</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="417"/>
        <source>Projector</source>
        <comment>Singular</comment>
        <translation>프로젝터</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="418"/>
        <source>Projectors</source>
        <comment>Plural</comment>
        <translation>프로젝터</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="419"/>
        <source>Replace Background</source>
        <translation>배경 바꾸기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="420"/>
        <source>Replace live background.</source>
        <translation>라이브 배경을 바꿉니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="421"/>
        <source>Replace live background is not available when the WebKit player is disabled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="423"/>
        <source>Reset Background</source>
        <translation>배경 초기화</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="424"/>
        <source>Reset live background.</source>
        <translation>라이브 배경을 초기화합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="425"/>
        <source>Required, this will be displayed in footer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="426"/>
        <source>s</source>
        <comment>The abbreviated unit for seconds</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="428"/>
        <source>Save &amp;&amp; Preview</source>
        <translation>저장하고 미리보기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="429"/>
        <source>Search</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="430"/>
        <source>Search Themes...</source>
        <comment>Search bar place holder text </comment>
        <translation>테마 검색...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="431"/>
        <source>You must select an item to delete.</source>
        <translation>삭제할 항목을 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="432"/>
        <source>You must select an item to edit.</source>
        <translation>편집할 항목을 선택해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="433"/>
        <source>Settings</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="434"/>
        <source>Save Service</source>
        <translation>예배 저장</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="435"/>
        <source>Service</source>
        <translation>예배</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="436"/>
        <source>Please type more text to use &apos;Search As You Type&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="437"/>
        <source>Optional &amp;Split</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="438"/>
        <source>Split a slide into two only if it does not fit on the screen as one slide.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="64"/>
        <source>Starting import...</source>
        <translation>가져오기 시작함...</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="441"/>
        <source>Stop Play Slides in Loop</source>
        <translation>슬라이드 반복 재생 멈춤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="442"/>
        <source>Stop Play Slides to End</source>
        <translation>마지막 슬라이드 표시 후 멈춤</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="443"/>
        <source>Theme</source>
        <comment>Singular</comment>
        <translation>테마</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="444"/>
        <source>Themes</source>
        <comment>Plural</comment>
        <translation>테마</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="445"/>
        <source>Tools</source>
        <translation>도구</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="446"/>
        <source>Top</source>
        <translation>상단</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="447"/>
        <source>Unsupported File</source>
        <translation>지원하지 않는 파일</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="448"/>
        <source>Verse Per Slide</source>
        <translation>슬라이드 당 절 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="449"/>
        <source>Verse Per Line</source>
        <translation>한 줄 당 절 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="450"/>
        <source>Version</source>
        <translation>버전</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="451"/>
        <source>View</source>
        <translation>보기</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="452"/>
        <source>View Mode</source>
        <translation>보기 상태</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="453"/>
        <source>Video</source>
        <translation>동영상</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="454"/>
        <source>Web Interface, Download and Install latest Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="455"/>
        <source>Book Chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="456"/>
        <source>Chapter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="457"/>
        <source>Verse</source>
        <translation>절</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="459"/>
        <source>Psalm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="460"/>
        <source>Book names may be shortened from full names, for an example Ps 23 = Psalm 23</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="706"/>
        <source>Written by</source>
        <translation>작성자: </translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="163"/>
        <source>Delete the selected item.</source>
        <translation>선택한 항목을 삭제합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="166"/>
        <source>Move selection up one position.</source>
        <translation>선택 항목을 한 단계 위로 이동합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="169"/>
        <source>Move selection down one position.</source>
        <translation>선택 항목을 한단계 아래로 이동합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/lib/ui.py" line="310"/>
        <source>&amp;Vertical Align:</source>
        <translation>수직 정렬(&amp;V):</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="53"/>
        <source>Finished import.</source>
        <translation>가져오기를 마쳤습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="54"/>
        <source>Format:</source>
        <translation>형식:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="56"/>
        <source>Importing</source>
        <translation>가져오는 중</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="57"/>
        <source>Importing &quot;{source}&quot;...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="58"/>
        <source>Select Import Source</source>
        <translation>가져올 원본 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="59"/>
        <source>Select the import format and the location to import from.</source>
        <translation>가져오기 형식 및 가져올 위치를 선택하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="60"/>
        <source>Open {file_type} File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="61"/>
        <source>Open {folder_name} Folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="62"/>
        <source>%p%</source>
        <translation>%p%</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="63"/>
        <source>Ready.</source>
        <translation>준비.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="65"/>
        <source>You need to specify one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>가져올 %s 파일을 지정해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="67"/>
        <source>You need to specify at least one %s file to import from.</source>
        <comment>A file type e.g. OpenSong</comment>
        <translation>최소한의 가져올 %s 파일을 지정해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/widgets/wizard.py" line="69"/>
        <source>You need to specify one %s folder to import from.</source>
        <comment>A song format e.g. PowerSong</comment>
        <translation>가져올 %s 폴더를 지정해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/bibleimportform.py" line="372"/>
        <source>Welcome to the Bible Import Wizard</source>
        <translation>성경 가져오기 마법사를 사용하시는 여러분 반갑습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="137"/>
        <source>Welcome to the Duplicate Song Removal Wizard</source>
        <translation>중복 곡 제거 마법사를 사용하시는 여러분 반갑습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="144"/>
        <source>Welcome to the Song Export Wizard</source>
        <translation>곡 내보내기 마법사를 사용하시는 여러분 반갑습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="135"/>
        <source>Welcome to the Song Import Wizard</source>
        <translation>곡 가져오기 마법사를 사용하시는 여러분 반갑습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="33"/>
        <source>Author</source>
        <comment>Singular</comment>
        <translation>작성자</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="34"/>
        <source>Authors</source>
        <comment>Plural</comment>
        <translation>작성자</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="35"/>
        <source>Author Unknown</source>
        <translation>알 수 없는 작성자</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="37"/>
        <source>Songbook</source>
        <comment>Singular</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="38"/>
        <source>Songbooks</source>
        <comment>Plural</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="39"/>
        <source>Title and/or verses not found</source>
        <translation>제목 또는 절이 없습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="40"/>
        <source>Song Maintenance</source>
        <translation>곡 관리</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="41"/>
        <source>Topic</source>
        <comment>Singular</comment>
        <translation>주제</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="42"/>
        <source>Topics</source>
        <comment>Plural</comment>
        <translation>주제</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/ui.py" line="43"/>
        <source>XML syntax error</source>
        <translation>XML 문법 오류</translation>
    </message>
</context>
<context>
    <name>OpenLP.core.lib</name>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="400"/>
        <source>{one} and {two}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/lib/__init__.py" line="402"/>
        <source>{first} and {last}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenPL.PJLink</name>
    <message>
        <location filename="../../openlp/core/projectors/constants.py" line="499"/>
        <source>Other</source>
        <translation>기타</translation>
    </message>
</context>
<context>
    <name>Openlp.ProjectorTab</name>
    <message>
        <location filename="../../openlp/core/projectors/tab.py" line="119"/>
        <source>Source select dialog interface</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PresentationPlugin</name>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="168"/>
        <source>&lt;strong&gt;Presentation Plugin&lt;/strong&gt;&lt;br /&gt;The presentation plugin provides the ability to show presentations using a number of different programs. The choice of available presentation programs is available to the user in a drop down box.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="181"/>
        <source>Presentation</source>
        <comment>name singular</comment>
        <translation>프리젠테이션</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="182"/>
        <source>Presentations</source>
        <comment>name plural</comment>
        <translation>프리젠테이션</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="186"/>
        <source>Presentations</source>
        <comment>container title</comment>
        <translation>프리젠테이션</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="190"/>
        <source>Load a new presentation.</source>
        <translation>새 프리젠테이션을 불러옵니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="194"/>
        <source>Delete the selected presentation.</source>
        <translation>선택한 프리젠테이션을 삭제합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="195"/>
        <source>Preview the selected presentation.</source>
        <translation>선택한 프리젠테이션을 미리봅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="196"/>
        <source>Send the selected presentation live.</source>
        <translation>선택한 프리젠테이션 라이브를 보냅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/presentationplugin.py" line="197"/>
        <source>Add the selected presentation to the service.</source>
        <translation>선택한 프리젠테이션을 예배 프로그램에 추가합니다.</translation>
    </message>
</context>
<context>
    <name>PresentationPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="62"/>
        <source>Select Presentation(s)</source>
        <translation>프리젠테이션 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="63"/>
        <source>Automatic</source>
        <translation>자동</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="64"/>
        <source>Present using:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="92"/>
        <source>Presentations ({text})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>File Exists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="182"/>
        <source>A presentation with that filename already exists.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="207"/>
        <source>This type of presentation is not supported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>Missing Presentation</source>
        <translation>프리젠테이션 빠짐</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="378"/>
        <source>The presentation {name} no longer exists.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/mediaitem.py" line="369"/>
        <source>The presentation {name} is incomplete, please reload.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PresentationPlugin.PowerpointDocument</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/powerpointcontroller.py" line="536"/>
        <source>An error occurred in the PowerPoint integration and the presentation will be stopped. Restart the presentation if you wish to present it.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PresentationPlugin.PresentationTab</name>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="105"/>
        <source>Available Controllers</source>
        <translation>존재하는 컨트롤러</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="111"/>
        <source>PDF options</source>
        <translation>PDF 옵션</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="112"/>
        <source>PowerPoint options</source>
        <translation>파워포인트 옵션</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="113"/>
        <source>Allow presentation application to be overridden</source>
        <translation>프리젠테이션 프로그램 설정 중복 적용 허용</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="115"/>
        <source>Clicking on the current slide advances to the next effect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="118"/>
        <source>Let PowerPoint control the size and monitor of the presentations
(This may fix PowerPoint scaling issues in Windows 8 and 10)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="122"/>
        <source>Use given full path for mudraw or ghostscript binary:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="124"/>
        <source>Select mudraw or ghostscript binary</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="131"/>
        <source>{name} (unavailable)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/presentations/lib/presentationtab.py" line="228"/>
        <source>The program is not ghostscript or mudraw which is required.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RemotePlugin</name>
    <message>
        <location filename="../../openlp/core/api/http/server.py" line="159"/>
        <source>Importing Website</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RemotePlugin.Mobile</name>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="57"/>
        <source>Remote</source>
        <translation>원격</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="58"/>
        <source>Stage View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="59"/>
        <source>Live View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="60"/>
        <source>Chords View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="67"/>
        <source>Service Manager</source>
        <translation>예배 내용 관리자</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="68"/>
        <source>Slide Controller</source>
        <translation>슬라이드 컨트롤러</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="69"/>
        <source>Alerts</source>
        <translation>알림</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="70"/>
        <source>Search</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="71"/>
        <source>Home</source>
        <translation>처음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="72"/>
        <source>Refresh</source>
        <translation>새로 고침</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="73"/>
        <source>Blank</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="74"/>
        <source>Theme</source>
        <translation>테마</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="75"/>
        <source>Desktop</source>
        <translation>데스크톱</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="76"/>
        <source>Show</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="77"/>
        <source>Prev</source>
        <translation>이전</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="78"/>
        <source>Next</source>
        <translation>다음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="79"/>
        <source>Text</source>
        <translation>텍스트</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="80"/>
        <source>Show Alert</source>
        <translation>경고 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="81"/>
        <source>Go Live</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="82"/>
        <source>Add to Service</source>
        <translation>예배 프로그램에 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="83"/>
        <source>Add &amp;amp; Go to Service</source>
        <translation>예배 프로그램에 추가하고 이동</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="84"/>
        <source>No Results</source>
        <translation>결과 없음</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="85"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="86"/>
        <source>Service</source>
        <translation>예배</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="87"/>
        <source>Slides</source>
        <translation>슬라이드</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/endpoint/core.py" line="88"/>
        <source>Settings</source>
        <translation>설정</translation>
    </message>
</context>
<context>
    <name>RemotePlugin.RemoteTab</name>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="158"/>
        <source>Remote Interface</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="159"/>
        <source>Server Settings</source>
        <translation>서버 설정</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="160"/>
        <source>Serve on IP address:</source>
        <translation>제공 IP 주소:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="161"/>
        <source>Port number:</source>
        <translation>포트 번호:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="162"/>
        <source>Remote URL:</source>
        <translation>원격 URL:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="163"/>
        <source>Stage view URL:</source>
        <translation>스테이지 뷰 URL:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="164"/>
        <source>Live view URL:</source>
        <translation>라이브 뷰 URL:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="165"/>
        <source>Chords view URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="166"/>
        <source>Display stage time in 12h format</source>
        <translation>12시간 형식으로 스테이지 시간 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="167"/>
        <source>Show thumbnails of non-text slides in remote and stage view.</source>
        <translation>원격 및 스테이지 보기의 비 텍스트 슬라이드 미리 보기 그림을 표시합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="169"/>
        <source>Remote App</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="170"/>
        <source>Scan the QR code or click &lt;a href=&quot;{qr}&quot;&gt;download&lt;/a&gt; to download an app for your mobile device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="174"/>
        <source>User Authentication</source>
        <translation>사용자 인증</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="177"/>
        <source>User id:</source>
        <translation>사용자 ID:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="178"/>
        <source>Password:</source>
        <translation>암호:</translation>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="179"/>
        <source>Current Version number:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/api/tab.py" line="180"/>
        <source>Latest Version number:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongPlugin.ReportSongList</name>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>Save File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>song_extract.csv</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="45"/>
        <source>CSV format (*.csv)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report Creation</source>
        <translation>보고서 작성</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="82"/>
        <source>Report 
{name} 
has been successfully created. </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>Song Extraction Failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/reporting.py" line="90"/>
        <source>An error occurred while extracting: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongUsagePlugin</name>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="90"/>
        <source>&amp;Song Usage Tracking</source>
        <translation>곡 재생 기록 추적(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>&amp;Delete Tracking Data</source>
        <translation>추적 데이터 삭제(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="92"/>
        <source>Delete song usage data up to a specified date.</source>
        <translation>지정 날짜의 곡 재생 상태 데이터를 삭제합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>&amp;Extract Tracking Data</source>
        <translation>추적 데이터 추출(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="98"/>
        <source>Generate a report on song usage.</source>
        <translation>곡 재생 상태 보고서를 만듭니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="104"/>
        <source>Toggle Tracking</source>
        <translation>추적 설정 전환</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="118"/>
        <source>Toggle the tracking of song usage.</source>
        <translation>곡 재생 추적 설정을 전환합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="154"/>
        <source>Song Usage</source>
        <translation>곡 재생 기록</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="178"/>
        <source>Song usage tracking is active.</source>
        <translation>곡 재생 기록 추적을 활성화했습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="183"/>
        <source>Song usage tracking is inactive.</source>
        <translation>곡 재생 기록 추적을 비활성화했습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="193"/>
        <source>display</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="201"/>
        <source>printed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="238"/>
        <source>&lt;strong&gt;SongUsage Plugin&lt;/strong&gt;&lt;br /&gt;This plugin tracks the usage of songs in services.</source>
        <translation>&lt;strong&gt;SongUsage 플러그인&lt;/strong&gt;&lt;br /&gt;이 플러그인은 예배 프로그램의 곡 재생 상태를 추적합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="249"/>
        <source>SongUsage</source>
        <comment>name singular</comment>
        <translation>SongUsage</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="250"/>
        <source>SongUsage</source>
        <comment>name plural</comment>
        <translation>SongUsage</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/songusageplugin.py" line="254"/>
        <source>SongUsage</source>
        <comment>container title</comment>
        <translation>SongUsage</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDeleteForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="64"/>
        <source>Delete Song Usage Data</source>
        <translation>곡 재생 상태 데이터 삭제</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeletedialog.py" line="66"/>
        <source>Select the date up to which the song usage data should be deleted. 
All data recorded before this date will be permanently deleted.</source>
        <translation>삭제할 곡 재생 상태 데이터에 해당하는 최근 날짜를 선택하십시오.
선택한 날짜 이전에 기록한 모든 데이터를 완전히 삭제합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Delete Selected Song Usage Events?</source>
        <translation>선택한 곡 재생 상태 기록을 삭제하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="51"/>
        <source>Are you sure you want to delete selected Song Usage data?</source>
        <translation>정말로 선택한 곡 재생 상태 기록을 삭제하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>Deletion Successful</source>
        <translation>삭제 성공</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedeleteform.py" line="60"/>
        <source>All requested data has been deleted successfully.</source>
        <translation>요청한 모든 데이터 삭제에 성공했습니다.</translation>
    </message>
</context>
<context>
    <name>SongUsagePlugin.SongUsageDetailForm</name>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="85"/>
        <source>Song Usage Extraction</source>
        <translation>곡 재생 기록 추출</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="87"/>
        <source>Select Date Range</source>
        <translation>날짜 범위를 선택하십시오</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="88"/>
        <source>to</source>
        <translation>~</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetaildialog.py" line="89"/>
        <source>Report Location</source>
        <translation>보고서 위치</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>Output Path Not Selected</source>
        <translation>출력 경로를 선택하지 않았습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="84"/>
        <source>You have not set a valid output location for your song usage report.
Please select an existing path on your computer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="91"/>
        <source>usage_detail_{old}_{new}.txt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report Creation</source>
        <translation>보고서 작성</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="111"/>
        <source>Report
{name}
has been successfully created.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>Report Creation Failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songusage/forms/songusagedetailform.py" line="118"/>
        <source>An error occurred while creating the report: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="301"/>
        <source>Arabic (CP-1256)</source>
        <translation>아라비아어(CP-1256)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="302"/>
        <source>Baltic (CP-1257)</source>
        <translation>발트해 언어(CP-1257)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="303"/>
        <source>Central European (CP-1250)</source>
        <translation>중앙 유럽어(CP-1250)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="304"/>
        <source>Cyrillic (CP-1251)</source>
        <translation>키릴어(CP-1251)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="305"/>
        <source>Greek (CP-1253)</source>
        <translation>그리스어(CP-1253)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="306"/>
        <source>Hebrew (CP-1255)</source>
        <translation>히브리어(CP-1255)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="307"/>
        <source>Japanese (CP-932)</source>
        <translation>일본어(CP-932)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="308"/>
        <source>Korean (CP-949)</source>
        <translation>한국어(CP-949)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="309"/>
        <source>Simplified Chinese (CP-936)</source>
        <translation>중국어 간체(CP-936)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="310"/>
        <source>Thai (CP-874)</source>
        <translation>타이어(CP-874)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="311"/>
        <source>Traditional Chinese (CP-950)</source>
        <translation>중국어 번체(CP-950)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="312"/>
        <source>Turkish (CP-1254)</source>
        <translation>터키어(CP-1254)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="313"/>
        <source>Vietnam (CP-1258)</source>
        <translation>베트남어(CP-1258)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="314"/>
        <source>Western European (CP-1252)</source>
        <translation>서유럽어(CP-1252)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Character Encoding</source>
        <translation>문자 인코딩</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="323"/>
        <source>The codepage setting is responsible
for the correct character representation.
Usually you are fine with the preselected choice.</source>
        <translation>코드 페이지를 설정하면
문자를 올바르게 표시합니다.
보통 이전에 선택한 값이 좋습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="331"/>
        <source>Please choose the character encoding.
The encoding is responsible for the correct character representation.</source>
        <translation>문자 인코딩을 선택하십시오.
문자를 올바르게 표시하는 인코딩 설정입니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>&amp;Song</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="186"/>
        <source>Import songs using the import wizard.</source>
        <translation>가져오기 마법사로 곡을 가져옵니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>CCLI SongSelect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="192"/>
        <source>Import songs from CCLI&apos;s SongSelect service.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="206"/>
        <source>Exports songs using the export wizard.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="223"/>
        <source>Songs</source>
        <translation>곡</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>&amp;Re-index Songs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="224"/>
        <source>Re-index the songs database to improve searching and ordering.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find &amp;Duplicate Songs</source>
        <translation>중복 노래 찾기(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="230"/>
        <source>Find and remove duplicate songs in the song database.</source>
        <translation>노래 데이터베이스에서 중복 노래를 찾아 제거합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Song List Report</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="235"/>
        <source>Produce a CSV file of all the songs in the database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="259"/>
        <source>Reindexing songs...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="261"/>
        <source>Reindexing songs</source>
        <translation>노래 다시 인덱싱하는 중</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="304"/>
        <source>&lt;strong&gt;Songs Plugin&lt;/strong&gt;&lt;br /&gt;The songs plugin provides the ability to display and manage songs.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="347"/>
        <source>Song</source>
        <comment>name singular</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="348"/>
        <source>Songs</source>
        <comment>name plural</comment>
        <translation>곡</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="352"/>
        <source>Songs</source>
        <comment>container title</comment>
        <translation>곡</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="358"/>
        <source>Add a new song.</source>
        <translation>새 노래를 추가합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="359"/>
        <source>Edit the selected song.</source>
        <translation>선택한 노래를 편집합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="360"/>
        <source>Delete the selected song.</source>
        <translation>선택한 노래를 삭제합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="361"/>
        <source>Preview the selected song.</source>
        <translation>선택한 노래를 미리 들어봅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="362"/>
        <source>Send the selected song live.</source>
        <translation>선택한 노래를 실시간으로 내보냅니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="363"/>
        <source>Add the selected song to the service.</source>
        <translation>선택한 노래를 서비스에 추가합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/songsplugin.py" line="388"/>
        <source>Importing Songs</source>
        <translation>곡 가져오는 중</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="67"/>
        <source>Words</source>
        <comment>Author who wrote the lyrics of a song</comment>
        <translation>단어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="68"/>
        <source>Music</source>
        <comment>Author who wrote the music of a song</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="69"/>
        <source>Words and Music</source>
        <comment>Author who wrote both lyrics and music of a song</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/db.py" line="71"/>
        <source>Translation</source>
        <comment>Author who translated the song</comment>
        <translation>번역</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.AuthorsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="74"/>
        <source>Author Maintenance</source>
        <translation>저작자 관리</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="75"/>
        <source>Display name:</source>
        <translation>표시 이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="76"/>
        <source>First name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsdialog.py" line="77"/>
        <source>Last name:</source>
        <translation>성:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="92"/>
        <source>You need to type in the first name of the author.</source>
        <translation>저작자 이름을 입력해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="97"/>
        <source>You need to type in the last name of the author.</source>
        <translation>저작자 성을 입력해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/authorsform.py" line="102"/>
        <source>You have not set a display name for the author, combine the first and last names?</source>
        <translation>저작자 표시 이름을 설정하지 않았습니다. 성과 이름을 합쳐넣을까요?</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.CCLIFileImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/cclifile.py" line="84"/>
        <source>The file does not have a valid extension.</source>
        <translation>파일 이름에 유효한 확장자가 없습니다.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.DreamBeamImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/dreambeam.py" line="101"/>
        <source>Invalid DreamBeam song file_path. Missing DreamSong tag.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.EasyWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="316"/>
        <source>Administered by {admin}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="393"/>
        <source>&quot;{title}&quot; could not be imported. {entry}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="206"/>
        <source>This file does not exist.</source>
        <translation>파일이 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="210"/>
        <source>Could not find the &quot;Songs.MB&quot; file. It must be in the same folder as the &quot;Songs.DB&quot; file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="225"/>
        <source>This file is not a valid EasyWorship database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="254"/>
        <source>Could not retrieve encoding.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="337"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="350"/>
        <source>This does not appear to be a valid Easy Worship 6 database directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="352"/>
        <source>This is not a valid Easy Worship 6 database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="426"/>
        <source>Unexpected data formatting.</source>
        <translation>예상치 못한 데이터 형식입니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="431"/>
        <source>No song text found.</source>
        <translation>노래 텍스트가 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/easyworship.py" line="471"/>
        <source>
[above are Song Tags with notes imported from EasyWorship]</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.EditBibleForm</name>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="134"/>
        <source>Meta Data</source>
        <translation>메타데이터</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/bibles/forms/editbibledialog.py" line="155"/>
        <source>Custom Book Names</source>
        <translation>사용자 정의 책 이름</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditSongForm</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="427"/>
        <source>&amp;Save &amp;&amp; Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="301"/>
        <source>Song Editor</source>
        <translation>노래 편집기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="302"/>
        <source>&amp;Title:</source>
        <translation>제목(&amp;T):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="303"/>
        <source>Alt&amp;ernate title:</source>
        <translation>대체 제목(&amp;E):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="304"/>
        <source>&amp;Lyrics:</source>
        <translation>가사(&amp;L):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="305"/>
        <source>&amp;Verse order:</source>
        <translation>절 순서(&amp;V):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="308"/>
        <source>Ed&amp;it All</source>
        <translation>모두 편집(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="310"/>
        <source>Title &amp;&amp; Lyrics</source>
        <translation>이름 및 가사(&amp;&amp;)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="313"/>
        <source>&amp;Add to Song</source>
        <translation>노래 추가(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="314"/>
        <source>&amp;Edit Author Type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="337"/>
        <source>&amp;Remove</source>
        <translation>제거(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="316"/>
        <source>&amp;Manage Authors, Topics, Songbooks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="318"/>
        <source>A&amp;dd to Song</source>
        <translation>노래에 추가(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="319"/>
        <source>R&amp;emove</source>
        <translation>제거(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="321"/>
        <source>Add &amp;to Song</source>
        <translation>곡에 추가(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="322"/>
        <source>Re&amp;move</source>
        <translation>제거(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="323"/>
        <source>Authors, Topics &amp;&amp; Songbooks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="326"/>
        <source>New &amp;Theme</source>
        <translation>새 테마(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="327"/>
        <source>Copyright Information</source>
        <translation>저작권 정보</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="330"/>
        <source>Comments</source>
        <translation>설명</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="331"/>
        <source>Theme, Copyright Info &amp;&amp; Comments</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="333"/>
        <source>Linked Audio</source>
        <translation>연결한 오디오</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="335"/>
        <source>Add &amp;File(s)</source>
        <translation>파일 추가(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="336"/>
        <source>Add &amp;Media</source>
        <translation>미디어 추가(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="338"/>
        <source>Remove &amp;All</source>
        <translation>모두 제거(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="340"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; Not all of the verses are in use.</source>
        <translation>&lt;strong&gt;경고:&lt;/strong&gt; 모든 절을 활용하지 않습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongdialog.py" line="342"/>
        <source>&lt;strong&gt;Warning:&lt;/strong&gt; You have not entered a verse order.</source>
        <translation>&lt;strong&gt;경고:&lt;/strong&gt; 가사 절 순서를 입력하지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="202"/>
        <source>There are no verses corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="207"/>
        <source>There is no verse corresponding to &quot;{invalid}&quot;. Valid entries are {valid}.
Please enter the verses separated by spaces.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="211"/>
        <source>Invalid Verse Order</source>
        <translation>잘못된 가사 절 순서`</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="225"/>
        <source>You need to type in a song title.</source>
        <translation>곡 제목을 입력해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="231"/>
        <source>You need to type in at least one verse.</source>
        <translation>최소한 1절은 입력해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="237"/>
        <source>You need to have an author for this song.</source>
        <translation>이 곡의 작사가가 있어야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="256"/>
        <source>There are misplaced formatting tags in the following verses:

{tag}

Please correct these tags before continuing.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="265"/>
        <source>You have {count} verses named {name} {number}. You can have at most 26 verses with the same name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>Add Author</source>
        <translation>작성자 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="582"/>
        <source>This author does not exist, do you want to add them?</source>
        <translation>이 작성자가 없습니다. 추가하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="603"/>
        <source>This author is already in the list.</source>
        <translation>목록에 이미 이 작성자가 있습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="610"/>
        <source>You have not selected a valid author. Either select an author from the list, or type in a new author and click the &quot;Add Author to Song&quot; button to add the new author.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Edit Author Type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="634"/>
        <source>Choose type for this author</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>Add Topic</source>
        <translation>주제 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="661"/>
        <source>This topic does not exist, do you want to add it?</source>
        <translation>이 주제가 없습니다. 추가하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="679"/>
        <source>This topic is already in the list.</source>
        <translation>목록에 이미 이 주제가 있습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="688"/>
        <source>You have not selected a valid topic. Either select a topic from the list, or type in a new topic and click the &quot;Add Topic to Song&quot; button to add the new topic.</source>
        <translation>올바른 주제를 선택하지 않았습니다. 목록에서 주제를 선택하든지 새 주제를 입력한 후 &quot;곡에 새 주제 추가&quot; 단추를 눌러 추가하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>Add Songbook</source>
        <translation>곡집 추가</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="707"/>
        <source>This Songbook does not exist, do you want to add it?</source>
        <translation>이 곡집이 없습니다. 추가하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="724"/>
        <source>This Songbook is already in the list.</source>
        <translation>이 곡집이 이미 목록에 있습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="732"/>
        <source>You have not selected a valid Songbook. Either select a Songbook from the list, or type in a new Songbook and click the &quot;Add to Song&quot; button to add the new Songbook.</source>
        <translation>유효한 곡집을 선택하지 않았습니다. 목록에서 곡집을 선택하거나, 새 곡집 이름을 입력한 후 &quot;곡에 추가&quot; 단추를 눌러 새 곡집을 추가하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editsongform.py" line="932"/>
        <source>Open File(s)</source>
        <translation>파일 열기</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.EditVerseForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="91"/>
        <source>Edit Verse</source>
        <translation>가사 절 편집</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="92"/>
        <source>&amp;Verse type:</source>
        <translation>가사 절 형식(&amp;V):</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="102"/>
        <source>&amp;Forced Split</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="103"/>
        <source>Split the verse when displayed regardless of the screen size.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="105"/>
        <source>&amp;Insert</source>
        <translation>삽입(&amp;I)</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="106"/>
        <source>Split a slide into two by inserting a verse splitter.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="109"/>
        <source>Transpose:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="110"/>
        <source>Up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editversedialog.py" line="111"/>
        <source>Down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="146"/>
        <source>Transposing failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/editverseform.py" line="240"/>
        <source>Invalid Chord</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.ExportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="128"/>
        <source>Select Destination Folder</source>
        <translation>대상 폴더 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="143"/>
        <source>Song Export Wizard</source>
        <translation>곡 내보내기 마법사</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="146"/>
        <source>This wizard will help to export your songs to the open and free &lt;strong&gt;OpenLyrics &lt;/strong&gt; worship song format.</source>
        <translation>이 마법사는 공개 자유 &lt;strong&gt;OpenLyrics&lt;/strong&gt; 찬양곡 형식으로 곡을 내보내는 과정을 안내합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="149"/>
        <source>Select Songs</source>
        <translation>곡 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="150"/>
        <source>Check the songs you want to export.</source>
        <translation>내보낼 곡을 표시하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="153"/>
        <source>Uncheck All</source>
        <translation>모두 표시 해제</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="154"/>
        <source>Check All</source>
        <translation>모두 표시</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="155"/>
        <source>Select Directory</source>
        <translation>디렉터리 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="156"/>
        <source>Select the directory where you want the songs to be saved.</source>
        <translation>곡을 저장할 디렉터리를 선택하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="158"/>
        <source>Directory:</source>
        <translation>디렉터리:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="159"/>
        <source>Exporting</source>
        <translation>내보내는 중</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="160"/>
        <source>Please wait while your songs are exported.</source>
        <translation>곡을 내보내는 동안 기다리십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="176"/>
        <source>You need to add at least one Song to export.</source>
        <translation>내보낼 곡을 적어도 하나는 추가해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>No Save Location specified</source>
        <translation>저장 위치를 지정하지 않았습니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="190"/>
        <source>You need to specify a directory.</source>
        <translation>디렉터리를 지정해야합니다</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="239"/>
        <source>Starting export...</source>
        <translation>내보내기 시작 중...</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.FoilPresenterSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/foilpresenter.py" line="387"/>
        <source>Invalid Foilpresenter song file. No verses found.</source>
        <translation>잘못된 Folipresenter 곡 파일 형식입니다. 가사 절이 없습니다.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.GeneralTab</name>
    <message>
        <location filename="../../openlp/core/ui/advancedtab.py" line="339"/>
        <source>Enable search as you type</source>
        <translation>입력과 동시에 검색 활성화</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.ImportWizardForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="134"/>
        <source>Song Import Wizard</source>
        <translation>곡 가져오기 마법사</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="137"/>
        <source>This wizard will help you to import songs from a variety of formats. Click the next button below to start the process by selecting a format to import from.</source>
        <translation>이 마법사는 다양한 형식의 곡을 가져오는 과정을 도와줍니다. 아래의 다음 단추를 눌러 가져올 위치에서 형식을 선택하는 과정을 시작하십시오.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="152"/>
        <source>Add Files...</source>
        <translation>파일추가...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="154"/>
        <source>Remove File(s)</source>
        <translation>파일삭제</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="165"/>
        <source>Please wait while your songs are imported.</source>
        <translation>음악을 가져오는 중입니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="169"/>
        <source>Copy</source>
        <translation>복사하기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="170"/>
        <source>Save to File</source>
        <translation>파일로 저장하기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songimportform.py" line="336"/>
        <source>Your Song import failed. {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="197"/>
        <source>This importer has been disabled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="209"/>
        <source>OpenLyrics Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="210"/>
        <source>OpenLyrics or OpenLP 2 Exported Song</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="217"/>
        <source>OpenLP 2 Databases</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="220"/>
        <source>Generic Document/Presentation</source>
        <translation>일반 문서/프리젠테이션</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="223"/>
        <source>The generic document/presentation importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="226"/>
        <source>Select Document/Presentation Files</source>
        <translation>문서/프리젠테이션 파일 선택</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="232"/>
        <source>CCLI SongSelect Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="239"/>
        <source>ChordPro Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="246"/>
        <source>DreamBeam Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="253"/>
        <source>EasySlides XML File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="260"/>
        <source>EasyWorship Song Database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="268"/>
        <source>EasyWorship 6 Song Data Directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="276"/>
        <source>EasyWorship Service File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="283"/>
        <source>Foilpresenter Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="291"/>
        <source>LiveWorship Database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="298"/>
        <source>LyriX Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="299"/>
        <source>LyriX (Exported TXT-files)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="306"/>
        <source>MediaShout Database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="307"/>
        <source>The MediaShout importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="323"/>
        <source>OPS Pro database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="324"/>
        <source>The OPS Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="333"/>
        <source>PowerPraise Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="340"/>
        <source>You need to specify a valid PowerSong 1.0 database folder.</source>
        <translation>올바른 PowerSong 1.0 데이터베이스 폴더를 지정해야합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="347"/>
        <source>PresentationManager Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="354"/>
        <source>ProPresenter Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="361"/>
        <source>Singing The Faith Exported Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="363"/>
        <source>First use Singing The Faith Electonic edition to export the song(s) in Text format.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="371"/>
        <source>SongBeamer Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="379"/>
        <source>SongPro Text Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="380"/>
        <source>SongPro (Export File)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="381"/>
        <source>In SongPro, export your songs using the File -&gt; Export menu</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="388"/>
        <source>SongShow Plus Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="395"/>
        <source>Songs Of Fellowship Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="397"/>
        <source>The Songs of Fellowship importer has been disabled because OpenLP cannot access OpenOffice or LibreOffice.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="405"/>
        <source>SundayPlus Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="412"/>
        <source>VideoPsalm Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="413"/>
        <source>VideoPsalm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="414"/>
        <source>The VideoPsalm songbooks are normally located in {path}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="422"/>
        <source>Words Of Worship Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="430"/>
        <source>Worship Assistant Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="432"/>
        <source>Worship Assistant (CSV)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="433"/>
        <source>In Worship Assistant, export your Database to a CSV file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="441"/>
        <source>WorshipCenter Pro Song Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="443"/>
        <source>The WorshipCenter Pro importer is only supported on Windows. It has been disabled due to a missing Python module. If you want to use this importer, you will need to install the &quot;pyodbc&quot; module.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="453"/>
        <source>ZionWorx (CSV)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importer.py" line="454"/>
        <source>First convert your ZionWorx database to a CSV text file, as explained in the &lt;a href=&quot;http://manual.openlp.org/songs.html#importing-from-zionworx&quot;&gt;User Manual&lt;/a&gt;.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.LiveWorshipImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="87"/>
        <source>Extracting data from database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="133"/>
        <source>Could not find Valentina DB ADK libraries </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/liveworship.py" line="161"/>
        <source>Loading the extracting data</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.LyrixImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>File {name}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/lyrix.py" line="104"/>
        <source>Error: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaFilesForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="65"/>
        <source>Select Media File(s)</source>
        <translation>미디어 파일(여러개) 선택하세요</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/mediafilesdialog.py" line="66"/>
        <source>Select one or more audio files from the list below, and click OK to import them into this song.</source>
        <translation>하나 또는 여러개의 오디오 파일을 이하 리스트에서 선택하고 OK를 클릭하여 노래들을 가져옵니다.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaItem</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="721"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Titles</source>
        <translation>제목</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Titles...</source>
        <translation>제목 검색</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="140"/>
        <source>Maintain the lists of authors, topics and books.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Entire Song</source>
        <translation>전체 노래</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Entire Song...</source>
        <translation>전체 노래 검색</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Lyrics</source>
        <translation>가사</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Lyrics...</source>
        <translation>가사 검색...</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Authors...</source>
        <translation>작곡자 검색</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Topics...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Songbooks...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Copyright</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search Copyright...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>CCLI number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="150"/>
        <source>Search CCLI number...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="512"/>
        <source>Are you sure you want to delete the following songs?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="540"/>
        <source>copy</source>
        <comment>For song cloning</comment>
        <translation>복사하기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="642"/>
        <source>Media</source>
        <translation>미디어</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="699"/>
        <source>CCLI License: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/mediaitem.py" line="729"/>
        <source>Failed to render Song footer html.
See log for details</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.MediaShoutImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/mediashout.py" line="62"/>
        <source>Unable to open the MediaShout database.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OPSProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="65"/>
        <source>Unable to connect the OPS Pro database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opspro.py" line="87"/>
        <source>&quot;{title}&quot; could not be imported. {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLPSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openlp.py" line="109"/>
        <source>Not a valid OpenLP 2 song database.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenLyricsExport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/openlyricsexport.py" line="68"/>
        <source>Exporting &quot;{title}&quot;...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.OpenSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/opensong.py" line="139"/>
        <source>Invalid OpenSong song file. Missing song tag.</source>
        <translation>유효하지 않은 OpenSong 파일. Tag 유실.</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.PowerSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No songs to import.</source>
        <translation>아무 노래도 가져오지 않음.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="100"/>
        <source>No {text} files found.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="118"/>
        <source>Invalid {text} file. Unexpected byte value.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="136"/>
        <source>Invalid {text} file. Missing &quot;TITLE&quot; header.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="142"/>
        <source>Invalid {text} file. Missing &quot;COPYRIGHTLINE&quot; header.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/powersong.py" line="148"/>
        <source>Verses not found. Missing &quot;PART&quot; header.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.PresentationManagerImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/presentationmanager.py" line="57"/>
        <source>File is not in XML-format, which is the only format supported.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SingingTheFaithImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="192"/>
        <source>Unknown hint {hint}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>File {file}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/singingthefaith.py" line="287"/>
        <source>Error: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongBookForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="66"/>
        <source>Songbook Maintenance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="67"/>
        <source>&amp;Name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookdialog.py" line="68"/>
        <source>&amp;Publisher:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songbookform.py" line="61"/>
        <source>You need to type in a name for the book.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongExportForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="253"/>
        <source>Finished export. To import these files use the &lt;strong&gt;OpenLyrics&lt;/strong&gt; importer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="257"/>
        <source>Your song export failed.</source>
        <translation>음악불러오기 실패.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songexportform.py" line="259"/>
        <source>Your song export failed because this error occurred: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="67"/>
        <source>Cannot access OpenOffice or LibreOffice</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="82"/>
        <source>Unable to open file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/openoffice.py" line="84"/>
        <source>File not found</source>
        <translation>파일을 찾지 못함</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="104"/>
        <source>copyright</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/songimport.py" line="118"/>
        <source>The following songs could not be imported:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongMaintenanceForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="248"/>
        <source>Could not add your author.</source>
        <translation>저작자를 추가할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="251"/>
        <source>This author already exists.</source>
        <translation>작성자가 이미 추가되어 있습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="264"/>
        <source>Could not add your topic.</source>
        <translation>주제를 추가하지 못함</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="267"/>
        <source>This topic already exists.</source>
        <translation>이 주제는 이미 존재합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="281"/>
        <source>Could not add your book.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="284"/>
        <source>This book already exists.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="389"/>
        <source>Could not save your changes.</source>
        <translation>변경된 사항을 저장할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="316"/>
        <source>The author {original} already exists. Would you like to make songs with author {new} use the existing author {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="329"/>
        <source>Could not save your modified author, because the author already exists.</source>
        <translation>변경된 작성자를 저장할 수 없습니다. 이미 작성자가 존재합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="352"/>
        <source>The topic {original} already exists. Would you like to make songs with topic {new} use the existing topic {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="362"/>
        <source>Could not save your modified topic, because it already exists.</source>
        <translation>변경된 주제를 저장할 수 없습니다. 주제가 이미 존재합니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="391"/>
        <source>The book {original} already exists. Would you like to make songs with book {new} use the existing book {original}?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Delete Author</source>
        <translation>작성자 삭제</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>Are you sure you want to delete the selected author?</source>
        <translation>모든 선택된 저자를 삭제하는 것이 확실합니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="487"/>
        <source>This author cannot be deleted, they are currently assigned to at least one song.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Delete Topic</source>
        <translation>주제 삭제</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>Are you sure you want to delete the selected topic?</source>
        <translation>선택된 주제를 삭제하는 것이 확실합니까?</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="499"/>
        <source>This topic cannot be deleted, it is currently assigned to at least one song.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Delete Book</source>
        <translation>선경 삭제하기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>Are you sure you want to delete the selected book?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songmaintenanceform.py" line="510"/>
        <source>This book cannot be deleted, it is currently assigned to at least one song.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongSelectForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="233"/>
        <source>CCLI SongSelect Importer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="234"/>
        <source>&lt;strong&gt;Note:&lt;/strong&gt; An Internet connection is required in order to import songs from CCLI SongSelect.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="238"/>
        <source>Username:</source>
        <translation>사용자 이름:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="239"/>
        <source>Password:</source>
        <translation>암호:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="240"/>
        <source>Save username and password</source>
        <translation>아이디 및 비밀번호 저장하기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="241"/>
        <source>Login</source>
        <translation>로그인</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="242"/>
        <source>Search Text:</source>
        <translation>문구 검색:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="243"/>
        <source>Search</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="244"/>
        <source>Stop</source>
        <translation>멈추기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="346"/>
        <source>Found {count:d} song(s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="247"/>
        <source>Logout</source>
        <translation>로그아웃</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="248"/>
        <source>View</source>
        <translation>보기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="249"/>
        <source>Title:</source>
        <translation>제목:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="250"/>
        <source>Author(s):</source>
        <translation>작곡자:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="251"/>
        <source>Copyright:</source>
        <translation>저작권:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="252"/>
        <source>CCLI Number:</source>
        <translation>CCLI 번호</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="253"/>
        <source>Lyrics:</source>
        <translation>가사:</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="254"/>
        <source>Back</source>
        <translation>뒤로</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectdialog.py" line="255"/>
        <source>Import</source>
        <translation>가져오기</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>More than 1000 results</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="59"/>
        <source>Your search has returned more than 1000 results, it has been stopped. Please refine your search to fetch better results.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="139"/>
        <source>Logging out...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>Incomplete song</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="202"/>
        <source>This song is missing some information, like the lyrics, and cannot be imported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>Save Username and Password</source>
        <translation>사용자 명과 암호 저장</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="243"/>
        <source>WARNING: Saving your username and password is INSECURE, your password is stored in PLAIN TEXT. Click Yes to save your password or No to cancel this.</source>
        <translation>경고: 사용자명과 암호를 저장하는 것은 안전하지 못 합니다. 암호는 평문으로 저장됩니다. 암호를 저장하시려면 예를 누르시거나 취소하시려면 아니오를 누르세요.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>Error Logging In</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="268"/>
        <source>There was a problem logging in, perhaps your username or password is incorrect?</source>
        <translation>로그인 중 문제가 생겼습니다. 사용자명이나 암호가 잘 못 된것 같습니다.</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>Free user</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="276"/>
        <source>You logged in with a free account, the search will be limited to songs in the public domain.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Song Imported</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/songselectform.py" line="395"/>
        <source>Your song has been imported, would you like to import more songs?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.SongsTab</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="116"/>
        <source>Song related settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="117"/>
        <source>Enable &quot;Go to verse&quot; button in Live panel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="119"/>
        <source>Update service from song edit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="120"/>
        <source>Import missing songs from Service files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="122"/>
        <source>Add Songbooks as first slide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="124"/>
        <source>If enabled all text between &quot;[&quot; and &quot;]&quot; will be regarded as chords.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="126"/>
        <source>Chords</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="127"/>
        <source>Display chords in the main view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="128"/>
        <source>Ignore chords when importing songs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="130"/>
        <source>Chord notation to use:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="131"/>
        <source>English</source>
        <translation>Korean</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="132"/>
        <source>German</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="133"/>
        <source>Neo-Latin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="135"/>
        <source>Footer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="140"/>
        <source>Song Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="141"/>
        <source>Alternate Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="142"/>
        <source>Written By</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="143"/>
        <source>Authors when type is not set</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="145"/>
        <source>Authors (Type &quot;Words&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="147"/>
        <source>Authors (Type &quot;Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="149"/>
        <source>Authors (Type &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="151"/>
        <source>Authors (Type &quot;Translation&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="152"/>
        <source>Authors (Type &quot;Words&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="154"/>
        <source>Authors (Type &quot;Music&quot; &amp; &quot;Words and Music&quot;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="156"/>
        <source>Copyright information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="157"/>
        <source>Songbook Entries</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="159"/>
        <source>CCLI License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="160"/>
        <source>Song CCLI Number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="161"/>
        <source>Topics</source>
        <translation>주제</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Placeholder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="164"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="172"/>
        <source>can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="173"/>
        <source>list of entries, can be empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="177"/>
        <source>How to use Footers:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Footer Template</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="178"/>
        <source>Mako Syntax</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/songstab.py" line="181"/>
        <source>Reset Template</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.TopicsForm</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="60"/>
        <source>Topic Maintenance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsdialog.py" line="61"/>
        <source>Topic name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/topicsform.py" line="58"/>
        <source>You need to type in a topic name.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.VerseType</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="149"/>
        <source>Verse</source>
        <translation>절</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="150"/>
        <source>Chorus</source>
        <translation>후렴</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="151"/>
        <source>Bridge</source>
        <translation>브리지</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="152"/>
        <source>Pre-Chorus</source>
        <translation>Pre-전주</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="153"/>
        <source>Intro</source>
        <translation>전주</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="154"/>
        <source>Ending</source>
        <translation>아웃트로</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/__init__.py" line="155"/>
        <source>Other</source>
        <translation>기타</translation>
    </message>
</context>
<context>
    <name>SongsPlugin.VideoPsalmImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/videopsalm.py" line="134"/>
        <source>Error: {error}</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.WordsofWorshipSongImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/wordsofworship.py" line="177"/>
        <source>Invalid Words of Worship song file. Missing {text!r} header.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipAssistantImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Error reading CSV file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="90"/>
        <source>Line {number:d}: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="182"/>
        <source>Record {count:d}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="122"/>
        <source>Decoding error: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipassistant.py" line="127"/>
        <source>File not valid WorshipAssistant CSV format.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.WorshipCenterProImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/worshipcenterpro.py" line="59"/>
        <source>Unable to connect the WorshipCenter Pro database.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SongsPlugin.ZionWorxImport</name>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Error reading CSV file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="83"/>
        <source>Line {number:d}: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Record {index}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="102"/>
        <source>Decoding error: {error}</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="106"/>
        <source>File not valid ZionWorx CSV format.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/lib/importers/zionworx.py" line="120"/>
        <source>Record %d</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Wizard</name>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="136"/>
        <source>Wizard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="140"/>
        <source>This wizard will help you to remove duplicate songs from the song database. You will have a chance to review every potential duplicate song before it is deleted. So no songs will be deleted without your explicit approval.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="145"/>
        <source>Searching for duplicate songs.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="146"/>
        <source>Please wait while your songs database is analyzed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="148"/>
        <source>Here you can decide which songs to remove and which ones to keep.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="155"/>
        <source>Review duplicate songs ({current}/{total})</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>Information</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../../openlp/plugins/songs/forms/duplicatesongremovalform.py" line="221"/>
        <source>No duplicate songs have been found in the database.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>common.languages</name>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>(Afan) Oromo</source>
        <comment>Language code: om</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Abkhazian</source>
        <comment>Language code: ab</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afar</source>
        <comment>Language code: aa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Afrikaans</source>
        <comment>Language code: af</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Albanian</source>
        <comment>Language code: sq</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amharic</source>
        <comment>Language code: am</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Amuzgo</source>
        <comment>Language code: amu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ancient Greek</source>
        <comment>Language code: grc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Arabic</source>
        <comment>Language code: ar</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Armenian</source>
        <comment>Language code: hy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Assamese</source>
        <comment>Language code: as</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Aymara</source>
        <comment>Language code: ay</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Azerbaijani</source>
        <comment>Language code: az</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bashkir</source>
        <comment>Language code: ba</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Basque</source>
        <comment>Language code: eu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bengali</source>
        <comment>Language code: bn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bhutani</source>
        <comment>Language code: dz</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bihari</source>
        <comment>Language code: bh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bislama</source>
        <comment>Language code: bi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Breton</source>
        <comment>Language code: br</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Bulgarian</source>
        <comment>Language code: bg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Burmese</source>
        <comment>Language code: my</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Byelorussian</source>
        <comment>Language code: be</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cakchiquel</source>
        <comment>Language code: cak</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Cambodian</source>
        <comment>Language code: km</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Catalan</source>
        <comment>Language code: ca</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Chinese</source>
        <comment>Language code: zh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Comaltepec Chinantec</source>
        <comment>Language code: cco</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Corsican</source>
        <comment>Language code: co</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Croatian</source>
        <comment>Language code: hr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Czech</source>
        <comment>Language code: cs</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Danish</source>
        <comment>Language code: da</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Dutch</source>
        <comment>Language code: nl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>English</source>
        <comment>Language code: en</comment>
        <translation>Korean</translation>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Esperanto</source>
        <comment>Language code: eo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Estonian</source>
        <comment>Language code: et</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Faeroese</source>
        <comment>Language code: fo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Fiji</source>
        <comment>Language code: fj</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Finnish</source>
        <comment>Language code: fi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>French</source>
        <comment>Language code: fr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Frisian</source>
        <comment>Language code: fy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Galician</source>
        <comment>Language code: gl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Georgian</source>
        <comment>Language code: ka</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>German</source>
        <comment>Language code: de</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greek</source>
        <comment>Language code: el</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Greenlandic</source>
        <comment>Language code: kl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Guarani</source>
        <comment>Language code: gn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Gujarati</source>
        <comment>Language code: gu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Haitian Creole</source>
        <comment>Language code: ht</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hausa</source>
        <comment>Language code: ha</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hebrew (former iw)</source>
        <comment>Language code: he</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hiligaynon</source>
        <comment>Language code: hil</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hindi</source>
        <comment>Language code: hi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Hungarian</source>
        <comment>Language code: hu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Icelandic</source>
        <comment>Language code: is</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Indonesian (former in)</source>
        <comment>Language code: id</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingua</source>
        <comment>Language code: ia</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Interlingue</source>
        <comment>Language code: ie</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inuktitut (Eskimo)</source>
        <comment>Language code: iu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Inupiak</source>
        <comment>Language code: ik</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Irish</source>
        <comment>Language code: ga</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Italian</source>
        <comment>Language code: it</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Jakalteko</source>
        <comment>Language code: jac</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Japanese</source>
        <comment>Language code: ja</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Javanese</source>
        <comment>Language code: jw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>K&apos;iche&apos;</source>
        <comment>Language code: quc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kannada</source>
        <comment>Language code: kn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kashmiri</source>
        <comment>Language code: ks</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kazakh</source>
        <comment>Language code: kk</comment>
        <translation type="unfinished"/>
    </message>
    <message encoding="UTF-8">
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kekchí </source>
        <comment>Language code: kek</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kinyarwanda</source>
        <comment>Language code: rw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirghiz</source>
        <comment>Language code: ky</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kirundi</source>
        <comment>Language code: rn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Korean</source>
        <comment>Language code: ko</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Kurdish</source>
        <comment>Language code: ku</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Laothian</source>
        <comment>Language code: lo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latin</source>
        <comment>Language code: la</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Latvian, Lettish</source>
        <comment>Language code: lv</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lingala</source>
        <comment>Language code: ln</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Lithuanian</source>
        <comment>Language code: lt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Macedonian</source>
        <comment>Language code: mk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malagasy</source>
        <comment>Language code: mg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malay</source>
        <comment>Language code: ms</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Malayalam</source>
        <comment>Language code: ml</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maltese</source>
        <comment>Language code: mt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mam</source>
        <comment>Language code: mam</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Maori</source>
        <comment>Language code: mri</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Marathi</source>
        <comment>Language code: mr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Moldavian</source>
        <comment>Language code: mo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Mongolian</source>
        <comment>Language code: mn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nahuatl</source>
        <comment>Language code: nah</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nauru</source>
        <comment>Language code: na</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Nepali</source>
        <comment>Language code: ne</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Norwegian</source>
        <comment>Language code: no</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Occitan</source>
        <comment>Language code: oc</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Oriya</source>
        <comment>Language code: or</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Pashto, Pushto</source>
        <comment>Language code: ps</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Persian</source>
        <comment>Language code: fa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Plautdietsch</source>
        <comment>Language code: pdt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Polish</source>
        <comment>Language code: pl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Portuguese</source>
        <comment>Language code: pt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Punjabi</source>
        <comment>Language code: pa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Quechua</source>
        <comment>Language code: qu</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Rhaeto-Romance</source>
        <comment>Language code: rm</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Romanian</source>
        <comment>Language code: ro</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Russian</source>
        <comment>Language code: ru</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Samoan</source>
        <comment>Language code: sm</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sangro</source>
        <comment>Language code: sg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sanskrit</source>
        <comment>Language code: sa</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Scots Gaelic</source>
        <comment>Language code: gd</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbian</source>
        <comment>Language code: sr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Serbo-Croatian</source>
        <comment>Language code: sh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sesotho</source>
        <comment>Language code: st</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Setswana</source>
        <comment>Language code: tn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Shona</source>
        <comment>Language code: sn</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sindhi</source>
        <comment>Language code: sd</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Singhalese</source>
        <comment>Language code: si</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Siswati</source>
        <comment>Language code: ss</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovak</source>
        <comment>Language code: sk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Slovenian</source>
        <comment>Language code: sl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Somali</source>
        <comment>Language code: so</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Spanish</source>
        <comment>Language code: es</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Sudanese</source>
        <comment>Language code: su</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swahili</source>
        <comment>Language code: sw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Swedish</source>
        <comment>Language code: sv</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tagalog</source>
        <comment>Language code: tl</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tajik</source>
        <comment>Language code: tg</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tamil</source>
        <comment>Language code: ta</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tatar</source>
        <comment>Language code: tt</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tegulu</source>
        <comment>Language code: te</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Thai</source>
        <comment>Language code: th</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tibetan</source>
        <comment>Language code: bo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tigrinya</source>
        <comment>Language code: ti</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tonga</source>
        <comment>Language code: to</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Tsonga</source>
        <comment>Language code: ts</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkish</source>
        <comment>Language code: tr</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Turkmen</source>
        <comment>Language code: tk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Twi</source>
        <comment>Language code: tw</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uigur</source>
        <comment>Language code: ug</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Ukrainian</source>
        <comment>Language code: uk</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Urdu</source>
        <comment>Language code: ur</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uspanteco</source>
        <comment>Language code: usp</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Uzbek</source>
        <comment>Language code: uz</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Vietnamese</source>
        <comment>Language code: vi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Volapuk</source>
        <comment>Language code: vo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Welch</source>
        <comment>Language code: cy</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Wolof</source>
        <comment>Language code: wo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Xhosa</source>
        <comment>Language code: xh</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yiddish (former ji)</source>
        <comment>Language code: yi</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Yoruba</source>
        <comment>Language code: yo</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zhuang</source>
        <comment>Language code: za</comment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../openlp/core/common/i18n.py" line="54"/>
        <source>Zulu</source>
        <comment>Language code: zu</comment>
        <translation type="unfinished"/>
    </message>
</context>
</TS>